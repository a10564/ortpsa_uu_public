﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API
{
    public class TokenAuthOption
    {
        public static string Audience { get; } = "ExampleAudience";
        public static string Issuer { get; } = "ExampleIssuer";
        public static TimeSpan ExpiresSpan { get; } = TimeSpan.FromMinutes(43200);
        public static string TokenType { get; } = "Bearer";
    }
}
