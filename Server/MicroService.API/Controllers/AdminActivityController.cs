﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class AdminActivityController : BaseController
    {
        private IAdminActivityService _adminActivityService;
        public AdminActivityController(IMicroServiceLogger logger, IAdminActivityService adminActivityService) : base(logger)
        {
            _adminActivityService = adminActivityService;
        }

        #region GET EXAM SLOTS AND DATES
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to get exam slotes and dates.
        /// </summary>
        /// <param name="filterOptions"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/GetExamSlotsAndDates")]
        public IActionResult GetExamSlotsAndDates([FromBody] ExamDateFilterPropertiesDto filterOptions)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _adminActivityService.GetExamSlotsAndDates(filterOptions),
                    Success = true
                });
            });
        }
        #endregion

        #region UPDATE EXAM SLOTS AND DATES
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to update exam slotes and dates.
        /// </summary>
        /// <param name="changedDates"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/services/app/UpdateExamSlotsAndDates")]
        public IActionResult UpdateExamSlotsAndDates([FromBody]PropertiesForDateScheduleDto changedDates)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _adminActivityService.UpdateExamSlotsAndDates(changedDates),
                    Success = true,
                    Message = "Dates updated successfully"
                });
            });
        }
        #endregion

        #region ADD EXAM SLOTS AND DATES
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to add exam slotes and dates.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/AddExamSlotsAndDates")]
        public IActionResult AddExamSlotsAndDates([FromBody]PropertiesForDateScheduleDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _adminActivityService.AddExamSlotsAndDates(input),
                    Success = true,
                    Message = "Dates added successfully"
                });
            });
        }
        #endregion

        #region GET CENTER ALLOCATED TO EACH COLLEGE
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to get exam centers allocated to colleges
        /// </summary>
        /// <param name="filterparams"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/GetCentersAllocatedToCollege")]
        public IActionResult GetCentersAllocatedToCollege([FromBody] ExamDateFilterPropertiesDto filterOptions)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _adminActivityService.GetExamCentersAllocatedToColleges(filterOptions),
                    Success = true,
                });
            });
        }
        #endregion

        #region ALLOCATE CENTERS FOR COLLEGES
        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   13-01-2020
        /// Description :   This method is used to allocate exam centers.
        /// </summary>
        /// <param name="filterparams"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/AllocateCenters")]
        public IActionResult AllocateCenters([FromBody] PropertiesToAllocateCentersDto filterOptions)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _adminActivityService.AllocateCenters(filterOptions),
                    Success = true,
                    Message = "Centers allocated successfully"
                });
            });
        }
        #endregion
    }
}
