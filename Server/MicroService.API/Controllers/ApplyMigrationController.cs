﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.MigrationService;
using MicroService.Core.WorkflowAttribute;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Rmp.AssetMgmt.API.CustomAttribute;
using System;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.StudentDashboard;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ApplyMigrationController : BaseController
    {
        private IStudentMigrationService _studentMigrationService;
        public ApplyMigrationController(IMicroServiceLogger Logger, IStudentMigrationService studentMigrationService) : base(Logger)
        {
            _studentMigrationService = studentMigrationService;
        }

        #region MIGRATION APPLIED
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 04-02-2020
        /// Description     : This method is used to add student migration details.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/MigrationApplied")]
        public IActionResult StudentMigrationApplied([FromBody]UniversityMigrationDetailDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentMigrationService.StudentMigrationApplied(input),
                    Success = true,
                    Message = "Migration application submitted successfully."
                });
            });
        }
        #endregion

        #region GET STUDENT MIGRATION DETAILS 
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 05-02-2020
        ///  Description     : This method is used to get student migration details.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/UniversityMigration/{id}")]
        public IActionResult GetStudentMigrationDetail(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentMigrationService.GetStudentMigrationDetailById(id),
                    Success = true
                });
            });
        }
        #endregion

        #region MIGRATION APPLICATION UPDATE
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 05-02-2020
        /// Description     : This method is used to update student migration details.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("/api/services/app/UniversityMigration/{id}")]
        public IActionResult UniversityMigration(Guid id, [FromBody]UniversityMigrationDetailDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentMigrationService.GetStudentUniversityMigrationDetailsById(id, input),
                    Success = true,
                    Message = "Application updated successfully"
                });
            });
        }
        #endregion

        #region GET STUDENT Registration DETAILS 
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 04-02-2020
        /// Description     : This method is used to get student registration number details.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/GetStudentRegistrationDetails")]
        public IActionResult GetStudentRegistrationDetails()
        {
            var ResultData = _studentMigrationService.GetStudentUniversityRegistrationDetails();
            if (ResultData != null)
            {
                return GetHttpResponse(() =>
                {
                    return Ok(new RequestResult
                    {
                        Result = ResultData,
                        Success = true
                    });
                });
            }
            else
            {
                return GetHttpResponse(() =>
                {
                    return Ok(new RequestResult
                    {
                        Result = null,
                        Success = false,
                        Message = "Registration Number application has not been approved yet."
                    });
                });
            }
        }
		#endregion




		#region STATE CHANGE APIs FOR UNIVERSITY MIGRATION CERTIFICATE WORKFLOW ENTITY
		/// <summary>
		/// Author          : Sonymon Mishra
		/// Date            : 14-02-2020
		/// Description     : Normal state change api, no business involved
		/// </summary>
		/// <param name="id">Id of the university registration entity</param>
		/// <param name="universityRegDto">University registration dto</param>
		/// <returns></returns>
		[ActionName("University Migration Certificate Application Status Update")]
		[BelongsToWorkflow("Migration Certificate Workflow")]
		[Route("/api/services/app/UniversityMigrationCertificate/{id}/workflow-state-update")]
		[HttpPut]
		public IActionResult ForwardProcess(Guid id, [FromBody]UniversityMigrationDetailDto universityMigrationDto)
		{
			return GetHttpResponse(() =>
			{
				var result = _studentMigrationService.UpdateMigrationApplicationStatus(id, universityMigrationDto);
				return Ok(new RequestResult
				{
					Result = null,
					Success = true
				});
			});
		}
		
		/// <summary>
		/// Author          : Sonymon Mishra
		/// Date            : 14-02-2020
		/// Description     : Normal state change api, no business involved
		/// </summary>
		/// <param name="id">Id of the university registration entity</param>
		/// <param name="universityRegDto">University registration dto</param>
		/// <returns></returns>
		[ActionName("University Migration Application Status Update by Comp.Cell")]
		[BelongsToWorkflow("Migration Certificate Workflow")]
		[Route("/api/services/app/UniversityMigrationCertificate/{id}/workflow-state-update-by-comp-cell")]
		[HttpPut]
		public IActionResult ForwardProcessByCompCell(Guid id, [FromBody]UniversityMigrationDetailDto universityRegDto)
		{
			return GetHttpResponse(() =>
			{
				
				return Ok(new RequestResult
				{
					Result = _studentMigrationService.UpdateMigrationApplicationStatusByCompCell(id, universityRegDto),
					Success = true
				});
			});
		}
		
        /// <summary>
		/// Author          : Sumbul Samreen
		/// Date            : 28-02-2020
		/// Description     : State change action triggered by Principal. Logic involved. Before 48hours of last principal update, state can't be changed
		/// </summary>
		/// <param name="id">Id of the university registration entity</param>
		/// <param name="universityRegDto">University registration dto</param>
		/// <returns></returns>
		[ActionName("University Migration Application Status Update By Principal")]
		[BelongsToWorkflow("Migration Certificate Workflow")]
		[Route("/api/services/app/UniversityMigrationCertificate/{id}/workflow-state-update-principal")]
		[HttpPut]
		public IActionResult ForwardProcessByPrinicipal(Guid id, [FromBody]UniversityMigrationDetailDto universityMigrationDto)
		{
			return GetHttpResponse(() =>
			{

                return Ok(new RequestResult
				{
					Result = _studentMigrationService.MigrationCertificateApplicationStatusUpdateByPrincipal(id, universityMigrationDto),
					Success = true
				});
			});
		}

        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :  28-02-2020
        /// Description : This method is used to change status of the workflow on rejection of Migration certificate by asst.coe
        /// </summary>
        /// <param name="id"></param>
        /// <param name="universityMigrationDto"></param>
        /// <returns></returns>
        [ActionName("University Migration Application Status Reject")]
		[BelongsToWorkflow("Migration Certificate Workflow")]
		[Route("/api/services/app/UniversityMigrationCertificate/{id}/workflow-state-update-reject")]
		[HttpPut]
		public IActionResult RejectProcess(Guid id, [FromBody]UniversityMigrationDetailDto universityMigrationDto)
		{
			return GetHttpResponse(() =>
			{
				
				return Ok(new RequestResult
				{
					Result = _studentMigrationService.UpdateMigrationApplicationStatusToRejectState(id, universityMigrationDto),
					Success = true
				});
			});
		}
        #endregion

        #region DOWNLOAD MIGRATION CERTIFICATE FOR PRINCIPAL
        /// <summary>
        /// Author          : Sumbul Samreen
        /// Date            : 04-03-2020
        /// Description     : Download Migration Certificate for principal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/Migration/DownloadMigrationCertificate")]
        public IActionResult DownloadMigrationCertificate([FromBody] certificateDownloadDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentMigrationService.DownloadMigrationCertificate(param),
                    Success = true,
                    Message = "Migration Certificate downloaded successfully"
                });
            });
        }
        #endregion
    }
}
