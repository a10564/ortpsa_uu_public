﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.ApplyProvisionalCertificateDetail;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ApplyProvisionalCertificateController : BaseController
    {
        private IApplyProvisionalCertificateService _applyProvisionalCertificateService;
        public ApplyProvisionalCertificateController(IMicroServiceLogger Logger, IApplyProvisionalCertificateService applyProvisionalCertificateService) : base(Logger)
        {
            _applyProvisionalCertificateService = applyProvisionalCertificateService;
        }
        //to be changed later
        #region PROVISIONAL CERTIFICATE APPLICATION SUBMIT
        /// <summary>
        /// Author: Lopamudra Senapati
        /// Date: 11-Nov-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ApplyForProvisionalCertificate")]
        public IActionResult ApplyForProvisionalCertificate([FromBody]ProvisionalCertificateApplicationDetailDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _applyProvisionalCertificateService.ApplyForProvisionalCertificate(input),
                    Success = true,
                    Message = "Provisional certificate application submitted successfully."
                });
            });
        }
        #endregion
    }
}
