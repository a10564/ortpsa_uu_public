﻿using MicroService.API.GlobalResources;
using MicroService.Core.WorkflowCommon;
using MicroService.SharedKernel;
using MicroService.Utility.Common;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
//using Resources;
using System;
using System.Reflection;

namespace MicroService.API.Controllers
{
    public class BaseController : Controller
    {
        public IMicroServiceLogger _logger;

        public BaseController(IMicroServiceLogger Logger)
        {
            _logger = Logger;
        }

        protected IActionResult GetHttpResponse(Func<IActionResult> codeToExecute)
        {
            IActionResult result = null;
            try
            {
                result = codeToExecute.Invoke();
            }
            catch (MicroServiceException sbe)
            {
                string errorMessage = GetErrorMessage(sbe);
                _logger.Warn(errorMessage);
                return Ok(new RequestResult
                {
                    Success = false,
                    Error = new ErrorDetail() { Message = errorMessage }
                });
            }
            catch(WorkflowException wfExptn)
            {
                _logger.Warn(wfExptn.Message);
                return Ok(new RequestResult
                {
                    Success = false,
                    Error = new ErrorDetail() { Message = wfExptn.Message }
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return BadRequest(new RequestResult
                {
                    Success = false,
                    Error = new ErrorDetail() { Message = "There was a problem handling your request. Please contact support." }
                });
            }
            return result;
        }

        #region Error code reading
        private string GetErrorMessage(MicroServiceException _microServiceException)
        {
            if (_microServiceException.SubstitutionParams == null)
            {
                if (!String.IsNullOrEmpty(_microServiceException.ErrorMessage))
                {
                    return _microServiceException.ErrorMessage;
                }
                else
                {
                    return GetMessageForCode(_microServiceException.ErrorCode);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(_microServiceException.ErrorMessage))
                {
                    return string.Format(_microServiceException.ErrorMessage, _microServiceException.SubstitutionParams);
                }
                else
                {
                    string errorMessage = GetMessageForCode(_microServiceException.ErrorCode);
                    //Now replace the parameters
                    if (_microServiceException.SubstitutionParams.Length > 1)
                    {
                        return string.Format(_microServiceException.ErrorMessage, _microServiceException.SubstitutionParams);
                    }
                    else
                    {
                        return string.Format(errorMessage, _microServiceException.SubstitutionParams);
                    }
                }
            }

        }

        private string GetMessageForCode(string errorCode)
        {
            string value = String.Empty;
            try
            {
                value = RESError.ResourceManager.GetString(errorCode);
            }
            catch (MicroServiceException ex)
            {
                value = GetDefaultErrorMessage();
            }
            return value;
        }

        private static string GetDefaultErrorMessage()
        {
            return RESError.ResourceManager.GetString(Constant.ERROR_GENERIC);
        } 
        #endregion
    }
}
