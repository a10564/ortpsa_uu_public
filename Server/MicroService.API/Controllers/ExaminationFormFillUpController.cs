﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Core.WorkflowAttribute;
using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using MicroService.Utility.Common;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using Rmp.AssetMgmt.API.CustomAttribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ExaminationFormFillUpController : BaseController
    {
        private IExaminationFormFillUpService _examinationFormFillUpService;
        public ExaminationFormFillUpController(IMicroServiceLogger logger, IExaminationFormFillUpService examinationFormFillUpService)
             : base(logger)
        {
            _examinationFormFillUpService = examinationFormFillUpService;
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   12-11-19
        /// Description :   This methode is used to call examformfillupservice to insert data
        /// </summary>
        /// <param name="regNo"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/ExaminationFormFillUp/ExamFormFillUpDetails")]
        public IActionResult ExamFormFillUpDetails([FromBody] ExaminationFormFillUpDetailsDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.ApplyExaminationFormFillUp(param),
                    Success = true,
                    Message = "Semester examination application submitted successfully."
                });
            });        
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   12-11-19
        /// Description :   This methode is used to get the examformfillupdetails by id
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/ExaminationFormFillUp/GetExamFormFillUpDetails/{id}")]
        public IActionResult GetExamFormFillUpDetails(Guid id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetExamFormFillUpDetails(id),
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author  :   Sitikanta Pradhan
        /// Date    :   16-nov-19
        /// Description :   
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/ExaminationFormFillUp/GetExamFormFillUpDetails")]
        public IActionResult GetExamFormFillUpUserRoleWiseState()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetExamFormFillUpState(),
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   16-11-19
        /// Description :   This methode is used to get the amount per college wise
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateModel]
        [Route("/api/services/app/ExaminationFormFillUp/GetCollegeWiseFormFillUpAmount")]
        public IActionResult GetCollegeWiseFormFillUpAmount([FromBody] SearchParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetCollegeWiseFormFillUpAmount(param),
                    Success = true
                });
            });
        }

        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/StateWiseMoveToBackground")]
        public IActionResult StateWiseMoveToBackground([FromBody] BackgroundProcessDto param)
        {
            return GetHttpResponse(() =>
            {
                //_examinationFormFillUpService.StateWiseMoveToBackground(param);
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.SaveToBackGroundTable(param),
                Message = "The request has been submitted and the intimation will be sent through mail",
                    Success = true
                });
            });
        }        

        /// <summary>
        /// Author  :  Lopamudra Senapati
        /// Date    :   15-11-19
        /// Description :   This methode is used to add the formfilled data to background process Table
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/AddExamFormDetailsToBackgroundTable")]
        public IActionResult AddExamFormDetailsToBackgroundTable([FromBody] BackgroundProcessDto param)
        {
            return GetHttpResponse(() =>
            {

                _examinationFormFillUpService.AddExamFormDetailsToBackgroundTable(param);
                return Ok(new RequestResult
                {
                    Result =  EnOperationStatus.SUCCESS.ToString(),
                    Message="The request has been submitted and the intimation will be sent through mail",
                    Success = true
                });
            });
        }

        #region Academic Start Year (To Do After Go Live)
        /// <summary>
        /// Author  :   Sitikanta Pradhan
        /// Date    :   18-Dec-19
        /// Description :   Get session start year in examinationsession table
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/ExaminationFormFillUp/GetAcademicStartYear")]
        public IActionResult GetAcademicStartYear()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetAcademicStartYear(),
                    Success = true
                });
            });
        }
        #endregion




        #region STATE CHANGE APIs FOR STUDENT EXAMINATION FORM FILLUP WORKFLOW WORKFLOW ENTITY
        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 19-11-2019
        /// Description     : Principal verifies examination form details
        /// </summary>
        /// <param name="id">Id of the examination form entity</param>
        /// <param name="examFormDto">Examination form dto</param>
        /// <returns></returns>
        [ActionName("Principal Examination Form Fillup Verification Status Update")]
        [BelongsToWorkflow("Examination Form Fillup Workflow")]
        [Route("/api/services/app/ExaminationFormFillUp/{id}/workflow-principal-verification")]
        [HttpPut]
        public IActionResult PrincipalVerification(Guid id, [FromBody]ExaminationFormFillUpDetailsDto examFormDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.VerificationByPrincipal(id, examFormDto),
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 19-11-2019
        /// Description     : Principal rejects examination form details
        /// </summary>
        /// <param name="id">Id of the examination form entity</param>
        /// <param name="examFormDto">Examination form dto</param>
        /// <returns></returns>
        [ActionName("Principal Examination Form Fillup Rejection Status Update")]
        [BelongsToWorkflow("Examination Form Fillup Workflow")]
        [Route("/api/services/app/ExaminationFormFillUp/{id}/workflow-principal-rejection")]
        [HttpPut]
        public IActionResult PrincipalReject(Guid id, [FromBody]ExaminationFormFillUpDetailsDto examFormDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.RejectedByPrincipal(id, examFormDto),
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 19-11-2019
        /// Description     : Principal verifies examination form details
        /// </summary>
        /// <param name="id">Id of the examination form entity</param>
        /// <param name="examFormDto">Examination form dto</param>
        /// <returns></returns>
        [ActionName("Examination Form Fillup Status Update")]
        [BelongsToWorkflow("Examination Form Fillup Workflow")]
        [Route("/api/services/app/ExaminationFormFillUp/{id}/workflow-status-update")]
        [HttpPut]
        public IActionResult UpdateState(Guid id, [FromBody]ExaminationFormFillUpDetailsDto examFormDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.UpdateState(id, examFormDto),
                    Success = true
                });
            });
        }
        #endregion
        
        /// <summary>
        /// Author          : Anurag Digal
        /// Date            : 10-02-2020
        /// Description     : Download Admit card for principal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/Examination/DownloadAdmitCard")]
        public IActionResult DownloadAdmitCard([FromBody] certificateDownloadDto obj)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.DownloadAdmitCard(obj),
                    Success = true,
                    Message = "Admit Card downloaded successfully"
                });
            });
        }

        #region GET FIRST SEMESTER STUDENT RECORD FOR LOGIN USER
        [HttpGet]
        [Route("api/services/app/GetFirstSemeRecordDetails/{studentName}/{dob}/{academicStart}/{courseType}/{collegeCode}/{courseCode}/{streamCode}/{departmentCode}/{subjectCode}/{semesterCode}")]
        public IActionResult GetFirstSemeRecordDetails(string studentName, string dob, int academicStart, string courseType, string collegeCode, string courseCode, string streamCode, string departmentCode, string subjectCode, string semesterCode)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetFirstSemeRecordForLoginUser(studentName, dob, academicStart, courseType, collegeCode, courseCode, streamCode, departmentCode, subjectCode, semesterCode),
                    Success = true
                });
            });
        }
        #endregion

        #region MARK ENTRY APIS
        /// <summary>
        /// Author          :   Anurag Digal
        /// Date            :   25-09-2021
        /// Description     :   This methode is called AddExamFormFillUpMark to save semester wise mark
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/services/app/Examination/AddExamFormFillUpMark")]
        public IActionResult AddExamFormFillUpMark([FromBody] List<ExaminationFormFillUpSemesterMarksDto> input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.AddExamFormFillUpMark(input),
                    Success = true,
                    Message = "Mark added successfully."
                });
            });
        }

        /// <summary>
        /// Author          :   Anurag Digal
        /// Date            :   25-09-2021
        /// Description     :   This methode is called GetExamFormFillUpMark to fetch semester wise mark based on id and ackownledgement number
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ackNo"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/services/app/Examination/GetExamFormFillUpMark/{id}/{ackNo}")]
        public IActionResult GetExamFormFillUpMark(Guid Id, string ackNo)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetExamFormFillUpMark(Id, ackNo),
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion
        #region Get student details and semister details
        /// <summary>
        /// Author          :   Bikash kumar sethi
        /// Date            :   25-09-2021
        /// Description     :   this method is used to get all semister details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ackNo"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/services/app/ExaminationFormFillUp/GetSemesterPaperDetails/{id}/{ackNo}")]
        public IActionResult GetSemesterPaperDetails(Guid id, string ackNo)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetSemesterPaperDetails(id, ackNo),
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion

        #region 
        /// <summary>
        /// Author  :  Bikash kumar sethi
        /// Date    :  30-09-21
        /// Description :   This methode is used to add the formfilled data to background process Table
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/AddMarkDetailsToBackgroundTable")]
        public IActionResult AddMarkDetailsToBackgroundTable([FromBody] BackgroundProcessDto param) 
        {
            return GetHttpResponse(() =>
            {

                _examinationFormFillUpService.AddMarkDetailsToBackgroundTable(param); 
                return Ok(new RequestResult
                {
                    Result = EnOperationStatus.SUCCESS.ToString(),
                    Message = "The request has been submitted and the intimation will be sent through mail.",
                    Success = true
                });
            });
        }
        #endregion

        #region Get UU Filter String Data
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/GetSemesterFilterObject")]
        public IActionResult GetSemesterFilterObject([FromBody] SearchParamDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetSemesterFilterObject(input),
                    Success = true,
                    Message = ""
                });
            });
        }
        #endregion
        #region  Get Student Paper And Mark Details
        /// <summary>
        /// Author  :  Bikash kumar sethi
        /// Date    :  01-10-21
        /// Description :   This methode is used to get student paper and mark data 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/ExaminationFormFillUp/GetStudentPaperAndMarkDetails/{Id}/{ack}")]
        public IActionResult GetStudentPaperAndMarkDetails(Guid Id,string ack)
        {
            return GetHttpResponse(() =>
            {                
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetStudentPaperAndMarkDetails(Id, ack),
                    Message = "Student details fetch successfully.",
                    Success = true
                });
            });
        }
        #endregion

        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/GetMarkDetailsForL1Support")]
        public IActionResult GetMarkDetailsForL1Support([FromBody] SearchParamDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetMarkDetailsForL1Support(input),
                    Success = true,
                    Message = ""
                });
            });
        }

        /// <summary>
        /// Author  :  Anurag Digal
        /// Date    :  06-11-2021
        /// Description :   This methode is used to download mark statement in pdf format
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/DownloadSemesterExamPdfReport")]
        public IActionResult DownloadSemesterExamPdfReport([FromBody] SearchParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.DownloadSemesterExamPdfReport(param),
                    Success = true,
                    Message = ""
                });
            });
        }

        /// <summary>
        /// Author  :   Bikash ku sethi
        ///Date     :   08-11-2021
        ///This method is used to get the examformfillupdetails by rollNo
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/ExaminationFormFillUp/GetExamFormFillUpDetailsByRollNo/{rollNo}")]
        public IActionResult GetExamFormFillUpDetailsByRollNo(string rollNo)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetExamFormFillUpDetailsByRollNo(rollNo), 
                    Success = true
                });
            });
        }

        /// <summary>
        /// Author  : Bikash kumar sethi 
        /// Date    :  03-12-2021
        /// Description :   This methode is used to download paper /subject wise mark report 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/DownloadPaperWiseMarkReport")]
        public IActionResult DownloadPaperWiseMarkReport([FromBody] SearchParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.DownloadPaperWiseMarkReport(param),
                    Success = true,
                    Message = "File downloaded successfully."
                });
            });
        }

        /// <summary>
        /// Author  : Anurag Digal 
        /// Date    :  05-03-2022
        /// Description :   This method is used get all students with their internal external and practical marks.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/GetAllStudentByCollegeAndSubject")]
        public IActionResult GetAllStudentByCollegeAndSubject([FromBody] SearchParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.GetAllStudentByCollegeAndSubject(param),
                    Success = true,
                });
            });
        }

        /// <summary>
        /// Author  : Anurag Digal 
        /// Date    :  05-03-2022
        /// Description :   This method is used store the term end mark by all students per college.
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/ExaminationFormFillUp/SaveTermEndMarkAllStudentByCollege")]
        public IActionResult SaveTermEndMarkAllStudentByCollege([FromBody] List<TermEndMarkDto> input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _examinationFormFillUpService.SaveTermEndMarkAllStudentByCollege(input),
                    Success = true,
                    Message = "Result saved successfully."
                });
            });
        }
    }
}
