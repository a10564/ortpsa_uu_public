﻿using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.GeographyOder;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class GeographyAppController : BaseController
    {
        private IGeographyAppService _geographyAppService;
        public GeographyAppController(IMicroServiceLogger logger,IGeographyAppService geographyAppService)
              : base(logger)
        {
            _geographyAppService = geographyAppService;
        }

        [HttpPost]
        [Route("/api/services/app/GeographyOrder")]
        public IActionResult Create([FromBody]GeographyOrderCreateDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.CreateGeographyOrder(input),
                    Success = true
                });
            });
        }

        [HttpPut]
        [Route("/api/services/app/GeographyOrder/{id}")]
        public IActionResult Update(long id,[FromBody] GeographyOrderDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.UpdateGeographyOrder(id,input),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/GeographyOrder")]
        public IActionResult GetAllGeographyOrder()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.GetAllGeographyOrder(),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/GeographyOrder/{id}")]
        public IActionResult GetGeographyOrder(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.GetGeographyOrder(id),
                    Success = true
                });
            });
        }

        [HttpDelete]
        [Route("/api/services/app/GeographyOrder/{id}")]
        public IActionResult Delete(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.Delete(id),
                    Success = true
                });
            });
        }


        [HttpPost]
        [Route("/api/services/app/GeographyDetail")]
        public IActionResult CreateGeographyDetail([FromBody] GeographyDetailCreateDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.CreateGeographyDetail(input),
                    Success = true
                });
            });
        }

        [HttpPut]
        [Route("/api/services/app/GeographyDetail/{id}")]
        public IActionResult UpdateGeographyDetail(long id,[FromBody] GeographyDetailDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.UpdateGeographyDetail(id,input),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/GeographyDetail/{id}")]
        public IActionResult GetGeographyDetail(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.GetGeographyDetail(id),
                    Success = true
                });
            });
        }

        [HttpGet]
        [Route("/api/services/app/GeographyDetail")]
        public IActionResult GetAllGeographyDetail()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.GetAllGeographyDetail(),
                    Success = true
                });
            });
        }

        [HttpDelete]
        [Route("/api/services/app/GeographyDetail/{id}")]
        public IActionResult DeleteGeographyDetail(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _geographyAppService.DeleteGeographyDetail(id),
                    Success = true
                });
            });
        }
    }
}
