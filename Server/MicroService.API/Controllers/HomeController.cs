﻿using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.QRCode;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class HomeController : BaseController
    {
        ICommonAppService _commonSrvc;
        private IConfiguration _iconfiguration;
        private IMicroServiceUOW _MSUoW;
        public HomeController(IMicroServiceLogger Logger, ICommonAppService commonSrvc, IConfiguration configuration, IMicroServiceUOW MSUoW) : base(Logger)
        {
            _commonSrvc = commonSrvc;
            _iconfiguration = configuration;
            _MSUoW = MSUoW;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Home/GetLookupData/{parentLookupTypeList}")]
        public IActionResult GetLookupData(string parentLookupTypeList)
        {
            return GetHttpResponse(() =>
            {
                var parentLookupType = parentLookupTypeList.Split(",").Select(parent => Convert.ToInt64(parent)).ToArray();
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetLookupData(parentLookupType),
                    Success = true
                });
            });
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Home/GetMinusCurrentDate")]
        public IActionResult GetLookupData()
        {
            return GetHttpResponse(() =>
            {

                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetMinusCurrentDate(),
                    Success = true
                });
            });
        }

        #region Validate QRCode
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date:    25-02-2020
        /// Description  :This method validates the QRCode
        /// </summary>
        /// <param name="params"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Home/ValidateQRCode/{id}/{number}")]
        public IActionResult ValidateQRCode(long id, string number)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.ValidateQRCode(id, number),
                    Success = true
                });
            });
        }
        #endregion

        #region Validate QRCode
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 06-MAR-2020
        /// Description  :Get the each service wise year list
        /// </summary>
        /// <param name="serviceNumber"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Home/GetYearList/{serviceNumber}")]
        public IActionResult GetYearList(long serviceNumber)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetYearOfAdmissionList(serviceNumber),
                    Success = true
                });
            });
        }
        #endregion

        #region Get Login Student Record
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 18-Sep-2020
        /// Description  :Get Login Student Record
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Home/GetLoginStudentData")]
        public IActionResult GetLoginStudentData()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _commonSrvc.GetLoginStudentData(),
                    Success = true
                });
            });
        }
        #endregion

        #region Transaction Report Download in XLSX Format
        /// <summary>
        /// Author : Anurag Digal
        /// Date    : 16/02/2021
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="Todate"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("Home/DownloadTransaction")]
        public IActionResult DownloadTransaction(DateTime FromDate, DateTime Todate)
        {
            //xlsxcontenttype
            string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            var result = _MSUoW.TransactionLogRepository
                        .GetTransactionLogs()
                        .Where(trans => trans.IsActive && trans.CreationTime.Date >= FromDate.Date
                                && trans.CreationTime.Date <= Todate.Date)
                        .AsQueryable();

            var records = result.OrderByDescending(p => p.CreationTime).ToList();
            var dataTable = new DataTable("Transactions");

            dataTable.Columns.Add("SlNo", typeof(int));
            dataTable.Columns.Add("ConsumerName", typeof(string));
            dataTable.Columns.Add("EmailId", typeof(string));
            dataTable.Columns.Add("ContactNo", typeof(string));
            dataTable.Columns.Add("CSCPayid", typeof(string));
            dataTable.Columns.Add("UniqueRefNo", typeof(string));
            dataTable.Columns.Add("BankRefNo", typeof(string));
            dataTable.Columns.Add("Amount", typeof(double));
            dataTable.Columns.Add("Status", typeof(string));
            dataTable.Columns.Add("PaymentDate", typeof(string));
            int counter = 1;
            records.ForEach(item =>
            {
                var row = dataTable.NewRow();
                row["SlNo"] = counter++;
                row["ConsumerName"] = item.ConsumerName;
                row["EmailId"] = item.EmailId;
                row["ContactNo"] = item.ContactNo;
                row["CSCPayid"] = item.MihPayid;
                row["UniqueRefNo"] = item.UniqueRefNo;
                row["BankRefNo"] = item.BankRefNo;
                row["Amount"] = item.Amount;
                row["Status"] = item.Status;
                //convert payment date to ist format
                var istdate = TimeZoneInfo.ConvertTimeFromUtc(item.CreationTime, TZConvert.GetTimeZoneInfo("India Standard Time"));
                //TimeZoneInfo.ConvertTimeFromUtc(item.CreationTime, TZConvert.GetTimeZoneInfo(_commonSrvc.GetActiveConfigurationValueFromKey("TimeZone").ToString()));
                row["PaymentDate"] = istdate.ToString("dd-MMM-yyyy HH:mm:ss");
                dataTable.Rows.Add(row);
            });

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            if (dataTable.Rows.Count > 0)
            {
                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Transactions");
                    worksheet.Cells["A1"].LoadFromDataTable(dataTable, PrintHeaders: true);
                    worksheet.Cells["A1:I1"].Style.Font.Bold = true;
                    for (var col = 1; col < dataTable.Columns.Count + 1; col++)
                    {
                        worksheet.Column(col).AutoFit();
                    }
                    return File(package.GetAsByteArray(), XlsxContentType, "Transaction report-" + FromDate.Date.ToString("dd-MM-yy") + "-" + Todate.Date.ToString("dd-MM-yy") + ".xlsx");
                }
            }
            else
            {
                return Ok(new
                {
                    Success = false,
                    Result = new { Message = "Unable to find any transactions for the given date/s" }
                });
            }
        }
        #endregion
    }
}
