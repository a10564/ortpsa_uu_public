﻿using MicroService.API.CustomAttribute;
using MicroService.Business;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{

    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class AcademicStatementViewODataController : ODataBaseController
    {

        private IExaminationFormFillUpService _examinationFormFillUpService;
        public AcademicStatementViewODataController(IMicroServiceLogger logger, IExaminationFormFillUpService examinationFormFillUpService)
             : base(logger)
        {
            _examinationFormFillUpService = examinationFormFillUpService;
        }

        [EnableQuery]
        public IQueryable<AcademicStatementView> Get()
        {
            return _examinationFormFillUpService.GetAllAcademicStatementList();
        }
    }
}
