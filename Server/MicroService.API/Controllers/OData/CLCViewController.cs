﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class CLCViewController : ODataBaseController
    {
        private IStudentCLCService _studentCLCService;
        public CLCViewController(IMicroServiceLogger Logger, IStudentCLCService studentCLCService) : base(Logger)
        {
            _studentCLCService = studentCLCService;
        }

        [HttpGet]
        [EnableQuery]
        public IQueryable<CLCView> Get()
        {
            return _studentCLCService.GetAllCLCViewData();
        }
    }
}
