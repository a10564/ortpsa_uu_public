﻿using MicroService.API.CustomAttribute;
using MicroService.Business;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Views;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{

    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ExaminationPaperWiseMarkODataController : ODataBaseController
    {

        private IExaminationFormFillUpService _examinationFormFillUpService;        
        public ExaminationPaperWiseMarkODataController(IMicroServiceLogger logger, IExaminationFormFillUpService examinationFormFillUpService)
             : base(logger)
        {
            _examinationFormFillUpService = examinationFormFillUpService;         
        }

        [EnableQuery]
        [HttpPost]
        public IQueryable<ExaminationPaperWiseMarkDto> Post([FromBody] OdataQueryBuilder builder)
        {
            IQueryable<ExaminationPaperWiseMarkDto> examinationDetailsData = _examinationFormFillUpService.GetAllPaperWiseMark();
            return OdataFilter(ref examinationDetailsData, builder);
        }
    }
}
