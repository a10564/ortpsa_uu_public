﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class FeaturesController : ODataBaseController
    {
        private IFeatureAppService _featureAppService;
        public FeaturesController(IMicroServiceLogger Logger, IFeatureAppService featureAppService) : base(Logger)
        {
            _featureAppService = featureAppService;
        }

       
        [HttpGet]
        [EnableQuery]
        public IQueryable<FeatureView> Get()
        {
            return _featureAppService.GetAllFeaturesData();
        }
    }
}
