﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class GeographyDetailsController : ODataBaseController
    {
        private IGeographyAppService _geographyAppService;
        public GeographyDetailsController(IMicroServiceLogger Logger, IGeographyAppService geographyAppService) : base(Logger)
        {
            _geographyAppService = geographyAppService;
        }

        [EnableQuery]
        [HttpGet]
        public IQueryable<GeographyDetailView> Get()
        {
            return _geographyAppService.GetAllGeographysData();
        }
    }
}
