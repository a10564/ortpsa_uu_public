﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class NursingAcademicStatementODataController : ODataBaseController
    {
        private INursingFormFillUpService _nursingFormFillUpService;
        public NursingAcademicStatementODataController(IMicroServiceLogger logger, INursingFormFillUpService nursingFormFillUpService)
             : base(logger)
        {
            _nursingFormFillUpService = nursingFormFillUpService;
        }

        [EnableQuery]
        public IQueryable<NursingAcademicStatementDto> Get()
        {
            return _nursingFormFillUpService.GetNursingAcademicDetails();
        }
    }
}
