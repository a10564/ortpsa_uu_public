﻿using MicroService.API.CustomAttribute;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroService.Business.Abstract;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class NursingMarkStatementViewODataController : ODataBaseController
    {
        private INursingFormFillUpService _nursingFormFillUpService;
        public NursingMarkStatementViewODataController(IMicroServiceLogger logger, INursingFormFillUpService nursingFormFillUpService)
             : base(logger)
        {
            _nursingFormFillUpService = nursingFormFillUpService;
        }
        [EnableQuery]
        public IQueryable<NursingMarkStatementView> Get()
        {
            return _nursingFormFillUpService.GetAllNursingMarkDdetails();
        }
    }
}
