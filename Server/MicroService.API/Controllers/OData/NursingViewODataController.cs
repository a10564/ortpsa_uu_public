﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class NursingViewODataController : ODataBaseController
    {
        private INursingFormFillUpService _nursingFormFillUpService;
        public NursingViewODataController(IMicroServiceLogger Logger, INursingFormFillUpService nursingFormFillUpService) : base(Logger)
        {
            _nursingFormFillUpService = nursingFormFillUpService;
        }

        [EnableQuery]
        [HttpPost]
        public IQueryable<NursingViewExcludeBackgroundProcessTable> Post([FromBody] OdataQueryBuilder builder)
        {
            IQueryable<NursingViewExcludeBackgroundProcessTable> examinationDetailsData = _nursingFormFillUpService.GetRoleBasedActiveNursingInboxData(); ;
            return OdataFilter(ref examinationDetailsData, builder);
        }
    }
}
