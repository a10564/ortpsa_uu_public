﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class ServiceWiseStudentDetailsController : ODataBaseController
    {
        private ISupportClass _support; 
        public ServiceWiseStudentDetailsController(IMicroServiceLogger Logger, ISupportClass support) : base(Logger) 
        {
            _support = support;
        }

        [EnableQuery]
        [HttpGet]
        public IQueryable<ServiceWiseStudentDetailsDto> Get() 
        {
            return _support.ServiceWiseStudentDetail();
        }       
    }
}
