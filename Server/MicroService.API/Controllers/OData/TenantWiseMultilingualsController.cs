﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers.OData
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class TenantWiseMultilingualsController : ODataBaseController
    {
        private ITenantWiseMultilingualAppService _tenantWiseMultilingualAppService;
        public TenantWiseMultilingualsController(IMicroServiceLogger Logger, ITenantWiseMultilingualAppService tenantWiseMultilingualAppService) : base(Logger)
        {
            _tenantWiseMultilingualAppService = tenantWiseMultilingualAppService;
        }

        [EnableQuery]
        [HttpGet]
        public IQueryable<Multilingual> Get()
        {
            return _tenantWiseMultilingualAppService.GetAllMultilingualsData();
        }
    }
}
