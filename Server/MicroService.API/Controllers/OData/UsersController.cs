﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.SharedKernel;
//using MicroService.Domain.Models.Views;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class UsersController : ODataBaseController
    {
        private IUserService _userService;
        public UsersController(IMicroServiceLogger logger, IUserService userService)
             : base(logger)
        {
            _userService = userService;
        }

        [EnableQuery]
        public IQueryable<UserView> Get()
        {
            return _userService.GetAllUserViewData();
        }

        [EnableQuery]
        [HttpPost]
        public IQueryable<UserView> Post([FromBody]OdataQueryBuilder builder)
        {
            IQueryable<UserView> customers = _userService.GetAllUserViewData();
            return OdataFilter(ref customers, builder);
        }
    }
}
