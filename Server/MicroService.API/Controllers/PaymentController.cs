﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Text;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Entity;
using Microservice.Common;
using MicroService.Utility.Logging.Abstract;
using MicroService.SharedKernel;
using Microservice.Common.Abstract;
using Microsoft.AspNetCore.Authorization;
using MicroService.Domain.Models.Dto.Payment;
using MicroService.API.CustomAttribute;

namespace MicroService.API.Controllers
{
	/// <summary>
	/// Author: Sonymon Mishra
	/// Date: 17-Jan-2020
	/// Payment Controller
	/// </summary>
	/// <returns></returns>
	public class PaymentController : BaseController
	{
		private readonly IPaymentService _payementService;
		private IConfiguration _iconfiguration;
		private ICommonAppService _common;
		private IMicroserviceCommon _MSCommon;
		
		public PaymentController(IPaymentService payementService, IMicroserviceCommon MSCommon, IConfiguration iconfiguration, ICommonAppService icommon, IMicroServiceLogger logger) : base(logger)
		{
			_payementService = payementService;
			_iconfiguration = iconfiguration;
			_common = icommon;
			_MSCommon = MSCommon;
			_logger = logger;
		}

		public IActionResult Index()
		{
			return View();
		}

		private IActionResult BadRequestRedirect()
		{
			//_logger.Error("Request might be null or thye form has no content type");
			string returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("PaymentFailedReturnUrl").Value;
			return RedirectPermanent(returnUrl);
		}

		[HttpPost]
		[Route("/Payment/Success")]
		public IActionResult Success(IFormCollection form)
		{
			return GetHttpResponse(() =>
			{
				if (Request != null && form != null)
				{
					if (!Request.HasFormContentType)
					{						
						return BadRequestRedirect();
					}
					
					TransactionLog paymentDetail = new TransactionLog();
					bool isSuccess = false;
					string key = "";
					string returnUrl = string.Empty;

					try
					{
						paymentDetail = _payementService.buildPaymentDetail(form, EnPaymentStatus.Success);
						try
						{
							string[] merc_hash_vars_seq;
							string merc_hash_string = string.Empty;
							string merc_hash = string.Empty;
							string order_id = string.Empty;
							string hash_seq = _iconfiguration.GetSection("Payment").GetSection("HashSequence").Value;
							if (form["status"].ToString().Equals("success", StringComparison.InvariantCultureIgnoreCase))
							{
								merc_hash_vars_seq = hash_seq.Split('|');
								Array.Reverse(merc_hash_vars_seq);
								merc_hash_string = _iconfiguration.GetSection("Payment").GetSection("BIPGSalt").Value + "|" + form["status"].ToString();


								foreach (string merc_hash_var in merc_hash_vars_seq)
								{
									merc_hash_string += "|";
									merc_hash_string = merc_hash_string + (Convert.ToString(form[merc_hash_var]) != null ? Convert.ToString(form[merc_hash_var]) : "");
									
								}

								merc_hash = _payementService.GenerateMD5SignatureForPayU(merc_hash_string).ToLower();

								if (merc_hash != Convert.ToString(form["reqhash"]))
								{
									//Value didn't match that means some paramter value change between transaction                        
									paymentDetail.Description = "Hash value did not matched";
									paymentDetail.ErrorDescription = "Hash value did not matched";
									paymentDetail.Status = "Failed";
									isSuccess = false;									
								}
								else
								{
									//if hash value match for before transaction data and after transaction data
									//that means success full transaction  , see more in response
									paymentDetail.Description = "Hash value matched";
									isSuccess = true;
								}
							}
							else
							{
								//Value didn't match that means some paramter value change between transaction                        
								paymentDetail.Description = "Hash value did not matched";
								paymentDetail.ErrorDescription = "Hash value did not matched";
								paymentDetail.Status = "Failed";
								isSuccess = false;								
							}
						}
						catch (Exception error)
						{
							paymentDetail.ErrorDescription = "Message: " + Convert.ToString(error.Message) + " | " + "Stack Trace: " + Convert.ToString(error.StackTrace);
							paymentDetail.Description = "Hash value did not matched";
							paymentDetail.Status = "Failed";
							isSuccess = false;
							_logger.Error(paymentDetail.Description + " : " + error.Message);
							_logger.Error("Stack Trace: " + Convert.ToString(error.StackTrace));
							return BadRequestRedirect();
						}
					}
					catch (Exception error)
					{

						paymentDetail.ErrorDescription = "Message: " + Convert.ToString(error.Message) + " | " + "Stack Trace: " + Convert.ToString(error.StackTrace);
						paymentDetail.Description = "Content type did not matched";
						paymentDetail.Status = "Failed";
						isSuccess = false;
						_logger.Error(paymentDetail.Description + " : " + error.Message);
						_logger.Error("Stack Trace: " + Convert.ToString(error.StackTrace));
						return BadRequestRedirect();
					}

					if (isSuccess)
					{
						if (paymentDetail.DeviceType == _MSCommon.GetEnumDescription(enDeviceType.Mobile))
						{
							returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("MobileReturnUrl").Value;
						}
						else
						{
							returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("ClientReturnUrl").Value;
						}
						

						bool isDuplicateTxn = _payementService.CheckDuplicateTxnId(paymentDetail.UniqueRefNo, enTransaction.Successful);
						if (isDuplicateTxn)
						{
							paymentDetail.Status = "Failed";
							paymentDetail.Description = "Duplicate Trasaction";
							paymentDetail.ErrorDescription = "Duplicate Trasaction";

							//Perform operation & return Id as key
							Tuple<bool, Guid?, int> result = _payementService.doOperation(paymentDetail, false);
							if (result != null && result.Item1)
							{
								key = result.Item2.ToString();
								return RedirectPermanent(returnUrl + "Failed" + "/" + System.Web.HttpUtility.UrlEncode(key));
							}
							else
							{
								return BadRequestRedirect();
							}
							
						}
						else
						{
							Tuple<bool, Guid?, int> result = _payementService.doOperation(paymentDetail, true);
							if (result != null && result.Item1)
							{
								key = result.Item2.ToString();
								return RedirectPermanent(returnUrl + "Successful" + "/" + System.Web.HttpUtility.UrlEncode(key));
							}
							else
							{
								return BadRequestRedirect();
							}
						}
					}
					else
					{
						Tuple<bool, Guid?, int> result = _payementService.doOperation(paymentDetail, false);
						if (result != null && result.Item1)
						{
							key = result.Item2.ToString();
							if (paymentDetail.DeviceType == _MSCommon.GetEnumDescription(enDeviceType.Mobile))
							{
								returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("MobileReturnUrl").Value;
							}
							else
							{
								returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("ClientReturnUrl").Value;
							}

						}
						//Send to the client in query param
						return RedirectPermanent(returnUrl + "Failed" + "/" + System.Web.HttpUtility.UrlEncode(key));
					}
				}
				else
				{
					_logger.Warn("Bad Response");                
					return BadRequestRedirect();
				}
			});
		}

		[HttpPost]
		[Route("Payment/Failure")]
		public IActionResult Failure(IFormCollection form)
		{
			return GetHttpResponse(() =>
			{				

				if (Request != null && form != null)
				{
					if (!Request.HasFormContentType)
					{						
						return BadRequestRedirect();
					}
					
					TransactionLog paymentDetail = new TransactionLog();
					//bool isSuccess = false;
					string key = "";
					string returnUrl = string.Empty;

					try
					{
						paymentDetail = _payementService.buildPaymentDetail(form, EnPaymentStatus.Failure);
						try
						{
							string[] merc_hash_vars_seq;
							string merc_hash_string = string.Empty;
							string merc_hash = string.Empty;
							string order_id = string.Empty;
							string hash_seq = _iconfiguration.GetSection("Payment").GetSection("HashSequence").Value;
							if (form["status"].ToString().Equals("failure", StringComparison.InvariantCultureIgnoreCase))
							{
								merc_hash_vars_seq = hash_seq.Split('|');
								Array.Reverse(merc_hash_vars_seq);
								merc_hash_string = _iconfiguration.GetSection("Payment").GetSection("BIPGSalt").Value + "|" + form["status"].ToString();

								foreach (string merc_hash_var in merc_hash_vars_seq)
								{
									merc_hash_string += "|";
									merc_hash_string = merc_hash_string + (Convert.ToString(form[merc_hash_var]) != null ? Convert.ToString(form[merc_hash_var]) : "");
									
								}

								merc_hash = _payementService.GenerateMD5SignatureForPayU(merc_hash_string).ToLower();

								if (merc_hash != Convert.ToString(form["reqhash"]))
								{
									//Value didn't match that means some paramter value change between transaction                        
									paymentDetail.Description = "Hash value did not matched";
									paymentDetail.ErrorDescription = "Hash value did not matched";
									paymentDetail.Status = "Failed";
								}
								else
								{
									//if hash value match for before transaction data and after transaction data
									//that means success full transaction  , see more in response
									paymentDetail.Description = "Hash value matched";
								}
							}
							else
							{
								//Value didn't match that means some paramter value change between transaction                        
								paymentDetail.Description = "Hash value did not matched";
								paymentDetail.ErrorDescription = "Hash value did not matched";
								paymentDetail.Status = "Failed";
							}
						}
						catch (Exception error)
						{
							paymentDetail.ErrorDescription = "Message: " + Convert.ToString(error.Message) + " | " + "Stack Trace: " + Convert.ToString(error.StackTrace);
							paymentDetail.Description = "Hash value did not matched";
							paymentDetail.Status = "Failed";
							_logger.Error(paymentDetail.Description + " : " + error.Message);
							_logger.Error("Stack Trace: " + Convert.ToString(error.StackTrace));
						}
					}
					catch (Exception error)
					{

						paymentDetail.ErrorDescription = "Message: " + Convert.ToString(error.Message) + " | " + "Stack Trace: " + Convert.ToString(error.StackTrace);
						paymentDetail.Description = "Content type did not matched";
						paymentDetail.Status = "Failed";
						_logger.Error(paymentDetail.Description + " : " + error.Message);
						_logger.Error("Stack Trace: " + Convert.ToString(error.StackTrace));
					}

					paymentDetail.Status = "Failed";
					paymentDetail.Description = "Failed Transaction";

					//Perform operation & return Id as key					
					string encryptedKey = string.Empty;
					Tuple<bool, Guid?, int> result = _payementService.doOperation(paymentDetail, false);
					
					if (result != null && result.Item1)
					{
						key = result.Item2.ToString();

						if (paymentDetail.DeviceType == _MSCommon.GetEnumDescription(enDeviceType.Mobile))
						{
							returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("MobileReturnUrl").Value;
						}
						else
						{
							returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("ClientReturnUrl").Value;
						}
					}

					//Send to the client in query param
					return RedirectPermanent(returnUrl + "Failed" + "/" + System.Web.HttpUtility.UrlEncode(key));
				}
				else
				{
					_logger.Warn("Bad Response");
					return BadRequestRedirect();
				}

			});
		}

		[HttpPost]
		[Route("Payment/Cancel")]
		public IActionResult Cancel(IFormCollection form)
		{
			return GetHttpResponse(() =>
			{

				if (Request != null && form != null)
				{
					if (!Request.HasFormContentType)
					{
						return BadRequestRedirect();
					}

					TransactionLog paymentDetail = new TransactionLog();
					//bool isSuccess = false;
					string key = "";
					string returnUrl = string.Empty;

					try
					{
						paymentDetail = _payementService.buildPaymentDetail(form, EnPaymentStatus.Cancel);
						try
						{
							string[] merc_hash_vars_seq;
							string merc_hash_string = string.Empty;
							string merc_hash = string.Empty;
							string order_id = string.Empty;
							string hash_seq = _iconfiguration.GetSection("Payment").GetSection("HashSequence").Value;
							if (form["status"].ToString().Equals("failure", StringComparison.InvariantCultureIgnoreCase))
							{
								merc_hash_vars_seq = hash_seq.Split('|');
								Array.Reverse(merc_hash_vars_seq);
								merc_hash_string = _iconfiguration.GetSection("Payment").GetSection("BIPGSalt").Value + "|" + form["status"].ToString();

								foreach (string merc_hash_var in merc_hash_vars_seq)
								{
									merc_hash_string += "|";
									merc_hash_string = merc_hash_string + (Convert.ToString(form[merc_hash_var]) != null ? Convert.ToString(form[merc_hash_var]) : "");

								}

								merc_hash = _payementService.GenerateMD5SignatureForPayU(merc_hash_string).ToLower();

								if (merc_hash != Convert.ToString(form["reqhash"]))
								{
									//Value didn't match that means some paramter value change between transaction                        
									paymentDetail.Description = "Hash value did not matched";
									paymentDetail.ErrorDescription = "Hash value did not matched";
									paymentDetail.Status = "Failed";
								}
								else
								{
									//if hash value match for before transaction data and after transaction data
									//that means success full transaction  , see more in response
									paymentDetail.Description = "Hash value matched";
								}
							}
							else
							{
								//Value didn't match that means some paramter value change between transaction                        
								paymentDetail.Description = "Hash value did not matched";
								paymentDetail.ErrorDescription = "Hash value did not matched";
								paymentDetail.Status = "Failed";
							}
						}
						catch (Exception error)
						{
							paymentDetail.ErrorDescription = "Message: " + Convert.ToString(error.Message) + " | " + "Stack Trace: " + Convert.ToString(error.StackTrace);
							paymentDetail.Description = "Hash value did not matched";
							paymentDetail.Status = "Failed";
							_logger.Error(paymentDetail.Description + " : " + error.Message);
							_logger.Error("Stack Trace: " + Convert.ToString(error.StackTrace));
						}
					}
					catch (Exception error)
					{

						paymentDetail.ErrorDescription = "Message: " + Convert.ToString(error.Message) + " | " + "Stack Trace: " + Convert.ToString(error.StackTrace);
						paymentDetail.Description = "Content type did not matched";
						paymentDetail.Status = "Failed";
						_logger.Error(paymentDetail.Description + " : " + error.Message);
						_logger.Error("Stack Trace: " + Convert.ToString(error.StackTrace));
					}

					paymentDetail.Status = "Cancelled";
					paymentDetail.Description = "Cancelled Transaction";
					
					//Perform operation & return Id as key
					string encryptedKey = string.Empty;
					Tuple<bool, Guid?, int> result = _payementService.doOperation(paymentDetail,false);					
					if (result != null && result.Item1)
					{
						key = result.Item2.ToString();

						if (paymentDetail.DeviceType == _MSCommon.GetEnumDescription(enDeviceType.Mobile))
						{
							returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("MobileReturnUrl").Value;
						}
						else
						{
							returnUrl = _iconfiguration.GetSection("ClientApplicationURL").Value + _iconfiguration.GetSection("Payment").GetSection("ClientReturnUrl").Value;
						}
					}

					//Send to the client in query param
					return RedirectPermanent(returnUrl + "Cancel" + "/" + System.Web.HttpUtility.UrlEncode(key));
				}
				else
				{
					_logger.Warn("Bad Response");
					return BadRequestRedirect();
				}

			});
		}

		[CustomAuthorize(APIId = 0)]
		[HttpPost]
		[Route("/api/services/app/payment")]
		public IActionResult Payment([FromBody] TransactionLog input)
		{
			return GetHttpResponse(() =>
			{
				bool isValid = _payementService.IsPaymentRequestinValidState(input);

				if (isValid)
				{
					input = _payementService.PrepareDataForPayment(input);
					string paymentGatewayURL = _payementService.PreparePGForm(input);
					_payementService.CreateRequestPayu(input);

					return Ok(new RequestResult
					{
						Result = paymentGatewayURL,
						Success = true,
						Message = "PayU Request Successful"
					});
				}
				else
				{
					return Ok(new RequestResult
					{
						Success = false,
						Message = "The Payment Request is denied. Request Not in Payment Pending State."
					});
				}
			});
		}

		[HttpGet]
		[Route("/api/services/app/payment/{id}")]
		public IActionResult Payment(string id)
		{
			return GetHttpResponse(() =>
			{
				string key = System.Web.HttpUtility.UrlDecode(id);

				return Ok(new RequestResult
				{
					Result = _payementService.GetTransactionLogById(key),
					Success = true,
					Message = "PayU Request Successful"
				});
			});
		}

		[CustomAuthorize(APIId = 0)]
		[HttpGet]
		[Route("/api/services/app/GetPricingInfo/{id}/{ser}")]
		public IActionResult GetPricingInfo(string id, string ser)
		{
			return GetHttpResponse(() =>
			{
				Tuple<PaymentPricingInfoDto, string> result = _payementService.GetPricingInfoByService(id, ser);
				if (result.Item1 != null)
				{
					return Ok(new RequestResult
					{
						Result = result.Item1,
						Success = true,
						Message = result.Item2
					});
				}
				else
				{
					return Ok(new RequestResult
					{
						Success = false,
						Message = result.Item2
					});
				}
			});
		}

        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 30-Dec-2019
        /// Download payment receipt (mobile team only uesd)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/paymentReceipt/{id}")]
        public IActionResult paymentReceipt(string id)
        {
            return GetHttpResponse(() =>
            {
                string key = System.Web.HttpUtility.UrlDecode(id);

                return Ok(new RequestResult
                {
                    Result = _payementService.DownloadPaymentReceiptById(key),
                    Success = true,
                    Message = "PayU Request Successful"
                });
            });
        }
    }
}