﻿using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.Role;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class RoleAppController : BaseController
    {
        IRoleAppService _roleAppService;
        public RoleAppController(IMicroServiceLogger logger, IRoleAppService roleAppService
            )
             : base(logger)
        {
            _roleAppService = roleAppService;
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("/api/services/app/Role")]
        public IActionResult Create([FromBody]CreateRoleDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _roleAppService.CreateRole(input),
                    Success = true,
                    Message = "Role Created Successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpPut]
        [Route("/api/services/app/Role/{id}")]
        public IActionResult Update(long id,[FromBody] RoleDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _roleAppService.UpdateRole(id, input),
                    Success = true,
                    Message = "Role Updated Successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Role/{id}")]
        public IActionResult GetRole(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _roleAppService.GetRoleDetails(id),
                    Success = true,
                    //Message = "Role Updated Successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpDelete]
        [Route("/api/services/app/Role/{id}")]
        public IActionResult DeleteRole(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _roleAppService.DeleteRoleDetails(id),
                    Success = true,
                    Message = "Role Deleted Successfully"
                });
            });
        }

        //[AllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Roles")]
        public IActionResult GetRoles()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _roleAppService.GetRoleDetails(),
                    Success = true,
                    //Message = "Role Deleted Successfully"
                });
            });
        }
    }
}
