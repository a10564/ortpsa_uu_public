﻿using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    [CustomAuthorize(APIId = 0)]
    public class StudentDashboardController : BaseController
    {
        IStudentDashboardAppService _studentDashboard;
        public StudentDashboardController(IMicroServiceLogger logger, IStudentDashboardAppService studentDashboard)
             : base(logger)
        {
            _studentDashboard = studentDashboard;
        }

        [HttpGet]
        [Route("/api/services/app/StudentDashboard/GetAllAppliedService")]
        public IActionResult GetAllAppliedService()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.GetAllAppliedService(),
                    Success = true,
                    Message = ""
                });
            });
        }

        /// <summary>
        /// Anurag Digal
        /// 03-10-19
        /// This methode is calling downloadservice methode from student dashboard service
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/StudentDashboard/DownloadService")]
        public IActionResult DownloadService([FromBody] DownloadParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.DownloadService(param),
                    Success = true,
                    Message = "File Download Successfully"
                });
            });
        }

        //[HttpPost]
        //[Route("/api/services/app/StudentDashboard/DownloadServiceForWeb")]
        //public IActionResult DownloadServiceForWeb([FromBody] DownloadParamDto param)
        //{
        //    return GetHttpResponse(() =>
        //    {
        //        PDFDownloadFile result = _studentDashboard.DownloadService(param);
        //        return File(result.PdfData, "application/octet-stream", result.FileName);                
        //    });
        //}

        #region  DOWNLOAD ACKNOWLEDGEMENT FILE
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 10/OCT/2019 
        /// </summary>
        /// param = param
        [HttpPost]
        [Route("/api/services/app/StudentDashboard/DownloadAcknowledgement")]
        public IActionResult DownloadAcknowledgement([FromBody] DownloadParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.DownloadAcknowledgementService(param),
                    Success = true,
                    Message = "File Downloaded Successfully"
                });
            });
        }

        //DOWNLOAD ACKNOWLEDGEMENT FILE (for Web)
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 11/OCT/2019 
        /// </summary>
        /// param = param
        [HttpPost]
        [Route("/api/services/app/StudentDashboard/DownloadAcknowledgementForWeb")]
        public IActionResult DownloadAcknowledgementForWeb([FromBody] DownloadParamDto param)
        {
            return GetHttpResponse(() =>
            {
                //PDFDownloadFile result = _studentDashboard.DownloadAcknowledgementService(param);
                //return File(result.PdfData, "application/octet-stream", result.FileName);
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.DownloadAcknowledgementService(param),
                    Success = true,
                    Message = "File Downloaded Successfully"
                });
            });
        }
        #endregion

        [HttpGet]
        [Route("/api/services/app/StudentDashboard/ServiceStatus/{docType}")]
        public IActionResult ServiceStatus(long docType)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.GetServiceStatus(docType),
                    Success = true
                });
            });
        }

        #region GET SERVICE PENDING ACTIONLIST
        [HttpGet]
        [Route("/api/services/app/StudentDashboard/PendingActionList")]
        public IActionResult PendingActionList()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.PendingActionList(),
                    Success = true
                });
            });
        }
        #endregion

        #region
        /// <summary>
        /// Sitikanta Pradhan
        /// 08-Nov-19
        /// This methode is calling downloadservice methode from student dashboard service
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/services/app/StudentDashboard/DownloadMigrationService")]
        public IActionResult DownloadMigrationService([FromBody] DownloadParamDto param)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.DownloadMigrationService(param),
                    Success = true,
                    Message = "File Download Successfully"
                });
            });
        }
        #endregion

        [HttpGet]
        [Route("/api/services/app/StudentDashboard/GetServicePrimarykey/{doctype}")]
        public IActionResult GetServicePrimarykey(long docType)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.GetServicePrimarykey(docType),
                    Success = true
                });
            });
        }

        #region Get Semester Exam Notification
        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   08-11-2021
        /// Author  :   This method is used to get the semester exam notification
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/StudentDashboard/GetSemesterExamNotification")]
        public IActionResult GetSemesterExamNotification()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.GetSemesterExamNotification(),
                    Success = true
                });
            });
        }
        #endregion

        #region Get Role wise Menu
        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   20-12-2021
        /// Author  :   This method is used to get role wise report menu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/services/app/StudentDashboard/GetRoleWiseReportMenu")]
        public IActionResult GetRoleWiseMenu()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _studentDashboard.GetRoleWiseReportMenu(),
                    Success = true
                });
            });
        }
        #endregion

    }
}
