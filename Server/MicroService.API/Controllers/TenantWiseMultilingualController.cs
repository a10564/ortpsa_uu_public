﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.TenantWiseMultilingual;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    public class TenantWiseMultilingualController : BaseController
    {
        private ITenantWiseMultilingualAppService _tenantWiseMultilingualAppService;
        public TenantWiseMultilingualController(IMicroServiceLogger Logger, ITenantWiseMultilingualAppService tenantWiseMultilingualAppService) 
            : base(Logger)
        {
            _tenantWiseMultilingualAppService = tenantWiseMultilingualAppService;
        }

        [HttpPost("/api/services/app/Module/")]
        public IActionResult AddModule(ModuleDto moduleDetailDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.AddModule(moduleDetailDto),
                    Success = true,
                    Message = "Module Added Successfully"
                });
            });
        }

        [HttpPost("/api/services/app/Language/")]
        public IActionResult Language(LanguageDto languageDto)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.AddLanguage(languageDto),
                    Success = true,
                    Message = "Language Added Successfully"
                });
            });
        }

        [HttpGet("/api/services/app/Module/{moduleId}/Multilingual/{languageType}")]
        public IActionResult GetMultilingualData(long moduleId, string languageType)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.GetMultilingualData(moduleId, languageType),
                    Success = true,
                    //Message = "Language Added Successfully"
                });
            });
        }

        [HttpGet("/api/services/app/Multilingual/")]
        public IActionResult GetMultilingualDetail()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.GetMultilingualDetail(),
                    Success = true,
                    //Message = "Language Added Successfully"
                });
            });
        }

        [HttpGet("/api/services/app/Multilingual/Root/")]
        public IActionResult GetMultilingualDetailRootTenant()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.GetMultilingualDetailRootTenant(),
                    Success = true,
                    //Message = "Language Added Successfully"
                });
            });
        }

        [HttpPost("/api/services/app/Multilingual/")]
        public IActionResult AddMultiLingualDataForTenant(List<MultilingualDto> multilingualData)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.AddMultiLingualDataForTenant(multilingualData),
                    Success = true,
                    //Message = "Language Added Successfully"
                });
            });
        }

        [HttpPost("/api/services/app/Multilingual/InitialSetup/")]
        public IActionResult AddMultiLingualDataForRootTenant(List<MultilingualDto> multilingualData)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _tenantWiseMultilingualAppService.AddMultiLingualDataForRootTenant(multilingualData),
                    Success = true,
                    //Message = "Language Added Successfully"
                });
            });
        }

    }
}
