﻿using Microservice.Common;
using MicroService.API.CustomAttribute;
using MicroService.Business.Abstract;
using MicroService.Domain.Models.Dto.User;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rmp.AssetMgmt.API.CustomAttribute;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Controllers
{
    [Produces("application/json")]
    //[CustomAuthorize(APIId = 0)]
    public class UserController : BaseController
    {
        IUserService _userService;
        public UserController(IMicroServiceLogger logger,IUserService userService)
             : base(logger)
        {
            _userService = userService;
        }

        /// <summary>
        /// Anurag Digal
        /// This methode is used to Student Registration
        /// </summary>
        /// <summary>
        /// Sitikanta Pradhan (Modified)
        /// Date: 24-Dec-2019
        /// Model validation
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        [Route("/api/services/app/User/Create")]
        public IActionResult Create([FromBody]CreateUserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.CreateUser(input, (long)enUserType.Student),
                    Success = true,
                    Message = "User Created Successfully"
                });
            });
        }

        /// <summary>
        /// Anurag Digal
        /// This methode is used for Principal Registration
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        [Route("/api/services/app/User/PricipalRegistration")]
        public IActionResult PricipalRegistration([FromBody]CreateUserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.CreateUser(input, (long)enUserType.Principal),
                    Success = true,
                    Message = "Pricipal Register Successfully"
                });
            });
        }
        /// <summary>
        /// Sumbul Samreen
        /// This methode is used for HOD Registration
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        [Route("/api/services/app/User/HODRegistration")]
        public IActionResult HODRegistration([FromBody]CreateUserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.CreateUser(input, (long)enUserType.HOD),
                    Success = true,
                    Message = "HOD Register Successfully"
                });
            });
        }

        /// <summary>
        /// Anurag Digal
        /// This methode is used for Registrar Registration
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        [Route("/api/services/app/User/RegistrarRegistration")]
        public IActionResult RegistrarRegistration([FromBody]CreateUserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.CreateUser(input, (long)enUserType.Registrar),
                    Success = true,
                    Message = "Pricipal Register Successfully"
                });
            });
        }

        /// <summary>
        /// Anurag Digal
        /// This methode is used for Comp. Cell Registration
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        [Route("/api/services/app/User/CompCellRegistration")]
        public IActionResult CompCellRegistration([FromBody]CreateUserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.CreateUser(input, (long)enUserType.CompCell),
                    Success = true,
                    Message = "Pricipal Register Successfully"
                });
            });
        }

        /// <summary>
        /// Anurag Digal
        /// This methode is used for Asst. CoE Registration
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ValidateModel]
        [HttpPost]
        [Route("/api/services/app/User/AsstCoERegistration")]
        public IActionResult AsstCoERegistration([FromBody]CreateUserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.CreateUser(input, (long)enUserType.AsstCoE),
                    Success = true,
                    Message = "Pricipal Register Successfully"
                });
            });
        }

        [CustomAuthorize(APIId = 0)]
        [ValidateModel]
        [HttpPut]
        [Route("/api/services/app/User/{id}")]
        public IActionResult Update(long id,[FromBody] UserDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.UpdateUser(id, input),
                    Success = true,
                    Message = "User Updated Successfully"
                });
            });
        }

        //[AllowAnonymous]
        //[HttpGet]
        //[Route("/api/services/app/User/{id}")]
        //public IActionResult GetUser(long id)
        //{
        //    return GetHttpResponse(() =>
        //    {
        //        return Ok(new RequestResult
        //        {
        //            Result = _userService.GetUserDetails(id),
        //            Success = true,
        //            //Message = "User Updated Successfully"
        //        });
        //    });
        //}
        #region  GET LOGIN USER DETAILS
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 27/SEP/2019 
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize(APIId = 0)]
        [HttpGet]
        [Route("/api/services/app/GetLastLoginTime")]
        public IActionResult GetLastLoginTime()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.GetUserDetails(),
                    Success = true,
                    //Message = "User Updated Successfully"
                });
            });
        }
        #endregion

        //[AllowAnonymous]
        [CustomAuthorize(APIId = 0)]
        [HttpDelete]
        [Route("/api/services/app/User/{id}")]
        public IActionResult DeleteUser(long id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.DeleteUserDetails(id),
                    Success = true,
                    Message = "User Deleted Successfully"
                });
            });
        }

        [HttpPut]
        [ValidateModel]
        [Route("/api/services/app/User/ChangePassword")]
        public IActionResult ChangePassword([FromBody]ChangePasswordDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.ChangeUserPassword(input),
                    Success = true,
                    Message = "Password Updated Successfully"
                });
            });
        }
        [AllowAnonymous]
        [HttpPut]
        [Route("/api/services/app/User/ResetPassword")]
        public IActionResult ResetPassword([FromBody]ResetPasswordDto input)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.ResetUserPassword(input),
                    Success = true,
                    Message = "Password Reset Successfully"
                });
            });
        }

        //[AllowAnonymous]
        [CustomAuthorize(APIId = 0)]
        [HttpGet]
        [Route("/api/services/app/GetAllUserInfo")]
        public IActionResult GetAllUserInfo()
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.GetAllUserInfo(),
                    Success = true                    
                });
            });
        }


        //[AllowAnonymous]
        //[HttpGet]
        //[Route("/api/services/app/ResetPasswordForAllUsers")]
        //public IActionResult ResetPasswordForAllUsers(string password)
        //{
        //	return GetHttpResponse(() =>
        //	{
        //		return Ok(new RequestResult
        //		{
        //			Result = _userService.ResetPasswordForAllUsers(password),
        //			Success = true
        //		});
        //	});
        //}

        [CustomAuthorize(APIId = 0)]
        [HttpGet]
        [Route("/api/services/app/GetUserById/{id}")]
        public IActionResult GetUserById(string id)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.GetUserInfoById(id),
                    Success = true
                });
            });
        }

        [CustomAuthorize(APIId = 0)]
        [ValidateModel]
        [HttpPut]
        [Route("/api/services/app/UpdateStudentDetails")]
        public IActionResult UpdateStudentDetails([FromBody] CreateUserDto userObj)
        {
            return GetHttpResponse(() =>
            {
                return Ok(new RequestResult
                {
                    Result = _userService.UpdateStudent(userObj),
                    Success = true,
                    Message = "User Updated Successfully"
                });
            });
        }


    }
}
