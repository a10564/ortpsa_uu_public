﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using MicroService.SharedKernel;
using System.IdentityModel.Tokens.Jwt;
using MicroService.Utility.Common.Abstract;

namespace MicroService.API.CustomAttribute
{
    public class CustomAuthorize : Attribute, IActionFilter
    {

        public int APIId { get; set; }

        public CustomAuthorize()
        {


        }
        // runs before action method
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var errorMessage = new ErrorDetail();
            errorMessage.Message = "Unauthorized Access!";
            string APIErrorMessage = "Access Denied.";
            var unAuthorizeResult = new RequestResult() { UnAuthorizedRequest = true, Error = errorMessage };
            var _redis = context.HttpContext.RequestServices.GetService<IRedisDataHandlerService>();
            
            
            
                var req = context.HttpContext.Request;

                // Grab the current request headers
                var headers = req.Headers;

                if (!req.Headers.ContainsKey("Authorization"))
                    context.Result = new BadRequestObjectResult(unAuthorizeResult);

                var authHeader = req.Headers["Authorization"];

                // Ensure that all of your properties are present in the current Request
                if (!String.IsNullOrEmpty(authHeader))
                {
                    if (IsTokenExpired(authHeader))
                    {
                        context.Result = new BadRequestObjectResult(unAuthorizeResult);
                    }
                    else
                    {
                        try
                        {

                            var authHeadr = AuthenticationHeaderValue.Parse(authHeader);
                            //JObject redisData = new JObject();
                            string authTokenKey = Convert.ToString(authHeadr).Replace("Bearer", "").Trim();
                           
                            var isValid = IsValidAuthentication(_redis.GetClaimsData<long?>("Id"), APIId);
                            if (!isValid)
                            {
                                context.Result = new BadRequestObjectResult(unAuthorizeResult);
                            }
                            else if (!IsValidAPIData(_redis.GetClaimsListData<string>("RoleFeatureApiIds"), APIId))
                            {
                                unAuthorizeResult.UnAuthorizedRequest = false;
                                unAuthorizeResult.Error.Message = APIErrorMessage;
                                context.Result = new BadRequestObjectResult(unAuthorizeResult);
                            }
                        }
                        catch (Exception e)
                        {
                            context.Result = new BadRequestObjectResult(unAuthorizeResult);
                        }
                    }

                }
                else
                {
                    context.Result = new BadRequestObjectResult(unAuthorizeResult);
                }


        }
        // runs after action method
        public void OnActionExecuted(ActionExecutedContext context)
        { }

        private string[] GetAutherizationHeaderValues(string rawAuthzHeader)
        {

            var credArray = rawAuthzHeader.Split(':');

            if (credArray.Length == 4)
            {
                return credArray;
            }
            else
            {
                return null;
            }

        }

        private bool IsValidAuthentication(long? userId, int apiId)
        {
            if (userId == null)
            {
                return false;
            }
            else
            {
                return true;
            }


        }
        private bool IsValidAPIData(List<string> roleFeatureMapperApiIds, int apiId)
        {
            if (apiId != (int)EnApiId.All)
            {
                if (roleFeatureMapperApiIds == null)
                {
                    return false;
                }
                else if (!validateRoleFeatureMapper(roleFeatureMapperApiIds, apiId))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        protected bool validateRoleFeatureMapper(List<string> RoleFeatureMapper, int apiId)
        {
            if (RoleFeatureMapper.Count == 0)
            {
                return false;
            }
            else if (RoleFeatureMapper.IndexOf(apiId.ToString()) == -1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// Decodes the JWT token and checks whether the Token has expired or not.
        /// If token has expired then it returns true ,other wise false
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private bool IsTokenExpired(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            string orginal = Convert.ToString(token).Replace("Bearer", "").Trim();
            var decodeData = handler.ReadToken(orginal) as JwtSecurityToken;
            //var jti = tokenS.Claims.First(claim => claim.Type == "Id").Value;
            if (decodeData.ValidTo < DateTime.UtcNow)
            {
                return true;
            }
            return false;
        }

    }

    internal class Http403Result : ActionResult
    {
        public void ExecuteResult(ControllerContext context)
        {
            // Set the response code to 403.
            context.HttpContext.Response.StatusCode = 403;
        }
    }
    public enum EnApiId
    {
        All = 0
    }
}
