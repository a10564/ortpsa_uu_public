﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MicroService.API.Model
{
    public class AuthenticateModel
    {
        [Required]
        //[StringLength(MaxEmailAddressLength)]
       
        public string UserNameOrEmailAddress { get; set; }
        
        public string Password { get; set; }

        public bool RememberClient { get; set; }

        //Added by Amar to previlledge the tenant login passed as the param at the time of login.
        public string TenancyName { get; set; }
    }
}
