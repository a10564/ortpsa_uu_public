﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace MicroService.API
{
    public class Program
    {
        private static IConfiguration _configuration;
        public static void Main(string[] args)
        {
            try
            {
                _configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile(
                        $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json",
                        true)
                    .AddEnvironmentVariables()
                    .AddCommandLine(args)
                    .Build();

                // Serilog setup
                // Serilog error message file setup

                string serilogErrorFilePath = _configuration["serilogErrorFilePath"];//"C:\\Users\\BI112\\Desktop\\New folder";
                if (!Directory.Exists(serilogErrorFilePath))
                {
                    Directory.CreateDirectory(serilogErrorFilePath);
                }
                string serilogErrorFileName = serilogErrorFilePath + "/log.txt";

                Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Warning()
                        .WriteTo.File(serilogErrorFileName, retainedFileCountLimit: null, rollingInterval: RollingInterval.Day)
                        .CreateLogger();

                var file = File.CreateText(serilogErrorFileName);
                Serilog.Debugging.SelfLog.Enable(file);



                var host = BuildWebHost(args);

                host.Run();

            }
            catch (Exception exception)
            {
                Log.Fatal(exception, "Site terminated");                
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IWebHost BuildWebHost(string[] args)
        {
            var builder = WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(_configuration)
                .UseStartup<Startup>()
                .UseUrls(_configuration.GetSection("DeploymentURLIP").Value);

            return builder.Build();
        }
    }
}
