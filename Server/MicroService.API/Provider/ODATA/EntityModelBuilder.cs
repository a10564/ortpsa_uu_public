﻿//using MicroService.Domain.Models;
//using MicroService.Domain.Models.AggregateRoot;
using MicroService.Domain.Models.Views;
using MicroService.Domain.Models.Entity;
using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Domain.Models.Dto.Nursing;

namespace MicroService.API.Provider.ODATA
{
    public class EntityModelBuilder
    {
        public IEdmModel GetEdmModel(IServiceProvider serviceProvider)
        {
            var builder = new ODataConventionModelBuilder(serviceProvider);


            //// Enabling the OData method for entity 'FarmerRegistration'
            //builder.EntitySet<FarmerRegistration>(nameof(FarmerRegistration))
            //           .EntityType
            //           .Filter() // Allow for the $filter Command
            //           .Count() // Allow for the $count Command
            //           .Expand() // Allow for the $expand Command
            //           .OrderBy() // Allow for the $orderby Command
            //           .Page() // Allow for the $top and $skip Commands
            //           .Select();// Allow for the $select Command; 

            //// Enabling the OData method for entity 'Farmer'
            //builder.EntitySet<Farmer>(nameof(Farmer))
            //           .EntityType
            //           .Filter() // Allow for the $filter Command
            //           .Count() // Allow for the $count Command
            //           .Expand() // Allow for the $expand Command
            //           .OrderBy() // Allow for the $orderby Command
            //           .Page() // Allow for the $top and $skip Commands
            //           .Select();// Allow for the $select Command;

            builder.EntitySet<Role>("Roles")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<FeatureView>("Features")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<GeographyDetailView>("GeographyDetails")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<Tenants>("Tenants")
                      .EntityType
                      .Filter() // Allow for the $filter Command
                      .Count() // Allow for the $count Command
                      .Expand() // Allow for the $expand Command
                      .OrderBy() // Allow for the $orderby Command
                      .Page() // Allow for the $top and $skip Commands
                      .Select();// Allow for the $select Command;

            builder.EntitySet<Multilingual>("TenantWiseMultilinguals")
                      .EntityType
                      .Filter() // Allow for the $filter Command
                      .Count() // Allow for the $count Command
                      .Expand() // Allow for the $expand Command
                      .OrderBy() // Allow for the $orderby Command
                      .Page() // Allow for the $top and $skip Commands
                      .Select();// Allow for the $select Command;

            builder.EntitySet<UserView>("Users")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<AcknowledgementView>("StudentRegistrationView")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<UniversityRegistrationDetail>("UniversityRegistrationNumber")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<MigrationView>("StudentMigrationView")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<DuplicateRegistrationView>("DuplicateRegistrationView")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;           

            builder.EntitySet<ExaminationViewExcludeBackgroundProcessTable>("ExaminationView")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<SubjectMaster>("SubjectOdata")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<DepartmentMaster>("DepartmentOdata")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<CollegeMaster>("CollegeOdata")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<CLCView>("CLCView")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;

            builder.EntitySet<StudentProfileView>("StudentProfile")
                       .EntityType
                       .Filter() // Allow for the $filter Command
                       .Count() // Allow for the $count Command
                       .Expand() // Allow for the $expand Command
                       .OrderBy() // Allow for the $orderby Command
                       .Page() // Allow for the $top and $skip Commands
                       .Select();// Allow for the $select Command;
            builder.EntitySet<AcademicStatementView>("AcademicStatementViewOData")
                     .EntityType
                     .Filter() // Allow for the $filter Command
                     .Count() // Allow for the $count Command
                     .Expand() // Allow for the $expand Command
                     .OrderBy() // Allow for the $orderby Command
                     .Page() // Allow for the $top and $skip Commands
                     .Select();// Allow for the $select Command;

            builder.EntitySet<ServiceWiseStudentDetailsDto>("ServiceWiseStudentDetails") 
                      .EntityType
                      .Filter() // Allow for the $filter Command
                      .Count() // Allow for the $count Command
                      .Expand() // Allow for the $expand Command
                      .OrderBy() // Allow for the $orderby Command
                      .Page() // Allow for the $top and $skip Commands
                      .Select();// Allow for the $select Command;
            builder.EntitySet<MarkStatementView>("MarkStatementViewOData")
                     .EntityType
                     .Filter() // Allow for the $filter Command
                     .Count() // Allow for the $count Command
                     .Expand() // Allow for the $expand Command
                     .OrderBy() // Allow for the $orderby Command
                     .Page() // Allow for the $top and $skip Commands
                     .Select();// Allow for the $select Command;

            builder.EntitySet<ExaminationPaperWiseMarkDto>("ExaminationPaperWiseMarkOData")
                   .EntityType
                   .Filter() // Allow for the $filter Command
                   .Count() // Allow for the $count Command
                   .Expand() // Allow for the $expand Command
                   .OrderBy() // Allow for the $orderby Command
                   .Page() // Allow for the $top and $skip Commands
                   .Select();// Allow for the $select Command;

            builder.EntitySet<NursingViewExcludeBackgroundProcessTable>("NursingViewOData")
                   .EntityType
                   .Filter() // Allow for the $filter Command
                   .Count() // Allow for the $count Command
                   .Expand() // Allow for the $expand Command
                   .OrderBy() // Allow for the $orderby Command
                   .Page() // Allow for the $top and $skip Commands
                   .Select();// Allow for the $select Command;

            builder.EntitySet<NursingAcademicStatementDto>("NursingAcademicStatementOData")
                   .EntityType
                   .Filter() // Allow for the $filter Command
                   .Count() // Allow for the $count Command
                   .Expand() // Allow for the $expand Command
                   .OrderBy() // Allow for the $orderby Command
                   .Page() // Allow for the $top and $skip Commands
                   .Select();// Allow for the $select Command;
            builder.EntitySet<NursingMarkStatementView>("NursingMarkStatementViewOData")
                   .EntityType
                   .Filter() // Allow for the $filter Command
                   .Count() // Allow for the $count Command
                   .Expand() // Allow for the $expand Command
                   .OrderBy() // Allow for the $orderby Command
                   .Page() // Allow for the $top and $skip Commands
                   .Select();// Allow for the $select Command;
            return builder.GetEdmModel();
        }
    }
}


