﻿using MicroService.Core;
using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Utility.Exception;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MicroService.API
{
    public class WorkflowDtoConfigurationPublisher
    {
        private WorkflowDbContext _workflowDbContext;

        public WorkflowDtoConfigurationPublisher()
        {
            _workflowDbContext = new WorkflowDbContext();
        }

        public List<Type> GetUIConfigurableDtoTypes()
        {
            var entities = new List<Type>();
            var type = typeof(UIConfigurableAttribute);

            // Domain project reference
            var entityTypes = Assembly.Load("MicroService.Domain.Models")
                //.GetCallingAssembly()
                .GetTypes().ToList();

            foreach (var entType in entityTypes)
            {
                var entityAttr = Attribute.GetCustomAttributes(entType);
                foreach (var attr in entityAttr)
                {
                    if (attr is UIConfigurableAttribute)
                    {
                        entities.Add(entType);
                    }
                }
            }
            return entities;
        }

        public void GetWorkflowDtoPropertyMetadata(IConfiguration _configuration)
        {
            List<TenantDtoConfig> tenantDtoConfigs = new List<TenantDtoConfig>();
            List<Type> workflowDtos = GetUIConfigurableDtoTypes();
            var microserviceName = _configuration["Microservice:Name"];
            int microserviceId = 0;
            try
            {
                int.TryParse(_configuration["Microservice:Id"], out microserviceId);
            }
            catch (Exception ex)
            {
                throw new MicroServiceException { ErrorCode = "FAR003" };
            }
            
            if (microserviceId != 0 && microserviceName != null)
            {
                workflowDtos.ForEach(dto =>
                {
                    TenantDtoConfig tenantDtoConfig = new TenantDtoConfig();
                    dynamic taskdtoMetadataProperty = new MetadataAssemblyInfo().GetTaskParameters(dto);
                    dynamic dtoMetadataProperty = new MetadataAssemblyInfo().GetPropertyMetadata(dto);
                    tenantDtoConfig.DtoName = dto.Name;
                    tenantDtoConfig.MicroserviceName = microserviceName;
                    tenantDtoConfig.MicroserviceId = microserviceId;
                    tenantDtoConfig.Title = dtoMetadataProperty.title;
                    tenantDtoConfig.TenantId = 0;
                    tenantDtoConfig.TaskFields = JsonConvert.SerializeObject(taskdtoMetadataProperty);
                    tenantDtoConfig.Fields = JsonConvert.SerializeObject(dtoMetadataProperty.fields);
                    tenantDtoConfig.CreatorUserId = 0;
                    tenantDtoConfig.CreationTime = DateTime.UtcNow;
                    tenantDtoConfig.IsActive = true;

                    tenantDtoConfigs.Add(tenantDtoConfig);
                });
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "FAR004" };
            }
            // 1. Now write the code to publish the data to topic
            // 1. It is the responsibility of the topic to send the data to an API and that API should save the data into Db.
            // 2. Idea has been changed, now we are only saving it in shared DB
            if(tenantDtoConfigs.Count > 0)
            {
                _workflowDbContext.TenantDtoConfig.RemoveRange(_workflowDbContext.TenantDtoConfig.Where(tdto => tenantDtoConfigs.Any(tConfig => tConfig.DtoName == tdto.DtoName)
                        && tdto.MicroserviceId == tenantDtoConfigs.First().MicroserviceId && tdto.TenantId == 0));

                _workflowDbContext.TenantDtoConfig.AddRange(tenantDtoConfigs);
                _workflowDbContext.SaveChanges();
            }
        }
    }
}
