﻿using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.NumberGeneration;
using MicroService.Domain.Models.Dto.QRCode;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Views;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Business.Abstract
{
    public interface ICommonAppService
    {
        IQueryable<Lookup> GetLookupData(long[] parentLookupTypeList);
        //DateTime CurrentDate();
        IQueryable<CollegeMaster> GetCollegeDetails(string courseTypeCode);
        IQueryable<CollegeMaster> GetCollegeDetails(string courseTypeCode, string streamCode, string subjectCode);
        IQueryable<DepartmentMaster> GetDepartmentDetails(string streamCode, string courseCode);
		string GetNextSeqNumber(long processType, string customPreFix = "");	
        IQueryable<SubjectMaster> GetSubjectDetails(string filterCode, string departmentCode);
        IQueryable<ExaminationPaperMaster> GetPaperDetails(string courseType, string courseCode, string streamCode, string departmentCode, string subjectCode, string semesterCode);
        bool GetProcessType(long processType,long serviceType);
        //string BuildRegistrationNumber(NumberGenerationDto studentInfo);
		RequestResult CallExternalAPI(string baseUrl, string apiURL, object content, string apiType);
        DateTime GetMinusCurrentDate();
        #region OData Query Section
        IQueryable<SubjectMaster> GetAllSubject();
        IQueryable<DepartmentMaster> GetAllDepartment();
        IQueryable<CollegeMaster> GetAllCollege();
        #endregion
        string GetUserRoleWiseCollege(long roleId);
        QRCodeValidateResponseDto ValidateQRCode(long id, string number);
        List<int> GetYearOfAdmissionList(long serviceNumber);
        StudentProfileView GetLoginStudentData();
        string NumberToWords(int number);
        string DecimalToWords(decimal number);


        List<CollegeMaster> GetCollegeMasterList();
        List<CollegeMaster> GetAllGetCollegeMasterListDataFromCache(Func<CollegeMaster, bool> predicate);
        void UpdateCollegeMasterCache();
        List<CollegeMaster> GetAllGetCollegeMasterListDataFromCache();
        List<LateFineMaster> GetAllLateFineMasterListDataFromCache();
        List<ExaminationPaperMaster> GetActiveExaminationPaperMasterList();
        List<ExaminationMaster> GetActiveExaminationMasterList();
        List<EmailTemplate> GetEmailTemplateDataFromCache();
        List<ExaminationSession> GetActiveExaminationSessionList();
        List<CollegeMaster> GetCollegeMasterBySubject(string courseTypeCode, string streamCode, string subjectCode);
        List<Lookup> GetSessionListBySubject(string subjectCode);
        List<Lookup> GetAllLookupsDataFromCache(Func<Lookup, bool> predicate);
        RoleUserWiseSubjectMapper GetRoleUserWiseSubject();
        CollegeServiceMapper GetCollegeWiseServiceDetails(string CollegeCode);
    }
}
