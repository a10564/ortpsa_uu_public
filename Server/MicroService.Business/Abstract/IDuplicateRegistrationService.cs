﻿using MicroService.Domain.Models.Dto.DuplicateRegistrationDetail;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IDuplicateRegistrationService
    {
        DuplicateRegistrationDetailDto GetRegistrationDetail(string regNo);
        DuplicateRegistrationDetailDto DuplicateRegistrationDetailApply(DuplicateRegistrationDetailDto param);
        IQueryable<DuplicateRegistrationView> GetRoleBasedActiveDuplicateRegistrationInboxData();
        DuplicateRegistrationDetailDto GetDuplicateRegistrationDetail(Guid id);
    }
}
