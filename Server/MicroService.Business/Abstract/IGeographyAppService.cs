﻿using MicroService.Domain.Models.Dto.GeographyOder;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IGeographyAppService
    {
        GeographyOrderDto CreateGeographyOrder(GeographyOrderCreateDto input);
        GeographyOrderDto UpdateGeographyOrder(long id, GeographyOrderDto input);
        List<GeographyOrder> GetAllGeographyOrder();
        GeographyOrder GetGeographyOrder(long id);
        bool Delete(long id);

        GeographyDetailDto CreateGeographyDetail(GeographyDetailCreateDto input);
        GeographyDetailDto UpdateGeographyDetail(long id, GeographyDetailDto input);
        GeographyDetail GetGeographyDetail(long id);
        List<GeographyDetail> GetAllGeographyDetail();
        bool DeleteGeographyDetail(long id);

        IQueryable<GeographyDetailView> GetAllGeographysData();
    }
}
