﻿using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Dto.Report;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IReportService
    {
        GridModelDto GetAllStudentReport(BackgroundProcessDto reportParam);
        PDFDownloadFile CallSPForReportDownload(BackgroundProcessDto param);
        PDFDownloadFile DownloadStatement(SearchParamDto param);

    }
}
