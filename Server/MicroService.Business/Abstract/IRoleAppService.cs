﻿using MicroService.Domain.Models.Dto.Role;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IRoleAppService
    {
        RoleDto CreateRole(CreateRoleDto input);
        RoleDto UpdateRole(long id, RoleDto input);
        Role GetRoleDetails(long id);
        bool DeleteRoleDetails(long id);
        List<Role> GetRoleDetails();

        IQueryable<Role> GetAllRolesData();
    }
}
