﻿using MicroService.Domain.Models.Dto.CLC;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IStudentCLCService
    {
        ApplyCLCDto StudentCLCApplied(ApplyCLCDto input);
        ApplyCLCDto GetClcDetailById(Guid id);
        IQueryable<CLCView> GetAllCLCViewData();
        ApplyCLCDto Update(Guid id, ApplyCLCDto input);
    }
}
