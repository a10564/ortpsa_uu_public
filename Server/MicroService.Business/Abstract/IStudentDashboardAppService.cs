﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Dto.UniversityService;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IStudentDashboardAppService
    {
        List<AppliedServiceDto> GetAllAppliedService();
        PDFDownloadFile DownloadService(DownloadParamDto param);
        List<ServiceStatusDto> GetServiceStatus(long docType);
        PDFDownloadFile DownloadAcknowledgementService(DownloadParamDto param);
        List<PendingActionListDto> PendingActionList();
        Guid GetServicePrimarykey(long docType);
        PDFDownloadFile DownloadMigrationService(DownloadParamDto param);
        SemesterExaminationNotificationDto GetSemesterExamNotification();
        List<KeyValuePair<string, string>> GetRoleWiseReportMenu();
        PDFDownloadFile DownloadNursingAdmitCard(DownloadParamDto param);
    }
}
