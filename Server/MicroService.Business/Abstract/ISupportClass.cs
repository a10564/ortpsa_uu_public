﻿using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public  interface ISupportClass
    {
        List<ServiceMaster> GetAllServiceDetails();
        IQueryable<ServiceWiseStudentDetailsDto> ServiceWiseStudentDetail();
        List<CollegeMaster> GetAllCollegeListData();
        List<ServiceWiseStudentDetailsDto> GetAllCollegeWiseCandidateFeeReport(SearchParamDto param);
        DataTable GetReportsCollegeWise(SearchParamDto param);
    }
}
