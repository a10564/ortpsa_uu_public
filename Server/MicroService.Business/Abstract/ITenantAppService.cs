﻿
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.Tenants;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface ITenantAppService
    {
        TenantDto CreateTenant(CreateTenantDto input);
        TenantDto UpdateTenant(long id, TenantDto input);
        Tenants GetTenantDetails(long id);
        bool DeleteTenantDetails(long id);

        IQueryable<Tenants> GetAllTenantsData();        
    }
}
