﻿using MicroService.Domain.Models.Dto.TenantWiseMultilingual;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface ITenantWiseMultilingualAppService
    {
        ModuleDto AddModule(ModuleDto moduleDetailDto);
        LanguageDto AddLanguage(LanguageDto languageDto);
        object GetMultilingualData(long moduleId, string languageType);
        List<MultilingualDto> GetMultilingualDetail();
        List<MultilingualDto> GetMultilingualDetailRootTenant();
        bool AddMultiLingualDataForTenant(List<MultilingualDto> multilingualData);
        string AddMultiLingualDataForRootTenant(List<MultilingualDto> multilingualData);

        IQueryable<Multilingual> GetAllMultilingualsData();
    }
}
