﻿using Microservice.Common;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.User;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business.Abstract
{
    public interface IUserService
    {
        bool CreateUser(CreateUserDto input,long userType);
        UserDto UpdateUser(long id, UserDto input);
        //Users GetUserDetails(long id);
        bool DeleteUserDetails(long id);
        bool ChangeUserPassword(ChangePasswordDto input);
        bool ResetUserPassword(ResetPasswordDto input);
        IQueryable<UserView> GetAllUserViewData();
        Users AuthenticateUser(AuthenticateModelDto model);
        //void UpdateLastLogin();
        LoginUserDetails GetUserDetails();
        //long GenerateOtp();
        bool GenerateOtpAndSendToUser(string emailAddress, string mobileNumber);
        UserOtp GetGeneratedOtp(string emailAddress, string mobileNumber);
        UserDetailsDto GetAllUserInfo();
        bool GenerateAndSendOtp(UserOtpDto otp);
		int ResetPasswordForAllUsers(string password);
        IQueryable<StudentProfileView> GetStudentProfileData();
        UserDetailsDto GetUserInfoById(string id);
        bool UpdateStudent(CreateUserDto userObj);
    }
}
