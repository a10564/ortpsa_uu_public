﻿using AutoMapper;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ApplyProvisionalCertificateDetail;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MicroService.Business
{
    public class ApplyProvisionalCertificateService : BaseService, IApplyProvisionalCertificateService
    {
        private IMicroServiceUOW _MSUoW;
        private IMicroserviceCommon _MSCommon;
        private IStudentRegistrationNoService _StdRegdService;
        public ApplyProvisionalCertificateService(IRedisDataHandlerService redisData, IMicroServiceUOW MSUoW, IMicroserviceCommon MSCommon, IStudentRegistrationNoService StdRegdService) : base(redisData)
        {
            _StdRegdService = StdRegdService;
            _MSUoW = MSUoW;
            _MSCommon = MSCommon;
        }
        #region
        /// <summary>
        /// Author          : Lopamudra Senapati 
        /// Date            : 08-11-2019
        /// Description     :This method adds data to table
        /// <summary>
        public ProvisionalCertificateApplicationDetailDto ApplyForProvisionalCertificate(ProvisionalCertificateApplicationDetailDto input)
        {
            long serviceId = (long)enServiceType.ProvisionalCertificate;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            if (user.UserType == (long)enRoles.Student)
            {
                if (input != null)
                {
                    if (ValidateInput(input))
                    {
                        ProvisionalCertificateApplicationDetail applicationData = Mapper.Map<ProvisionalCertificateApplicationDetailDto, ProvisionalCertificateApplicationDetail>(input);
                        var newGuid = Guid.NewGuid();
                        string guidForAck = newGuid.ToString();
                        applicationData.Id = newGuid;
                        string prefix = "ACK";
                        applicationData.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(prefix, serviceId, guidForAck);
                        _MSUoW.ProvisionalCertificateApplicationDetail.Add(applicationData);
                        _MSUoW.Commit();
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == serviceId && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();

                        // Send email to student
                        _StdRegdService.SendAcknowledgementEmail(user.EmailAddress, applicationData.CandidateName, serviceName, applicationData.AcknowledgementNo);
                        input = Mapper.Map<ProvisionalCertificateApplicationDetail, ProvisionalCertificateApplicationDetailDto>(applicationData);
                        return input;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU013" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU052" };
            return input;
        }
        #endregion
        #region VALIDATE INPUT
        /// <summary>
        /// Author          : Lopamudra Senapati 
        /// Date            : 11-11-2019
        /// Description     :This method validate input data
        /// <summary>
        private bool ValidateInput(ProvisionalCertificateApplicationDetailDto input)
        {
            string regularExpressionForName = @"^[a-zA-Z\s\.]+$";//This regex is used to allow only alphabets, dot and spaces in string
            string regExDOB = @"^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}+$";
            if (input.CandidateName == null || input.CandidateName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.CandidateName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.RollNo == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU065" };
            }
            else if (input.UniversityRegdNo == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU064" };
            }
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseType == null || input.CourseType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
            {
                input.DepartmentCode = "";
            }
            else if ((input.CourseType == "02") && (input.DepartmentCode == null || input.DepartmentCode == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU056" };
            }
            else if (input.CourseCode == null || input.CourseCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU059" };
            }
            input.CandidateName = input.CandidateName.Trim();
            input.CollegeCode = input.CollegeCode.Trim();
            input.CourseType = input.CourseType.Trim();
            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();
            return true;
        }
        #endregion
    }
}
