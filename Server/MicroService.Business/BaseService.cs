﻿using MicroService.Utility.Common.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Business
{
    public class BaseService
    {
        private IRedisDataHandlerService _redisData;

        public BaseService(IRedisDataHandlerService redisData)
        {
            _redisData = redisData;
        }

        public List<string> GetRoleListOfCurrentUser()
        {
            return _redisData.GetClaimsListData<string>("RoleIds");
        }

        public long GetCurrentTenantId()
        {
            return _redisData.GetClaimsData<long>("TenantId");
        }

        public long GetCurrentUserId()
        {
            return _redisData.GetClaimsData<long>("Id");
        }

        public List<long> GetGeography()
        {
              return _redisData.GetClaimsListData<long>("Geography");
        }

        /// <summary>
        /// Author      : Dattatreya Dash
        /// Date        : 15-10-2019
        /// Description : Returns the user type of the current logged in user
        /// </summary>
        /// <returns>User type of the logged in user</returns>
        public long GetUserType()
        {
            return _redisData.GetClaimsData<long>("UserType");
        }

        /// <summary>
        /// Author      : Dattatreya Dash
        /// Date        : 15-10-2019
        /// Description : Returns the college code of the current logged in user. It will mostly have value for principal role.
        /// </summary>
        /// <returns>College code of the current logged in user</returns>
        public string GetCollegeCode()
        {
            return _redisData.GetClaimsData<string>("CollegeCode");
        }

        /// <summary>
        /// Author      : Dattatreya Dash
        /// Date        : 08-11-2019
        /// Description : Returns the department code of the current logged in user. It will mostly have value for HOD role.
        /// </summary>
        /// <returns>College code of the current logged in user</returns>
        public string GetDepartmentCode()
        {
            return _redisData.GetClaimsData<string>("DepartmentCode");
        }

        /// <summary>
        /// Author      : Anurag Digal
        /// Date        : 18-11-2019
        /// Description : Returns the device type of the current logged in user. It will mostly have value when any mobile user logged Into the application
        /// </summary>
        /// <returns>College code of the current logged in user</returns>
        public string GetDeviceType()
        {
            return _redisData.GetClaimsData<string>("DeviceType");
        }

        /// <summary>
        /// Author      : Sitikanta Pradhan
        /// Date        : 18-Nov-2019
        /// </summary>
        //public string GetIsAffiliated()
        //{
        //    return _redisData.GetClaimsData<string>("IsAffiliated");
        //}
    }
}
