﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Views;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using System.Threading.Tasks;
using MicroService.DataAccess.Helpers;
using MicroService.DataAccess;
using MicroService.Utility.Logging.Abstract;
using MicroService.Core;
using MicroService.Core.WorkflowTransactionHistrory;
using System.Text;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.AdminActivity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using MicroService.Domain.Models.Dto.Workflow;
using MicroService.Domain.Models.Dto.StudentDashboard;
using TimeZoneConverter;

namespace MicroService.Business
{
    public class ExaminationFormFillUpService : BaseService, IExaminationFormFillUpService
    {
        private IMicroServiceUOW _MSUoW;
        private readonly IHostingEnvironment _environment;
        private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        //private MetadataAssemblyInfo workflowHelper;
        private WorkflowDbContext _workflowDbContext;
        private IMicroserviceCommon _MSCommon;
        private IConfiguration _IConfiguration;
        private IRedisDataHandlerService _redisData;
        private IMicroServiceLogger _logger;
        private readonly string WorkflowName = "";
        private ICommonAppService _commonAppService;

        public ExaminationFormFillUpService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IConfiguration IConfiguration,
            IHostingEnvironment environment,
            IWFTransactionHistroryAppService WFTransactionHistroryAppService,
            IMicroserviceCommon MSCommon,
            IMicroServiceLogger logger,
            ICommonAppService commonAppService
          ) : base(redisData)
        {
            _MSUoW = MSUoW;
            _redisData = redisData;
            _logger = logger;
            _environment = environment;
            //_IConfiguration = IConfiguration;
            _WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            _workflowDbContext = new WorkflowDbContext();
            //workflowHelper = new MetadataAssemblyInfo();
            _MSCommon = MSCommon;
            _IConfiguration = IConfiguration;
            WorkflowName = _IConfiguration.GetSection("ExamFormFillup:WorkflowName").Value;
            _commonAppService = commonAppService;
        }

        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 14-Nov-2019
        /// Method is used for Odata Controller which will give examination student record as per usertype
        /// </summary>
        /// <returns></returns>
        public IQueryable<ExaminationViewExcludeBackgroundProcessTable> GetRoleBasedActiveExaminationInboxData(bool isCountRequired)
        {
            // isCountRequired = true is used to show count in dashboard page
            // isCountRequired = false is used to show data in inbox page
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            IQueryable<ExaminationViewExcludeBackgroundProcessTable> inboxDataAsPerRole = null;
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            if (roleNames.Contains(Convert.ToString((long)enRoles.COE)))
            {
                List<string> stateList = GetRoleSpecificWorkflowStateList();
                inboxDataAsPerRole = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.GetAll().Where(w => w.IsActive && stateList.Contains(w.State));
            }
            else if (roleNames.Contains(Convert.ToString((long)enRoles.Principal)) || roleNames.Contains(Convert.ToString((long)enRoles.HOD)))
            {
                if (!isCountRequired)
                {
                    inboxDataAsPerRole = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId) || t.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:EndState").Value.Trim().ToLower())
                 || t.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:PaymentPendingState").Value.Trim().ToLower());
                }
                else
                {
                    inboxDataAsPerRole = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId)));
                }

            }
            else
            {
                if (!isCountRequired)
                {
                    inboxDataAsPerRole = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId) || t.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:EndState").Value.Trim().ToLower()));
                }
                else
                {
                    inboxDataAsPerRole = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId)));
                }

            }

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    if (isCountRequired == false)
                    {// Data for inbox, remove records already available in background table
                        var backgroundProcessIdList = _MSUoW.BackgroundProcess.GetAll().Where(bgp => (bgp.InstanceTrigger == _IConfiguration.GetSection("ExamFormFillup:PrincipalToCOE").Value ||
                        bgp.InstanceTrigger == _IConfiguration.GetSection("ExamFormFillup:MarkForwardToCOE").Value) && bgp.IsActive).Select(g => g.InstanceId);
                        inboxDataAsPerRole = inboxDataAsPerRole.Where(data => !backgroundProcessIdList.Contains(data.Id) && data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.IsActive);
                    }
                    else
                    {
                        inboxDataAsPerRole = inboxDataAsPerRole.Where(data => CollegeCode.Contains(data.CollegeCode));
                    }
                    break;
                case (long)enUserType.HOD:
                    // Not implemented changes related to 'isCountRequired'
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower() && data.IsActive);
                    break;
                case (long)enUserType.Student:
                    // Changes related to param 'isCountRequired' is not required
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
                case (long)enUserType.AsstCoE:
                    if (isCountRequired == false)
                    {// Data for inbox, remove records already available in background table
                        var triggerList = workflowTransition.Select(trans => trans.Trigger).ToList();
                        var backgroundRecord = _MSUoW.BackgroundProcess.FindBy(bgp => triggerList.Contains(bgp.InstanceTrigger) && bgp.IsActive).ToList();
                        backgroundRecord.ForEach(bkgRcrd =>
                        {
                            inboxDataAsPerRole = inboxDataAsPerRole.Where(data =>
                                !(data.CourseTypeCode == bkgRcrd.CourseTypeCode
                                && data.CollegeCode == bkgRcrd.CollegeCode
                                && data.StreamCode == bkgRcrd.StreamCode
                                && data.SubjectCode == bkgRcrd.SubjectCode
                                && data.DepartmentCode == bkgRcrd.DepartmentCode
                                && data.CourseCode == bkgRcrd.CourseCode
                                && data.SemesterCode == bkgRcrd.SemesterCode
                                && data.AcademicStart == bkgRcrd.AcademicStart
                                && data.State == workflowTransition.Where(trans => trans.Trigger == bkgRcrd.InstanceTrigger).Select(trans => trans.SourceState).FirstOrDefault())
                                );
                        });
                    }
                    break;
            }

            return inboxDataAsPerRole;
        }

        /// <summary>
        /// Author  :   Anurag Digal
        /// Date    :   12-11-19
        /// Description :   This methode is save examination form fill details
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExaminationFormFillUpDetailsDto ApplyExaminationFormFillUp(ExaminationFormFillUpDetailsDto param)
        {
            ExaminationMaster examinationMasterData = _commonAppService.GetActiveExaminationMasterList().Where(a => a.CourseType == param.CourseTypeCode && a.StreamCode == param.StreamCode
             && a.SubjectCode == param.SubjectCode && (a.CourseCode == param.CourseCode || a.CourseCode == "" || a.CourseCode == null)
             && (a.DepartmentCode == param.DepartmentCode || a.DepartmentCode == "" || a.DepartmentCode == null)
             && a.SemesterCode == param.SemesterCode && a.TenantId == GetCurrentTenantId() && a.IsActive == true
             && a.ServiceMasterId == (long)enServiceType.BCABBAExamApplicationForm).FirstOrDefault();
            if (examinationMasterData != null)
            {
                ExaminationSession examinationSessionData = _MSUoW.ExaminationSession.FindBy(s => s.ExaminationMasterId == examinationMasterData.Id && s.StartYear == param.AcademicStart
                && s.IsActive == true).FirstOrDefault();
                List<ExaminationFormFillUpDates> examinationFormFillUpDatesData = _MSUoW.ExaminationFormFillUpDates.FindBy(b => b.ExaminationMasterId == examinationMasterData.Id
                   && b.ExaminationSessionId == examinationSessionData.Id && b.IsActive == true && b.TenantId == GetCurrentTenantId()).ToList();
                var startDate = examinationFormFillUpDatesData.OrderBy(a => a.SlotOrder).Select(start => start.StartDate).FirstOrDefault();
                var endDate = examinationFormFillUpDatesData.OrderByDescending(b => b.SlotOrder).Select(end => end.EndDate).FirstOrDefault();
                DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));

                List<ExaminationFormFillUpDetails> examinationDetailsList = new List<ExaminationFormFillUpDetails>();
                ExaminationFormFillUpDetails examinationDetails = new ExaminationFormFillUpDetails();

                if (startDate.Date <= currTime.Date && endDate.Date >= currTime.Date)
                {
                    long serviceId = (long)enServiceType.BCABBAExamApplicationForm;
                    Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
                    if (user.UserType == (long)enRoles.Student)
                    {
                        //fetch student examination form fill up details if exist
                        examinationDetailsList = _MSUoW.ExaminationFormFillUpDetails.GetAll().Where(d => d.IsActive && d.CreatorUserId == GetCurrentUserId()).ToList();
                        if (param != null && param.ServiceStudentAddresses != null && param.ServiceStudentAddresses.Count > 0 && param.ExaminationPaperDetails.Count > 0)
                        {
                            if (ValidateInput(param))
                            {
                                examinationDetails = examinationDetailsList
                                                    .Where(d => d.SemesterCode == param.SemesterCode
                                                    && d.State.Trim().ToLower() != _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                                    .FirstOrDefault();
                                if (examinationDetails != null)
                                {
                                    UserServiceApplied userAppliedService = _MSUoW.UserServiceApplied.FindBy(d => d.InstanceId == examinationDetails.Id && d.IsActive).FirstOrDefault();

                                    if (userAppliedService != null)
                                    {
                                        if (userAppliedService.LastStatus.Trim().ToLower() != _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower()
                                        || examinationDetails.State.Trim().ToLower() != _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                        {
                                            throw new MicroServiceException { ErrorCode = "OUU123", SubstitutionParams = new object[1] { examinationDetails.State } };
                                        }
                                    }
                                }

                                ExaminationFormFillUpDetails examFillUpDetailsCheck = new ExaminationFormFillUpDetails();

                                if (param.SemesterCode == _MSCommon.GetEnumDescription(EnSemester.FirstSemester))
                                {
                                    param.RegistrationNumber = null;
                                    param.RollNumber = null;
                                }
                                // ADDED BY SITI
                                else
                                {
                                    examFillUpDetailsCheck = examinationDetailsList
                                                            .Where(a => a.IsActive
                                                            && a.CreatorUserId == GetCurrentUserId()
                                                            && a.SemesterCode == _MSCommon.GetEnumDescription(EnSemester.FirstSemester)
                                                            && (a.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:AdmitcardGenerated").Value.Trim().ToLower()
                                                            || a.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:MarkUploaded").Value.Trim().ToLower()))
                                                            .FirstOrDefault();
                                    if (examFillUpDetailsCheck.RegistrationNumber != param.RegistrationNumber)
                                    {
                                        throw new MicroServiceException() { ErrorCode = "OUU140" };
                                    }
                                }

                                Guid newGuid = Guid.NewGuid();
                                //here we set default null for second semester and onwards.these data will update during payment time
                                param.PaymentDate = null;
                                param.CSCPayid = null;
                                param.LastModificationTime = null;
                                param.LastModifierUserId = null;
                                param.LastStatusUpdateDate = null;
                                param.PrincipalHODRemarks = null;
                                param.LastStatusUpdateDate = null;
                                ExaminationFormFillUpDetails examFillUpDetails = AutoMapper.Mapper.Map<ExaminationFormFillUpDetailsDto, ExaminationFormFillUpDetails>(param);
                                examFillUpDetails.RollNumber = examFillUpDetailsCheck.RollNumber;
                                examFillUpDetails.RegistrationNumber = examFillUpDetailsCheck.RegistrationNumber;
                                examFillUpDetails.Id = newGuid;
                                examFillUpDetails.ApplicationType = param.ApplicationType;
                                examFillUpDetails.InternetHandlingCharges = 0;
                                examFillUpDetails.FormFillUpMoneyToBePaid = 0;
                                examFillUpDetails.LateFeeAmount = 0;

                                //student Address section

                                List<ServiceStudentAddress> examStudentAddressList = new List<ServiceStudentAddress>();
                                param.ServiceStudentAddresses.ForEach(address =>
                                {
                                    ServiceStudentAddress examStudentAddress = AutoMapper.Mapper.Map<ServiceStudentAddressDto, ServiceStudentAddress>(address);
                                    examStudentAddress.Id = Guid.NewGuid();
                                    examStudentAddress.ServiceType = serviceId;
                                    if (param.IsSameAsPresent)
                                    {
                                        examStudentAddress.AddressType = address.AddressType;
                                    }
                                    examStudentAddressList.Add(examStudentAddress);
                                });
                                examFillUpDetails.ServiceStudentAddresses = examStudentAddressList;
                                // end of student Address Section

                                //student exam paper section
                                List<ExaminationPaperDetail> examPaperList = new List<ExaminationPaperDetail>();
                                param.ExaminationPaperDetails.ForEach(paper =>
                                {
                                    if (paper.ExaminationPaperId == Guid.Empty)
                                    {
                                        throw new MicroServiceException() { ErrorCode = "OUU089" };
                                    }
                                    ExaminationPaperDetail examPaper = AutoMapper.Mapper.Map<ExaminationPaperDetailsDto, ExaminationPaperDetail>(paper);
                                    examPaper.Id = Guid.NewGuid();
                                    examPaper.ExaminationPaperId = paper.ExaminationPaperId;
                                    examPaperList.Add(examPaper);
                                });
                                examFillUpDetails.ExaminationPaperDetails = examPaperList;
                                //end of student exam paper section
                                if (param.SemesterCode == _MSCommon.GetEnumDescription(EnSemester.FirstSemester))
                                {
                                    param.ImageDocument.ForEach(content =>
                                    {
                                        if (ImageValidation(content))
                                        {
                                            content.Id = Guid.NewGuid();
                                            examFillUpDetails.ImageDocument = ImageWrite(param, newGuid);
                                        }
                                    });
                                }
                                else
                                {
                                    param.ImageDocument.ForEach(content =>
                                    {
                                        content.Id = Guid.NewGuid();
                                        content.FilePath = content.FilePath.Replace(_IConfiguration.GetSection("ApplicationURL").Value, "");
                                    });
                                    examFillUpDetails.ImageDocument = param.ImageDocument;
                                }

                                #region WORKFLOW
                                var tenantLatestWF = _workflowDbContext.TenantWorkflow
                                    .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName)
                                    .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();

                                if (tenantLatestWF != null)
                                {
                                    examFillUpDetails.TenantWorkflowId = tenantLatestWF.Id;
                                }
                                else
                                {
                                    var defaultWF = _workflowDbContext.TenantWorkflow
                                   .Where(tenantWF => tenantWF.TenantId == 0 && tenantWF.Workflow == WorkflowName)  //Default tenant id
                                   .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();

                                    examFillUpDetails.TenantWorkflowId = defaultWF.Id;
                                }
                                param.LoggedInUserRoles = GetRoleListOfCurrentUser();
                                string initialTrigger = string.Empty;
                                try
                                {
                                    initialTrigger = _IConfiguration.GetSection("WorkflowInitialTrigger").Value;
                                }
                                catch (Exception)
                                {
                                    initialTrigger = "Initialize";
                                }
                                string initialState = string.Empty;
                                try
                                {
                                    initialState = _IConfiguration.GetSection("WorkflowInitialState").Value;
                                }
                                catch (Exception)
                                {
                                    initialState = "Init";
                                }
                                examFillUpDetails.State = initialState;
                                param._trigger = initialTrigger;
                                var executionStatus = param.ExecuteProcess(examFillUpDetails, param);

                                #endregion

                                // Acknowledgement will be sent only after payment is successful
                                if (param.SemesterCode == _MSCommon.GetEnumDescription(EnSemester.FirstSemester))
                                {
                                    examFillUpDetails.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(_MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK), serviceId, newGuid.ToString());
                                }
                                else
                                {
                                    examFillUpDetails.AcknowledgementNo = examFillUpDetailsCheck.AcknowledgementNo;
                                }
                                _MSUoW.ExaminationFormFillUpDetails.Add(examFillUpDetails);

                                // Service applied
                                UserServiceApplied usrSrvcApplied = new UserServiceApplied()
                                {
                                    IsAvailableToDL = false,
                                    LastStatus = examFillUpDetails.State,
                                    ServiceId = serviceId,
                                    UserId = GetCurrentUserId(),
                                    InstanceId = newGuid
                                };
                                _MSUoW.UserServiceApplied.Add(usrSrvcApplied);
                                _MSUoW.Commit();

                                #region EMAIL-SENDING
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.BCABBAExamApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();

                                #region SEND ACKNOWLEDGEMENT EMAIL AFTER PAYMENT IS DONE
                                //// Send email to student
                                SendAcknowledgementEmail(user.EmailAddress, examFillUpDetails.StudentName, serviceName, examFillUpDetails.AcknowledgementNo);

                                #region SEND SMS FUNCTIONALITY

                                EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AcknowledgementSMS && s.IsActive == true).FirstOrDefault();
                                if (acknowledgementSMS != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = acknowledgementSMS.SMSContentId;
                                    string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                    OTPMessage otpObj = new OTPMessage();
                                    StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                                    body.Replace("--studentName--", examFillUpDetails.StudentName);
                                    body.Replace("--serviceName--", serviceName);
                                    body.Replace("--acknowledgementNo--", examFillUpDetails.AcknowledgementNo);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = user.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                                #endregion

                                #endregion
                                // Send email to principal/hod in case of application submit, because the workflow is built like wise
                                // Fetch principal/hod email id of the applying student registration form
                                Users correspondingPrincipalOrHOD = null;

                                if (_MSCommon.GetEnumDescription((enCourseType)enCourseType.Ug) == examFillUpDetails.CourseTypeCode)
                                {// Ug course type | FInd principal
                                    correspondingPrincipalOrHOD = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.IsActive && usr.CollegeCode == examFillUpDetails.CollegeCode).FirstOrDefault();
                                }
                                else
                                {// Pg course type | Find HOD
                                    correspondingPrincipalOrHOD = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.IsActive && usr.CollegeCode == examFillUpDetails.CollegeCode && examFillUpDetails.DepartmentCode == usr.DepartmentCode).FirstOrDefault();
                                }

                                if (correspondingPrincipalOrHOD != null)
                                {
                                    SendEmailToApprovingAuthorityOnStateChange(correspondingPrincipalOrHOD.EmailAddress, correspondingPrincipalOrHOD.Name, serviceName);
                                }
                                #endregion
                                Dictionary<string, object> triggerParameterDictionary = null;
                                _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, newGuid, DateTime.UtcNow, initialState, initialTrigger, examFillUpDetails.State, examFillUpDetails.TenantWorkflowId);
                                param = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(examFillUpDetails);
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU013" };
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU052" };
                }

                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU115" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU117" };
            }
            return param;
        }

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Anurag Digal
        /// Date: 12-11-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidateInput(ExaminationFormFillUpDetailsDto input)
        {
            string regularExpressionForName = @"^(?!\.)(?!.*\.$)(?!.*?\.\.)[a-zA-Z0-9 .]+$";//This regex is used to allow only alphabets, dot and spaces in string
            //string regExDOB = @"^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}+$";    //current it is not used
            string registrationExpression = @"^[0-9]{16}$";

            if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
            {
                input.DepartmentCode = "";
            }
            if (input.SemesterCode != _MSCommon.GetEnumDescription(EnSemester.FirstSemester))
            {
                if (input.RegistrationNumber == null || input.RegistrationNumber.Trim() == String.Empty)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU064" };
                }
                if (input.RegistrationNumber != null && !Regex.IsMatch(input.RegistrationNumber, registrationExpression))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU091" };
                }
            }
            else if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.FatherName == null || input.FatherName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU039" };
            }
            else if (!Regex.IsMatch(input.FatherName, regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU040" };
            }
            else if (input.GuardianName == null || input.GuardianName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU076" };
            }
            else if (!Regex.IsMatch(input.GuardianName, regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU077" };
            }
            else if (input.Nationality == null || string.IsNullOrEmpty(input.Nationality) || string.IsNullOrWhiteSpace(input.Nationality))
            {
                throw new MicroServiceException() { ErrorCode = "OUU078" };
            }
            else if (input.Caste == null || string.IsNullOrEmpty(input.Caste) || string.IsNullOrWhiteSpace(input.Caste))
            {
                throw new MicroServiceException() { ErrorCode = "OUU079" };
            }
            else if (input.Gender == null || string.IsNullOrEmpty(input.Gender) || string.IsNullOrWhiteSpace(input.Gender))
            {
                throw new MicroServiceException() { ErrorCode = "OUU080" };
            }
            else if (input.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            else if (input.YearOfMatriculation == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU081" };
            }
            else if (input.YearOfPreviousExamPassed == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU082" };
            }
            else if ((input.YearOfPreviousExamPassed < input.YearOfMatriculation) || (input.YearOfMatriculation == input.YearOfPreviousExamPassed))
            {
                throw new MicroServiceException() { ErrorCode = "OUU092" };
            }
            else if (input.PreviousAcademicSubject == null || string.IsNullOrEmpty(input.PreviousAcademicSubject) || string.IsNullOrWhiteSpace(input.PreviousAcademicSubject))
            {
                throw new MicroServiceException() { ErrorCode = "OUU083" };
            }
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseTypeCode == null || input.CourseTypeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.AcademicSession == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if ((input.CourseTypeCode == "02") && (input.DepartmentCode == null || input.DepartmentCode == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU056" };
            }
            else if (input.CourseTypeCode == "02" && (!string.IsNullOrEmpty(input.DepartmentCode) || !string.IsNullOrWhiteSpace(input.DepartmentCode)) && (input.CourseCode == null || input.CourseCode.Trim() == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU059" };
            }
            else if (input.SemesterCode == null || string.IsNullOrEmpty(input.SemesterCode) || string.IsNullOrWhiteSpace(input.SemesterCode))
            {
                throw new MicroServiceException() { ErrorCode = "OUU084" };
            }
            else if (input.AcademicSession == null || string.IsNullOrEmpty(input.AcademicSession) || string.IsNullOrWhiteSpace(input.AcademicSession))
            {
                throw new MicroServiceException() { ErrorCode = "OUU085" };
            }
            else if (input.AcademicStart == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU086" };
            }
            else if (input.ApplicationType == null || string.IsNullOrEmpty(input.ApplicationType) || string.IsNullOrWhiteSpace(input.ApplicationType))
            {
                throw new MicroServiceException() { ErrorCode = "OUU087" };
            }
            else if (input.ServiceStudentAddresses.Count() == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU088" };
            }
            else if (input.ExaminationPaperDetails.Count() == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU089" };
            }
            input.StudentName = input.StudentName.Trim();
            input.FatherName = input.FatherName.Trim();
            input.CollegeCode = input.CollegeCode.Trim();
            input.CourseTypeCode = input.CourseTypeCode.Trim();
            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();
            return true;
        }
        #endregion

        #region IMAGE VALIDATION
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 22-oct-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ImageValidation(Documents input)
        {
            if (input.FileExtension == "" || input.FileExtension == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU047" };
            }
            if (input.FileExtension != "" || input.FileExtension != null)
            {
                var regEx = new Regex(@"(jpg|jpeg)$");
                if (!regEx.IsMatch(input.FileExtension.ToLower()))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU048" };
                }
            }
            if (input.FileContent.Length >= Convert.ToInt32(_IConfiguration.GetSection("RegistrationNumber:UploadImageSize").Value))
            {
                throw new MicroServiceException() { ErrorCode = "OUU049" };
            }
            if (string.IsNullOrEmpty(input.DocumentType) || string.IsNullOrWhiteSpace(input.DocumentType))
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            if (input.SourceType == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            return true;
        }
        #endregion

        #region PRIVATE COMMON DOCUMENT WRITE METHOD
        /// <summary>
        /// Modified By -   Anurag Digal
        /// Date        -   14-12-2019
        /// Description -   Linux machine deployment path added
        /// </summary>
        /// <param name="input"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private List<Documents> ImageWrite(ExaminationFormFillUpDetailsDto input, Guid id)
        {
            string[] name = id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3];
            List<Documents> documentList = new List<Documents>();
            input.ImageDocument.ForEach((file) =>
            {
                string path = string.Empty;
                file.ParentSourceId = id;
                if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
                {
                    path = _environment.ContentRootPath;
                }
                else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
                {
                    path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
                }

                string fileName = finalName + "." + file.FileExtension;
                string DirectoryPath = _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpCertificatePath").Value;
                DirectoryPath = DirectoryPath.Replace('\\', '/');
                var Path = path + DirectoryPath + fileName;
                if (!Directory.Exists(path + DirectoryPath))
                {
                    Directory.CreateDirectory(path + DirectoryPath);
                }

                if (File.Exists(Path))
                {
                    File.Delete(Path);
                }
                byte[] bytes = (byte[])file.FileContent;
                File.WriteAllBytes(Path, bytes);
                file.FilePath = DirectoryPath + fileName;
                file.FilePath = file.FilePath.Replace('\\', '/');
                documentList.Add(file);
            });
            return documentList;
        }
        #endregion

        /// <summary>
        ///Author   :   Anurag Digal
        ///Date     :   12-11-2019
        ///This method is used to get the examformfillupdetails by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExaminationFormFillUpDetailsDto GetExamFormFillUpDetails(Guid id)
        {
            ExaminationFormFillUpDetailsDto formFillUpData = new ExaminationFormFillUpDetailsDto();

            if (id != Guid.Empty)
            {
                //get all the exam formfillupdata from repository
                ExaminationFormFillUpDetails formRecord = _MSUoW.ExaminationFormFillUpRepository.GetAllExamFormFillUpData().Where(x => x.Id == id && x.IsActive).FirstOrDefault();
                if (formRecord != null && formRecord.ImageDocument != null && formRecord.ImageDocument.Count > 0)
                {
                    formRecord.ImageDocument[0].FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, formRecord.ImageDocument[0].FilePath);
                }
                if (formRecord != null)
                {
                    formFillUpData = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(formRecord);
                }
            }
            return formFillUpData;
        }

        /// <summary>
        ///Author   :   Bikash ku sethi
        ///Date     :   08-11-2021
        ///This method is used to get the examformfillupdetails by rollNo
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExaminationFormFillUpDetailsDto GetExamFormFillUpDetailsByRollNo(string rollNo)
        {
            ExaminationFormFillUpDetailsDto formFillUpData = new ExaminationFormFillUpDetailsDto();

            if (rollNo != null)
            {
                //get all the exam formfillupdata from repository
                ExaminationFormFillUpDetails formRecord = _MSUoW.ExaminationFormFillUpRepository.GetAllExamFormFillUpData().Where(x => x.RollNumber == rollNo &&
                x.CreatorUserId == GetCurrentUserId() && x.IsActive).FirstOrDefault();
                if (formRecord != null && formRecord.ImageDocument != null && formRecord.ImageDocument.Count > 0)
                {
                    formRecord.ImageDocument[0].FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, formRecord.ImageDocument[0].FilePath);
                    formFillUpData = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(formRecord);
                }
                else
                {
                    throw new MicroServiceException() { ErrorMessage = "Provided roll no is not valid." };
                }
            }
            return formFillUpData;
        }
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 16-Nov-2019
        /// Method is used for status as per usertype
        /// </summary>
        /// <returns></returns>
        public List<string> GetExamFormFillUpState()
        {
            long currentUserType = GetUserType();
            List<string> status = new List<string>();
            string endState = null;
            if (currentUserType != (long)enUserType.Student && currentUserType != (long)enUserType.Level1Support && currentUserType != (long)enUserType.CompCell)
            {
                endState = _IConfiguration.GetSection("ExamFormFillup:EndState").Value;
            }
            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                case (long)enUserType.HOD:
                    status = GetRoleSpecificWorkflowStateList();
                    break;
                case (long)enUserType.AsstCoE:
                    status = GetRoleSpecificWorkflowStateList();
                    break;
                case (long)enUserType.COE:
                    status = GetRoleSpecificWorkflowStateList();
                    break;
                case (long)enUserType.Level1Support: //returning hard codded value as this role is not included in workflow
                    status.AddRange(_IConfiguration.GetSection("ExamFormFillup:FormStates").Value.Split(",").ToList());
                    //status.Add(Constant.PaymentDone);
                    //status.Add(Constant.RollNumberGenerationPending);
                    //status.Add(Constant.AdmitCardGenerationPending);
                    //status.Add(Constant.AdmitCardGenerated);
                    break;
                case (long)enUserType.CompCell:
                    status.Add(_IConfiguration.GetSection("ExamFormFillup:MarkUploaded").Value);
                    break;
            }
            if (currentUserType != (long)enUserType.Student && currentUserType != (long)enUserType.Level1Support && currentUserType != (long)enUserType.CompCell)
            {
                status.Add(endState);
            }
            //default adding the endstate to the list as endstate is display to all roles as defined
            return status;
        }

        public List<string> GetRoleSpecificWorkflowStateList()
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            long maxTenantWorkflowId = tenantWFTrans.Max(p => p.TenantWorkflowId);

            List<TenantWorkflowTransitionConfig> workflowTransition = new List<TenantWorkflowTransitionConfig>();
            if (roleNames.Contains(((int)enRoles.COE).ToString()))
            {
                List<string> RoleIds = new List<string>();
                //RoleIds.Add(Convert.ToString((long)enRoles.Student));
                //RoleIds.Add(Convert.ToString((long)enRoles.Principal));
                //RoleIds.Add(Convert.ToString((long)enRoles.HOD));

                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).Any(x => !RoleIds.Contains(x))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();
            }
            else
            {
                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList().Contains(role))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();
            }

            string wfInitState = _IConfiguration.GetSection("WorkflowInitialState").Value;
            List<string> listOfStates = workflowTransition.Where(s => s.SourceState != wfInitState).Select(p => p.SourceState).Distinct().ToList();
            if (roleNames.Contains(((int)enRoles.Principal).ToString()) || roleNames.Contains(((int)enRoles.HOD).ToString()))
            {
                listOfStates.Add("Payment Pending");
            }
            if (roleNames.Contains(((int)enRoles.AsstCoE).ToString()))
            {
                listOfStates.Add(_IConfiguration.GetSection("ExamFormFillup:AdmitcardGenerated").Value);
            }
            return listOfStates;

        }

        /// <summary>
        ///Author   :   Anurag Digal
        ///Date     :   14-11-2019
        ///This method is used to get college wise formfillupamount details
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<CollegeWiseAmountForCoeDto> GetCollegeWiseFormFillUpAmount(SearchParamDto param)
        {
            List<CollegeWiseAmountForCoeDto> resultset = new List<CollegeWiseAmountForCoeDto>();
            string instanceTrigger = string.Empty;
            //get the college wise amount details from the examinantion view
            if (param.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
            {
                switch (param.State)
                {
                    case Constant.RollNumberGenerationPending:

                        instanceTrigger = _IConfiguration.GetSection("ExamFormFillup:ForwardToGenerateRollNo").Value;
                        resultset = _MSUoW.ExaminationFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.AdmitCardGenerationPending:
                        instanceTrigger = _IConfiguration.GetSection("ExamFormFillup:ForwardToGenerateAdmitCard").Value;
                        resultset = _MSUoW.ExaminationFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.AdmitCardGenerated:
                        instanceTrigger = _IConfiguration.GetSection("ExamFormFillup:AdmitcardGenerated").Value;
                        resultset = _MSUoW.ExaminationFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.MarkUploaded:
                        instanceTrigger = _IConfiguration.GetSection("ExamFormFillup:MarkUploaded").Value;
                        resultset = _MSUoW.ExaminationFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.ResultPublication:
                        instanceTrigger = _IConfiguration.GetSection("ExamFormFillup:EndState").Value;
                        resultset = _MSUoW.ExaminationFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;

                    default:
                        break;
                }
            }
            else
            {
                //this condition is for pg case
                //return _MSUoW.ExaminationView.FindBy(x => x.CourseTypeCode == param.courseTypeCode && x.CourseCode == param.courseCode && x.)
            }
            return resultset;
        }

        #region ASYNC CALL TO SAVE DATA FROM EXAMFORMFILLUPDATAILS TABLE TO BACKGROUNDPROCESS TABLE IN STATE PRINCIPAL TO COE

        public bool AddExamFormDetailsToBackgroundTable(BackgroundProcessDto parameters)
        {
            if (parameters != null)
            {
                ExaminationMaster masterData = _MSUoW.ExaminationMaster.FindBy(a => a.CourseType == parameters.CourseTypeCode
                                            && a.SemesterCode == parameters.SemesterCode
                                            && a.StreamCode == parameters.StreamCode
                                            && a.SubjectCode == parameters.SubjectCode
                                            && a.TenantId == 3
                                            && a.IsActive == true).FirstOrDefault();
                if (masterData != null)
                {
                    ExaminationSession sessionData = _MSUoW.ExaminationSession.FindBy(sess => sess.ExaminationMasterId == masterData.Id && sess.IsActive && sess.StartYear == parameters.StartYear).FirstOrDefault();
                    if (sessionData != null)
                    {
                        ExaminationFormFillUpDates finalDate = _MSUoW.ExaminationFormFillUpDates.FindBy(dates => dates.ExaminationSessionId == sessionData.Id && dates.IsActive).OrderByDescending(a => a.SlotOrder).FirstOrDefault();
                        //var currTime = DateTime.UtcNow;
                        DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));
                        if (finalDate.EndDate < currTime.Date)
                        {
                            var currentUserId = GetCurrentUserId();
                            long userRole = _MSUoW.Users.FindBy(u => u.IsActive && u.Id == currentUserId).Select(s => s.UserType).FirstOrDefault();
                            if (userRole == (long)enUserType.Principal)
                            {
                                //var currTime = DateTime.UtcNow;
                                //Process Tracker Table Entry
                                ProcessTracker processTableData = new ProcessTracker();
                                processTableData.Id = Guid.NewGuid();
                                Guid processTableId = processTableData.Id;
                                processTableData.ServiceId = (long)enServiceType.BCABBAExamApplicationForm;
                                processTableData.ProcessType = (Int32)enProcessName.SendToCOE;
                                processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
                                processTableData.IsActive = true;
                                processTableData.CreatorUserId = currentUserId;
                                processTableData.CreationTime = DateTime.UtcNow;
                                processTableData.TenantId = Convert.ToInt64(3);
                                _MSUoW.ProcessTracker.Add(processTableData);
                                _MSUoW.Commit();
                                AddExamFormDetailsDataToBackgrounTable(parameters, currentUserId, processTableId);
                            }
                            else throw new MicroServiceException() { ErrorCode = "OUU093" };
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU095" };// Do finalDate error
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU114" };// Do sessionData error
                }
                else throw new MicroServiceException() { ErrorCode = "OUU114" }; // Do masterData error
            }
            else throw new MicroServiceException() { ErrorCode = "OUU075" };

            return true;
        }

        public async Task<bool> AddExamFormDetailsDataToBackgrounTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            await Task.Run(() =>
            {
                AddExamFormDetailsToBackgroundTable(parameters, userId, processTableId);
                return true;
            });
            return false;
        }

        /// <summary>
        /// Author: Lopamudra Senapati
        /// Date: 15-11-2019
        /// Method is used for Odata Controller which will give examination student record as per usertype
        /// </summary>
        /// <returns></returns>
        public bool AddExamFormDetailsToBackgroundTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            using (var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger))
            {
                List<BackgroundProcess> data = new List<BackgroundProcess>();
                try
                {

                    if (parameters.SelectedInstanceIds != null && parameters.IsAllSelected == false)
                    {
                        var ids = parameters.SelectedInstanceIds.Split(",").ToList();
                        data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && ids.Contains(d.Id.ToString()) && d.State == parameters.State)
                                                      .Select(s => new BackgroundProcess
                                                      {
                                                          Id = Guid.NewGuid(),
                                                          ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                                                          InstanceTrigger = _IConfiguration.GetSection("ExamFormFillup:PrincipalToCOE").Value,
                                                          InstanceId = s.Id,
                                                          Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),//will be false after sent to COE
                                                          TenantWorkflowId = s.TenantWorkflowId,
                                                          CreatorUserId = userId,
                                                          CreationTime = DateTime.UtcNow,
                                                          IsActive = true,
                                                          TenantId = Convert.ToInt64(3),
                                                          CollegeCode = parameters.CollegeCode[0], // as principal send only one college record
                                                          CourseCode = parameters.CourseCode,
                                                          StreamCode = parameters.StreamCode,
                                                          CourseTypeCode = parameters.CourseTypeCode,
                                                          DepartmentCode = parameters.DepartmentCode,
                                                          SubjectCode = parameters.SubjectCode,
                                                          SemesterCode = parameters.SemesterCode,
                                                          AcademicStart = parameters.StartYear
                                                      }).ToList();
                    }
                    else if (parameters.SelectedInstanceIds == null && parameters.IsAllSelected)
                    {
                        if (parameters.DepartmentCode == null)
                        {
                            data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && d.State == parameters.State && d.CourseTypeCode == parameters.CourseTypeCode
                                                                   && parameters.CollegeCode.Contains(d.CollegeCode) && d.StreamCode == parameters.StreamCode && d.SemesterCode == parameters.SemesterCode
                                                                   && d.SubjectCode == parameters.SubjectCode && d.AcademicStart == parameters.StartYear)
                                                  .Select(s => new BackgroundProcess
                                                  {
                                                      Id = Guid.NewGuid(),
                                                      ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                                                      InstanceTrigger = _IConfiguration.GetSection("ExamFormFillup:PrincipalToCOE").Value,
                                                      InstanceId = s.Id,
                                                      Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                                      TenantWorkflowId = s.TenantWorkflowId,
                                                      CreatorUserId = userId,
                                                      CreationTime = DateTime.UtcNow,
                                                      IsActive = true,
                                                      TenantId = Convert.ToInt64(3),
                                                      CollegeCode = parameters.CollegeCode[0],
                                                      CourseCode = parameters.CourseCode,
                                                      StreamCode = parameters.StreamCode,
                                                      CourseTypeCode = parameters.CourseTypeCode,
                                                      DepartmentCode = parameters.DepartmentCode,
                                                      SubjectCode = parameters.SubjectCode,
                                                      SemesterCode = parameters.SemesterCode,
                                                      AcademicStart = parameters.StartYear
                                                  }).ToList();
                        }
                        else
                        {
                            data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && d.State == parameters.State && d.CourseTypeCode == parameters.CourseTypeCode
                                                                   && parameters.CollegeCode.Contains(d.CollegeCode) && d.StreamCode == parameters.StreamCode && d.SemesterCode == parameters.SemesterCode
                                                                   && d.SubjectCode == parameters.SubjectCode && d.DepartmentCode == parameters.DepartmentCode && d.CourseCode == parameters.CourseCode && d.AcademicStart == parameters.StartYear)
                                                  .Select(s => new BackgroundProcess
                                                  {
                                                      Id = Guid.NewGuid(),
                                                      ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                                                      InstanceTrigger = _IConfiguration.GetSection("ExamFormFillup:PrincipalToCOE").Value,
                                                      InstanceId = s.Id,
                                                      Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                                      TenantWorkflowId = s.TenantWorkflowId,
                                                      CreatorUserId = userId,
                                                      CreationTime = DateTime.UtcNow,
                                                      IsActive = true,
                                                      TenantId = Convert.ToInt64(3),
                                                      CollegeCode = parameters.CollegeCode[0],
                                                      CourseCode = parameters.CourseCode,
                                                      StreamCode = parameters.StreamCode,
                                                      CourseTypeCode = parameters.CourseTypeCode,
                                                      DepartmentCode = parameters.DepartmentCode,
                                                      SubjectCode = parameters.SubjectCode,
                                                      SemesterCode = parameters.SemesterCode,
                                                      AcademicStart = parameters.StartYear
                                                  }).ToList();
                        }
                    }
                    _MSUoW.BackgroundProcess.AddRange(data);

                    ProcessTracker processTracker = new ProcessTracker();
                    var currTime = DateTime.UtcNow;
                    processTracker = _MSUoW.ProcessTracker.GetAll().Where(w => w.Id == processTableId && w.CreatorUserId == userId).FirstOrDefault();
                    processTracker.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.ReadyForProcessing);
                    processTracker.LastModifierUserId = processTracker.CreatorUserId;
                    processTracker.LastModificationTime = currTime;
                    _MSUoW.ProcessTracker.Update(processTracker);
                    _MSUoW.Commit();
                }
                catch (Exception e)
                {
                }
                return true;
            }
            // string formStatus = _IConfiguration.GetSection("ExamFormFillup:FormStates").Value;

        }

        #endregion

        #region ASYNC CALL TO MOVE RECORD FROM EXAMFORMFILLUPDETAILS TABLES TO BACKGROUNDPROCESS TABLE IN STATE ROLLNOGENERATION/ADMITCARD GENERATION PENDING
        /// <summary>
        /// Author  :   Anurag Diga
        /// Date    :   20-11-19
        /// This methode is async call which will add a record in process tracker and move matching record from examformfillupdetails table to backgrounprocess table
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        //public bool StateWiseMoveToBackground(BackgroundProcessDto param)
        //{
        //    if (param != null)
        //    {
        //        var currTime = DateTime.UtcNow;
        //        var currentUserId = GetCurrentUserId();
        //        //Process Tracker Table Entry
        //        ProcessTracker processTableData = new ProcessTracker();
        //        Guid id = Guid.NewGuid();
        //        processTableData.Id = id;
        //        Guid processTableId = id;
        //        string stateName = string.Empty;

        //        //get the statelist from the appsetting json file
        //        string[] statelist = _IConfiguration.GetSection("ExamFormFillup:COEStates").Value.Split(',');
        //        if (param.State.ToString().Trim().ToLower() == statelist[0].Trim().ToLower().ToString())
        //        {
        //            processTableData.ProcessType = (Int32)enProcessName.RoleNumberGeneration;
        //        }
        //        else if (param.State.ToString().Trim().ToLower() == statelist[1].Trim().ToLower().ToString())
        //        {
        //            processTableData.ProcessType = (Int32)enProcessName.AdmitCardGeneration;
        //        }

        //        //end of get the statelist
        //        processTableData.ServiceId = (long)enServiceType.BCABBAExamApplicationForm;
        //        processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
        //        processTableData.IsActive = true;
        //        processTableData.CreatorUserId = currentUserId;
        //        processTableData.CreationTime = currTime;
        //        processTableData.TenantId = Convert.ToInt64(3);
        //        _MSUoW.ProcessTracker.Add(processTableData);
        //        _MSUoW.Commit();

        //        BackGroundTable(param, currentUserId, processTableId);
        //    }
        //    else throw new MicroServiceException() { ErrorCode = "OUU075" };

        //    return true;
        //}

        //public async Task<bool> BackGroundTable(BackgroundProcessDto param, long userId, Guid processTableId)
        //{
        //    await Task.Run(() =>
        //    {
        //        SaveToBackGroundTable(param, userId, processTableId);
        //        return true;
        //    });
        //    return false;
        //}

        public bool SaveToBackGroundTable(BackgroundProcessDto param)
        {
            bool status = false;
            //var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger);
            List<BackgroundProcess> backgroundProcessesList = new List<BackgroundProcess>();
            string stateUpdateName = string.Empty;
            try
            {
                if (param != null)
                {
                    //Process Tracker Table Entry
                    ProcessTracker processTableData = new ProcessTracker();
                    Guid newGuid = Guid.NewGuid();
                    processTableData.Id = newGuid;
                    string stateName = string.Empty;

                    //get the statelist from the appsetting json file
                    string[] statelist = _IConfiguration.GetSection("ExamFormFillup:COEStates").Value.Split(',');
                    if (param.State.ToString().Trim().ToLower() == statelist[0].Trim().ToLower().ToString())
                    {
                        processTableData.ProcessType = (Int32)enProcessName.RoleNumberGeneration;
                    }
                    else if (param.State.ToString().Trim().ToLower() == statelist[1].Trim().ToLower().ToString())
                    {
                        processTableData.ProcessType = (Int32)enProcessName.AdmitCardGeneration;
                    }

                    //end of get the statelist
                    processTableData.ServiceId = (long)enServiceType.BCABBAExamApplicationForm;
                    processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
                    processTableData.IsActive = true;
                    processTableData.CreatorUserId = GetCurrentUserId();
                    processTableData.CreationTime = DateTime.UtcNow;
                    processTableData.TenantId = Convert.ToInt64(3);
                    _MSUoW.ProcessTracker.Add(processTableData);
                    _MSUoW.Commit();

                    //GET THE STATE NAME TO MATCH WHICH STATE IT BELONGS TO 
                    stateUpdateName = param.State.Trim().Replace(" ", string.Empty).ToLower() == Constant.AdmitCardGenerationPending.Trim().Replace(" ", string.Empty).ToLower() ? _IConfiguration.GetSection("ExamFormFillup:ForwardToGenerateAdmitCard").Value : _IConfiguration.GetSection("ExamFormFillup:ForwardToGenerateRollNo").Value;

                    if (!param.IsAllSelected)
                    {
                        if (param != null && param.CollegeWiseAmount.Count > 0)
                        {
                            param.CollegeWiseAmount.ForEach(rec =>
                            {
                                BackgroundProcess backgroundObj = new BackgroundProcess();
                                backgroundObj.Id = Guid.NewGuid();
                                backgroundObj.ServiceType = (long)enServiceType.BCABBAExamApplicationForm;
                                backgroundObj.InstanceTrigger = stateUpdateName;
                                backgroundObj.Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending); //will be false after sent to COE
                                backgroundObj.TenantWorkflowId = 0;    //need to clarify for tenantworkflow id
                                backgroundObj.CreatorUserId = GetCurrentUserId();
                                backgroundObj.CreationTime = DateTime.UtcNow;
                                backgroundObj.IsActive = true;
                                backgroundObj.TenantId = Convert.ToInt64(3);
                                backgroundObj.CollegeCode = rec.CollegeCode; // as principal send only one college record
                                backgroundObj.CourseCode = param.CourseCode;
                                backgroundObj.StreamCode = param.StreamCode;
                                backgroundObj.CourseTypeCode = param.CourseTypeCode;
                                backgroundObj.DepartmentCode = param.DepartmentCode;
                                backgroundObj.SubjectCode = param.SubjectCode;
                                backgroundObj.SemesterCode = param.SemesterCode;
                                backgroundObj.AcademicStart = param.StartYear;
                                backgroundProcessesList.Add(backgroundObj);
                            });

                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU075" }; //if 0 record found then break
                    }
                    else
                    {
                        if (param != null && param.CollegeCode.Count > 0)
                        {
                            SearchParamDto parameter = new SearchParamDto();
                            parameter.CollegeCode = param.CollegeCode;
                            parameter.CourseTypeCode = param.CourseTypeCode;
                            parameter.CourseCode = param.CourseCode;
                            parameter.StreamCode = param.StreamCode;
                            parameter.StartYear = param.StartYear;
                            parameter.DepartmentCode = param.DepartmentCode;
                            parameter.SubjectCode = param.SubjectCode;
                            parameter.SemesterCode = param.SemesterCode;
                            parameter.State = param.State;

                            List<CollegeWiseAmountForCoeDto> collegeWiseData = GetCollegeWiseFormFillUpAmount(parameter);
                            if (collegeWiseData != null)
                            {
                                collegeWiseData.ForEach(clg =>
                                {
                                    BackgroundProcess backgroundObj = new BackgroundProcess();
                                    backgroundObj.Id = Guid.NewGuid();
                                    backgroundObj.ServiceType = (long)enServiceType.BCABBAExamApplicationForm;
                                    backgroundObj.InstanceTrigger = stateUpdateName;
                                    backgroundObj.Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending); //will be false background updated
                                    backgroundObj.TenantWorkflowId = 0;    //need to clarify for tenantworkflow id
                                    backgroundObj.CreatorUserId = GetCurrentUserId();
                                    backgroundObj.CreationTime = DateTime.UtcNow;
                                    backgroundObj.IsActive = true;
                                    backgroundObj.TenantId = Convert.ToInt64(3);
                                    backgroundObj.CollegeCode = clg.CollegeCode;
                                    backgroundObj.CourseCode = param.CourseCode;
                                    backgroundObj.StreamCode = param.StreamCode;
                                    backgroundObj.CourseTypeCode = param.CourseTypeCode;
                                    backgroundObj.DepartmentCode = param.DepartmentCode;
                                    backgroundObj.SubjectCode = param.SubjectCode;
                                    backgroundObj.SemesterCode = param.SemesterCode;
                                    backgroundObj.AcademicStart = param.StartYear;
                                    backgroundProcessesList.Add(backgroundObj);
                                });
                            }
                            else throw new MicroServiceException() { ErrorCode = "OUU075" }; //if 0 record found then break
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU075" }; //if 0 record found then break
                    }
                    //if (param.CollegeWiseAmount.Count > 0)
                    //{
                    //    param.CollegeWiseAmount.ForEach(rec =>
                    //    {
                    //        data = _MSUoW.ExaminationFormFillUpDetails.FindBy(x => x.CollegeCode == rec.CollegeCode && x.StreamCode == param.StreamCode && x.SubjectCode == param.SubjectCode
                    //                                                            && x.State.Trim().ToLower() == param.State.Trim().ToLower()
                    //                                                            && x.SemesterCode == param.SemesterCode && x.AcademicStart == param.StartYear && x.IsActive)
                    //        .Select(p => new BackgroundProcess
                    //        {
                    //            Id = Guid.NewGuid(),
                    //            ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                    //            InstanceTrigger = stateUpdateName,
                    //            InstanceId = p.Id,
                    //            Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                    //            TenantWorkflowId = (long)enServiceType.BCABBAExamApplicationForm,
                    //            CreatorUserId = userId,
                    //            CreationTime = DateTime.UtcNow,
                    //            IsActive = true,
                    //            TenantId = Convert.ToInt64(3)
                    //        })
                    //        .ToList();
                    //    });
                    //}

                    //}
                    //else
                    //{
                    //    ////first get the filtered record from the parameter
                    //    //data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(x => x.CourseTypeCode.Trim() == param.CourseTypeCode.Trim()
                    //    //    && param.CollegeCode.Contains(x.CollegeCode) && x.StreamCode == param.StreamCode && x.SubjectCode == param.SubjectCode && x.AcademicStart == param.StartYear
                    //    //    && x.SemesterCode == param.SemesterCode && x.State.Trim().ToLower() == param.State.Trim().ToLower())
                    //    //    .Select(p => new BackgroundProcess
                    //    //    {
                    //    //        Id = Guid.NewGuid(),
                    //    //        ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                    //    //        InstanceTrigger = stateUpdateName,
                    //    //        InstanceId = p.Id,
                    //    //        Status = "1",
                    //    //        TenantWorkflowId = (long)enServiceType.BCABBAExamApplicationForm,
                    //    //        CreatorUserId = userId,
                    //    //        CreationTime = DateTime.UtcNow,
                    //    //        IsActive = true,
                    //    //        TenantId = Convert.ToInt64(3)
                    //    //    }).ToList();
                    //    ////if no record found then break
                    //    //if (data == null)
                    //    //{
                    //    //    throw new MicroServiceException() { ErrorCode = "" };
                    //    //}

                    //}

                    _MSUoW.BackgroundProcess.AddRange(backgroundProcessesList);


                    ProcessTracker processTracker = new ProcessTracker();
                    processTracker = _MSUoW.ProcessTracker.GetAll().Where(w => w.Id == newGuid).FirstOrDefault();
                    processTracker.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.ReadyForProcessing);
                    processTracker.LastModifierUserId = processTracker.CreatorUserId;
                    processTracker.LastModificationTime = DateTime.UtcNow;
                    _MSUoW.ProcessTracker.Update(processTracker);
                    _MSUoW.Commit();
                    status = true;
                }
                else throw new MicroServiceException() { ErrorCode = "OUU075" };
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
            return status;
        }
        #endregion

        /// <summary>
        /// Author                  : Dattatreya Dash
        /// Date                    : 21-11-2019
        /// Description             : Send acknowledgement email
        /// </summary>
        private bool SendAcknowledgementEmail(string studentEmailAddress, string studentName, string serviceName, string acknowledgementNo)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AcknowledgementNo && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                body.Replace("--studentName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--acknowledgementNo--", acknowledgementNo);
                body.Replace("--url--", applicationUrl);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }

        /// <summary>
        /// Sends email to next approving authority
        /// </summary>
        /// <param name="emailAddress">Email id of approving authority</param>
        /// <param name="userName">Username of approving authority</param>
        /// <param name="serviceName">Service name for which approval process is pending</param>
        /// <returns></returns>
        private bool SendEmailToApprovingAuthorityOnStateChange(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ServiceRequestReceived && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// <summary>
        /// Sends email to next approving authority
        /// </summary>
        /// <param name="emailAddress">Email id of approving authority</param>
        /// <param name="userName">Username of approving authority</param>
        /// <param name="serviceName">Service name for which approval process is pending</param>
        /// <returns></returns>
        private bool SendEmailToStudentToPay(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupPaymentPendingEmail && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        private bool SendSMSToStudentToPay(string phoneNumber, string studentName)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupPaymentPendingSMS && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string contentId = string.Empty;
                contentId = templateData.SMSContentId;
                try
                {
                    string apiUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--studentName--", studentName);
                    body.Replace("--url--", applicationUrl);

                    OTPMessage otpObj = new OTPMessage();
                    otpObj.Message = body.ToString();
                    otpObj.MobileNumber = phoneNumber;
                    _MSCommon.SendSMS(apiUrl, otpObj, contentId);
                }
                catch (Exception e)
                {
                    _logger.Error(e.Message);
                }
            }
            return true;
        }

        #region Academic Start Year (To Do After Go Live)
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 18-Dec-2019
        /// </summary>
        /// <returns></returns>
        public List<int> GetAcademicStartYear()
        {
            List<int> academicYear = _MSUoW.ExaminationSession.FindBy(sess => sess.IsActive).OrderBy(a => a.StartYear).Select(a => a.StartYear).Distinct().ToList();
            if (academicYear.Count > 0)
            {
                return academicYear;
            }
            else
            {
                academicYear.Add(DateTime.UtcNow.Year);
                return academicYear;
            }
        }
        #endregion

        #region WORKFLOW
        public ExaminationFormFillUpDetailsDto UpdateState(Guid id, ExaminationFormFillUpDetailsDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.ExaminationFormFillUpDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<ExaminationFormFillUpDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.ExaminationFormFillUpDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users nextApprovingUser;
                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();


                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                    string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.BCABBAExamApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    string examYear = DateTime.UtcNow.Year.ToString();

                    var examinationMaster = _MSUoW.ExaminationMaster.FindBy(examMstr =>
                        examMstr.CourseType == examinationFormFillUpDetails.CourseTypeCode
                        && examMstr.StreamCode == examinationFormFillUpDetails.StreamCode
                        && examMstr.SubjectCode == examinationFormFillUpDetails.SubjectCode
                        && examMstr.CourseCode == examinationFormFillUpDetails.CourseCode
                        && examMstr.DepartmentCode == examinationFormFillUpDetails.DepartmentCode
                        && examMstr.SemesterCode == examinationFormFillUpDetails.SemesterCode
                    ).FirstOrDefault();

                    if (examinationMaster != null)
                    {
                        var examinationSession = _MSUoW.ExaminationSession.FindBy(examSsn => examSsn.ExaminationMasterId == examinationMaster.Id && examSsn.StartYear == examinationFormFillUpDetails.AcademicStart).FirstOrDefault();
                        if (examinationSession != null)
                        {
                            examYear = examinationSession.ExamYear.ToString();
                        }
                    }

                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == examinationFormFillUpDetails.CollegeCode).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion

                    #region Step k
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null)
                        {
                            SendEmailAndSMSOnAdmitCardGenerated(applyingStudent.EmailAddress, applyingStudent.PhoneNumber, applyingStudent.UserName, serviceName, examYear);
                            //SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, examinationFormFillUpDetails.RegistrationNumber);
                        }

                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        public ExaminationFormFillUpDetailsDto RejectedByPrincipal(Guid id, ExaminationFormFillUpDetailsDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.ExaminationFormFillUpDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<ExaminationFormFillUpDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    //input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.ExaminationFormFillUpDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users nextApprovingUser;
                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();


                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                    string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.BCABBAExamApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    string examYear = DateTime.UtcNow.Year.ToString();

                    var examinationMaster = _MSUoW.ExaminationMaster.FindBy(examMstr =>
                        examMstr.CourseType == examinationFormFillUpDetails.CourseTypeCode
                        && examMstr.StreamCode == examinationFormFillUpDetails.StreamCode
                        && examMstr.SubjectCode == examinationFormFillUpDetails.SubjectCode
                        && examMstr.CourseCode == examinationFormFillUpDetails.CourseCode
                        && examMstr.DepartmentCode == examinationFormFillUpDetails.DepartmentCode
                        && examMstr.SemesterCode == examinationFormFillUpDetails.SemesterCode
                    ).FirstOrDefault();

                    if (examinationMaster != null)
                    {
                        var examinationSession = _MSUoW.ExaminationSession.FindBy(examSsn => examSsn.ExaminationMasterId == examinationMaster.Id && examSsn.StartYear == examinationFormFillUpDetails.AcademicStart).FirstOrDefault();
                        if (examinationSession != null)
                        {
                            examYear = examinationSession.ExamYear.ToString();
                        }
                    }

                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == examinationFormFillUpDetails.CollegeCode).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion

                    #region Step k
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        // get the principal hod remarks for rejection
                        var triggerParameter = input.PossibleWorkflowTransitions.Where(p => p.IsActive && p.Trigger.Trim().ToLower() == trigger.Trim().ToLower()
                                     && p.TransitState.Trim().ToLower() == examinationFormFillUpDetails.State.Trim().ToLower() && p.SourceState.Trim().ToLower() == sourceState.Trim().ToLower())
                                     .Select(ds => ds.TriggerParameters).FirstOrDefault();
                        TriggerParameterDto[] arr = JsonConvert.DeserializeObject<TriggerParameterDto[]>(triggerParameter);
                        if (arr.Count() > 0)
                        {
                            var columnName = arr.FirstOrDefault().PropertyName;
                            string rejectedRemarks = examinationFormFillUpDetails.GetType().GetProperty(columnName).GetValue(examinationFormFillUpDetails).ToString();

                            var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();
                            if (applyingStudent != null)
                            {
                                SendEmailAndSMSOnApplicationRejection(applyingStudent.EmailAddress, applyingStudent.PhoneNumber, applyingStudent.UserName, serviceName, rejectedRemarks);
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        public ExaminationFormFillUpDetailsDto VerificationByPrincipal(Guid id, ExaminationFormFillUpDetailsDto input)
        {
            #region Step b starts
            if (input == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU054" };
            }
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.ExaminationFormFillUpDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<ExaminationFormFillUpDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.ExaminationFormFillUpDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users applyingStudent;
                    applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();

                    if (applyingStudent != null)
                    {// Here is a student
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.BCABBAExamApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();

                        //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId);
                        //if (IsLastTransition == true)
                        //{// Rejected
                        //    SendEmailAndSMSOnApplicationRejection(applyingStudent.EmailAddress, applyingStudent.PhoneNumber, applyingStudent.Name, serviceName);
                        //}
                        //else // Verified
                        //{
                        // Sending Email
                        if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                        {
                            SendEmailToStudentToPay(applyingStudent.EmailAddress, applyingStudent.Name, serviceName);
                        }

                        // Sending SMS
                        if (serviceName != null && serviceName != string.Empty && applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                        {
                            SendSMSToStudentToPay(applyingStudent.PhoneNumber, applyingStudent.Name);
                        }
                        //}





                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        private void UpdateUserServiceAppliedDetail(ExaminationFormFillUpDetails examinationFormFillUpDetails)
        {
            UserServiceApplied usrSrvcApplied = _MSUoW.UserServiceApplied.FindBy(usrSrvc => usrSrvc.InstanceId == examinationFormFillUpDetails.Id).OrderByDescending(usrSrvc => usrSrvc.CreationTime).FirstOrDefault();
            if (usrSrvcApplied != null)
            {
                usrSrvcApplied.LastStatus = examinationFormFillUpDetails.State;
                try
                {
                    var actionRequiringStateListForStudent = (_IConfiguration.GetSection("ExamFormFillup:ActionRequiringStates").Value).Split(",");
                    if (actionRequiringStateListForStudent.Count() > 0 && Array.IndexOf(actionRequiringStateListForStudent, usrSrvcApplied.LastStatus) != -1)
                    {// Latest state requires student action
                        usrSrvcApplied.IsActionRequired = true;
                    }
                    else
                    {// Latest state does not require student action
                        usrSrvcApplied.IsActionRequired = false;
                    }
                }
                catch (Exception e) { usrSrvcApplied.IsActionRequired = false; }
                _MSUoW.UserServiceApplied.Update(usrSrvcApplied);
            }
        }

        public ExaminationFormFillUpDetailsDto PaymentCompleted(Guid id, ExaminationFormFillUpDetailsDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.ExaminationFormFillUpDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = _IConfiguration.GetSection("ExamFormFillup:PaymentTrigger").Value;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<ExaminationFormFillUpDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.ExaminationFormFillUpDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<ExaminationFormFillUpDetails, ExaminationFormFillUpDetailsDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users nextApprovingUser;
                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();

                    if (nextApprovingUser != null)
                    {
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.BCABBAExamApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                        if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                            SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                    }


                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        private bool IsLastTransitionOfWorkflow(List<TenantWorkflowTransitionConfig> possibleTransitions, string currentState, long? parentWFId)
        {
            bool isLastStateTransition = false;
            var transitionsAvailable = possibleTransitions.Where(transition => transition.SourceState == currentState).FirstOrDefault();
            if (transitionsAvailable == null)
            {
                isLastStateTransition = true;
            }
            return isLastStateTransition;
        }

        private bool SendEmailOnWorkflowCompletion(string emailAddress, string userName, string regNumber)
        {// Needs modification. Create email template
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGenerated && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--regNo--", regNumber);
                    // Needs modification. Create email template
                    //_MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        private bool SendEmailAndSMSOnApplicationRejection(string emailAddress, string phoneNumber, string userName, string serviceName, string rejectedRemarks)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupRejectionEmail && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--rmks--", rejectedRemarks);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--url--", applicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            if (phoneNumber != null && phoneNumber != string.Empty && phoneNumber.Length == 10)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupRejectionSMS && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string contentId = string.Empty;
                    contentId = templateData.SMSContentId;
                    try
                    {
                        string apiUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                        string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                        StringBuilder body = new StringBuilder(templateData.Body);
                        body.Replace("--studentName--", userName);
                        body.Replace("--url--", applicationUrl);

                        OTPMessage otpObj = new OTPMessage();
                        otpObj.Message = body.ToString();
                        otpObj.MobileNumber = phoneNumber;
                        _MSCommon.SendSMS(apiUrl, otpObj, contentId);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e.Message);
                    }
                }
            }
            return true;
        }

        private bool SendEmailAndSMSOnAdmitCardGenerated(string emailAddress, string phoneNumber, string userName, string serviceName, string year)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupAdmitCardGenerated && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--studentName--", userName);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--year--", year);
                    body.Replace("--url--", applicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            if (phoneNumber != null && phoneNumber != string.Empty && phoneNumber.Length == 10)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupAdmitCardGeneratedSMS && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string contentId = string.Empty;
                    contentId = templateData.SMSContentId;
                    try
                    {
                        string apiUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                        string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                        StringBuilder body = new StringBuilder(templateData.Body);
                        body.Replace("--studentName--", userName);
                        body.Replace("--serviceName--", serviceName);
                        body.Replace("--year--", year);
                        body.Replace("--url--", applicationUrl);

                        OTPMessage otpObj = new OTPMessage();
                        otpObj.Message = body.ToString();
                        otpObj.MobileNumber = phoneNumber;
                        _MSCommon.SendSMS(apiUrl, otpObj, contentId);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e.Message);
                    }
                }
            }
            return true;
        }
        #endregion

        #region ADMIT CARD DOWNLOAD FOR PRINCIPAL
        public PDFDownloadFile DownloadAdmitCard(certificateDownloadDto obj)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);
            List<string> userRole = GetRoleListOfCurrentUser();
            if (userRole.Contains(Convert.ToString((long)enRoles.Principal)) || userRole.Contains(Convert.ToString((long)enRoles.HOD))
                || userRole.Contains(Convert.ToString((long)enRoles.COE)) || userRole.Contains(Convert.ToString((long)enRoles.AsstCoE)))
            {
                if (obj != null)
                {

                    #region DATA GATHERING FROM TABLE FOR ADMITCARD
                    var examFormData = _MSUoW.FormFillupViewForAdmitCard.GetAll().Where(x =>
                                        x.AcknowledgementNo != null
                                        && x.RollNumber.Trim().ToLower() == obj.RegdOrRollNo.Trim().ToLower()
                                        && x.AcknowledgementNo.Trim().ToString() == obj.AcknowNum.Trim().ToString()
                                        && ((x.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:AdmitcardGenerated").Value.Trim().ToLower())
                                        || (x.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:MarkUploaded").Value.Trim().ToLower()))
                                        )
                                        .OrderByDescending(o => o.CreationTime).FirstOrDefault();

                    if (examFormData != null)
                    {
                        //get all papermaster from cache
                        List<ExaminationPaperMaster> examPapersList = _commonAppService.GetActiveExaminationPaperMasterList().Where(x => x.IsActive).ToList();
                        IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == examFormData.Id);

                        List<ExaminationPaperMaster> appliedExamPapers = examPapersList.Where(a => examPaperIds.Select(b => b.ExaminationPaperId).Contains(a.Id)).ToList();
                        AdmitCardCenterAndExamNameView centerAndSession = new AdmitCardCenterAndExamNameView();
                        if (examFormData.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
                        {
                            centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                                   && e.SubjectCode == examFormData.SubjectCode && e.SemesterCode == examFormData.SemesterCode && e.CollegeCode == examFormData.CollegeCode
                                                   && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                        }
                        else
                        {
                            centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.IsActive && e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                                   && e.StreamCode == examFormData.StreamCode && e.DepartmentCode == examFormData.DepartmentCode && e.SubjectCode == examFormData.SubjectCode
                                                   && e.SemesterCode == examFormData.SemesterCode && e.CollegeCode == examFormData.CollegeCode
                                                   && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                        }



                        #endregion

                        #region  Email content set part
                        if (centerAndSession != null)
                        {
                            List<ExaminationPaperMaster> allExamPaperList = examPapersList.Where(e => e.ExaminationMasterId == centerAndSession.Id).ToList();
                            id = examFormData.Id;
                            if (id != Guid.Empty)
                            {
                                UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == obj.ServiceType && serv.InstanceId == id && serv.IsActive)
                                            .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                                if (appliedData != null && !appliedData.IsAvailableToDL)
                                    throw new MicroServiceException() { ErrorCode = "OUU050" };
                            }
                            else
                                throw new MicroServiceException() { ErrorCode = "OUU051" };
                            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AdmitCardToStudent && s.IsActive == true).FirstOrDefault();

                            if (templateData != null)
                            {
                                string description = templateData.Body;
                                string url = _IConfiguration.GetSection("ApplicationURL").Value;
                                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                                string examControllerSign = _IConfiguration.GetSection("ControllerOfExaminationSignature").Value;

                                description = description.Replace("--collegeLogo--", url + collegeLogo);
                                description = description.Replace("--examName--", centerAndSession.SubjectString + " " + centerAndSession.SemesterString + " " + " EXAMINATION - " + centerAndSession.ExamYear);
                                if (examFormData.ImagePath != null)
                                {
                                    description = description.Replace("--photo--", url + examFormData.ImagePath);
                                }
                                else
                                {
                                    description = description.Replace("--photo--", "No Photo Available");
                                }
                                description = description.Replace("--regdNo--", examFormData.RegistrationNumber);
                                description = description.Replace("--studentName--", examFormData.StudentName);
                                description = description.Replace("--rollNo--", examFormData.RollNumber);
                                if (centerAndSession.CenterCollegeString.Trim().ToLower() == examFormData.CollegeName.Trim().ToLower())
                                {
                                    description = description.Replace("--college--", examFormData.CollegeName);
                                    description = description.Replace("--center--", "");
                                }
                                else
                                {
                                    description = description.Replace("--college--", examFormData.CollegeName);
                                    description = description.Replace("--center--", " / " + centerAndSession.CenterCollegeString);

                                }

                                description = description.Replace("--controllerSignature--", url + examControllerSign);

                                var stringtoappend = "";

                                //if (allExamPaperList.Count() == appliedExamPapers.Count())
                                //{
                                //    stringtoappend = "ALL";
                                //}
                                //else
                                //{
                                appliedExamPapers.ForEach(sub =>
                                {

                                    if (sub.IsParctical == true)
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                    }
                                    else
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                    }
                                });
                                stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);
                                //}

                                description = description.Replace("--papers--", stringtoappend);
                                flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                                flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                                flattenedPDFDownloadDetails.FileName = "AdmitCard_" + examFormData.RegistrationNumber + ".pdf";
                            }
                            else
                                throw new MicroServiceException { ErrorCode = "OUU009" };
                        }
                        else throw new MicroServiceException { ErrorCode = "OUU125" };
                    }
                    else throw new MicroServiceException { ErrorCode = "OUU125" };
                    #endregion
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU010" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU126" };
            }
            return flattenedPDFDownloadDetails;
        }
        #endregion

        #region
        public ExaminationFormFillUpDetails GetFirstSemeRecordForLoginUser(string studentName, string dob, int academicStart, string courseType, string collegeCode, string courseCode, string streamCode, string departmentCode, string subjectCode, string semesterCode)
        {
            ExaminationFormFillUpDetails record = new ExaminationFormFillUpDetails();
            courseCode = (courseCode == "null" ? null : courseCode);
            departmentCode = (departmentCode == "null" ? string.Empty : departmentCode);
            string state = _IConfiguration.GetSection("ExamFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower();
            record = _MSUoW.ExaminationFormFillUpDetails.FindBy(a => a.IsActive && a.CreatorUserId == GetCurrentUserId() && a.StudentName.Trim().ToLower() == studentName.Trim().ToLower()
                     && a.DateOfBirth == Convert.ToDateTime(dob) && a.AcademicStart == academicStart && a.CourseTypeCode == courseType && a.CollegeCode == collegeCode && a.CourseCode == courseCode
                     && a.DepartmentCode == departmentCode && a.StreamCode == streamCode && a.SubjectCode == subjectCode && a.SemesterCode == semesterCode
                     && a.State.Trim().ToLower() != state).FirstOrDefault();

            return record;
        }
        #endregion
        public IQueryable<AcademicStatementView> GetAllAcademicStatementList()
        {
            var id = GetCurrentTenantId();
            return _MSUoW.AcademicStatementViewRepository.GetAllAcademicStatementDetails(id).OrderBy(o => o.StudentName);

        }

        public IQueryable<AcademicStatementView> GetAllAcademicStatementList(SearchParamDto param)
        {
            IQueryable<AcademicStatementView> academicStatementData = null;
            var id = GetCurrentTenantId();
            List<string> roleId = GetRoleListOfCurrentUser();
            if (roleId.Contains(((long)enRoles.Principal).ToString()))
            {
                academicStatementData = _MSUoW.AcademicStatementViewRepository.GetAllAcademicStatementDetailswithsearchparam(param, id);
            }
            else if (roleId.Contains(((long)enRoles.AsstCoE).ToString()) || roleId.Contains(((long)enRoles.COE).ToString()))
            {
                academicStatementData = _MSUoW.AcademicStatementViewRepository.GetAllAcademicStatementDetailswithsearchparam(param, id)
                    .Where(s => s.State == param.State);
            }

            return academicStatementData;
        }

        #region MARK ENTRY APIS
        /// <summary>
        /// Author          :   Anurag Digal
        /// Date            :   25-09-21
        /// Description     :   This methode is used to add/Update Examination Form Fillup Semester Marks
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool AddExamFormFillUpMark(List<ExaminationFormFillUpSemesterMarksDto> input)
        {
            //validate input
            ValidateMarkEntryInput(input);

            //get from the input list
            Guid formFillUpId = input.FirstOrDefault().ExaminationFormFillUpDetailsId;
            string rollNo = input.FirstOrDefault().RollNumber;
            string registrationNo = input.FirstOrDefault().RegistrationNumber;
            string ackNo = input.FirstOrDefault().AcknowledgementNo;
            string state = string.Empty;

            //check role
            List<string> roleIds = GetRoleListOfCurrentUser();
            if (roleIds.Contains(((long)enRoles.Principal).ToString()))
            {
                state = Constant.AdmitCardGenerated;
            }
            else if (roleIds.Contains(((long)enRoles.COE).ToString()) || roleIds.Contains(((long)enRoles.AsstCoE).ToString()))
            {
                state = Constant.MarkUploaded;
            }

            //validate if candidate is geniune
            ExaminationFormFillUpDetails examDeatils = _MSUoW.ExaminationFormFillUpDetails.GetAll()
                                                        .Where(x => x.IsActive
                                                        && x.Id == formFillUpId
                                                        && x.PaymentDate != null
                                                        && x.AcknowledgementNo == ackNo
                                                        && x.RollNumber == rollNo
                                                        && x.RegistrationNumber == registrationNo
                                                        && x.State.Trim().ToLower() == state.Trim().ToLower())
                                                        .FirstOrDefault();

            if (examDeatils == null)
                throw new MicroServiceException() { ErrorMessage = "Please provide valid candidate. If not contact support." };


            List<ExaminationFormFillUpSemesterMarks> newMarkList = new List<ExaminationFormFillUpSemesterMarks>();
            List<ExaminationFormFillUpSemesterMarks> oldMarkList = new List<ExaminationFormFillUpSemesterMarks>();
            //check if alrady added
            oldMarkList = _MSUoW.ExaminationFormFillUpSemesterMarks.GetAll()
                        .Where(mark => mark.IsActive
                        && mark.ExaminationFormFillUpDetailsId == formFillUpId
                        && mark.AcknowledgementNo == ackNo)
                        .ToList();
            try
            {
                if (oldMarkList.Count() == 0)  //add mechanism
                {
                    //create object for ExaminationFormFillUpMarkEntry
                    newMarkList = AutoMapper.Mapper.Map<List<ExaminationFormFillUpSemesterMarksDto>, List<ExaminationFormFillUpSemesterMarks>>(input);
                    newMarkList.ForEach(mark =>
                    {
                        mark.Id = Guid.NewGuid();
                    });
                    _MSUoW.ExaminationFormFillUpSemesterMarks.AddRange(newMarkList);
                }
                else
                {
                    //update mechanism
                    oldMarkList.ForEach(oml =>
                    {
                        ExaminationFormFillUpSemesterMarksDto newPaperMarkDetails = input.
                                                                                    Where(nm => nm.ExaminationPaperId == oml.ExaminationPaperId
                                                                                    && nm.ExaminationFormFillUpDetailsId == oml.ExaminationFormFillUpDetailsId)
                                                                                    .FirstOrDefault();
                        if (newPaperMarkDetails != null)
                        {
                            if (newPaperMarkDetails.MarkEntryType == _MSCommon.GetEnumDescription(enMarkEntryType.Internal))
                            {
                                oml.InternalMark = newPaperMarkDetails.InternalMark;
                                oml.IsInternalAttained = newPaperMarkDetails.IsInternalAttained;
                            }
                            else if (newPaperMarkDetails.MarkEntryType == _MSCommon.GetEnumDescription(enMarkEntryType.External))
                            {
                                oml.ExternalMark = newPaperMarkDetails.ExternalMark;
                                oml.IsExternalAttained = newPaperMarkDetails.IsExternalAttained;
                            }
                            else if (newPaperMarkDetails.MarkEntryType == _MSCommon.GetEnumDescription(enMarkEntryType.Practical) && newPaperMarkDetails.IsPractical == true)
                            {
                                oml.IsPractical = newPaperMarkDetails.IsPractical;
                                oml.PracticalMark = newPaperMarkDetails.PracticalMark;
                                oml.IsPracticalAttained = newPaperMarkDetails.IsPracticalAttained;
                            }
                        }
                    });

                    _MSUoW.ExaminationFormFillUpSemesterMarks.UpdateRange(oldMarkList);
                }
                _MSUoW.Commit();
                return true;
            }
            catch (Exception ex)
            {
                throw new MicroServiceException() { ErrorMessage = ex.StackTrace };
            }
        }

        private void ValidateMarkEntryInput(List<ExaminationFormFillUpSemesterMarksDto> input)
        {
            if (input.Count() == 0)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data receive." };
            }

            //get papers for ug semester 1 
            List<ExaminationPaperMaster> paperList = _commonAppService.GetActiveExaminationPaperMasterList()
                                                    .Where(x => x.IsActive
                                                    && input.Select(s => s.ExaminationPaperId).Contains(x.Id))
                                                    .ToList();

            input.ForEach(mark =>
            {

                if (string.IsNullOrEmpty(mark.MarkEntryType))
                {
                    throw new MicroServiceException() { ErrorMessage = "Mark entry type cannot be blank." };
                }

                if (!string.IsNullOrEmpty(mark.MarkEntryType))
                {
                    ExaminationPaperMaster paperDtls = paperList.Where(x => x.Id == mark.ExaminationPaperId).FirstOrDefault();
                    if (mark.MarkEntryType == _MSCommon.GetEnumDescription(enMarkEntryType.Internal))
                    {
                        if (mark.IsInternalAttained == false && (mark.InternalMark < 0 || mark.InternalMark == null))
                            throw new MicroServiceException() { ErrorMessage = "Internal mark cannot be less than 0 or null for " + paperDtls.PaperName + "." };
                        if (mark.IsInternalAttained == false && (mark.InternalMark > paperDtls.InternalMark))
                            throw new MicroServiceException() { ErrorMessage = "Internal mark cannot greater than " + paperDtls.InternalMark + " for " + paperDtls.PaperName + "." };
                    }
                    else if (mark.MarkEntryType == _MSCommon.GetEnumDescription(enMarkEntryType.External))
                    {
                        if (mark.IsExternalAttained == false && (mark.ExternalMark < 0 || mark.ExternalMark == null))
                            throw new MicroServiceException() { ErrorMessage = "External mark cannot be less than 0 or null for " + paperDtls.PaperName + "." };
                        if (mark.IsExternalAttained == false && (mark.ExternalMark > paperDtls.ExternalMark))
                            throw new MicroServiceException() { ErrorMessage = "External mark cannot greater than " + paperDtls.ExternalMark + " for " + paperDtls.PaperName + "." };

                    }
                    else if (mark.MarkEntryType == _MSCommon.GetEnumDescription(enMarkEntryType.Practical) && mark.IsPractical == true)
                    {
                        if (mark.IsPracticalAttained == false && (mark.PracticalMark < 0 || mark.PracticalMark == null))
                            throw new MicroServiceException() { ErrorMessage = "Practical mark cannot be less than 0 or null for " + paperDtls.PaperName + "." };
                        if (mark.IsPracticalAttained == false && (mark.PracticalMark > paperDtls.PracticalMark))
                            throw new MicroServiceException() { ErrorMessage = "Practical mark cannot greater than " + paperDtls.PracticalMark + " for " + paperDtls.PaperName + "." };

                    }
                }
            });
        }

        /// <summary>
        /// Author          :   Anurag Digal
        /// Date            :   25-09-21
        /// Description     :   This methode is get Examination Form Fillup Semester Marks based on id and ackownledgement number
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ackNo"></param>
        /// <returns></returns>
        public List<ExaminationFormFillUpSemesterMarksDto> GetExamFormFillUpMark(Guid id, string ackNo)
        {
            List<ExaminationFormFillUpSemesterMarksDto> markList = new List<ExaminationFormFillUpSemesterMarksDto>();

            //validate if id and ackNo is valid
            if (id == Guid.Empty && string.IsNullOrEmpty(ackNo))
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received" };
            }

            //get mark list from ExaminationFormFillUpSemesterMarks
            List<ExaminationFormFillUpSemesterMarks> semesterMarkList = new List<ExaminationFormFillUpSemesterMarks>();
            semesterMarkList = _MSUoW.ExaminationFormFillUpSemesterMarks.GetAll()
                                .Where(mark => mark.IsActive
                                && mark.AcknowledgementNo == ackNo
                                && mark.ExaminationFormFillUpDetailsId == id)
                                .ToList();

            if (semesterMarkList.Count() == 0)
                throw new MicroServiceException() { ErrorMessage = "No mark record found." };

            //convert entity to dto
            markList = AutoMapper.Mapper.Map<List<ExaminationFormFillUpSemesterMarks>, List<ExaminationFormFillUpSemesterMarksDto>>(semesterMarkList);

            return markList;
        }
        #endregion

        /// <summary>
        /// Author          :   Bikash kumar sethi 
        /// Date            :   27-09-2021
        /// Description     :   This methode is get all semester and student details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ackNo"></param>
        /// <returns></returns>
        #region  Get student details and semister details
        public SemesterAndStudentDetailsDto GetSemesterPaperDetails(Guid Id, string ack)
        {
            SemesterAndStudentDetailsDto semesterAndStudentDetails = new SemesterAndStudentDetailsDto();
            if (Id == null || ack == "")
            {
                throw new MicroServiceException() { ErrorCode = "Please check input type" };
            }
            ExaminationView examDetails = _MSUoW.ExaminationView.GetAll().Where(w => w.Id == Id && w.AcknowledgementNo == ack).FirstOrDefault();
            if (examDetails == null)
            {
                throw new MicroServiceException() { ErrorCode = "Sorry no student details found. Please contact support." };
            }
            //check role
            //List<string> roleIds = GetRoleListOfCurrentUser();
            //get rolewise current states
            List<string> roleStates = GetExamFormFillUpState();

            //if role wise state match
            //if(examDetails.State == Constant.AdmitCardGenerated)
            if (roleStates.Contains(examDetails.State))
            {
                if (examDetails.RollNumber == "" || examDetails.RegistrationNumber == "")
                {
                    throw new MicroServiceException() { ErrorCode = "Some thing went wrong.Please contact help support" };
                }
                if (examDetails != null)
                {
                    semesterAndStudentDetails.Name = examDetails.StudentName;
                    semesterAndStudentDetails.RegistrationNumber = examDetails.RegistrationNumber;
                    semesterAndStudentDetails.RollNumber = examDetails.RollNumber;
                    semesterAndStudentDetails.AcknowledgementNo = examDetails.AcknowledgementNo;

                    semesterAndStudentDetails.State = examDetails.State;
                    semesterAndStudentDetails.CourseType = examDetails.CourseTypeString;
                    semesterAndStudentDetails.College = examDetails.CollegeString;
                    semesterAndStudentDetails.Stream = examDetails.StreamString;
                    semesterAndStudentDetails.Subject = examDetails.SubjectString;
                    semesterAndStudentDetails.Semester = examDetails.SemesterString;
                    semesterAndStudentDetails.YearOfAddmission = examDetails.AcademicStart.ToString();

                    semesterAndStudentDetails.CourseTypeCode = examDetails.CourseTypeCode;
                    semesterAndStudentDetails.CollegeCode = examDetails.CollegeCode;
                    semesterAndStudentDetails.StreamCode = examDetails.StreamCode;
                    semesterAndStudentDetails.SubjectCode = examDetails.SubjectCode;
                    semesterAndStudentDetails.SemesterCode = examDetails.SemesterCode;
                }
                List<Guid> paperDetailsId = new List<Guid>();
                paperDetailsId = _MSUoW.ExaminationPaperDetail.GetAll().Where(e => e.ExaminationFormFillUpDetailsId == Id).Select(s => s.ExaminationPaperId).ToList();
                semesterAndStudentDetails.ExaminationPaperMasters = _commonAppService.GetActiveExaminationPaperMasterList().Where(w => paperDetailsId.Contains(w.Id)).OrderBy(o => o.CategoryAbbreviation).ToList();
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "Candidate is not in the requisite state. Please contact support." };
            }
            return semesterAndStudentDetails;
        }
        #endregion

        #region ASYNC CALL TO SAVE DATA FROM MARK DETAILS TABLE TO BACKGROUNDPROCESS TABLE IN STATE PRINCIPAL TO COE
        /// <summary>
        /// Author:Biaksh kumar sethi
        /// Date: 10-09-21
        /// Method is used for Odata Controller which will give examination student record as per usertype
        /// </summary>
        /// <returns></returns>
        public bool AddMarkDetailsToBackgroundTable(BackgroundProcessDto parameters)
        {
            if (parameters != null)
            {
                ExaminationMaster masterData = _MSUoW.ExaminationMaster.FindBy(a => a.CourseType == parameters.CourseTypeCode
                                            && a.SemesterCode == parameters.SemesterCode
                                            && a.StreamCode == parameters.StreamCode
                                            && a.SubjectCode == parameters.SubjectCode
                                            && a.TenantId == 3
                                            && a.IsActive == true).FirstOrDefault();
                if (masterData != null)
                {
                    ExaminationSession sessionData = _MSUoW.ExaminationSession.FindBy(sess => sess.ExaminationMasterId == masterData.Id && sess.IsActive && sess.StartYear == parameters.StartYear).FirstOrDefault();
                    if (sessionData != null)
                    {
                        ExaminationFormFillUpDates finalDate = _MSUoW.ExaminationFormFillUpDates.FindBy(dates => dates.ExaminationSessionId == sessionData.Id && dates.IsActive).OrderByDescending(a => a.SlotOrder).FirstOrDefault();
                        //var currTime = DateTime.UtcNow;
                        DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));
                        if (finalDate.EndDate < currTime.Date)
                        {
                            var currentUserId = GetCurrentUserId();
                            long userRole = _MSUoW.Users.FindBy(u => u.IsActive && u.Id == currentUserId).Select(s => s.UserType).FirstOrDefault();
                            if (userRole == (long)enUserType.Principal)
                            {
                                //var currTime = DateTime.UtcNow;
                                //Process Tracker Table Entry
                                ProcessTracker processTableData = new ProcessTracker();
                                processTableData.Id = Guid.NewGuid();
                                Guid processTableId = processTableData.Id;
                                processTableData.ServiceId = (long)enServiceType.BCABBAExamApplicationForm;
                                processTableData.ProcessType = (Int32)enProcessName.MarkForwardToCOE;
                                processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
                                processTableData.IsActive = true;
                                processTableData.CreatorUserId = currentUserId;
                                processTableData.CreationTime = DateTime.UtcNow;
                                processTableData.TenantId = Convert.ToInt64(3);
                                _MSUoW.ProcessTracker.Add(processTableData);
                                _MSUoW.Commit();
                                AddMarkDetailsDataToBackgrounTable(parameters, currentUserId, processTableId);
                            }
                            else throw new MicroServiceException() { ErrorCode = "OUU093" };
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU095" };// Do finalDate error
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU114" };// Do sessionData error
                }
                else throw new MicroServiceException() { ErrorCode = "OUU114" }; // Do masterData error
            }
            else throw new MicroServiceException() { ErrorCode = "OUU075" };

            return true;
        }

        public async Task<bool> AddMarkDetailsDataToBackgrounTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            await Task.Run(() =>
            {
                AddMarkDetailsToBackgroundTable(parameters, userId, processTableId);
                return true;
            });
            return false;
        }


        public bool AddMarkDetailsToBackgroundTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            using (var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger))
            {
                List<BackgroundProcess> data = new List<BackgroundProcess>();
                try
                {

                    if (parameters.SelectedInstanceIds != null && parameters.IsAllSelected == false)
                    {
                        var ids = parameters.SelectedInstanceIds.Split(",").ToList();
                        data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable
                                .GetAll()
                                .Where(d => d.IsActive
                                && ids.Contains(d.Id.ToString())
                                && d.State == parameters.State
                                && d.MarkUploaded == _MSCommon.GetEnumDescription(enMarkUploaded.Yes))
                                .Select(s => new BackgroundProcess
                                {
                                    Id = Guid.NewGuid(),
                                    ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                                    InstanceTrigger = _IConfiguration.GetSection("ExamFormFillup:MarkForwardToCOE").Value,
                                    InstanceId = s.Id,
                                    Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),//will be false after sent to COE
                                    TenantWorkflowId = s.TenantWorkflowId,
                                    CreatorUserId = userId,
                                    CreationTime = DateTime.UtcNow,
                                    IsActive = true,
                                    TenantId = Convert.ToInt64(3),
                                    CollegeCode = parameters.CollegeCode[0], // as principal send only one college record
                                    CourseCode = parameters.CourseCode,
                                    StreamCode = parameters.StreamCode,
                                    CourseTypeCode = parameters.CourseTypeCode,
                                    DepartmentCode = parameters.DepartmentCode,
                                    SubjectCode = parameters.SubjectCode,
                                    SemesterCode = parameters.SemesterCode,
                                    AcademicStart = parameters.StartYear
                                })
                                .ToList();
                    }
                    else if (parameters.SelectedInstanceIds == null && parameters.IsAllSelected)
                    {
                        if (parameters.DepartmentCode == null)
                        {
                            data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable
                                .GetAll().Where(d => d.IsActive
                                && d.State == parameters.State
                                && d.CourseTypeCode == parameters.CourseTypeCode
                                && parameters.CollegeCode.Contains(d.CollegeCode)
                                && d.StreamCode == parameters.StreamCode
                                && d.SemesterCode == parameters.SemesterCode
                                && d.SubjectCode == parameters.SubjectCode
                                && d.AcademicStart == parameters.StartYear
                                && d.MarkUploaded == _MSCommon.GetEnumDescription(enMarkUploaded.Yes)
                                )
                                .Select(s => new BackgroundProcess
                                {
                                    Id = Guid.NewGuid(),
                                    ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                                    InstanceTrigger = _IConfiguration.GetSection("ExamFormFillup:MarkForwardToCOE").Value,
                                    InstanceId = s.Id,
                                    Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                    TenantWorkflowId = s.TenantWorkflowId,
                                    CreatorUserId = userId,
                                    CreationTime = DateTime.UtcNow,
                                    IsActive = true,
                                    TenantId = Convert.ToInt64(3),
                                    CollegeCode = parameters.CollegeCode[0],
                                    CourseCode = parameters.CourseCode,
                                    StreamCode = parameters.StreamCode,
                                    CourseTypeCode = parameters.CourseTypeCode,
                                    DepartmentCode = parameters.DepartmentCode,
                                    SubjectCode = parameters.SubjectCode,
                                    SemesterCode = parameters.SemesterCode,
                                    AcademicStart = parameters.StartYear
                                })
                                .ToList();
                        }
                        else
                        {
                            data = _MSUoW.ExaminationViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && d.State == parameters.State && d.CourseTypeCode == parameters.CourseTypeCode
                                                                   && parameters.CollegeCode.Contains(d.CollegeCode) && d.StreamCode == parameters.StreamCode && d.SemesterCode == parameters.SemesterCode
                                                                   && d.SubjectCode == parameters.SubjectCode && d.DepartmentCode == parameters.DepartmentCode && d.CourseCode == parameters.CourseCode && d.AcademicStart == parameters.StartYear)
                                                  .Select(s => new BackgroundProcess
                                                  {
                                                      Id = Guid.NewGuid(),
                                                      ServiceType = (long)enServiceType.BCABBAExamApplicationForm,
                                                      InstanceTrigger = _IConfiguration.GetSection("ExamFormFillup:MarkForwardToCOE").Value,
                                                      InstanceId = s.Id,
                                                      Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                                      TenantWorkflowId = s.TenantWorkflowId,
                                                      CreatorUserId = userId,
                                                      CreationTime = DateTime.UtcNow,
                                                      IsActive = true,
                                                      TenantId = Convert.ToInt64(3),
                                                      CollegeCode = parameters.CollegeCode[0],
                                                      CourseCode = parameters.CourseCode,
                                                      StreamCode = parameters.StreamCode,
                                                      CourseTypeCode = parameters.CourseTypeCode,
                                                      DepartmentCode = parameters.DepartmentCode,
                                                      SubjectCode = parameters.SubjectCode,
                                                      SemesterCode = parameters.SemesterCode,
                                                      AcademicStart = parameters.StartYear
                                                  }).ToList();
                        }
                    }
                    _MSUoW.BackgroundProcess.AddRange(data);

                    ProcessTracker processTracker = new ProcessTracker();
                    var currTime = DateTime.UtcNow;
                    processTracker = _MSUoW.ProcessTracker.GetAll().Where(w => w.Id == processTableId && w.CreatorUserId == userId).FirstOrDefault();
                    processTracker.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.ReadyForProcessing);
                    processTracker.LastModifierUserId = processTracker.CreatorUserId;
                    processTracker.LastModificationTime = currTime;
                    _MSUoW.ProcessTracker.Update(processTracker);
                    _MSUoW.Commit();
                }
                catch (Exception e)
                {
                }
                return true;
            }
            // string formStatus = _IConfiguration.GetSection("ExamFormFillup:FormStates").Value;

        }

        #endregion

        public SemesterFilterDto GetSemesterFilterObject(SearchParamDto input)
        {
            SemesterFilterDto filterObj = new SemesterFilterDto();
            if (input == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received." };
            }
            ExaminationView examDetails = _MSUoW.ExaminationView.GetAll()
                                            .Where(w => w.CourseTypeCode == input.CourseTypeCode
                                            //&& (w.CourseTypeCode==_MSCommon.GetEnumDescription(enCourseType.Pg) ?
                                            //(w.DepartmentCode==w.DepartmentCode 
                                            //&& w.CourseCode==w.CourseCode):  (w.DepartmentCode==null && w.CourseCode==null))                                          
                                            && w.StreamCode == input.StreamCode
                                            && w.SubjectCode == input.SubjectCode
                                            && w.SemesterCode == input.SemesterCode
                                            && w.State.Trim().ToLower() == input.State.Trim().ToLower()
                                            && w.CollegeCode == input.CollegeCode.FirstOrDefault())
                                            .FirstOrDefault();
            if (examDetails == null)
            {
                throw new MicroServiceException() { ErrorCode = "Please provide valid filter data." };
            }

            filterObj.State = examDetails.State;
            filterObj.CourseTypeCodeString = examDetails.CourseTypeString;
            filterObj.CourseTypeCode = examDetails.CourseTypeCode;
            filterObj.CollegeString = examDetails.CollegeString;
            filterObj.CollegeCode = examDetails.CollegeCode;
            filterObj.StreamString = examDetails.StreamString;
            filterObj.StreamCode = examDetails.StreamCode;
            filterObj.SubjectString = examDetails.SubjectString;
            filterObj.SubjectCode = examDetails.SubjectCode;
            filterObj.SemesterString = examDetails.SemesterString;
            filterObj.SemesterCode = examDetails.SemesterCode;
            filterObj.YearOfAddmission = examDetails.AcademicStart;
            filterObj.DepartmentCode = examDetails.SemesterCode;
            filterObj.DepartmentCodeString = examDetails.SemesterCode;
            filterObj.CourseCode = examDetails.CourseCode;
            filterObj.CourseCodeString = examDetails.CourseString;

            return filterObj;
        }

        #region Get all mark details
        public IQueryable<MarkStatementView> GetAllMarkDdetails()
        {
            return _MSUoW.MarkStatementView.GetAll().Where(e => e.IsActive);
        }
        #endregion

        /// <summary>
        /// Author:Biaksh kumar sethi
        /// Date: 01-10-21
        /// Method is used for Odata Controller which will give examination student record as per usertype
        /// </summary>
        /// <returns></returns>
        #region get all paperdetails and mark details
        public IQueryable<ExaminationFormFillUpSemesterMarksDto> GetStudentPaperAndMarkDetails(Guid Id, string ack)
        {
            IQueryable<ExaminationFormFillUpSemesterMarksDto> subjectdetails =
            from semes in _MSUoW.ExaminationFormFillUpSemesterMarks.GetAll().AsQueryable()
            join paper in _commonAppService.GetActiveExaminationPaperMasterList() on semes.ExaminationPaperId equals paper.Id
            join student in _MSUoW.ExaminationFormFillUpDetails.GetAll().AsQueryable() on semes.ExaminationFormFillUpDetailsId equals student.Id
            where student.Id == Id && student.AcknowledgementNo == ack && semes.IsActive == true && paper.IsActive == true && student.IsActive == true
            select new ExaminationFormFillUpSemesterMarksDto
            {
                Id = semes.Id,
                CreatorUserId = semes.CreatorUserId,
                CreationTime = semes.CreationTime,
                LastModifierUserId = semes.LastModifierUserId,
                LastModificationTime = semes.LastModificationTime,
                IsActive = semes.IsActive,
                TenantId = semes.TenantId,
                AcknowledgementNo = semes.AcknowledgementNo,
                ExaminationFormFillUpDetailsId = semes.ExaminationFormFillUpDetailsId,
                ExaminationPaperId = semes.ExaminationPaperId,
                // MarkEntryType = "",
                IsPractical = semes.IsPractical,
                InternalMark = semes.InternalMark,
                ExternalMark = semes.ExternalMark,
                PracticalMark = semes.PracticalMark,
                RegistrationNumber = semes.RegistrationNumber,
                RollNumber = semes.RollNumber,
                PaperName = paper.PaperName,
                PaperAbbreviation = paper.PaperAbbreviation,
                CategoryAbbreviation = paper.CategoryAbbreviation,

            };
            return subjectdetails.OrderBy(a => a.CategoryAbbreviation);
        }
        #endregion

        public List<MarkStatementView> GetMarkDetailsForL1Support(SearchParamDto input)
        {
            return GetAllMarkDdetails()
                    .Where(e => input.CourseTypeCode == input.CourseTypeCode
                    && input.CollegeCode.Contains(e.CollegeCode)
                    && input.StreamCode == e.StreamCode
                    && input.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg) ?
                    (input.DepartmentCode == e.DepartmentCode
                    && input.CourseCode == e.CourseCode && e.IsActive) :
                    e.IsActive
                    && input.SubjectCode == e.SubjectCode
                    && input.SemesterCode == e.SemesterCode
                    && input.State.Trim().ToLower() == e.State.Trim().ToLower()
                    && input.StartYear == e.AcademicStart)
                    .OrderBy(o => o.StudentName)
                    .ThenBy(t => t.AcknowledgementNo)
                    .ToList();
        }

        #region Export student wise subject details excel       
        //public GridModelExcelDto GetExcelSearchParamData(SearchParamDto input)
        //{
        //    GridModelExcelDto gridModelDto = new GridModelExcelDto();
        //    GridModelExcelDto gridModelDtoCopy = new GridModelExcelDto();
        //    gridModelDto.Columns = new List<GridColumnExcel>();
        //    gridModelDto.Rows = new List<GridRowExcel>();
        //    gridModelDtoCopy.Rows = new List<GridRowExcel>();
        //    var predicate = PredicateBuilder.True<ExaminationFormFillUpDetails>();

        //    //string resultsdata = GetPdfReport(input);

        //    if (input != null)
        //    {
        //        if (!string.IsNullOrEmpty(input.CourseTypeCode))
        //        {
        //            predicate = predicate.And(i => i.CourseTypeCode.ToLower().Equals(input.CourseTypeCode.ToLower()));
        //        }
        //        if (input.CollegeCode.Count > 0)
        //        {
        //            predicate = predicate.And(i => input.CollegeCode.Contains(i.CollegeCode));
        //        }
        //        if (!string.IsNullOrEmpty(input.StreamCode))
        //        {
        //            predicate = predicate.And(i => i.StreamCode.ToLower().Equals(input.StreamCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.SubjectCode))
        //        {
        //            predicate = predicate.And(i => i.SubjectCode.ToLower().Equals(input.SubjectCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.DepartmentCode))
        //        {
        //            predicate = predicate.And(i => i.DepartmentCode.ToLower().Equals(input.DepartmentCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.CourseCode))
        //        {
        //            predicate = predicate.And(i => i.CourseCode.ToLower().Equals(input.CourseCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.SemesterCode))
        //        {
        //            predicate = predicate.And(i => i.SemesterCode.ToLower().Equals(input.SemesterCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.State))
        //        {
        //            predicate = predicate.And(i => i.State.ToLower().Equals(input.State.ToLower()));
        //        }
        //        if (input.StartYear > 0)
        //        {
        //            predicate = predicate.And(i => i.AcademicStart.Equals(input.StartYear));
        //        }
        //    }

        //    IQueryable<ExaminationPaperMaster> papermaster = _MSUoW.ExaminationPaperMaster.GetAll().Where(s => s.IsActive == true);

        //    IQueryable<ExaminationFormFillUpDetails> ugStudentExaminationDetailsList = _MSUoW.ExaminationFormFillUpDetails.GetAll().
        //    Where(predicate).Where(u => u.IsActive == true && u.State == Constant.MarkUploaded).OrderBy(q => q.RollNumber);

        //    List<ExaminationView> examinaionViewDetails = new List<ExaminationView>();

        //    List<ExaminationFormFillUpDetails> ExamList = new List<ExaminationFormFillUpDetails>();
        //    gridModelDto.Count = ugStudentExaminationDetailsList.Count();
        //    ExamList = ugStudentExaminationDetailsList.ToList();

        //    List<Guid> instanceids = ExamList.Select(z => z.Id).ToList();
        //    examinaionViewDetails = _MSUoW.ExaminationView.GetAll().Where(w => w.IsActive && instanceids.Contains(w.Id)).ToList();
        //    List<ExaminationFormFillUpSemesterMarks> ExamPaperList = _MSUoW.ExaminationFormFillUpSemesterMarks.FindBy(p => instanceids.Contains(p.ExaminationFormFillUpDetailsId) && p.IsActive == true).ToList();

        //    //List<string> rollnolist = ExamList.Select(z => z.RollNumber).ToList();
        //    //List<ExaminationFormFillUpDetails> StudentList = _MSUoW.ExaminationFormFillUpDetails.FindBy(p => rollnolist.Contains(p.RollNumber) && p.IsActive == true && p.State == Constant.MarkUploaded).ToList();

        //    var data = from ugsed in examinaionViewDetails
        //               join ugespd in ExamPaperList on ugsed.Id equals ugespd.ExaminationFormFillUpDetailsId
        //               orderby ugespd.RollNumber
        //               //join std in StudentList on ugsed.RollNumber equals std.RollNumber into studentDetails
        //               //from ugstd in studentDetails.DefaultIfEmpty()
        //               join pm in papermaster on ugespd.ExaminationPaperId equals pm.Id
        //               where pm.IsActive = true
        //               orderby pm.CategoryAbbreviation
        //               select new
        //               {
        //                   Id = ugespd.Id,
        //                   InstantId = ugespd.ExaminationFormFillUpDetailsId,
        //                   StudentName = ugsed.StudentName,
        //                   FatherName = ugsed.FatherName,
        //                   GuardianName = ugsed.GuardianName,
        //                   DateOfBirth = ugsed.DateOfBirth,
        //                   RegistrationNumber = ugsed.RegistrationNumber,
        //                   RollNumber = ugsed.RollNumber,
        //                   AcknowledgementNo = ugsed.AcknowledgementNo,

        //                   CourseTypeCode = ugsed.CourseTypeCode,
        //                   CollegeCode = ugsed.CollegeCode,
        //                   StreamCode = ugsed.StreamCode,
        //                   SubjectCode = ugsed.SubjectCode,
        //                   DepartmentCode = ugsed.DepartmentCode,
        //                   CourseCode = ugsed.CourseCode,
        //                   SemesterCode = ugsed.SemesterCode,
        //                   AcademicStart = ugsed.AcademicStart,

        //                   CourseTypeString = ugsed.CourseTypeString,
        //                   CollegeString = ugsed.CollegeString,
        //                   StreamString = ugsed.StreamString,
        //                   SubjectString = ugsed.SubjectString,
        //                   DepartmentString = ugsed.DepartmentString,
        //                   CourseString = ugsed.CourseString,
        //                   SemesterString = ugsed.SemesterString,

        //                   CategoryAbbreviation = pm.CategoryAbbreviation,
        //                   PaperName = pm.PaperName,
        //                   TotalInternalMark = pm.InternalMark,
        //                   TotalPracticalMark = pm.PracticalMark,
        //                   ToralExternalMark = pm.ExternalMark,
        //                   SecuredInternalMark = ugespd.InternalMark == null ? "--" : ugespd.InternalMark.ToString(),
        //                   SecuredPracticalMark = ugespd.PracticalMark == null ? "--" : ugespd.PracticalMark.ToString(),
        //                   SecuredExternalMark = ugespd.ExternalMark == null ? "--" : ugespd.ExternalMark.ToString(),
        //                   IsActive = ugespd.IsActive,
        //                   ExamPaperId = ugespd.ExaminationPaperId

        //               };

        //    data = data.OrderBy(q => q.StudentName).Where(p => input.CollegeCode.Contains(p.CollegeCode));

        //    List<Guid> ids = new List<Guid>();
        //    if (data.Count() > 0)
        //    {
        //        ids = data.Select(s => s.InstantId).ToList();
        //        //Add Parent Column Names
        //        //gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Id", IsHidden = true });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Name", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Roll No.", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Course Type", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "College Code", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "College Name", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Stream ", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Subject", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Department", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Course ", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Semester ", IsHidden = false });
        //        gridModelDto.Columns.Add(new GridColumnExcel { ColumnName = "Academic Start", IsHidden = false });
        //    }
        //    else
        //    {
        //        gridModelDto.Columns = new List<GridColumnExcel>();
        //        gridModelDto.Rows = new List<GridRowExcel>();
        //        gridModelDto.Count = 0;
        //        return gridModelDto;
        //    }

        //    List<ExaminationFormFillUpSemesterMarks> paperData = new List<ExaminationFormFillUpSemesterMarks>();
        //    List<ExaminationFormFillUpSemesterMarks> paperValues = new List<ExaminationFormFillUpSemesterMarks>();
        //    int maxCount = 0;
        //    List<ExaminationFormFillUpSemesterMarks> maxSubjectList = new List<ExaminationFormFillUpSemesterMarks>();
        //    List<ExaminationFormFillUpSemesterMarks> subjectDetails = new List<ExaminationFormFillUpSemesterMarks>();
        //    List<ExaminationFormFillUpSemesterMarks> paperDetails = new List<ExaminationFormFillUpSemesterMarks>();
        //    List<ExaminationPaperMaster> paperMasterList = new List<ExaminationPaperMaster>();
        //    List<ExaminationFormFillUpDetails> examSubject = ExamList.Where(a => a.IsActive && ids.Contains(a.Id)).OrderBy(a => a.StudentName).ToList();

        //    examSubject.ForEach(col =>
        //    {
        //        subjectDetails = new List<ExaminationFormFillUpSemesterMarks>();

        //        subjectDetails = ExamPaperList.Where(a => a.IsActive && a.ExaminationFormFillUpDetailsId == col.Id).ToList();

        //        var tempCount = subjectDetails.Count();
        //        if (tempCount > maxCount)
        //        {
        //            maxCount = tempCount;
        //            maxSubjectList = subjectDetails.ToList();
        //        }
        //        if (subjectDetails.Count() > 0)
        //        {
        //            subjectDetails = subjectDetails.ToList();
        //            subjectDetails.ForEach(el =>
        //            {
        //                paperDetails.Add(el);
        //            });
        //        }
        //    });
        //    paperMasterList = papermaster.Where(w => maxSubjectList.Select(a => a.ExaminationPaperId).Contains(w.Id)).ToList();
        //    int count = 1;
        //    paperMasterList.OrderBy(p => p.CategoryAbbreviation).ToList().ForEach(p =>
        //    {
        //        //gridModelDto.Columns.Add(new GridColumnExcel
        //        //{
        //        //    ColumnName = "(" + count + ") " + p.PaperName.ToString(),
        //        //    IsHidden = false
        //        //});
        //        gridModelDto.Columns.Add(new GridColumnExcel
        //        {
        //            ColumnName = "(" + count + ") " + p.Category.ToString() + "-" + "Secured Internal Marks (Out of " + p.InternalMark + ")",
        //            IsHidden = false
        //        });
        //        gridModelDto.Columns.Add(new GridColumnExcel
        //        {
        //            ColumnName = "(" + count + ") " + p.Category.ToString() + "-" + "Secured External Marks (Out of " + p.ExternalMark + ")",
        //            IsHidden = false
        //        });
        //        gridModelDto.Columns.Add(new GridColumnExcel
        //        {
        //            ColumnName = "(" + count + ") " + p.Category.ToString() + "-" + "Secured Practical Marks (Out of " + p.PracticalMark + ")",
        //            IsHidden = false
        //        });
        //        count++;
        //    });

        //    var result2 = data.GroupBy(x => new { x.CourseTypeCode, x.CollegeCode, x.SemesterCode, x.StreamCode, x.CourseCode, x.SubjectCode, x.DepartmentCode, x.AcademicStart, x.RollNumber })
        //    .Select(b => new
        //    {
        //        // Accessing to DateOfIssue and IssuerName from Key.
        //        Id = b.Select(bn => bn.Id),
        //        StudentName = b.Select(bn => bn.StudentName).FirstOrDefault(),
        //        RollNumber = b.Key.RollNumber,
        //        AdmissionYear = b.Select(bn => bn.AcademicStart).FirstOrDefault(),
        //        CourseTypeCode = b.Key.CourseTypeCode,
        //        CourseTypeString = b.Select(a => a.CourseTypeString).FirstOrDefault(),
        //        CollegeCode = b.Key.CollegeCode,
        //        CollegeName = b.Select(a => a.CollegeString).FirstOrDefault(),
        //        SemesterCode = b.Key.SemesterCode,
        //        StreamString = b.Select(a => a.StreamString).FirstOrDefault(),
        //        StreamCode = b.Key.StreamCode,
        //        SemesterString = b.Select(a => a.SemesterString).FirstOrDefault(),
        //        CourseCode = b.Key.CourseCode,
        //        CourseString = b.Select(a => a.CourseString).FirstOrDefault(),
        //        SubjectCode = b.Key.SubjectCode,
        //        SubjectString = b.Select(a => a.SubjectString).FirstOrDefault(),
        //        DepartmentCode = b.Key.DepartmentCode,
        //        DepartmentString = b.Select(a => a.DepartmentString).FirstOrDefault(),
        //        AcademicStart = b.Key.AcademicStart,
        //        ExamPaperId = b.Select(a => a.ExamPaperId).FirstOrDefault(),

        //        children = b.Select(x => new {
        //            x.ExamPaperId,
        //            x.PaperName,
        //            x.CategoryAbbreviation,
        //            x.TotalInternalMark,
        //            x.TotalPracticalMark,
        //            x.ToralExternalMark,
        //            x.SecuredInternalMark,
        //            x.SecuredPracticalMark,
        //            x.SecuredExternalMark
        //        }).OrderBy(p => p.CategoryAbbreviation).ToList()
        //    }).ToList();

        //    foreach (var d in result2)
        //    {
        //        GridRowExcel newRow = new GridRowExcel();
        //        newRow.GridRowItems = new List<GridRowItemExcel>();
        //        //Add Parent values               

        //       /// newRow.GridRowItems.Add(new GridRowItemExcel { Value = Convert.ToString(d.Id), IsHidden = true });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.StudentName, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.RollNumber, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.CourseTypeString, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.CollegeCode, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.CollegeName, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.StreamString, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.SubjectString, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.DepartmentString, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.CourseString, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.SemesterString, IsHidden = false });
        //        newRow.GridRowItems.Add(new GridRowItemExcel { Value = d.AcademicStart.ToString(), IsHidden = false });


        //        List<ExaminationFormFillUpSemesterMarks> finalPaperDetails = new List<ExaminationFormFillUpSemesterMarks>();

        //        //order paper details based on the category abbreviation on examinationpapermaster

        //        List<ExaminationFormFillUpSemesterMarks> orderedPaperDetails = papermaster
        //                .Join(paperDetails.AsQueryable(), pm => pm.Id, pd => pd.ExaminationPaperId,
        //                (pm, pd) => new ExaminationFormFillUpSemesterMarks
        //                {
        //                    AcknowledgementNo=pd.AcknowledgementNo,
        //                    ExaminationFormFillUpDetailsId=pd.ExaminationFormFillUpDetailsId,
        //                    ExaminationPaperId=pd.ExaminationPaperId,
        //                    InternalMark=pd.InternalMark,
        //                    ExternalMark=pd.ExternalMark,
        //                    IsPractical=pd.IsPractical,
        //                    PracticalMark=pd.PracticalMark,
        //                    RegistrationNumber=pd.RegistrationNumber,
        //                    RollNumber=pd.RollNumber,
        //                    CategoryAbbreviation=pm.CategoryAbbreviation,
        //                    IsActive=pd.IsActive,
        //                    Id=pd.Id
        //                })
        //                .OrderBy(o=>o.CategoryAbbreviation)
        //                .ToList();

        //        orderedPaperDetails.OrderBy(p => p.AcknowledgementNo).ToList().ForEach(col =>
        //        {
        //            if (col.RollNumber == d.RollNumber)
        //            {
        //                finalPaperDetails.Add(col);
        //            }
        //        });

        //        if (finalPaperDetails.Count() > 0)
        //        {
        //            finalPaperDetails.ForEach(fpd =>
        //            {
        //                //newRow.GridRowItems.Add(new GridRowItemExcel
        //                //{
        //                //    Value = d.children.Where(p => p.ExamPaperId == fpd.Id).FirstOrDefault() == null ? null : d.children.Where(p => p.ExamPaperId == fpd.Id ).FirstOrDefault().PaperName.ToString(),
        //                //    IsHidden = false
        //                //});

        //                var daa = d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault();
        //                newRow.GridRowItems.Add(new GridRowItemExcel
        //                {
        //                    Value = d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault() == null ? null : d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault().SecuredInternalMark.ToString(),
        //                    //+ "/" +
        //                    //d.children.Where(p => p.ExamPaperId == fpd.Id).FirstOrDefault() == null ? null : d.children.Where(p => p.ExamPaperId == fpd.Id).FirstOrDefault().SecuredInternalMark.ToString(),
        //                    IsHidden = false
        //                });
        //                newRow.GridRowItems.Add(new GridRowItemExcel
        //                {
        //                    Value = d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault() == null ? null : d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault().SecuredExternalMark,
        //                    IsHidden = false
        //                });
        //                newRow.GridRowItems.Add(new GridRowItemExcel
        //                {
        //                    Value = d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault() == null ? null : d.children.Where(p => p.ExamPaperId == fpd.ExaminationPaperId).FirstOrDefault().SecuredPracticalMark,
        //                    IsHidden = false
        //                });
        //            });
        //        }

        //        gridModelDto.Rows.Add(newRow);
        //    }
        //    return gridModelDto;

        //}

        //public PDFDownloadFile DownloadSemesterExamPdfReport(SearchParamDto input)
        //{
        //    PDFDownloadFile pdfDetails = new PDFDownloadFile();
        //    var predicate = PredicateBuilder.True<ExaminationFormFillUpDetails>();

        //    if (input != null)
        //    {
        //        if (!string.IsNullOrEmpty(input.CourseTypeCode))
        //        {
        //            predicate = predicate.And(i => i.CourseTypeCode.ToLower().Equals(input.CourseTypeCode.ToLower()));
        //        }
        //        if (input.CollegeCode.Count > 0)
        //        {
        //            predicate = predicate.And(i => input.CollegeCode.Contains(i.CollegeCode));
        //        }
        //        if (!string.IsNullOrEmpty(input.StreamCode))
        //        {
        //            predicate = predicate.And(i => i.StreamCode.ToLower().Equals(input.StreamCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.SubjectCode))
        //        {
        //            predicate = predicate.And(i => i.SubjectCode.ToLower().Equals(input.SubjectCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.DepartmentCode))
        //        {
        //            predicate = predicate.And(i => i.DepartmentCode.ToLower().Equals(input.DepartmentCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.CourseCode))
        //        {
        //            predicate = predicate.And(i => i.CourseCode.ToLower().Equals(input.CourseCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.SemesterCode))
        //        {
        //            predicate = predicate.And(i => i.SemesterCode.ToLower().Equals(input.SemesterCode.ToLower()));
        //        }
        //        if (!string.IsNullOrEmpty(input.State))
        //        {
        //            predicate = predicate.And(i => i.State.ToLower().Equals(input.State.ToLower()));
        //        }
        //        if (input.StartYear > 0)
        //        {
        //            predicate = predicate.And(i => i.AcademicStart.Equals(input.StartYear));
        //        }
        //    }

        //    IQueryable<ExaminationPaperMaster> papermaster = _MSUoW.ExaminationPaperMaster.GetAll().Where(s => s.IsActive == true);

        //    IQueryable<ExaminationFormFillUpDetails> ugStudentExaminationDetailsList = _MSUoW.ExaminationFormFillUpDetails.GetAll().
        //                                                                                Where(predicate).Where(u => u.IsActive == true
        //                                                                                && u.State == Constant.MarkUploaded)
        //                                                                                .OrderBy(q => q.RollNumber);

        //    List<ExaminationView> examinaionViewDetails = new List<ExaminationView>();

        //    List<ExaminationFormFillUpDetails> ExamList = new List<ExaminationFormFillUpDetails>();
        //    //gridModelDto.Count = ugStudentExaminationDetailsList.Count();
        //    ExamList = ugStudentExaminationDetailsList.ToList();

        //    List<Guid> instanceids = ExamList.Select(z => z.Id).ToList();

        //    examinaionViewDetails = _MSUoW.ExaminationView.GetAll().Where(w => w.IsActive && instanceids.Contains(w.Id)).ToList();
        //    List<ExaminationFormFillUpSemesterMarks> ExamPaperList = _MSUoW.ExaminationFormFillUpSemesterMarks
        //                                                            .FindBy(p => instanceids.Contains(p.ExaminationFormFillUpDetailsId)
        //                                                            && p.IsActive == true)
        //                                                            .ToList();

        //    var data = from ugsed in examinaionViewDetails
        //               join ugespd in ExamPaperList on ugsed.Id equals ugespd.ExaminationFormFillUpDetailsId
        //               orderby ugespd.RollNumber
        //               join pm in papermaster on ugespd.ExaminationPaperId equals pm.Id
        //               where pm.IsActive = true
        //               orderby pm.CategoryAbbreviation
        //               select new
        //               {
        //                   Id = ugespd.Id,
        //                   InstantId = ugespd.ExaminationFormFillUpDetailsId,
        //                   StudentName = ugsed.StudentName,
        //                   //FatherName = ugsed.FatherName,
        //                  // GuardianName = ugsed.GuardianName,
        //                   //DateOfBirth = ugsed.DateOfBirth,
        //                   RegistrationNumber = ugsed.RegistrationNumber,
        //                   RollNumber = ugsed.RollNumber,
        //                   //AcknowledgementNo = ugsed.AcknowledgementNo,

        //                   CourseTypeCode = ugsed.CourseTypeCode,
        //                   CollegeCode = ugsed.CollegeCode,
        //                   StreamCode = ugsed.StreamCode,
        //                   SubjectCode = ugsed.SubjectCode,
        //                   DepartmentCode = ugsed.DepartmentCode,
        //                   CourseCode = ugsed.CourseCode,
        //                   SemesterCode = ugsed.SemesterCode,
        //                   AcademicStart = ugsed.AcademicStart,

        //                   CourseTypeString = ugsed.CourseTypeString,
        //                   CollegeString = ugsed.CollegeString,
        //                   StreamString = ugsed.StreamString,
        //                   SubjectString = ugsed.SubjectString,
        //                   DepartmentString = ugsed.DepartmentString,
        //                   CourseString = ugsed.CourseString,
        //                   SemesterString = ugsed.SemesterString,

        //                   CategoryAbbreviation = pm.CategoryAbbreviation,
        //                   PaperName = pm.PaperName,
        //                   TotalInternalMark = pm.InternalMark,
        //                   TotalPracticalMark = pm.PracticalMark,
        //                   ToralExternalMark = pm.ExternalMark,
        //                   SecuredInternalMark = ugespd.InternalMark == null ? "--" : ugespd.InternalMark.ToString(),
        //                   SecuredPracticalMark = ugespd.PracticalMark == null ? "--" : ugespd.PracticalMark.ToString(),
        //                   SecuredExternalMark = ugespd.ExternalMark == null ? "--" : ugespd.ExternalMark.ToString(),
        //                   IsActive = ugespd.IsActive,
        //                   ExamPaperId = ugespd.ExaminationPaperId

        //               };

        //    data = data.OrderBy(q => q.StudentName).Where(p => input.CollegeCode.Contains(p.CollegeCode));


        //    if (data.Count() > 0)
        //    {
        //        //get email template
        //        EmailTemplate templateData = _commonAppService.GetEmailTemplateDataFromCache().Where(x => x.TemplateType == (long)enEmailTemplate.SemesterMarkReport).FirstOrDefault();

        //        //creating table header object for college code, college name,stream, subject, academy year
        //        var examDetails = data.Select(x => new
        //        {
        //            CollegeCode = x.CollegeCode,
        //            CollegeName = x.CollegeString,
        //            CourseType = x.CourseTypeCode,
        //            CourseTypeName = x.CourseTypeString,
        //            Stream = x.StreamCode,
        //            StreamName = x.StreamString,
        //            Subject = x.SubjectCode,
        //            SubjectName = x.SubjectString,
        //            Semester = x.SemesterCode,
        //            SemesterName = x.SemesterString,
        //            AcademicStart = x.AcademicStart
        //        }).FirstOrDefault();

        //        //creating table object for pdf
        //        var headerObject = data
        //            .GroupBy(g=>new { g.CourseTypeCode, g.CollegeCode, g.SemesterCode, g.StreamCode, g.CourseCode, g.SubjectCode, g.DepartmentCode, g.AcademicStart, g.RollNumber })
        //            .Select(x => new
        //        {
        //            Name = x.Select(bn => bn.StudentName).FirstOrDefault(),
        //            RollNo = x.Key.RollNumber,
        //            RegNo = x.Select(bn => bn.RegistrationNumber).FirstOrDefault(),
        //            InstantId=x.Select(bn=>bn.InstantId).FirstOrDefault(),
        //                //x.RegistrationNumber,
        //            ExamPaperId = x.Select(bn => bn.ExamPaperId).ToList(),
        //                //x.ExamPaperId,
        //            //CategoryAbbreviation = x.Select(bn => bn.CategoryAbbreviation).FirstOrDefault(),
        //            //    //x.CategoryAbbreviation,
        //            //PaperName = x.Select(bn => bn.PaperName).FirstOrDefault(),
        //            //    //x.PaperName,
        //            //TotalInternalMark = x.Select(bn => bn.TotalInternalMark).FirstOrDefault(),
        //            //    //x.TotalInternalMark,
        //            //TotalPracticalMark = x.Select(bn => bn.TotalPracticalMark).FirstOrDefault(),
        //            //    //x.TotalPracticalMark,
        //            //ToralExternalMark = x.Select(bn => bn.ToralExternalMark).FirstOrDefault(),
        //            //    //x.ToralExternalMark,
        //            //SecuredInternalMark = x.Select(bn => bn.TotalInternalMark).FirstOrDefault() == null ? "--" : x.Select(bn => bn.TotalInternalMark).FirstOrDefault().ToString(),
        //            //    //x.TotalInternalMark == null ? "--" : x.TotalInternalMark.ToString(),
        //            //SecuredPracticalMark = x.Select(bn => bn.SecuredPracticalMark).FirstOrDefault() == null ? "--" : x.Select(bn => bn.SecuredPracticalMark).FirstOrDefault().ToString(),
        //            //    //x.SecuredPracticalMark == null ? "--" : x.SecuredPracticalMark.ToString(),
        //            //SecuredExternalMark = x.Select(bn => bn.SecuredExternalMark).FirstOrDefault() == null ? "--" : x.Select(bn => bn.SecuredExternalMark).FirstOrDefault().ToString(),
        //            //    //x.SecuredExternalMark == null ? "--" : x.SecuredExternalMark.ToString(),
        //        }).OrderBy(o => o.Name)
        //        //.ThenBy(to => to.CategoryAbbreviation)
        //        .ToList();

        //        if (templateData != null)
        //        {
        //            string description = templateData.Body;

        //            description = description.Replace("--collegeName--", examDetails.CollegeName);
        //            description = description.Replace("--collegeCode--", examDetails.CollegeCode);
        //            description = description.Replace("--stream--", examDetails.StreamName);
        //            description = description.Replace("--semester--", examDetails.SemesterName);
        //            description = description.Replace("--courseType--", examDetails.CourseTypeName);
        //            description = description.Replace("--subject--", examDetails.SubjectName);
        //            description = description.Replace("--academicYear--", examDetails.AcademicStart.ToString());

        //            string fixedTableHeaderName = "<td align=\"center\" valign=\"middle\" style=\"width:15%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Student Name</td>" +
        //                                          "<td align=\"center\" valign=\"middle\" style=\"width:15%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Roll No.</td>";
        //            string tableHeaderName = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" colspan=\"3\">--columnName--</td>";
        //            string replaceTableName = string.Empty;
        //            string assignReplaceTableName = string.Empty;
        //            string finalTableHeader = string.Empty;
        //            string IPEString = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">I</td> <td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">E</td> <td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">P</td>";
        //            string SecondHeader = string.Empty;
        //            string ValueRow = "<td align =\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 600;\">--value--</td>";
        //            string replaceValue = string.Empty;
        //            string FinalValue = string.Empty;
        //            string FinalRowValue=string.Empty;

        //            //for(int j=0; j<=100;j++)
        //            //{
        //            //    headerObject.Add(headerObject[j]);
        //            //}

        //            //creating header
        //            for (int i = 0; i < headerObject.Count(); i++)
        //            {
        //                //get exam paper details from exam semester mark table
        //                List<ExaminationFormFillUpSemesterMarks> markList = new List<ExaminationFormFillUpSemesterMarks>();
        //                headerObject[i].ExamPaperId.ForEach(paper =>
        //                {
        //                    ExaminationFormFillUpSemesterMarks paperMark = ExamPaperList.Where(ep => ep.ExaminationPaperId == paper && ep.ExaminationFormFillUpDetailsId == headerObject[i].InstantId).FirstOrDefault();
        //                    ExaminationPaperMaster paperMasterObj = papermaster.Where(w => w.Id == paperMark.ExaminationPaperId).FirstOrDefault();                            

        //                    //get paper code from paper master
        //                    paperMark.CategoryAbbreviation = paperMasterObj.CategoryAbbreviation;
        //                    paperMark.PaperAbbreviation = paperMasterObj.PaperAbbreviation;

        //                    markList.Add(paperMark);
        //                });

        //                FinalValue = string.Empty;
        //                replaceValue = string.Empty;
        //                replaceValue += ValueRow;
        //                FinalValue += replaceValue.Replace("--value--", headerObject[i].Name);
        //                replaceValue = string.Empty;
        //                replaceValue += ValueRow;
        //                FinalValue += replaceValue.Replace("--value--", headerObject[i].RollNo);
        //                if (markList.Count > 0)
        //                {
        //                    markList.ForEach(mark =>
        //                    {
        //                        replaceTableName = tableHeaderName;
        //                        assignReplaceTableName += replaceTableName.Replace("--columnName--", "Paper-"+mark.CategoryAbbreviation+"("+ mark.PaperAbbreviation + ")");
        //                        SecondHeader += IPEString;
        //                        //Bind value for each paper of 3 marks category
        //                        replaceValue = string.Empty;
        //                        replaceValue += ValueRow;
        //                        FinalValue += replaceValue.Replace("--value--", mark.InternalMark== null ? "--": mark.InternalMark.ToString());
        //                        replaceValue = string.Empty;
        //                        replaceValue += ValueRow;
        //                        FinalValue += replaceValue.Replace("--value--", mark.ExternalMark == null ? "--" : mark.ExternalMark.ToString());
        //                        replaceValue = string.Empty;
        //                        replaceValue += ValueRow;
        //                        FinalValue += replaceValue.Replace("--value--", mark.PracticalMark == null ? "--" : mark.PracticalMark.ToString());
        //                    });                          
        //                }
        //                if(i == 0)
        //                {
        //                    finalTableHeader += fixedTableHeaderName;
        //                    finalTableHeader += assignReplaceTableName;
        //                    SecondHeader = "<tr>" + SecondHeader + "</tr>";
        //                    finalTableHeader += SecondHeader;
        //                }

        //                FinalRowValue += "<tr>" + FinalValue + "</tr>";                      
        //                //finalTableHeader += FinalValue; 
        //                // header value section
        //            }

        //            description = description.Replace("--header--", finalTableHeader);
        //            description = description.Replace("--headerValue--", FinalRowValue);

        //            pdfDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
        //            pdfDetails.FileType = "application/octet-stream";
        //            pdfDetails.FileName = "Semester_Mark_Statement_"+examDetails.CollegeCode+".pdf";
        //        }

        //    }


        //    return pdfDetails;
        //}
        public PDFDownloadFile DownloadSemesterExamPdfReport(SearchParamDto input)
        {
            PDFDownloadFile pdfDetails = new PDFDownloadFile();
            var predicate = PredicateBuilder.True<ExaminationFormFillUpDetails>();

            if (input != null)
            {
                if (!string.IsNullOrEmpty(input.CourseTypeCode))
                {
                    predicate = predicate.And(i => i.CourseTypeCode.ToLower().Equals(input.CourseTypeCode.ToLower()));
                }
                if (input.CollegeCode.Count > 0)
                {
                    predicate = predicate.And(i => input.CollegeCode.Contains(i.CollegeCode));
                }
                if (!string.IsNullOrEmpty(input.StreamCode))
                {
                    predicate = predicate.And(i => i.StreamCode.ToLower().Equals(input.StreamCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.SubjectCode))
                {
                    predicate = predicate.And(i => i.SubjectCode.ToLower().Equals(input.SubjectCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.DepartmentCode))
                {
                    predicate = predicate.And(i => i.DepartmentCode.ToLower().Equals(input.DepartmentCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.CourseCode))
                {
                    predicate = predicate.And(i => i.CourseCode.ToLower().Equals(input.CourseCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.SemesterCode))
                {
                    predicate = predicate.And(i => i.SemesterCode.ToLower().Equals(input.SemesterCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.State))
                {
                    predicate = predicate.And(i => i.State.ToLower().Equals(input.State.ToLower()));
                }
                if (input.StartYear > 0)
                {
                    predicate = predicate.And(i => i.AcademicStart.Equals(input.StartYear));
                }
            }

            List<ExaminationPaperMaster> papermaster = _commonAppService.GetActiveExaminationPaperMasterList();

            IQueryable<ExaminationFormFillUpDetails> ugStudentExaminationDetailsList = _MSUoW.ExaminationFormFillUpDetails.GetAll().
                                                                                        Where(predicate).Where(u => u.IsActive == true
                                                                                        && u.State == Constant.MarkUploaded)
                                                                                        .OrderBy(q => q.RollNumber);

            List<ExaminationView> examinaionViewDetails = new List<ExaminationView>();

            List<ExaminationFormFillUpDetails> ExamList = new List<ExaminationFormFillUpDetails>();
            //gridModelDto.Count = ugStudentExaminationDetailsList.Count();
            ExamList = ugStudentExaminationDetailsList.ToList();

            List<Guid> instanceids = ExamList.Select(z => z.Id).ToList();

            examinaionViewDetails = _MSUoW.ExaminationView.GetAll().Where(w => w.IsActive && instanceids.Contains(w.Id)).ToList();
            List<ExaminationFormFillUpSemesterMarks> ExamPaperList = _MSUoW.ExaminationFormFillUpSemesterMarks
                                                                    .FindBy(p => instanceids.Contains(p.ExaminationFormFillUpDetailsId)
                                                                    && p.IsActive == true)
                                                                    .ToList();

            var data = from ugsed in examinaionViewDetails
                       join ugespd in ExamPaperList on ugsed.Id equals ugespd.ExaminationFormFillUpDetailsId
                       orderby ugespd.RollNumber
                       join pm in papermaster on ugespd.ExaminationPaperId equals pm.Id
                       where pm.IsActive = true
                       orderby pm.CategoryAbbreviation
                       select new
                       {
                           Id = ugespd.Id,
                           InstantId = ugespd.ExaminationFormFillUpDetailsId,
                           StudentName = ugsed.StudentName,
                           //FatherName = ugsed.FatherName,
                           // GuardianName = ugsed.GuardianName,
                           //DateOfBirth = ugsed.DateOfBirth,
                           RegistrationNumber = ugsed.RegistrationNumber,
                           RollNumber = ugsed.RollNumber,
                           //AcknowledgementNo = ugsed.AcknowledgementNo,

                           CourseTypeCode = ugsed.CourseTypeCode,
                           CollegeCode = ugsed.CollegeCode,
                           StreamCode = ugsed.StreamCode,
                           SubjectCode = ugsed.SubjectCode,
                           DepartmentCode = ugsed.DepartmentCode,
                           CourseCode = ugsed.CourseCode,
                           SemesterCode = ugsed.SemesterCode,
                           AcademicStart = ugsed.AcademicStart,

                           CourseTypeString = ugsed.CourseTypeString,
                           CollegeString = ugsed.CollegeString,
                           StreamString = ugsed.StreamString,
                           SubjectString = ugsed.SubjectString,
                           DepartmentString = ugsed.DepartmentString,
                           CourseString = ugsed.CourseString,
                           SemesterString = ugsed.SemesterString,

                           CategoryAbbreviation = pm.CategoryAbbreviation,
                           PaperName = pm.PaperName,
                           TotalInternalMark = pm.InternalMark,
                           TotalPracticalMark = pm.PracticalMark,
                           ToralExternalMark = pm.ExternalMark,
                           SecuredInternalMark = ugespd.InternalMark == null ? "--" : ugespd.InternalMark.ToString(),
                           SecuredPracticalMark = ugespd.PracticalMark == null ? "--" : ugespd.PracticalMark.ToString(),
                           SecuredExternalMark = ugespd.ExternalMark == null ? "--" : ugespd.ExternalMark.ToString(),
                           IsActive = ugespd.IsActive,
                           ExamPaperId = ugespd.ExaminationPaperId

                       };

            data = data.OrderBy(q => q.StudentName).Where(p => input.CollegeCode.Contains(p.CollegeCode));


            if (data.Count() > 0)
            {
                //get email template
                EmailTemplate templateData = _commonAppService.GetEmailTemplateDataFromCache().Where(x => x.TemplateType == (long)enEmailTemplate.SemesterMarkReport).FirstOrDefault();

                //creating table header object for college code, college name,stream, subject, academy year
                var examDetails = data.Select(x => new
                {
                    CollegeCode = x.CollegeCode,
                    CollegeName = x.CollegeString,
                    CourseType = x.CourseTypeCode,
                    CourseTypeName = x.CourseTypeString,
                    Stream = x.StreamCode,
                    StreamName = x.StreamString,
                    Subject = x.SubjectCode,
                    SubjectName = x.SubjectString,
                    Semester = x.SemesterCode,
                    SemesterName = x.SemesterString,
                    AcademicStart = x.AcademicStart
                }).FirstOrDefault();

                //creating table object for pdf
                var headerObject = data
                    .GroupBy(g => new { g.CourseTypeCode, g.CollegeCode, g.SemesterCode, g.StreamCode, g.CourseCode, g.SubjectCode, g.DepartmentCode, g.AcademicStart, g.RollNumber })
                    .Select(x => new
                    {
                        Name = x.Select(bn => bn.StudentName).FirstOrDefault(),
                        RollNo = x.Key.RollNumber,
                        RegNo = x.Select(bn => bn.RegistrationNumber).FirstOrDefault(),
                        InstantId = x.Select(bn => bn.InstantId).FirstOrDefault(),
                        //x.RegistrationNumber,
                        ExamPaperId = x.Select(bn => bn.ExamPaperId).ToList(),
                        //x.ExamPaperId,
                        //CategoryAbbreviation = x.Select(bn => bn.CategoryAbbreviation).FirstOrDefault(),
                        //    //x.CategoryAbbreviation,
                        //PaperName = x.Select(bn => bn.PaperName).FirstOrDefault(),
                        //    //x.PaperName,
                        //TotalInternalMark = x.Select(bn => bn.TotalInternalMark).FirstOrDefault(),
                        //    //x.TotalInternalMark,
                        //TotalPracticalMark = x.Select(bn => bn.TotalPracticalMark).FirstOrDefault(),
                        //    //x.TotalPracticalMark,
                        //ToralExternalMark = x.Select(bn => bn.ToralExternalMark).FirstOrDefault(),
                        //    //x.ToralExternalMark,
                        //SecuredInternalMark = x.Select(bn => bn.TotalInternalMark).FirstOrDefault() == null ? "--" : x.Select(bn => bn.TotalInternalMark).FirstOrDefault().ToString(),
                        //    //x.TotalInternalMark == null ? "--" : x.TotalInternalMark.ToString(),
                        //SecuredPracticalMark = x.Select(bn => bn.SecuredPracticalMark).FirstOrDefault() == null ? "--" : x.Select(bn => bn.SecuredPracticalMark).FirstOrDefault().ToString(),
                        //    //x.SecuredPracticalMark == null ? "--" : x.SecuredPracticalMark.ToString(),
                        //SecuredExternalMark = x.Select(bn => bn.SecuredExternalMark).FirstOrDefault() == null ? "--" : x.Select(bn => bn.SecuredExternalMark).FirstOrDefault().ToString(),
                        //    //x.SecuredExternalMark == null ? "--" : x.SecuredExternalMark.ToString(),
                    }).OrderBy(o => o.RollNo)
                //.ThenBy(to => to.CategoryAbbreviation)
                .ToList();

                ExaminationMaster examinationMaster = _commonAppService.GetActiveExaminationMasterList().Where(w =>
                                                       w.CourseType == input.CourseTypeCode &&
                                                       w.StreamCode == input.StreamCode &&
                                                       w.SubjectCode == input.SubjectCode &&
                                                       w.SemesterCode == input.SemesterCode).FirstOrDefault();
                List<ExaminationPaperMaster> allPaperdetails = new List<ExaminationPaperMaster>();
                if (examinationMaster != null)
                {
                    allPaperdetails = papermaster.Where(w => w.ExaminationMasterId == examinationMaster.Id).ToList();

                }

                if (allPaperdetails.Count > 0 && templateData != null)
                {
                    string description = templateData.Body;
                    description = description.Replace("--collegeName--", examDetails.CollegeName);
                    description = description.Replace("--collegeCode--", examDetails.CollegeCode);
                    description = description.Replace("--stream--", examDetails.StreamName);
                    description = description.Replace("--semester--", examDetails.SemesterName);
                    description = description.Replace("--courseType--", examDetails.CourseTypeName);
                    description = description.Replace("--subject--", examDetails.SubjectName);
                    description = description.Replace("--academicYear--", examDetails.AcademicStart.ToString());

                    // "<td align=\"center\" valign=\"middle\" style=\"width:15%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Student Name</td>" 

                    string fixedTableHeaderName = "<td align=\"center\" valign=\"middle\" style=\"width:15%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Roll No.</td>";
                    string tableHeaderName = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" colspan=\"4\">--columnName--</td>";
                    string replaceTableName = string.Empty;
                    string assignReplaceTableName = string.Empty;
                    string finalTableHeader = string.Empty;
                    string IPEString = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">I</td> <td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">T</td> <td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">P</td><td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">Total</td>";
                    string SecondHeader = string.Empty;
                    string ValueRow = "<td align =\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 600;\">--value--</td>";
                    string remarkHeader = "<td align =\"center\" valign=\"middle\" style=\"width:25%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Remarks</td>";
                    string totalSemesterHeader = "<td align =\"center\" valign=\"middle\" style=\"width:25%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Total Semester</td>";
                    string replaceValue = string.Empty;
                    string FinalValue = string.Empty;
                    string FinalRowValue = string.Empty;

                    //for (int j = 0; j <= 100; j++)
                    //{
                    //    headerObject.Add(headerObject[j]);
                    //}
                    //creating header
                    for (int i = 0; i < headerObject.Count(); i++)
                    {
                        //get exam paper details from exam semester mark table
                        List<ExaminationFormFillUpSemesterMarks> markList = new List<ExaminationFormFillUpSemesterMarks>();
                        headerObject[i].ExamPaperId.ForEach(paper =>
                        {
                            ExaminationFormFillUpSemesterMarks paperMark = ExamPaperList.Where(ep => ep.ExaminationPaperId == paper && ep.ExaminationFormFillUpDetailsId == headerObject[i].InstantId).FirstOrDefault();
                            ExaminationPaperMaster paperMasterObj = papermaster.Where(w => w.Id == paperMark.ExaminationPaperId).FirstOrDefault();

                            //get paper code from paper master
                            paperMark.CategoryAbbreviation = paperMasterObj.CategoryAbbreviation;
                            paperMark.PaperAbbreviation = paperMasterObj.PaperAbbreviation;

                            markList.Add(paperMark);
                        });

                        FinalValue = string.Empty;
                        replaceValue = string.Empty;
                        //replaceValue += ValueRow;
                        //FinalValue += replaceValue.Replace("--value--", headerObject[i].Name);
                        //replaceValue = string.Empty;
                        replaceValue += ValueRow;
                        FinalValue += replaceValue.Replace("--value--", headerObject[i].RollNo);

                        //adding mark details
                        if (markList.Count > 0)
                        {
                            long TotalSemester = 0;
                            //Create subject heading
                            allPaperdetails.ForEach((subj) =>
                            {
                                replaceTableName = tableHeaderName;
                                assignReplaceTableName += replaceTableName.Replace("--columnName--", "Paper-" + subj.CategoryAbbreviation + "(" + subj.PaperAbbreviation + ")");
                                replaceTableName = string.Empty;
                                SecondHeader += IPEString;

                                ExaminationFormFillUpSemesterMarks mark = markList.Where(w => w.ExaminationPaperId == subj.Id).FirstOrDefault();
                                if (mark != null)
                                {
                                    //Bind value for each paper of 3 marks category
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", mark.InternalMark == null ? "--" : mark.InternalMark.ToString());
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", mark.ExternalMark == null ? "--" : mark.ExternalMark.ToString());
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", mark.PracticalMark == null ? "--" : mark.PracticalMark.ToString());
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    long? inter = mark.InternalMark != null ? mark.InternalMark : 0;
                                    long? external = mark.ExternalMark != null ? mark.ExternalMark : 0;
                                    long? pratical = mark.PracticalMark != null ? mark.PracticalMark : 0;
                                    long? total = inter + external + pratical;
                                    TotalSemester += (long)total;
                                    FinalValue += replaceValue.Replace("--value--", total == null ? "--" : total.ToString());
                                }
                                //student not apply for this subject 
                                else
                                {

                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                }

                            });
                            replaceValue = string.Empty;
                            replaceValue += ValueRow;
                            FinalValue += replaceValue.Replace("--value--", TotalSemester.ToString());// for total semester
                            replaceValue = string.Empty;
                            replaceValue += ValueRow;
                            FinalValue += replaceValue.Replace("--value--", string.Empty);// for remark
                        }
                        if (i == 0)
                        {
                            finalTableHeader += "<tr>" + fixedTableHeaderName + assignReplaceTableName + totalSemesterHeader + remarkHeader + "</tr>";
                            SecondHeader = "<tr>" + SecondHeader + "</tr>";
                            finalTableHeader += SecondHeader;
                        }

                        FinalRowValue += "<tr>" + FinalValue + "</tr>";
                        // header value section
                    }

                    description = description.Replace("--header--", finalTableHeader);
                    description = description.Replace("--headerValue--", FinalRowValue);

                    pdfDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Landscape);
                    pdfDetails.FileType = "application/octet-stream";
                    pdfDetails.FileName = "Semester_Mark_Statement_" + examDetails.CollegeCode + ".pdf";
                }

            }


            return pdfDetails;
        }
        #endregion

        public IQueryable<ExaminationPaperWiseMarkDto> GetAllPaperWiseMark()
        {
            return _MSUoW.ExaminationFormFillupSemesterMarkRepository.GetAllPaperWiseMark();
        }
        public PDFDownloadFile DownloadPaperWiseMarkReport(SearchParamDto input)
        {
            PDFDownloadFile pdfDetails = new PDFDownloadFile();
            List<ExaminationPaperWiseMarkDto> allStudentDetails = new List<ExaminationPaperWiseMarkDto>();

            #region Validation Start
            if (input == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received." };
            }
            if (string.IsNullOrEmpty(input.CourseTypeCode) || string.IsNullOrWhiteSpace(input.CourseTypeCode))
            {
                throw new MicroServiceException() { ErrorMessage = "Course Type cannot be null." };
            }
            if (input.CollegeCode.Count() == 0)
            {
                throw new MicroServiceException() { ErrorMessage = "College cannot be null." };
            }
            if (string.IsNullOrEmpty(input.StreamCode) || string.IsNullOrWhiteSpace(input.StreamCode))
            {
                throw new MicroServiceException() { ErrorMessage = "Stream cannot be null." };
            }
            if (input.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg))
            {
                if (string.IsNullOrEmpty(input.CourseCode) || string.IsNullOrWhiteSpace(input.StreamCode))
                {
                    throw new MicroServiceException() { ErrorMessage = "Course cannot be null." };
                }
                if (string.IsNullOrEmpty(input.DepartmentCode) || string.IsNullOrWhiteSpace(input.DepartmentCode))
                {
                    throw new MicroServiceException() { ErrorMessage = "Department cannot be null." };
                }
            }
            if (string.IsNullOrEmpty(input.SubjectCode) || string.IsNullOrWhiteSpace(input.SubjectCode))
            {
                throw new MicroServiceException() { ErrorMessage = "Subject cannot be null." };
            }
            if (input.StartYear == 0)
            {
                throw new MicroServiceException() { ErrorMessage = "Academic Start cannot be null." };
            }
            if (string.IsNullOrEmpty(input.SemesterCode) || string.IsNullOrWhiteSpace(input.SemesterCode))
            {
                throw new MicroServiceException() { ErrorMessage = "Semester cannot be null." };
            }
            if (input.PaperId == Guid.Empty)
            {
                throw new MicroServiceException() { ErrorMessage = "Paper cannot be null." };
            }
            if (string.IsNullOrEmpty(input.MarkType) || string.IsNullOrWhiteSpace(input.MarkType))
            {
                throw new MicroServiceException() { ErrorMessage = "Mark Type cannot be null." };
            }
            #endregion

            if (GetRoleListOfCurrentUser().Contains(((long)enRoles.Principal).ToString()))
            {
                allStudentDetails = _MSUoW.ExaminationFormFillupSemesterMarkRepository.GetAllPaperWiseMark().Where(sub =>
                                                                                         sub.CourseTypeCode == input.CourseTypeCode &&
                                                                                         sub.CollegeCode == GetCollegeCode() &&
                                                                                         sub.StreamCode == input.StreamCode &&
                                                                                         sub.AcademicStart == input.StartYear &&
                                                                                         sub.SubjectCode == input.SubjectCode &&
                                                                                         sub.SemesterCode == input.SemesterCode &&
                                                                                         sub.ExaminationPaperId == input.PaperId).OrderBy(r => r.RollNumber).ToList();
            }
            if (GetRoleListOfCurrentUser().Contains(((long)enRoles.AsstCoE).ToString()) || GetRoleListOfCurrentUser().Contains(((long)enRoles.COE).ToString())
                || GetRoleListOfCurrentUser().Contains(((long)enRoles.Level1Support).ToString()) || GetRoleListOfCurrentUser().Contains(((long)enRoles.CompCell).ToString()))
            {
                allStudentDetails = _MSUoW.ExaminationFormFillupSemesterMarkRepository.GetAllPaperWiseMark().Where(sub =>
                                                                                        sub.CourseTypeCode == input.CourseTypeCode &&
                                                                                        sub.StreamCode == input.StreamCode &&
                                                                                        input.CollegeCode.Contains(sub.CollegeCode) &&
                                                                                        sub.AcademicStart == input.StartYear &&
                                                                                        sub.SubjectCode == input.SubjectCode &&
                                                                                        sub.SemesterCode == input.SemesterCode &&
                                                                                        sub.ExaminationPaperId == input.PaperId)
                                                                                        .OrderBy(r => r.RollNumber)
                                                                                        .ToList();
            }
            if (allStudentDetails.Count > 0)
            {
                var studentList = allStudentDetails.GroupBy(g => new { g.CollegeCode, g.CollegeCodeString }).ToList();

                List<long> templateList = new List<long>() { (long)enEmailTemplate.PaperSubjectWiseMarkReportMainContent, (long)enEmailTemplate.PaperSubjectWiseMarkMainContaintValue, (long)enEmailTemplate.PaperSubjectWiseMarkMainBodyValue };

                List<EmailTemplate> templateData = _commonAppService.GetEmailTemplateDataFromCache()
                                            .Where(x => templateList.Contains(x.TemplateType))
                                            .ToList();

                if (templateData.Count == 0)
                {
                    throw new MicroServiceException { ErrorMessage = "No templates details found" };
                }
                else
                {
                    string mainContent = templateData.Where(em => em.TemplateType == (long)enEmailTemplate.PaperSubjectWiseMarkReportMainContent).FirstOrDefault().Body;
                    string headerDescription = templateData.Where(em => em.TemplateType == (long)enEmailTemplate.PaperSubjectWiseMarkMainContaintValue).FirstOrDefault().Body;
                    string bodyDescription = templateData.Where(em => em.TemplateType == (long)enEmailTemplate.PaperSubjectWiseMarkMainBodyValue).FirstOrDefault().Body;

                    long[] lookupList = new long[] { (long)enLookUpType.MarkType };
                    string markDetailsType = _commonAppService.GetLookupData(lookupList).Where(w => w.LookupCode == input.MarkType).FirstOrDefault().LookupDesc;


                    //string tableHeaderName = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000;\" colspan=\"4\">--columnName--</td>";
                    //string pageBrake = "";
                    //string replaceValue = string.Empty;

                    string headerLoop = string.Empty;
                    string tableBody = string.Empty;
                    string finalbody = string.Empty;
                    string finalHtmlContent = string.Empty;
                    string footer = "<div style=\"page-break-before:always;background-color:white !important;text-color:white !important;height:0px !important;padding:0px !important;margin:0px !important;\"></div>";

                    int collegeCount = 1;

                    studentList.ForEach(stu =>
                    {
                        //if (headerCount == 0)
                        //{
                        //filter header details 
                        headerLoop = headerDescription;
                        headerLoop = headerLoop.Replace("--collegeName--", stu.Key.CollegeCodeString);
                        headerLoop = headerLoop.Replace("--collegeCode--", stu.Key.CollegeCode);
                        headerLoop = headerLoop.Replace("--stream--", stu.FirstOrDefault().StreamCodeString);
                        headerLoop = headerLoop.Replace("--semester--", stu.FirstOrDefault().SemesterCode);
                        headerLoop = headerLoop.Replace("--courseType--", stu.FirstOrDefault().CourseTypeCodeString);
                        headerLoop = headerLoop.Replace("--subject--", stu.FirstOrDefault().SubjectCodeString);
                        headerLoop = headerLoop.Replace("--academicYear--", stu.FirstOrDefault().AcademicStart.ToString());
                        headerLoop = headerLoop.Replace("--paper--", stu.FirstOrDefault().PaperName);
                        headerLoop = headerLoop.Replace("--markType--", markDetailsType);
                        //  headerCount = 1;
                        //}
                        int counter = 1;
                        finalbody = string.Empty;
                        foreach (var _student in stu)
                        {
                            tableBody = bodyDescription;
                            tableBody = tableBody.Replace("--slno--", counter.ToString());
                            tableBody = tableBody.Replace("--rollno--", _student.RollNumber);
                            if (input.MarkType == _MSCommon.GetEnumDescription(enMarkEntryType.Internal))
                            {
                                tableBody = tableBody.Replace("--mark--", _student.InternalMarkSecured.ToString());
                            }
                            else if (input.MarkType == _MSCommon.GetEnumDescription(enMarkEntryType.External))
                            {
                                tableBody = tableBody.Replace("--mark--", _student.ExternalMarkSecured.ToString());
                            }
                            else if (input.MarkType == _MSCommon.GetEnumDescription(enMarkEntryType.Practical))
                            {
                                tableBody = tableBody.Replace("--mark--", _student.PracticalMarkSecured.ToString());
                            }
                            finalbody += tableBody;
                            counter++;
                        }

                        headerLoop = headerLoop.Replace("--headerValue--", finalbody);
                        if (studentList.Count > collegeCount)
                        {
                            headerLoop = headerLoop.Replace("--footer--", footer);
                        }
                        else
                        {
                            headerLoop = headerLoop.Replace("--footer--", "");
                        }

                        collegeCount++;

                        finalHtmlContent += headerLoop;
                    });

                    mainContent = mainContent.Replace("--mainContent--", finalHtmlContent);
                    pdfDetails.PdfData = _MSCommon.ExportToPDF(mainContent, enOrientation.Portrait);
                    pdfDetails.FileType = "application/octet-stream";
                    pdfDetails.FileName = "Paper-Subject-Wise-Mark.pdf";
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "No record details found" };
            }
            return pdfDetails;
        }

        public List<TermEndMarkDto> GetAllStudentByCollegeAndSubject(SearchParamDto param)
        {
            List<TermEndMarkDto> marks = new List<TermEndMarkDto>();

            if (param == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data receive." };
            }

            if (param.PaperId == Guid.Empty)
            {
                throw new MicroServiceException() { ErrorMessage = "Please select a Paper." };
            }

            if (GetRoleListOfCurrentUser().Contains(Convert.ToString((long)enRoles.CompCell)))
            {
                //fetch from semestermark list
                var termEndMark = _MSUoW.ExaminationFormFillupSemesterMarkRepository.GetAllPaperWiseMark()
                                    .Where(e => e.IsActive == true
                                    && e.CourseTypeCode == param.CourseTypeCode
                                    && e.CollegeCode == param.CollegeCode.FirstOrDefault()
                                    && e.StreamCode == param.StreamCode
                                    && e.SubjectCode == param.SubjectCode
                                    && e.State == param.State
                                    && e.AcademicStart == param.StartYear
                                    && e.SemesterCode == param.SemesterCode
                                    && e.ExaminationPaperId == param.PaperId)
                                    .Select(s => new TermEndMarkDto
                                    {
                                        Id = s.Id,
                                        ExaminationPaperId = s.ExaminationPaperId,
                                        RollNumber = s.RollNumber,
                                        RegistrationNumber = s.RegistrationNumber,
                                        InternalMarkSecured = s.InternalMarkSecured,
                                        PracticalMarkSecured = s.PracticalMarkSecured,
                                        ExternalMark = s.ExternalMark,
                                        ExternalMarkSecured = s.ExternalMarkSecured,
                                        AcknowledgementNo = s.AcknowledgementNo,
                                        IsExternalAttained = s.IsExternalAttained,
                                        ExaminationMasterId = s.ExaminationMasterId,
                                        InternalMark = s.InternalMark,
                                        PracticalMark = s.PracticalMark
                                    })
                                    .ToList();

                if (termEndMark.Any())
                {
                    marks.AddRange(termEndMark);
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorMessage = "OUU093" };
            }

            return marks;
        }

        public bool SaveTermEndMarkAllStudentByCollege(List<TermEndMarkDto> input)
        {
            bool status = false;
            if (!input.Any())
            {
                throw new MicroServiceException() { ErrorMessage = "Invalid data received." };
            }

            //only one paper need to select at a time
            if (input.Select(x => x.ExaminationPaperId).Distinct().ToList().Count() > 1 || input.Select(x => x.ExaminationMasterId).Distinct().ToList().Count() > 1)
            {
                throw new MicroServiceException() { ErrorMessage = "Only one paper will be update per one request." };
            }

            //validate individual items to checks whether each term end mark is entered or not.
            ExaminationPaperMaster paper = _commonAppService.GetActiveExaminationPaperMasterList()
                                                    .Where(x => x.IsActive
                                                    && input.FirstOrDefault().ExaminationPaperId == x.Id)
                                                    .FirstOrDefault();

            input.ForEach(mark =>
            {
                if (mark.IsExternalAttained == false && (mark.ExternalMarkSecured < 0 || mark.ExternalMarkSecured == null))
                    throw new MicroServiceException() { ErrorMessage = "External mark cannot be less than 0 or null for " + paper.PaperName + "." };
                if (mark.IsExternalAttained == false && (mark.ExternalMarkSecured > paper.ExternalMark))
                    throw new MicroServiceException() { ErrorMessage = "External mark cannot greater than " + paper.ExternalMark + " for " + paper.PaperName + "." };
            });

            if (GetRoleListOfCurrentUser().Contains(Convert.ToString((long)enRoles.CompCell)))
            {
                //checked if the compcell has assign to perticular subject which paper marks he wants to update                
                RoleUserWiseSubjectMapper roleSubject = _commonAppService.GetRoleUserWiseSubject();
                if (roleSubject == null)
                {
                    throw new MicroServiceException() { ErrorMessage = "User is not authorized to add term end marks." };
                }

                //check the paperId belong to the role assign subject
                var subject = _MSUoW.SubjectMaster.GetAll().Where(x => x.IsActive && x.LookupCode == roleSubject.SubjectCode).FirstOrDefault();

                //get examinationmasterdata
                var exaMst = _MSUoW.ExaminationMaster.GetAll().Where(x => x.IsActive
                            && x.Id == input.FirstOrDefault().ExaminationMasterId
                            && x.SubjectCode == subject.LookupCode)
                            .FirstOrDefault();

                if (exaMst == null)
                {
                    throw new MicroServiceException() { ErrorMessage = "Trying to add marks from another paper code.Please contact support." };
                }

                //update semester mark table
                List<ExaminationFormFillUpSemesterMarks> semesterMarkList = _MSUoW.ExaminationFormFillUpSemesterMarks.GetAll()
                     .Where(x => x.IsActive && input.Select(g => g.Id).Contains(x.ExaminationFormFillUpDetailsId)
                     && paper.Id == x.ExaminationPaperId
                     && input.Select(a => a.AcknowledgementNo).Contains(x.AcknowledgementNo)
                     && input.Select(a => a.RollNumber).Contains(x.RollNumber))
                     .ToList();

                if (semesterMarkList.Count() > 0)
                {
                    semesterMarkList.ForEach(sem =>
                    {
                        var termEndRecord = input.Where(x => x.AcknowledgementNo == sem.AcknowledgementNo
                                            && x.RollNumber == sem.RollNumber
                                            && x.Id == sem.ExaminationFormFillUpDetailsId)
                                             .FirstOrDefault();
                        if (termEndRecord != null)
                        {
                            sem.IsExternalAttained = termEndRecord.IsExternalAttained;
                            sem.ExternalMark = termEndRecord.ExternalMarkSecured;
                        }
                    });

                    _MSUoW.ExaminationFormFillUpSemesterMarks.UpdateRange(semesterMarkList);
                    _MSUoW.Commit();
                    status = true;
                }
                else
                {
                    throw new MicroServiceException() { ErrorMessage = "No record found for update term end mark." };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorMessage = "OUU093" };
            }

            return status;
        }
    }
}



