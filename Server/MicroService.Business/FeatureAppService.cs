﻿using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.Feature;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business
{
    public class FeatureAppService : BaseService, IFeatureAppService
    {
        private IMicroServiceUOW _MSUoW;
        private List<FeatureRetrieveDto> parentFeatures = new List<FeatureRetrieveDto>();

        public FeatureAppService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData
            ) : base(redisData)
        {
            _MSUoW = MSUoW;
        }

        #region USER AUTHORIZATION
        public AuthorizedRoleFeatureDto GetUserAuthorizedDetails()
        {

            //Get User with assigned roles
            var user = _MSUoW.UserRepository.GetAllUsersData().Where(x => x.Id == GetCurrentUserId()).FirstOrDefault();
                //.FirstOrDefault(x => x.Id == AbpSession.UserId);

            //Get Features assigned to the roles
            user.RoleIds = user.UserRoles.Select(r => r.RoleId).ToArray();
            bool isAdmin = IsAdminRole(user.RoleIds);
            var assignedFeatureAndApis = _MSUoW.RoleFeatureApiMapper.GetAll()
                .Where(p => user.RoleIds.Contains(p.RoleId) && p.IsActive).ToList();

            var featureIds = assignedFeatureAndApis.Select(f => f.FeatureId).ToArray();
            var apiIds = assignedFeatureAndApis.Select(f => f.ApiDetailId).ToArray();

            //List of All Features
            var allFeatures = AutoMapper.Mapper.Map<List<FeatureRetrieveDto>>(_MSUoW.Feature.GetAll().Where(p => p.IsActive).ToList());

            //List of distinct Features
            var distinctFeatures = isAdmin ? allFeatures
                : allFeatures.Where(p => featureIds.Contains(p.Id)).ToList();

            // Test
            var reTrue= _MSUoW.ApiDetail.GetAll().Where(p => p.IsActive).ToList();
            var reFals = _MSUoW.ApiDetail.GetAll().Where(p => apiIds.Contains(p.Id) && p.IsActive).ToList();

            //Get Distinct ApiDetails List
            var apiList = isAdmin ? AutoMapper.Mapper.Map<List<ApiDetailRetrieveDto>>(_MSUoW.ApiDetail.GetAll().Where(p => p.IsActive).ToList())
                : AutoMapper.Mapper.Map<List<ApiDetailRetrieveDto>>(_MSUoW.ApiDetail.GetAll().Where(p => apiIds.Contains(p.Id) && p.IsActive).ToList());

            //Get the feature Heirarchy for dynamic menu
            var featureHeirarchy = AuthorizedFeatureTreeDto.BuildTree(PrepareFlattenTreeFeatureData(allFeatures, distinctFeatures));

            //FeatureApiMapper List
            var featureApiMapper = isAdmin ? AutoMapper.Mapper.Map<List<FeatureApiMapperRetrieveDto>>(_MSUoW.FeatureApiMapper.GetAll().Where(p => p.IsActive).ToList())
                : AutoMapper.Mapper.Map<List<FeatureApiMapperRetrieveDto>>(_MSUoW.FeatureApiMapper.GetAll().ToList()
                .Where(p => p.IsActive && featureIds.Contains(p.FeatureId) && apiIds.Contains(p.ApiDetailId)).ToList());

            //Set the apilist for features
            distinctFeatures.ForEach(p => p.FeatureApiMappers = featureApiMapper
            .Where(x => x.FeatureId == p.Id).ToList());
            distinctFeatures.ForEach(p => p.ApiList = apiList
            .Where(x => p.FeatureApiMappers.Select(q => q.ApiDetailId).ToArray().Contains(x.Id)).ToList());

            var result = new AuthorizedRoleFeatureDto
            {
                FirstName = user != null ? user.Name : "",
                LastName = user != null ? user.Surname : "",
                AssignedFeatures = MapAuthorizedFeatureDto(distinctFeatures),
                FeatureHeirarchy = featureHeirarchy,
                Roles = string.Join(",", new List<string>(GetRoleListOfCurrentUser()).ToArray())                
            };

            //Wrap the results and send response
            return result;
        }

        private bool IsAdminRole(long[] roles)
        {
            return (_MSUoW.Role.GetAll().Where(t => roles.Contains(t.Id) && t.Name == "Admin")
                .FirstOrDefault() != null) ? true : false;
        }

        private List<AuthorizedFeatureDto> MapAuthorizedFeatureDto(List<FeatureRetrieveDto> features)
        {
            var distinctFeatures = new List<AuthorizedFeatureDto>();
            features.ForEach(p => distinctFeatures.Add(new AuthorizedFeatureDto
            {
                Id = p.Id,
                ApiList = p.ApiList,
                FeatureApiMappers = p.FeatureApiMappers,
                FeatureOrderId = p.FeatureOrderId,
                Name = p.Name,
                ParentId = p.ParentId,
                Sequence = p.Sequence,
                IsActive = p.IsActive
            }));
            return distinctFeatures;
        }

        /// <summary>
        /// This method is used to generate the
        /// flatten data to build access tree.
        private List<AuthorizedFeatureDto> PrepareFlattenTreeFeatureData(List<FeatureRetrieveDto> allFeatures, List<FeatureRetrieveDto> assignedFeatures)
        {
            parentFeatures = new List<FeatureRetrieveDto>();
            assignedFeatures.ForEach(p => {
                AddParentNode(p, allFeatures);
            });

            return MapAuthorizedFeatureDto(assignedFeatures.Union(parentFeatures).OrderBy(p => p.ParentId).ToList());
        }

        private bool AddParentNode(FeatureRetrieveDto feature, List<FeatureRetrieveDto> allFeatures)
        {
            bool parentAdded = false;
            if (feature.ParentId != 0)
            {
                var parentFeature = allFeatures.Where(q => q.Id == feature.ParentId).FirstOrDefault();
                parentFeatures.Add(parentFeature);
                parentAdded = true;
                AddParentNode(parentFeature, allFeatures);
            }
            return parentAdded;
        }
        #endregion

        #region FEATURE SECTION
        //create feature
        public FeatureDto CreateFeature(FeatureCreateDto input)
        {
            var featureDetail = AutoMapper.Mapper.Map<Feature>(input);
            _MSUoW.Feature.Add(featureDetail);
            _MSUoW.Commit();
            return AutoMapper.Mapper.Map<FeatureDto>(featureDetail);
        }
        
        //update feature 
        public FeatureDto UpdateFeature(long id, FeatureDto input)
        {
            Feature featureDetail = _MSUoW.Feature.GetById(id);
            Feature featureMapper = new Feature();  //assigning  variable for new object of feature type
            if (featureDetail != null)
            {
                featureMapper = AutoMapper.Mapper.Map<FeatureDto, Feature>(input);
                _MSUoW.Feature.Update(featureMapper);
                _MSUoW.Commit();
            }            
            return AutoMapper.Mapper.Map<FeatureDto>(featureDetail);
        }

        //get feature
        public Feature GetFeature(long id)
        {
            return _MSUoW.Feature.GetById(id);
        }

        public bool DeleteFeature(long id)
        {
            bool deletFeature = false;
            var featureDetail = _MSUoW.Feature.GetById(id);
            if (featureDetail != null)
            {
                //assigning isactive to false for soft delete
                featureDetail.IsActive = false;
                _MSUoW.Feature.Update(featureDetail);
                _MSUoW.Commit();
                deletFeature = true;
            }
            return deletFeature;
        }

        public List<FeatureView> GetAllFeature()
        {
            return _MSUoW.FeatureView.GetAll().Where(s => s.IsActive).ToList();
        }
        #endregion

        #region FEATUREORDER SECTION
        public FeatureOrderDto CreateFeatureOrder(FeatureOrderCreateDto input)
        {
            var featureOrder = AutoMapper.Mapper.Map<FeatureOrder>(input);
            _MSUoW.FeatureOrder.Add(featureOrder);
            _MSUoW.Commit();
            return AutoMapper.Mapper.Map<FeatureOrderDto>(featureOrder);
        }

        public FeatureOrderDto UpdateFeatureOrder(long id, FeatureOrderDto input)
        {
            FeatureOrder featureOrder = _MSUoW.FeatureOrder.GetById(id);
            FeatureOrder featureOrderMapper = new FeatureOrder();
            if (featureOrder != null)
            {
                featureOrderMapper = AutoMapper.Mapper.Map<FeatureOrderDto, FeatureOrder>(input);
                _MSUoW.FeatureOrder.Update(featureOrderMapper);
                _MSUoW.Commit();
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU002" };
            }
            return AutoMapper.Mapper.Map<FeatureOrderDto>(featureOrderMapper);
        }

        public FeatureOrder GetFeatureOrder(long id)
        {
            return _MSUoW.FeatureOrder.GetById(id);
        }

        public bool DeleteFeatureOrder(long id)
        {
            bool status = false;
            var featureOrder = _MSUoW.FeatureOrder.GetById(id);
            if (featureOrder != null)
            {
                featureOrder.IsActive = false;
                _MSUoW.FeatureOrder.Update(featureOrder);
                _MSUoW.Commit();
                status = true;               
            }
            return status;
        }
        
        public List<FeatureOrder> GetAllFeatureOrder()
        {
            return _MSUoW.FeatureOrder.GetAll().Where(s => s.IsActive).ToList();
        }

        /// <summary>
        /// Get list of active Features related to the feature order
        /// and also the list contains the individuals apimappers
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public List<FeatureView> GetAllFeatureByOrderId(long orderId)
        {
            return _MSUoW.FeatureView.GetAll().Where(s => s.FeatureOrderId == orderId && s.IsActive).ToList();
        }
        #endregion

        #region API DETAILS
        public ApiDetail CreateApiDetail(ApiDetailsDto input)
        {
            ApiDetail inputMapper = new ApiDetail();
            if (input != null)
            {
                inputMapper = AutoMapper.Mapper.Map<ApiDetail>(input);
                _MSUoW.ApiDetail.Add(inputMapper);
                _MSUoW.Commit();
            }
            else
            {
                throw new MicroServiceException { ErrorCode = "OUU003" };
            }
            return inputMapper;
        }

        public List<ApiDetail> GetApiDetailsList()
        {
            return _MSUoW.ApiDetail.GetAll().Where(p => p.IsActive).ToList();
        }

        public List<FeatureApiMapper> GetFeatureApiMapperList()
        {
            //List<FeatureApiMapper> resultList = _MSUoW.FeatureApiMapperRepository.GetAllFeatureApi().Where(p => p.IsActive).ToList();
            List<FeatureApiMapper> resultList = _MSUoW.FeatureApiMapper.FindBy(p => p.IsActive).ToList();
            if (resultList.Count > 0)
            {
                List<long> apiIds = resultList.Select(s => s.ApiDetailId).ToList();
                List<ApiDetail> apiDetails = _MSUoW.ApiDetail.GetAll().Where(s => apiIds.Contains(s.Id)).ToList();
                resultList.ForEach(s =>
                {
                    s.ApiName = (apiDetails != null && apiDetails.Count > 0) ? apiDetails.Where(d => d.Id == s.ApiDetailId).FirstOrDefault().Name : string.Empty;
                });
            }      
            return resultList;
        }
        #endregion

        public IQueryable<FeatureView> GetAllFeaturesData()
        {
            return _MSUoW.FeatureView.GetAll().AsQueryable();
        }

    }
}
