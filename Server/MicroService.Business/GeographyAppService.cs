﻿using AutoMapper;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.GeographyOder;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business
{
    public class GeographyAppService : BaseService, IGeographyAppService
    {
        private IMicroServiceUOW _MSUoW;       

        public GeographyAppService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData
            ) : base(redisData)
        {
            _MSUoW = MSUoW;
        }

        public GeographyOrderDto CreateGeographyOrder(GeographyOrderCreateDto input)
        {
            var geographyOrder = AutoMapper.Mapper.Map<GeographyOrder>(input);
            _MSUoW.GeographyOrder.Add(geographyOrder);
            _MSUoW.Commit();            
            return AutoMapper.Mapper.Map<GeographyOrder,GeographyOrderDto > (geographyOrder);

            //await _geographyOrderRepository.InsertAsync(geographyOrder);
            //return Mapper.Map<GeographyOrderDto>(geographyOrder);
        }

        public GeographyOrderDto UpdateGeographyOrder(long id, GeographyOrderDto input)
        {
            GeographyOrder geographyOrder = _MSUoW.GeographyOrder.GetById(id);
            GeographyOrder geographyOrderMapper = new GeographyOrder();
            if(geographyOrder !=null)
            {
                geographyOrderMapper = AutoMapper.Mapper.Map(input, geographyOrder);
                _MSUoW.GeographyOrder.Update(geographyOrderMapper);
                _MSUoW.Commit();
            }
            //await _geographyOrderRepository.GetAsync(id);
            //Mapper.Map(input, geographyOrder);
            //await _geographyOrderRepository.UpdateAsync(geographyOrder);
            return AutoMapper.Mapper.Map<GeographyOrderDto>(geographyOrder);
        }

        public List<GeographyOrder> GetAllGeographyOrder()
        {
            return _MSUoW.GeographyOrder.GetAll().Where(s => s.IsActive).ToList();
        }

        public GeographyOrder GetGeographyOrder(long id)
        {
            return _MSUoW.GeographyOrder.GetById(id);
               // await _geographyOrderRepository.GetAsync(id);
        }

        public bool Delete(long id)
        {
            bool result = false;
            var geographyOrder = _MSUoW.GeographyOrder.GetById(id);
            if (geographyOrder != null)
            {
                geographyOrder.IsActive = false;
                _MSUoW.GeographyOrder.Update(geographyOrder);
                _MSUoW.Commit();
                result = true;
            }
            return result;
            //await _geographyOrderRepository.GetAsync(id);
            //geographyOrder.DeleteGeographyOrder();
            //await _geographyOrderRepository.UpdateAsync(geographyOrder);
        }

        public GeographyDetailDto CreateGeographyDetail(GeographyDetailCreateDto input)
        {
            if (input != null)
            {
                var geographyDetail = Mapper.Map<GeographyDetail>(input);

                // Check unique ness of geography
                var existingSameNamedGeography = _MSUoW.GeographyDetail.FindBy(geo => geo.Name.ToLower() == geographyDetail.Name.ToLower()).FirstOrDefault();
                if(existingSameNamedGeography == null)
                {
                    // ABP code
                    //geographyDetail.CreateGeographyDetail();
                    //await _geographyDetailRepository.InsertAsync(geographyDetail);

                    _MSUoW.GeographyDetail.Add(geographyDetail);
                    _MSUoW.Commit();
                    return Mapper.Map<GeographyDetailDto>(geographyDetail);
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU004" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU005" };
            }
        }

        public GeographyDetailDto UpdateGeographyDetail(long id, GeographyDetailDto input)
        {
            if (input != null)
            {
                // Check whether the record is available in database or not.
                GeographyDetail geographyDetail = _MSUoW.GeographyDetail.GetById(id);
                //GeographyDetail geographyDetailMapper = new GeographyDetail();
                if (geographyDetail != null)
                {
                    // Automap with server data
                    geographyDetail = Mapper.Map(input, geographyDetail);
                    // Check unique ness of geography
                    var existingSameNamedGeography = _MSUoW.GeographyDetail.FindBy(geo => geo.Name.ToLower() == geographyDetail.Name.ToLower()).FirstOrDefault();
                    if (existingSameNamedGeography == null || (existingSameNamedGeography != null && existingSameNamedGeography.Id == id))
                    {
                        _MSUoW.GeographyDetail.Update(geographyDetail);
                        _MSUoW.Commit();
                        return Mapper.Map<GeographyDetailDto>(geographyDetail);
                    }
                    else
                    {
                        throw new MicroServiceException() { ErrorCode = "OUU006" };
                    }
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU007" };
                }
                //await _geographyDetailRepository.GetAsync(id);

                //await _geographyDetailRepository.UpdateAsync(geographyDetail);
                
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU005" };
            }
        }

        public GeographyDetail GetGeographyDetail(long id)
        {
            return _MSUoW.GeographyDetail.GetById(id);
        }

        public List<GeographyDetail> GetAllGeographyDetail()
        {
            return _MSUoW.GeographyDetail.GetAll().Where(s => s.IsActive).ToList();
        }

        public bool DeleteGeographyDetail(long id)
        {
            bool result = false;
            var geographyDetail = _MSUoW.GeographyDetail.GetById(id);
            if (geographyDetail != null)
            {
                geographyDetail.IsActive = false;
                _MSUoW.GeographyDetail.Update(geographyDetail);
                _MSUoW.Commit();
                result = true;
            }
            return result;

            //await _geographyDetailRepository.GetAsync(id);
            //geographyDetail.DeleteGeographyDetail();
            //await _geographyDetailRepository.UpdateAsync(geographyDetail);
        }

        public IQueryable<GeographyDetailView> GetAllGeographysData()
        {
            return _MSUoW.GeographyDetailView.GetAll().AsQueryable();
        }
    }
}
