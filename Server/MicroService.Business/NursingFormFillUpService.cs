﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.Core;
using MicroService.Core.WorkflowEntity;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Dto.Workflow;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.Nursing;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Nursing;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TimeZoneConverter;
using MicroService.Domain.Models.Views;
using System.Threading.Tasks;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.DataAccess;
using MicroService.DataAccess.Helpers;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity.Lookup;

namespace MicroService.Business
{
    public class NursingFormFillUpService : BaseService, INursingFormFillUpService
    {
        private IMicroServiceUOW _MSUoW;
        private readonly IHostingEnvironment _environment;
        private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        //private MetadataAssemblyInfo workflowHelper;
        private WorkflowDbContext _workflowDbContext;
        private IMicroserviceCommon _MSCommon;
        private IConfiguration _IConfiguration;
        private IRedisDataHandlerService _redisData;
        private IMicroServiceLogger _logger;
        private readonly string WorkflowName = "";
        private ICommonAppService _commonAppService;

        public NursingFormFillUpService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IConfiguration IConfiguration,
            IHostingEnvironment environment,
            IWFTransactionHistroryAppService WFTransactionHistroryAppService,
            IMicroserviceCommon MSCommon,
            IMicroServiceLogger logger,
            ICommonAppService commonAppService
          ) : base(redisData)
        {
            _MSUoW = MSUoW;
            _redisData = redisData;
            _logger = logger;
            _environment = environment;
            //_IConfiguration = IConfiguration;
            _WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            _workflowDbContext = new WorkflowDbContext();
            //workflowHelper = new MetadataAssemblyInfo();
            _MSCommon = MSCommon;
            _IConfiguration = IConfiguration;
            WorkflowName = _IConfiguration.GetSection("NursingFormFillup:WorkflowName").Value;
            _commonAppService = commonAppService;
        }

        #region WORKFLOW
        public NursingFormFillUpDto VerificationByPrincipal(Guid id, NursingFormFillUpDto input)
        {
            #region Step b starts
            if (input == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU054" };
            }
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.NursingFormfillupDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<NursingFormfillupDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.NursingFormfillupDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<NursingFormfillupDetails, NursingFormFillUpDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users applyingStudent;
                    applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();

                    if (applyingStudent != null)
                    {// Here is a student
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.NursingApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();

                        //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId);
                        //if (IsLastTransition == true)
                        //{// Rejected
                        //    SendEmailAndSMSOnApplicationRejection(applyingStudent.EmailAddress, applyingStudent.PhoneNumber, applyingStudent.Name, serviceName);
                        //}
                        //else // Verified
                        //{
                        // Sending Email
                        if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                        {
                            SendEmailToStudentToPay(applyingStudent.EmailAddress, applyingStudent.Name, serviceName);
                        }

                        // Sending SMS
                        if (serviceName != null && serviceName != string.Empty && applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                        {
                            SendSMSToStudentToPay(applyingStudent.PhoneNumber, applyingStudent.Name);
                        }
                        //}





                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        public NursingFormFillUpDto RejectedByPrincipal(Guid id, NursingFormFillUpDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.NursingFormfillupDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<NursingFormfillupDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    //input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.NursingFormfillupDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<NursingFormfillupDetails, NursingFormFillUpDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users nextApprovingUser;
                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();


                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                    string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.NursingApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    string examYear = DateTime.UtcNow.Year.ToString();

                    var examinationMaster = _MSUoW.ExaminationMaster.FindBy(examMstr =>
                        examMstr.CourseType == examinationFormFillUpDetails.CourseTypeCode
                        && examMstr.StreamCode == examinationFormFillUpDetails.StreamCode
                        && examMstr.SubjectCode == examinationFormFillUpDetails.SubjectCode
                        && examMstr.CourseCode == examinationFormFillUpDetails.CourseCode
                        && examMstr.DepartmentCode == examinationFormFillUpDetails.DepartmentCode
                        && examMstr.SemesterCode == examinationFormFillUpDetails.CurrentAcademicYear
                    ).FirstOrDefault();

                    if (examinationMaster != null)
                    {
                        var examinationSession = _MSUoW.ExaminationSession.FindBy(examSsn => examSsn.ExaminationMasterId == examinationMaster.Id && examSsn.StartYear == examinationFormFillUpDetails.AcademicStart).FirstOrDefault();
                        if (examinationSession != null)
                        {
                            examYear = examinationSession.ExamYear.ToString();
                        }
                    }

                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == examinationFormFillUpDetails.CollegeCode).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion

                    #region Step k
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        // get the principal hod remarks for rejection
                        var triggerParameter = input.PossibleWorkflowTransitions.Where(p => p.IsActive && p.Trigger.Trim().ToLower() == trigger.Trim().ToLower()
                                     && p.TransitState.Trim().ToLower() == examinationFormFillUpDetails.State.Trim().ToLower() && p.SourceState.Trim().ToLower() == sourceState.Trim().ToLower())
                                     .Select(ds => ds.TriggerParameters).FirstOrDefault();
                        TriggerParameterDto[] arr = JsonConvert.DeserializeObject<TriggerParameterDto[]>(triggerParameter);
                        if (arr.Count() > 0)
                        {
                            var columnName = arr.FirstOrDefault().PropertyName;
                            string rejectedRemarks = examinationFormFillUpDetails.GetType().GetProperty(columnName).GetValue(examinationFormFillUpDetails).ToString();

                            var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();
                            if (applyingStudent != null)
                            {
                                SendEmailAndSMSOnApplicationRejection(applyingStudent.EmailAddress, applyingStudent.PhoneNumber, applyingStudent.UserName, serviceName, rejectedRemarks);
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        public NursingFormFillUpDto UpdateState(Guid id, NursingFormFillUpDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.NursingFormfillupDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<NursingFormfillupDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.NursingFormfillupDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<NursingFormfillupDetails, NursingFormFillUpDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users nextApprovingUser;
                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();


                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                    string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.NursingApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                    string examYear = DateTime.UtcNow.Year.ToString();

                    var examinationMaster = _MSUoW.ExaminationMaster.FindBy(examMstr =>
                        examMstr.CourseType == examinationFormFillUpDetails.CourseTypeCode
                        && examMstr.StreamCode == examinationFormFillUpDetails.StreamCode
                        && examMstr.SubjectCode == examinationFormFillUpDetails.SubjectCode
                        && examMstr.CourseCode == examinationFormFillUpDetails.CourseCode
                        && examMstr.DepartmentCode == examinationFormFillUpDetails.DepartmentCode
                        && examMstr.SemesterCode == examinationFormFillUpDetails.CurrentAcademicYear
                    ).FirstOrDefault();

                    if (examinationMaster != null)
                    {
                        var examinationSession = _MSUoW.ExaminationSession.FindBy(examSsn => examSsn.ExaminationMasterId == examinationMaster.Id && examSsn.StartYear == examinationFormFillUpDetails.AcademicStart).FirstOrDefault();
                        if (examinationSession != null)
                        {
                            examYear = examinationSession.ExamYear.ToString();
                        }
                    }

                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == examinationFormFillUpDetails.CollegeCode).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion

                    #region Step k
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null)
                        {
                            SendEmailAndSMSOnAdmitCardGenerated(applyingStudent.EmailAddress, applyingStudent.PhoneNumber, applyingStudent.UserName, serviceName, examYear);
                            //SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, examinationFormFillUpDetails.RegistrationNumber);
                        }

                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        private void UpdateUserServiceAppliedDetail(NursingFormfillupDetails examinationFormFillUpDetails)
        {
            UserServiceApplied usrSrvcApplied = _MSUoW.UserServiceApplied.FindBy(usrSrvc => usrSrvc.InstanceId == examinationFormFillUpDetails.Id).OrderByDescending(usrSrvc => usrSrvc.CreationTime).FirstOrDefault();
            if (usrSrvcApplied != null)
            {
                usrSrvcApplied.LastStatus = examinationFormFillUpDetails.State;
                try
                {
                    var actionRequiringStateListForStudent = (_IConfiguration.GetSection("NursingFormFillup:ActionRequiringStates").Value).Split(",");
                    if (actionRequiringStateListForStudent.Count() > 0 && Array.IndexOf(actionRequiringStateListForStudent, usrSrvcApplied.LastStatus) != -1)
                    {// Latest state requires student action
                        usrSrvcApplied.IsActionRequired = true;
                    }
                    else
                    {// Latest state does not require student action
                        usrSrvcApplied.IsActionRequired = false;
                    }
                }
                catch (Exception e) { usrSrvcApplied.IsActionRequired = false; }
                _MSUoW.UserServiceApplied.Update(usrSrvcApplied);
            }
        }

        public NursingFormFillUpDto PaymentCompleted(Guid id, NursingFormFillUpDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var examinationFormFillUpDetails = _MSUoW.NursingFormfillupDetails.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = _IConfiguration.GetSection("NursingFormFillup:PaymentTrigger").Value;
            #endregion
            if (examinationFormFillUpDetails != null && input.Id == id)
            {
                string sourceState = examinationFormFillUpDetails.State;
                if (examinationFormFillUpDetails.TenantWorkflowId != 0)
                {
                    #region Step c
                    examinationFormFillUpDetails.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<NursingFormfillupDetails>(examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId, trigger, input, ref examinationFormFillUpDetails, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(examinationFormFillUpDetails, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == examinationFormFillUpDetails.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == examinationFormFillUpDetails.State).ToList();
                    UpdateUserServiceAppliedDetail(examinationFormFillUpDetails);
                    #endregion

                    #region Step f
                    _MSUoW.NursingFormfillupDetails.Update(examinationFormFillUpDetails);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<NursingFormfillupDetails, NursingFormFillUpDto>(examinationFormFillUpDetails);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    Users nextApprovingUser;
                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == examinationFormFillUpDetails.CreatorUserId).FirstOrDefault();

                    if (nextApprovingUser != null)
                    {
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.NursingApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                        if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                            SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                    }


                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, examinationFormFillUpDetails.LastStatusUpdateDate, sourceState, trigger, examinationFormFillUpDetails.State, examinationFormFillUpDetails.TenantWorkflowId);
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }

        private bool IsLastTransitionOfWorkflow(List<TenantWorkflowTransitionConfig> possibleTransitions, string currentState, long? parentWFId)
        {
            bool isLastStateTransition = false;
            var transitionsAvailable = possibleTransitions.Where(transition => transition.SourceState == currentState).FirstOrDefault();
            if (transitionsAvailable == null)
            {
                isLastStateTransition = true;
            }
            return isLastStateTransition;
        }

        private bool SendEmailOnWorkflowCompletion(string emailAddress, string userName, string regNumber)
        {// Needs modification. Create email template
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGenerated && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--regNo--", regNumber);
                    // Needs modification. Create email template
                    //_MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        private bool SendEmailAndSMSOnApplicationRejection(string emailAddress, string phoneNumber, string userName, string serviceName, string rejectedRemarks)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupRejectionEmail && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--rmks--", rejectedRemarks);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--url--", applicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            if (phoneNumber != null && phoneNumber != string.Empty && phoneNumber.Length == 10)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupRejectionSMS && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string contentId = string.Empty;
                    contentId = templateData.SMSContentId;
                    try
                    {
                        string apiUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                        string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                        StringBuilder body = new StringBuilder(templateData.Body);
                        body.Replace("--studentName--", userName);
                        body.Replace("--url--", applicationUrl);

                        OTPMessage otpObj = new OTPMessage();
                        otpObj.Message = body.ToString();
                        otpObj.MobileNumber = phoneNumber;
                        _MSCommon.SendSMS(apiUrl, otpObj, contentId);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e.Message);
                    }
                }
            }
            return true;
        }

        private bool SendEmailAndSMSOnAdmitCardGenerated(string emailAddress, string phoneNumber, string userName, string serviceName, string year)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupAdmitCardGenerated && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--studentName--", userName);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--year--", year);
                    body.Replace("--url--", applicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            if (phoneNumber != null && phoneNumber != string.Empty && phoneNumber.Length == 10)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupAdmitCardGeneratedSMS && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string contentId = string.Empty;
                    contentId = templateData.SMSContentId;
                    try
                    {
                        string apiUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                        string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                        StringBuilder body = new StringBuilder(templateData.Body);
                        body.Replace("--studentName--", userName);
                        body.Replace("--serviceName--", serviceName);
                        body.Replace("--year--", year);
                        body.Replace("--url--", applicationUrl);

                        OTPMessage otpObj = new OTPMessage();
                        otpObj.Message = body.ToString();
                        otpObj.MobileNumber = phoneNumber;
                        _MSCommon.SendSMS(apiUrl, otpObj, contentId);
                    }
                    catch (Exception e)
                    {
                        _logger.Error(e.Message);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Sends email to next approving authority
        /// </summary>
        /// <param name="emailAddress">Email id of approving authority</param>
        /// <param name="userName">Username of approving authority</param>
        /// <param name="serviceName">Service name for which approval process is pending</param>
        /// <returns></returns>
        private bool SendEmailToStudentToPay(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupPaymentPendingEmail && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        private bool SendSMSToStudentToPay(string phoneNumber, string studentName)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ExamFormFillupPaymentPendingSMS && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string contentId = string.Empty;
                contentId = templateData.SMSContentId;
                try
                {
                    string apiUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--studentName--", studentName);
                    body.Replace("--url--", applicationUrl);

                    OTPMessage otpObj = new OTPMessage();
                    otpObj.Message = body.ToString();
                    otpObj.MobileNumber = phoneNumber;
                    _MSCommon.SendSMS(apiUrl, otpObj, contentId);
                }
                catch (Exception e)
                {
                    _logger.Error(e.Message);
                }
            }
            return true;
        }

        // <summary>
        /// Sends email to next approving authority
        /// </summary>
        /// <param name="emailAddress">Email id of approving authority</param>
        /// <param name="userName">Username of approving authority</param>
        /// <param name="serviceName">Service name for which approval process is pending</param>
        /// <returns></returns>
        private bool SendEmailToApprovingAuthorityOnStateChange(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ServiceRequestReceived && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", applicationUrl);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }
        #endregion

        /// <summary>
        /// Author  :   Bikash ku sethi
        /// Date    :   08-12-2021
        /// Description :   This methode is save nursing form fill details
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public NursingFormFillUpDto ApplyNursingFormFillUp(NursingFormFillUpDto param)
        {
            ExaminationMaster examinationMasterData = _commonAppService.GetActiveExaminationMasterList().Where(a => a.CourseType == param.CourseTypeCode && a.StreamCode == param.StreamCode
                         && a.SubjectCode == param.SubjectCode && (a.CourseCode == param.CourseCode || a.CourseCode == "" || a.CourseCode == null)
                         && (a.DepartmentCode == param.DepartmentCode || a.DepartmentCode == "" || a.DepartmentCode == null)
                         && a.SemesterCode == param.CurrentAcademicYear && a.TenantId == GetCurrentTenantId() && a.IsActive == true
                         && a.ServiceMasterId==(long)enServiceType.NursingApplicationForm).FirstOrDefault();
            if (examinationMasterData != null)
            {
                //cache
                ExaminationSession examinationSessionData = _commonAppService.GetActiveExaminationSessionList().Where(s => s.ExaminationMasterId == examinationMasterData.Id && s.StartYear == param.AcademicStart
                && s.IsActive == true).FirstOrDefault();
                List<ExaminationFormFillUpDates> examinationFormFillUpDatesData = _MSUoW.ExaminationFormFillUpDates.FindBy(b => b.ExaminationMasterId == examinationMasterData.Id
                   && b.ExaminationSessionId == examinationSessionData.Id && b.IsActive == true && b.TenantId == GetCurrentTenantId()).ToList();
                var startDate = examinationFormFillUpDatesData.OrderBy(a => a.SlotOrder).Select(start => start.StartDate).FirstOrDefault();
                var endDate = examinationFormFillUpDatesData.OrderByDescending(b => b.SlotOrder).Select(end => end.EndDate).FirstOrDefault();
                DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));

                List<NursingFormfillupDetails> nursingFormfillupDetailsList = new List<NursingFormfillupDetails>(); 
                NursingFormfillupDetails nursingFormfillupDetails = new NursingFormfillupDetails(); 

                if (startDate <= currTime && endDate >= currTime)
                {
                    long serviceId = (long)enServiceType.NursingApplicationForm;
                    Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
                    if (user.UserType == (long)enRoles.Student)
                    {
                        //fetch student examination form fill up details if exist
                        nursingFormfillupDetailsList = _MSUoW.NursingFormfillupDetails.GetAll().Where(d => d.IsActive && d.CreatorUserId == GetCurrentUserId()).ToList();
                        if (param != null  && param.ExaminationPaperDetails.Count > 0)
                        {
                            if (ValidateInput(param))
                            {
                                nursingFormfillupDetails = nursingFormfillupDetailsList
                                                    .Where(d => d.CurrentAcademicYear == param.CurrentAcademicYear
                                                    && d.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                                    .FirstOrDefault();
                                if (nursingFormfillupDetails != null)
                                {
                                    UserServiceApplied userAppliedService = _MSUoW.UserServiceApplied.FindBy(d => d.InstanceId == nursingFormfillupDetails.Id && d.IsActive).FirstOrDefault();

                                    if (userAppliedService != null)
                                    {
                                        if (userAppliedService.LastStatus.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower()
                                        || nursingFormfillupDetails.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                                        {
                                            throw new MicroServiceException { ErrorCode = "OUU123", SubstitutionParams = new object[1] { nursingFormfillupDetails.State } };
                                        }
                                    }
                                }

                                NursingFormfillupDetails nursingFillUpDetailsCheck = new NursingFormfillupDetails();
                                // this piece of code modify individual year 
                                if (param.CurrentAcademicYear.ToString() == _MSCommon.GetEnumDescription(enNursingYear.FirstYear))
                                {
                                    param.RegistrationNumber = null;
                                    param.RollNumber = null;
                                }
                                // ADDED BY SITI
                                else
                                {                               
                                    nursingFillUpDetailsCheck = nursingFormfillupDetailsList
                                                            .Where(a => a.IsActive
                                                            && a.CreatorUserId == GetCurrentUserId()
                                                            && a.CurrentAcademicYear ==  _MSCommon.GetEnumDescription(enNursingYear.FirstYear)
                                                            && (a.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:AdmitcardGenerated").Value.Trim().ToLower()
                                                           || a.State.Trim().ToLower() == _IConfiguration.GetSection("ExamFormFillup:MarkUploaded").Value.Trim().ToLower()))
                                                            .FirstOrDefault();
                                    if (nursingFillUpDetailsCheck.RegistrationNumber != param.RegistrationNumber)
                                    {
                                        throw new MicroServiceException() { ErrorCode = "OUU140" };
                                    }
                                }                              

                                Guid newGuid = Guid.NewGuid();
                                //here we set default null for second semester and onwards.these data will update during payment time
                                param.PaymentDate = null;
                                param.CSCPayid = null;
                                param.LastModificationTime = null;
                                param.LastModifierUserId = null;
                                param.LastStatusUpdateDate = null;
                                param.PrincipalHODRemarks = null;
                                param.LastStatusUpdateDate = null;
                                NursingFormfillupDetails nursingFillUpDetails = AutoMapper.Mapper.Map<NursingFormFillUpDto, NursingFormfillupDetails>(param);
                                nursingFillUpDetails.RollNumber = nursingFillUpDetailsCheck.RollNumber;
                                nursingFillUpDetails.RegistrationNumber = nursingFillUpDetailsCheck.RegistrationNumber;
                                nursingFillUpDetails.Id = newGuid;
                                nursingFillUpDetails.ApplicationType = param.ApplicationType;
                                nursingFillUpDetails.InternetHandlingCharges = 0;
                                nursingFillUpDetails.FormFillUpMoneyToBePaid = 0;
                                nursingFillUpDetails.LateFeeAmount = 0;
                                nursingFillUpDetails.CenterCharges = 0; 

                                //student exam paper section
                                List<ExaminationPaperDetail> nursingPaperList = new List<ExaminationPaperDetail>();
                                param.ExaminationPaperDetails.ForEach(paper =>
                                {
                                    if (paper.ExaminationPaperId == Guid.Empty)
                                    {
                                        throw new MicroServiceException() { ErrorCode = "OUU089" };
                                    }
                                    ExaminationPaperDetail examPaper = AutoMapper.Mapper.Map<ExaminationPaperDetailsDto, ExaminationPaperDetail>(paper);
                                    examPaper.Id = Guid.NewGuid();
                                    examPaper.ExaminationPaperId = paper.ExaminationPaperId;
                                    nursingPaperList.Add(examPaper);
                                });
                                nursingFillUpDetails.ExaminationPaperDetails = nursingPaperList;
                                //end of student exam paper section
                                List<Documents> documentAddedList = new List<Documents>();
                                if (param.CurrentAcademicYear.ToString() == _MSCommon.GetEnumDescription(enNursingYear.FirstYear))
                                {
                                    param.ImageDocument.ForEach(content =>
                                    {
                                        if (ImageValidation(content))
                                        {
                                            content.Id = Guid.NewGuid();
                                            // nursingFillUpDetails.ImageDocument = ImageWrite(param, newGuid,content.Id);
                                            // nursingFillUpDetails.ImageDocument.Add(ImageWrite(content, newGuid));
                                            documentAddedList.Add(ImageWrite(content, newGuid));
                                        }
                                    });
                                   nursingFillUpDetails.ImageDocument = documentAddedList;
                                }
                                else
                                {
                                    param.ImageDocument.ForEach(content =>
                                    {
                                        content.Id = Guid.NewGuid();
                                        content.FilePath = content.FilePath.Replace(_IConfiguration.GetSection("ApplicationURL").Value, "");
                                    });
                                    nursingFillUpDetails.ImageDocument = param.ImageDocument;
                                }

                                #region WORKFLOW
                                var tenantLatestWF = _workflowDbContext.TenantWorkflow
                                    .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName)
                                    .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();

                                if (tenantLatestWF != null)
                                {
                                    nursingFillUpDetails.TenantWorkflowId = tenantLatestWF.Id;
                                }
                                else
                                {
                                    var defaultWF = _workflowDbContext.TenantWorkflow
                                   .Where(tenantWF => tenantWF.TenantId == 0 && tenantWF.Workflow == WorkflowName)  //Default tenant id
                                   .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();

                                    nursingFillUpDetails.TenantWorkflowId = defaultWF.Id;
                                }
                                param.LoggedInUserRoles = GetRoleListOfCurrentUser();
                                string initialTrigger = string.Empty;
                                try
                                {
                                    initialTrigger = _IConfiguration.GetSection("WorkflowInitialTrigger").Value;
                                }
                                catch (Exception)
                                {
                                    initialTrigger = "Initialize";
                                }
                                string initialState = string.Empty;
                                try
                                {
                                    initialState = _IConfiguration.GetSection("WorkflowInitialState").Value;
                                }
                                catch (Exception)
                                {
                                    initialState = "Init";
                                }
                                nursingFillUpDetails.State = initialState;
                                param._trigger = initialTrigger;
                                var executionStatus = param.ExecuteProcess(nursingFillUpDetails, param);

                                #endregion

                                // Acknowledgement will be sent only after payment is successful
                                if (param.CurrentAcademicYear.ToString() == _MSCommon.GetEnumDescription(enNursingYear.FirstYear))
                                {
                                    //here acknowledment no generated
                                    nursingFillUpDetails.AcknowledgementNo = GenerateAcknowldmentNo(nursingFillUpDetails, (long)EnProcessType.AcknowledgmentNo, (long)enServiceType.NursingApplicationForm);
                                    //nursingFillUpDetails.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(_MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK), serviceId, newGuid.ToString());
                                }
                                else
                                {
                                    nursingFillUpDetails.AcknowledgementNo = nursingFillUpDetailsCheck.AcknowledgementNo;
                                }
                                _MSUoW.NursingFormfillupDetails.Add(nursingFillUpDetails);

                                // Service applied
                                UserServiceApplied usrSrvcApplied = new UserServiceApplied()
                                {
                                    IsAvailableToDL = false,
                                    LastStatus = nursingFillUpDetails.State,
                                    ServiceId = serviceId,
                                    UserId = GetCurrentUserId(),
                                    InstanceId = newGuid
                                };
                                _MSUoW.UserServiceApplied.Add(usrSrvcApplied);
                                _MSUoW.Commit();

                                #region EMAIL-SENDING
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.NursingApplicationForm && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();

                                #region SEND ACKNOWLEDGEMENT EMAIL AFTER PAYMENT IS DONE
                                //// Send email to student
                                //SendAcknowledgementEmail(user.EmailAddress, nursingFillUpDetails.StudentName, serviceName, nursingFillUpDetails.AcknowledgementNo);

                                #region SEND SMS FUNCTIONALITY

                                EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AcknowledgementSMS && s.IsActive == true).FirstOrDefault();
                                if (acknowledgementSMS != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = acknowledgementSMS.SMSContentId;
                                    string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                    OTPMessage otpObj = new OTPMessage();
                                    StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                                    body.Replace("--studentName--", nursingFillUpDetails.StudentName);
                                    body.Replace("--serviceName--", serviceName);
                                    body.Replace("--acknowledgementNo--", nursingFillUpDetails.AcknowledgementNo);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = user.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                                #endregion

                                #endregion
                                // Send email to principal/hod in case of application submit, because the workflow is built like wise
                                // Fetch principal/hod email id of the applying student registration form
                                Users correspondingPrincipalOrHOD = null;

                                if (_MSCommon.GetEnumDescription((enCourseType)enCourseType.Ug) == nursingFillUpDetails.CourseTypeCode)
                                {// Ug course type | FInd principal
                                    correspondingPrincipalOrHOD = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.IsActive && usr.CollegeCode == nursingFillUpDetails.CollegeCode).FirstOrDefault();
                                }
                                else
                                {// Pg course type | Find HOD
                                    correspondingPrincipalOrHOD = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.IsActive && usr.CollegeCode == nursingFillUpDetails.CollegeCode && nursingFillUpDetails.DepartmentCode == usr.DepartmentCode).FirstOrDefault();
                                }

                                if (correspondingPrincipalOrHOD != null)
                                {
                                    SendEmailToApprovingAuthorityOnStateChange(correspondingPrincipalOrHOD.EmailAddress, correspondingPrincipalOrHOD.Name, serviceName);
                                }
                                #endregion
                                Dictionary<string, object> triggerParameterDictionary = null;
                                _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, newGuid, DateTime.UtcNow, initialState, initialTrigger, nursingFillUpDetails.State, nursingFillUpDetails.TenantWorkflowId);
                                 param = AutoMapper.Mapper.Map<NursingFormfillupDetails, NursingFormFillUpDto>(nursingFillUpDetails);
                            }
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU013" };
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU052" };
                }

                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU115" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU117" };
            }
            return param;
        }
        public string GenerateAcknowldmentNo(NursingFormfillupDetails studentInfo, long processType, long serviceId)
        {
            string runningNum = string.Empty;

            string yearCode = studentInfo.AcademicStart.ToString().Substring(2);

            string prefix = _MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK);
            string startSeqNumber = string.Empty;
            long newSeq = 0;
            int zerosToBeAppended = 0;

            string subjectTypeLength = _IConfiguration.GetSection("AcknowledgementLength:SubjectTypeLength").Value;
            string collegeCodeLength = _IConfiguration.GetSection("AcknowledgementLength:CollegeCodeLength").Value;
            string runningSequenceLength = _IConfiguration.GetSection("AcknowledgementLength:RunningSequenceLength").Value;


            StringBuilder newSeqNumber = new StringBuilder();
            NumberGenerator regdNos = _MSUoW.NumberGenerator.GetAll()
                                        .Where(data => data.YearOfAdmission == studentInfo.AcademicStart.ToString().Substring(2)
                                        && data.CollegeCode == studentInfo.CollegeCode
                                        && data.SubjectCode == studentInfo.SubjectCode
                                        && data.ProcessType == processType
                                        && data.ServiceId == serviceId)
                                        .FirstOrDefault();
            if (regdNos != null)
            {
                // already exists
                startSeqNumber = Convert.ToString(regdNos.StartNumberSeq);
                newSeq = regdNos.LastGeneratedNumber + Convert.ToInt32(regdNos.NumberIncrementedBy);
                newSeqNumber.Append(Convert.ToString(newSeq));
                zerosToBeAppended = startSeqNumber.Length - Convert.ToString(newSeq).Length;
                for (int i = 0; i < zerosToBeAppended; i++)
                {
                    newSeqNumber.Insert(0, "0");
                }

                regdNos.SubjectCode = regdNos.SubjectCode.Length < int.Parse(subjectTypeLength) ? AddPrefix(int.Parse(subjectTypeLength), '0', regdNos.SubjectCode) : regdNos.SubjectCode;
                regdNos.CollegeCode = regdNos.CollegeCode.Length < int.Parse(collegeCodeLength) ? AddPrefix(int.Parse(collegeCodeLength), '0', regdNos.CollegeCode) : regdNos.CollegeCode;
                runningNum = (regdNos.LastGeneratedNumber + regdNos.NumberIncrementedBy).ToString();

                newSeqNumber.Insert(0, regdNos.PreFixWith + regdNos.SubjectCode + regdNos.CollegeCode);

                regdNos.LastGeneratedNumber = newSeq;
                _MSUoW.NumberGenerator.Update(regdNos);
            }
            else
            {
                //create a new object and insert in the table
                NumberGenerator newAckNo = new NumberGenerator();

                newAckNo.StartNumberSeq = "000";
                newAckNo.NumberIncrementedBy = 1;
                newAckNo.PreFixWith = prefix;
                newAckNo.LastGeneratedNumber = 0;
                newAckNo.ProcessType = (int)processType;
                newAckNo.YearOfAdmission = studentInfo.AcademicStart.ToString().Substring(2);
                newAckNo.CollegeCode = studentInfo.CollegeCode;
                newAckNo.ServiceId = serviceId;
                newAckNo.SubjectCode = studentInfo.SubjectCode;

                startSeqNumber = Convert.ToString(newAckNo.StartNumberSeq);
                newSeq = newAckNo.LastGeneratedNumber + Convert.ToInt32(newAckNo.NumberIncrementedBy);
                newSeqNumber.Append(Convert.ToString(newSeq));
                zerosToBeAppended = startSeqNumber.Length - Convert.ToString(newSeq).Length;
                for (int i = 0; i < zerosToBeAppended; i++)
                {
                    newSeqNumber.Insert(0, "0");
                }

                newAckNo.SubjectCode = newAckNo.SubjectCode.Length < int.Parse(subjectTypeLength) ? AddPrefix(int.Parse(subjectTypeLength), '0', newAckNo.SubjectCode) : newAckNo.SubjectCode;
                newAckNo.CollegeCode = newAckNo.CollegeCode.Length < int.Parse(collegeCodeLength) ? AddPrefix(int.Parse(collegeCodeLength), '0', newAckNo.CollegeCode) : newAckNo.CollegeCode;
                runningNum = (newAckNo.LastGeneratedNumber + newAckNo.NumberIncrementedBy).ToString();

                newSeqNumber.Insert(0, newAckNo.PreFixWith + newAckNo.SubjectCode + newAckNo.CollegeCode);

                newAckNo.LastGeneratedNumber = newSeq;
                _MSUoW.NumberGenerator.Add(newAckNo);
            }
            
            _MSUoW.Commit();
            return newSeqNumber.ToString();
        }

        string AddPrefix(int requiredlength, char prefix, string inputCode)
        {
            return inputCode.PadLeft(requiredlength, prefix); ;
        }

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Bikash ku sethi
        /// Date: 12-11-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidateInput(NursingFormFillUpDto input)
        {
            string regularExpressionForName = @"^(?!\.)(?!.*\.$)(?!.*?\.\.)[a-zA-Z0-9 .]+$";//This regex is used to allow only alphabets, dot and spaces in string
            //string regExDOB = @"^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}+$";    //current it is not used
            string registrationExpression = @"^[0-9]{16}$";

            if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
            {
                input.DepartmentCode = "";
            }
            if (string.IsNullOrEmpty(input.CurrentAcademicYear))
            {
                throw new MicroServiceException() { ErrorMessage = "Current academic year can not be blank." };
            }
            else if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            else if (input.Gender == null || string.IsNullOrEmpty(input.Gender) || string.IsNullOrWhiteSpace(input.Gender))
            {
                throw new MicroServiceException() { ErrorCode = "OUU080" };
            }
            else if (input.GenderString == null || string.IsNullOrEmpty(input.GenderString) || string.IsNullOrWhiteSpace(input.GenderString))
            {
                throw new MicroServiceException() { ErrorCode = "OUU080" };
            }
            else if (input.Nationality == null || string.IsNullOrEmpty(input.Nationality) || string.IsNullOrWhiteSpace(input.Nationality))
            {
                throw new MicroServiceException() { ErrorCode = "OUU078" };
            }
            else if (input.NationalityString == null || string.IsNullOrEmpty(input.NationalityString) || string.IsNullOrWhiteSpace(input.NationalityString))
            {
                throw new MicroServiceException() { ErrorCode = "OUU078" };
            }
            else if (input.Caste == null || string.IsNullOrEmpty(input.Caste) || string.IsNullOrWhiteSpace(input.Caste))
            {
                throw new MicroServiceException() { ErrorCode = "OUU079" };
            }
            else if (input.CasteString == null || string.IsNullOrEmpty(input.CasteString) || string.IsNullOrWhiteSpace(input.CasteString))
            {
                throw new MicroServiceException() { ErrorCode = "OUU079" };
            }
            else if (input.IsPwdString == null || string.IsNullOrEmpty(input.IsPwdString) || string.IsNullOrWhiteSpace(input.IsPwdString))
            {
                throw new MicroServiceException() { ErrorMessage = "PWD field can not be blank." };
            }
            else if (input.YearOfMatriculation == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU081" };
            }
            else if (input.YearOfPassingPreMedical == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU082" };
            }
            else if ((input.YearOfPassingPreMedical < input.YearOfMatriculation) || (input.YearOfMatriculation == input.YearOfPassingPreMedical))
            {
                throw new MicroServiceException() { ErrorMessage = "Year of passing pre-medical can not be less then year Of passing H.S.C." };
            }           
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CollegeCodeString == null || input.CollegeCodeString.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseTypeCode == null || input.CourseTypeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.CourseTypeCode == null || input.CourseTypeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.CurrentAcademicYear == null || input.CurrentAcademicYearString == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if (input.CurrentAcademicYear == null || string.IsNullOrEmpty(input.CurrentAcademicYear) || string.IsNullOrWhiteSpace(input.CurrentAcademicYear))
            {
                throw new MicroServiceException() { ErrorCode = "OUU085" };
            }
            else if ((input.CourseTypeCode == "02") && (input.DepartmentCode == null || input.DepartmentCode == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU056" };
            }
            else if (input.CourseTypeCode == "02" && (!string.IsNullOrEmpty(input.DepartmentCode) || !string.IsNullOrWhiteSpace(input.DepartmentCode)) && (input.CourseCode == null || input.CourseCode.Trim() == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU059" };
            }   
            else if (input.AcademicStart == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU086" };
            }
            else if (input.ApplicationType == null || string.IsNullOrEmpty(input.ApplicationType) || string.IsNullOrWhiteSpace(input.ApplicationType))
            {
                throw new MicroServiceException() { ErrorCode = "OUU087" };
            }           
            else if (input.ExaminationPaperDetails.Count() == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU089" };
            }
            else if(input.SubjectCode.ToString() == _MSCommon.GetEnumDescription(enSubject.PBNursing))
            {
                if(input.IsGnmQualified == false)
                {
                    throw new MicroServiceException() { ErrorMessage = "If you are P.B. Nursing student,you must qualify GNM." };
                }
            }
            else if (input.SubjectCode.ToString() == _MSCommon.GetEnumDescription(enSubject.BSCNursing))
            {
                if (input.IsHscQualified != true ) // either false or null not accepted in bsc nursing ,you must qualify Hsc 
                {
                    throw new MicroServiceException() { ErrorMessage = "If you are B.Sc Nursing student,you must qualify Higher Secondary School." };
                }
                if ( input.Caste == _MSCommon.GetEnumDescription(enCasteCode.SC) || input.Caste == _MSCommon.GetEnumDescription(enCasteCode.ST)) 
                {
                    if (input.HscPercentage < 40)
                    {
                        throw new MicroServiceException() { ErrorMessage = "Minimum 40% mark required for B.Sc nursing." };                        
                    }
                }
                if (input.Caste == _MSCommon.GetEnumDescription(enCasteCode.GEN) || input.Caste == _MSCommon.GetEnumDescription(enCasteCode.OBC))
                {
                    if (input.HscPercentage < 45)
                    {
                        throw new MicroServiceException() { ErrorMessage = "Minimum 45% mark required for B.Sc nursing." };
                    }
                }
            }
            if (input.PresentAddress == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Present address field can not be blank." };
            }
            if (input.PresentCountry == null || input.PresentCountryCode == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Present country field can not be blank." };
            }
            if (input.PresentState == null || input.PresentStateCode == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Present state field can not be blank." };
            }
            if (input.PresentCityCode == null || input.PresentCity == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Present city field can not be blank." };
            }
            if (input.PresentZip == null )
            {
                throw new MicroServiceException() { ErrorMessage = "Present zip field can not be blank." };
            }
            if (input.PermanentAddress == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Permanent address field can not be blank." };
            }
            if (input.PermanentCountry == null || input.PermanentCountryCode == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Permanent country field can not be blank" };
            }
            if (input.PresentState == null || input.PresentStateCode == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Permanent state field can not be blank." };
            }
            if (input.PermanentCity == null || input.PermanentCityCode == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Permanent city field can not be blank." };
            }
            if (input.PermanentZip == null)
            {
                throw new MicroServiceException() { ErrorMessage = "Permanent zip field can not be blank." };
            }

            if(input.CurrentAcademicYear!=_MSCommon.GetEnumDescription(enNursingYear.FirstYear))
            {
                if(string.IsNullOrEmpty(input.RegistrationNumber) || string.IsNullOrWhiteSpace(input.RegistrationNumber))
                {
                    throw new MicroServiceException() { ErrorMessage = "Registration Number cannot be blank." };
                }
            }
            if(input.YearOfMatriculation > input.YearOfPassingPreMedical)
            {
                throw new MicroServiceException() { ErrorMessage = "Year of passing pre-medical can not be less then year Of Passing H.S.C." };
            }
            if (input.YearOfMatriculation == input.YearOfPassingPreMedical)
            {
                throw new MicroServiceException() { ErrorMessage = "Year of passing pre-medical can not be equal to year Of Passing H.S.C." };
            }
            input.StudentName = input.StudentName.Trim();         
            input.CollegeCode = input.CollegeCode.Trim();
            input.CourseTypeCode = input.CourseTypeCode.Trim();
            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();
            input.CurrentAcademicYear = input.CurrentAcademicYear.Trim();
            return true;
        }
        #endregion

        #region IMAGE VALIDATION
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 22-oct-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ImageValidation(Documents input)
        {
            if (input.FileExtension == "" || input.FileExtension == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU047" };
            }
            if (input.FileExtension != "" || input.FileExtension != null)
            {
                var regEx = new Regex(@"(jpg|jpeg)$");
                if (!regEx.IsMatch(input.FileExtension.ToLower()))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU048" };
                }
            }
            if (input.FileContent.Length >= Convert.ToInt32(_IConfiguration.GetSection("RegistrationNumber:UploadImageSize").Value))
            {
                throw new MicroServiceException() { ErrorCode = "OUU049" };
            }
            if (string.IsNullOrEmpty(input.DocumentType) || string.IsNullOrWhiteSpace(input.DocumentType))
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            if (input.SourceType == 0)
            {
                throw new MicroServiceException() { ErrorCode = "OUU131" };
            }
            return true;
        }
        #endregion

        #region PRIVATE COMMON DOCUMENT WRITE METHOD
        /// <summary>
        /// Modified By -   Anurag Digal
        /// Date        -   14-12-2019
        /// Description -   Linux machine deployment path added
        /// </summary>
        /// <param name="input"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        //private List<Documents> ImageWrite(NursingFormFillUpDto input, Guid id)
        //{
        //    string[] name = id.ToString().Split("-");
        //    string finalName = name[1] + name[2] + name[4] + name[0] + name[3];
        //    List<Documents> documentList = new List<Documents>();
        //    input.ImageDocument.ForEach((file) =>
        //    {
        //        string path = string.Empty;
        //        file.ParentSourceId = id;
        //        if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
        //        {
        //            path = _environment.ContentRootPath;
        //        }
        //        else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
        //        {
        //            path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
        //        }

        //        string fileName = file.FileName + "." + file.FileExtension;
        //        string DirectoryPath = _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpCertificatePath").Value;
        //        DirectoryPath = DirectoryPath.Replace('\\', '/');
        //        var Path = path + DirectoryPath + fileName;
        //        if (!Directory.Exists(path + DirectoryPath))
        //        {
        //            Directory.CreateDirectory(path + DirectoryPath);
        //        }

        //        if (File.Exists(Path))
        //        {
        //            File.Delete(Path);
        //        }
        //        byte[] bytes = (byte[])file.FileContent;
        //        File.WriteAllBytes(Path, bytes);
        //        file.FilePath = DirectoryPath + fileName;
        //        file.FilePath = file.FilePath.Replace('\\', '/');
        //        documentList.Add(file);
        //    });
        //    return documentList;
        //}
        private Documents ImageWrite(Documents file, Guid id)
        {
            string[] name = id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3] + file.DocumentType;
            //List<Documents> documentList = new List<Documents>();         
        
            string path = string.Empty;
            file.ParentSourceId = id;
            if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
            {
                path = _environment.ContentRootPath;
            }
            else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
            {
                path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
            }

            string fileName = finalName + "." + file.FileExtension;
            string DirectoryPath = _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpCertificatePath").Value;
            DirectoryPath = DirectoryPath.Replace('\\', '/');
            var Path = path + DirectoryPath + fileName;
            if (!Directory.Exists(path + DirectoryPath))
            {
                Directory.CreateDirectory(path + DirectoryPath);
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            byte[] bytes = (byte[])file.FileContent;
            File.WriteAllBytes(Path, bytes);
            file.FilePath = DirectoryPath + fileName;
            file.FilePath = file.FilePath.Replace('\\', '/');
            //documentList.Add(file);          
            return file;
        }
        #endregion

        #region Send Acknowledgement Email
        /// <summary>
        /// Author                  : Bikash ku sethi 
        /// Date                    : 07-12-2021
        /// Description             : Send acknowledgement email
        /// </summary>
        private bool SendAcknowledgementEmail(string studentEmailAddress, string studentName, string serviceName, string acknowledgementNo)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AcknowledgementNo && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string serverUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", serverUrl + collegeLogo);
                body.Replace("--studentName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--acknowledgementNo--", acknowledgementNo);
                body.Replace("--url--", applicationUrl);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }
        #endregion


        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 14-Nov-2019
        /// Method is used for Odata Controller which will give examination student record as per usertype
        /// </summary>
        /// <returns></returns>
        public IQueryable<NursingViewExcludeBackgroundProcessTable> GetRoleBasedActiveNursingInboxData(bool isCountRequired)
        {
            // isCountRequired = true is used to show count in dashboard page
            // isCountRequired = false is used to show data in inbox page
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            IQueryable<NursingViewExcludeBackgroundProcessTable> inboxDataAsPerRole = null;
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            if (roleNames.Contains(Convert.ToString((long)enRoles.COE)))
            {
                List<string> stateList = GetRoleSpecificWorkflowStateList();
                inboxDataAsPerRole = _MSUoW.NursingViewExcludeBackgroundProcessTable.GetAll().Where(w => w.IsActive && stateList.Contains(w.State));
            }
            else if (roleNames.Contains(Convert.ToString((long)enRoles.Principal)) || roleNames.Contains(Convert.ToString((long)enRoles.HOD)))
            {
                if (!isCountRequired)
                {
                    inboxDataAsPerRole = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId) || t.State.Trim().ToLower() == _IConfiguration.GetSection("NursingFormFillup:EndState").Value.Trim().ToLower())
                 || t.State.Trim().ToLower() == _IConfiguration.GetSection("NursingFormFillup:PaymentPendingState").Value.Trim().ToLower());
                }
                else
                {
                    inboxDataAsPerRole = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId)));
                }

            }
            else
            {
                if (!isCountRequired)
                {
                    inboxDataAsPerRole = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId) || t.State.Trim().ToLower() == _IConfiguration.GetSection("NursingFormFillup:EndState").Value.Trim().ToLower()));
                }
                else
                {
                    inboxDataAsPerRole = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(t => t.IsActive == true && (workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId)));
                }

            }

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    if (isCountRequired == false)
                    {// Data for inbox, remove records already available in background table
                        var backgroundProcessIdList = _MSUoW.BackgroundProcess.GetAll().Where(bgp => (bgp.InstanceTrigger == _IConfiguration.GetSection("NursingFormFillup:PrincipalToCOE").Value ||
                        bgp.InstanceTrigger == _IConfiguration.GetSection("NursingFormFillup:MarkForwardToCOE").Value) && bgp.IsActive).Select(g => g.InstanceId);
                        inboxDataAsPerRole = inboxDataAsPerRole.Where(data => !backgroundProcessIdList.Contains(data.Id) && data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.IsActive);
                    }
                    else
                    {
                        inboxDataAsPerRole = inboxDataAsPerRole.Where(data => CollegeCode.Contains(data.CollegeCode));
                    }
                    break;
                case (long)enUserType.HOD:
                    // Not implemented changes related to 'isCountRequired'
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower() && data.IsActive);
                    break;
                case (long)enUserType.Student:
                    // Changes related to param 'isCountRequired' is not required
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
                case (long)enUserType.AsstCoE:
                    if (isCountRequired == false)
                    {// Data for inbox, remove records already available in background table
                        var triggerList = workflowTransition.Select(trans => trans.Trigger).ToList();
                        var backgroundRecord = _MSUoW.BackgroundProcess.FindBy(bgp => triggerList.Contains(bgp.InstanceTrigger) && bgp.IsActive).ToList();
                        backgroundRecord.ForEach(bkgRcrd =>
                        {
                            inboxDataAsPerRole = inboxDataAsPerRole.Where(data =>
                                !(data.CourseTypeCode == bkgRcrd.CourseTypeCode
                                && data.CollegeCode == bkgRcrd.CollegeCode
                                && data.StreamCode == bkgRcrd.StreamCode
                                && data.SubjectCode == bkgRcrd.SubjectCode
                                && data.DepartmentCode == bkgRcrd.DepartmentCode
                                && data.CourseCode == bkgRcrd.CourseCode
                                && data.CurrentAcademicYear == bkgRcrd.CurrentAcademicYear
                                && data.AcademicStart == bkgRcrd.AcademicStart
                                && data.State == workflowTransition.Where(trans => trans.Trigger == bkgRcrd.InstanceTrigger).Select(trans => trans.SourceState).FirstOrDefault())
                                );
                        });
                    }
                    break;
            }

            return inboxDataAsPerRole;
        }

        public List<string> GetRoleSpecificWorkflowStateList()
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            long maxTenantWorkflowId = tenantWFTrans.Max(p => p.TenantWorkflowId);

            List<TenantWorkflowTransitionConfig> workflowTransition = new List<TenantWorkflowTransitionConfig>();
            if (roleNames.Contains(((int)enRoles.COE).ToString()))
            {
                List<string> RoleIds = new List<string>();
                //RoleIds.Add(Convert.ToString((long)enRoles.Student));
                //RoleIds.Add(Convert.ToString((long)enRoles.Principal));
                //RoleIds.Add(Convert.ToString((long)enRoles.HOD));

                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).Any(x => !RoleIds.Contains(x))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();
            }
            else
            {
                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList().Contains(role))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();
            }

            string wfInitState = _IConfiguration.GetSection("WorkflowInitialState").Value;
            List<string> listOfStates = workflowTransition.Where(s => s.SourceState != wfInitState).Select(p => p.SourceState).Distinct().ToList();
            if (roleNames.Contains(((int)enRoles.Principal).ToString()) || roleNames.Contains(((int)enRoles.HOD).ToString()))
            {
                listOfStates.Add("Payment Pending");
            }
            if (roleNames.Contains(((int)enRoles.AsstCoE).ToString()))
            {
                listOfStates.Add(_IConfiguration.GetSection("NursingFormFillup:AdmitcardGenerated").Value);
            }
            return listOfStates;

        }

        /// <summary>
        ///Author   :   Satya Prakash Sahoo
        ///Date     :   10-12-2021
        ///This method is used to get the nursing form fill up details by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public NursingFormFillUpDto GetNursingFormFillUpDetails(Guid id)
        {
            NursingFormFillUpDto formFillUpData = new NursingFormFillUpDto();

            if (id != Guid.Empty)
            {
                NursingFormfillupDetails formRecord = _MSUoW.NursingFormFillUpRepository.GetAllNursingFormFillUpData().Where(x => x.Id == id && x.IsActive).FirstOrDefault();
                if (formRecord != null && formRecord.ImageDocument != null && formRecord.ImageDocument.Count > 0)
                {
                    formRecord.ImageDocument.ForEach(img =>
                    {
                        img.FilePath = string.Concat(_IConfiguration.GetSection("ApplicationURL").Value, img.FilePath);
                    });
                }
                if (formRecord != null)
                {
                    formFillUpData = AutoMapper.Mapper.Map<NursingFormfillupDetails, NursingFormFillUpDto>(formRecord);
                }
            }
            return formFillUpData;
        }
        #region ASYNC CALL TO SAVE DATA FROM EXAMFORMFILLUPDATAILS TABLE TO BACKGROUNDPROCESS TABLE IN STATE PRINCIPAL TO COE
        public bool AddNursingFormDetailsToBackgroundTable(BackgroundProcessDto parameters)
        {
            if (parameters != null)
            {
                ExaminationMaster masterData = _MSUoW.ExaminationMaster.GetAll().Where(a => a.CourseType == parameters.CourseTypeCode
                                            && a.SemesterCode == parameters.CurrentAcademicYear
                                            && a.StreamCode == parameters.StreamCode
                                            && a.SubjectCode == parameters.SubjectCode
                                            && a.TenantId == 3
                                            && a.IsActive == true && a.ServiceMasterId == (long)enServiceType.NursingApplicationForm).FirstOrDefault();
                if (masterData != null)
                {
                    ExaminationSession sessionData = _MSUoW.ExaminationSession.GetAll().Where(sess => sess.ExaminationMasterId == masterData.Id && sess.IsActive && sess.StartYear == parameters.StartYear).FirstOrDefault();
                    if (sessionData != null)
                    {
                        ExaminationFormFillUpDates finalDate = _MSUoW.ExaminationFormFillUpDates.GetAll().Where(dates => dates.ExaminationSessionId == sessionData.Id && dates.IsActive).OrderByDescending(a => a.SlotOrder).FirstOrDefault();
                        //var currTime = DateTime.UtcNow;
                        DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));
                        if (finalDate.EndDate < currTime)
                        {
                            var currentUserId = GetCurrentUserId();
                            long userRole = _MSUoW.Users.FindBy(u => u.IsActive && u.Id == currentUserId).Select(s => s.UserType).FirstOrDefault();
                            if (userRole == (long)enUserType.Principal)
                            {
                                //var currTime = DateTime.UtcNow;
                                //Process Tracker Table Entry
                                ProcessTracker processTableData = new ProcessTracker();
                                processTableData.Id = Guid.NewGuid();
                                Guid processTableId = processTableData.Id;
                                processTableData.ServiceId = (long)enServiceType.NursingApplicationForm;
                                processTableData.ProcessType = (Int32)enProcessName.SendToCOE;
                                processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
                                processTableData.IsActive = true;
                                processTableData.CreatorUserId = currentUserId;
                                processTableData.CreationTime = DateTime.UtcNow;
                                processTableData.TenantId = Convert.ToInt64(3);
                                _MSUoW.ProcessTracker.Add(processTableData);
                                _MSUoW.Commit();
                                AddExamFormDetailsDataToBackgrounTable(parameters, currentUserId, processTableId);
                            }
                            else throw new MicroServiceException() { ErrorCode = "OUU093" };
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU095" };// Do finalDate error
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU114" };// Do sessionData error
                }
                else throw new MicroServiceException() { ErrorCode = "OUU114" }; // Do masterData error
            }
            else throw new MicroServiceException() { ErrorCode = "OUU075" };

            return true;
        }

        public async Task<bool> AddExamFormDetailsDataToBackgrounTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            await Task.Run(() =>
            {
                AddExamFormDetailsToBackgroundTable(parameters, userId, processTableId);
                return true;
            });
            return false;
        }

        /// <summary>
        /// Author: Lopamudra Senapati
        /// Date: 15-11-2019
        /// Method is used for Odata Controller which will give examination student record as per usertype
        /// </summary>
        /// <returns></returns>
        public bool AddExamFormDetailsToBackgroundTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            using (var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger))
            {
                List<BackgroundProcess> data = new List<BackgroundProcess>();
                try
                {

                    if (parameters.SelectedInstanceIds != null && parameters.IsAllSelected == false)
                    {
                        var ids = parameters.SelectedInstanceIds.Split(",").ToList();
                        data = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && ids.Contains(d.Id.ToString()) && d.State == parameters.State)
                                                      .Select(s => new BackgroundProcess
                                                      {
                                                          Id = Guid.NewGuid(),
                                                          ServiceType = (long)enServiceType.NursingApplicationForm,
                                                          InstanceTrigger = _IConfiguration.GetSection("NursingFormFillup:PrincipalToCOE").Value,
                                                          InstanceId = s.Id,
                                                          Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),//will be false after sent to COE
                                                          TenantWorkflowId = s.TenantWorkflowId,
                                                          CreatorUserId = userId,
                                                          CreationTime = DateTime.UtcNow,
                                                          IsActive = true,
                                                          TenantId = Convert.ToInt64(3),
                                                          CollegeCode = parameters.CollegeCode[0], // as principal send only one college record
                                                          CourseCode = parameters.CourseCode,
                                                          StreamCode = parameters.StreamCode,
                                                          CourseTypeCode = parameters.CourseTypeCode,
                                                          DepartmentCode = parameters.DepartmentCode,
                                                          SubjectCode = parameters.SubjectCode,
                                                          SemesterCode = parameters.SemesterCode,
                                                          AcademicStart = parameters.StartYear,
                                                          CurrentAcademicYear = parameters.CurrentAcademicYear
                                                      }).ToList();
                    }
                    else if (parameters.SelectedInstanceIds == null && parameters.IsAllSelected)
                    {
                        if (parameters.DepartmentCode == null)
                        {
                            data = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && d.State == parameters.State && d.CourseTypeCode == parameters.CourseTypeCode
                                                                   && parameters.CollegeCode.Contains(d.CollegeCode) && d.StreamCode == parameters.StreamCode && d.CurrentAcademicYear == parameters.CurrentAcademicYear
                                                                   && d.SubjectCode == parameters.SubjectCode && d.AcademicStart == parameters.StartYear)
                                                  .Select(s => new BackgroundProcess
                                                  {
                                                      Id = Guid.NewGuid(),
                                                      ServiceType = (long)enServiceType.NursingApplicationForm,
                                                      InstanceTrigger = _IConfiguration.GetSection("NursingFormFillup:PrincipalToCOE").Value,
                                                      InstanceId = s.Id,
                                                      Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                                      TenantWorkflowId = s.TenantWorkflowId,
                                                      CreatorUserId = userId,
                                                      CreationTime = DateTime.UtcNow,
                                                      IsActive = true,
                                                      TenantId = Convert.ToInt64(3),
                                                      CollegeCode = parameters.CollegeCode[0],
                                                      CourseCode = parameters.CourseCode,
                                                      StreamCode = parameters.StreamCode,
                                                      CourseTypeCode = parameters.CourseTypeCode,
                                                      DepartmentCode = parameters.DepartmentCode,
                                                      SubjectCode = parameters.SubjectCode,
                                                      SemesterCode = parameters.SemesterCode,
                                                      AcademicStart = parameters.StartYear,
                                                      CurrentAcademicYear = parameters.CurrentAcademicYear
                                                  }).ToList();
                        }
                        else
                        {
                            data = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && d.State == parameters.State && d.CourseTypeCode == parameters.CourseTypeCode
                                                                   && parameters.CollegeCode.Contains(d.CollegeCode) && d.StreamCode == parameters.StreamCode && d.CurrentAcademicYear == parameters.CurrentAcademicYear
                                                                   && d.SubjectCode == parameters.SubjectCode && d.DepartmentCode == parameters.DepartmentCode && d.CourseCode == parameters.CourseCode && d.AcademicStart == parameters.StartYear)
                                                  .Select(s => new BackgroundProcess
                                                  {
                                                      Id = Guid.NewGuid(),
                                                      ServiceType = (long)enServiceType.NursingApplicationForm,
                                                      InstanceTrigger = _IConfiguration.GetSection("NursingFormFillup:PrincipalToCOE").Value,
                                                      InstanceId = s.Id,
                                                      Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                                      TenantWorkflowId = s.TenantWorkflowId,
                                                      CreatorUserId = userId,
                                                      CreationTime = DateTime.UtcNow,
                                                      IsActive = true,
                                                      TenantId = Convert.ToInt64(3),
                                                      CollegeCode = parameters.CollegeCode[0],
                                                      CourseCode = parameters.CourseCode,
                                                      StreamCode = parameters.StreamCode,
                                                      CourseTypeCode = parameters.CourseTypeCode,
                                                      DepartmentCode = parameters.DepartmentCode,
                                                      SubjectCode = parameters.SubjectCode,
                                                      SemesterCode = parameters.SemesterCode,
                                                      AcademicStart = parameters.StartYear,
                                                      CurrentAcademicYear = parameters.CurrentAcademicYear
                                                  }).ToList();
                        }
                    }
                    _MSUoW.BackgroundProcess.AddRange(data);

                    ProcessTracker processTracker = new ProcessTracker();
                    var currTime = DateTime.UtcNow;
                    processTracker = _MSUoW.ProcessTracker.GetAll().Where(w => w.Id == processTableId && w.CreatorUserId == userId).FirstOrDefault();
                    processTracker.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.ReadyForProcessing);
                    processTracker.LastModifierUserId = processTracker.CreatorUserId;
                    processTracker.LastModificationTime = currTime;
                    _MSUoW.ProcessTracker.Update(processTracker);
                    _MSUoW.Commit();
                }
                catch (Exception e)
                {
                }
                return true;
            }

        }
        #endregion
        public List<CollegeWiseAmountForCoeDto> GetCollegeWiseFormFillUpAmount(SearchParamDto param)
        {
            List<CollegeWiseAmountForCoeDto> resultset = new List<CollegeWiseAmountForCoeDto>();
            string instanceTrigger = string.Empty;
            //get the college wise amount details from the examinantion view
            if (param.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
            {
                switch (param.State)
                {
                    case Constant.RollNumberGenerationPending:

                        instanceTrigger = _IConfiguration.GetSection("NursingFormFillup:ForwardToGenerateRollNo").Value;
                        resultset = _MSUoW.NursingFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.AdmitCardGenerationPending:
                        instanceTrigger = _IConfiguration.GetSection("NursingFormFillup:ForwardToGenerateAdmitCard").Value;
                        resultset = _MSUoW.NursingFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.AdmitCardGenerated:
                        instanceTrigger = _IConfiguration.GetSection("NursingFormFillup:AdmitcardGenerated").Value;
                        resultset = _MSUoW.NursingFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.MarkUploaded:
                        instanceTrigger = _IConfiguration.GetSection("NursingFormFillup:MarkUploaded").Value;
                        resultset = _MSUoW.NursingFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;
                    case Constant.ResultPublication:
                        instanceTrigger = _IConfiguration.GetSection("NursingFormFillup:EndState").Value;
                        resultset = _MSUoW.NursingFormFillUpRepository.GetAllStudentStateWise(param, instanceTrigger).ToList();
                        break;

                    default:
                        break;
                }
            }
            else
            {
                //this condition is for pg case
                //return _MSUoW.ExaminationView.FindBy(x => x.CourseTypeCode == param.courseTypeCode && x.CourseCode == param.courseCode && x.)
            }
            return resultset;
        }

        public bool SaveToBackGroundTable(BackgroundProcessDto param)
        {
            bool status = false;
            //var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger);
            List<BackgroundProcess> backgroundProcessesList = new List<BackgroundProcess>();
            string stateUpdateName = string.Empty;
            try
            {
                if (param != null)
                {
                    //Process Tracker Table Entry
                    ProcessTracker processTableData = new ProcessTracker();
                    Guid newGuid = Guid.NewGuid();
                    processTableData.Id = newGuid;
                    string stateName = string.Empty;

                    //get the statelist from the appsetting json file
                    string[] statelist = _IConfiguration.GetSection("NursingFormFillup:COEStates").Value.Split(',');
                    if (param.State.ToString().Trim().ToLower() == statelist[0].Trim().ToLower().ToString())
                    {
                        processTableData.ProcessType = (Int32)enProcessName.RoleNumberGeneration;
                    }
                    else if (param.State.ToString().Trim().ToLower() == statelist[1].Trim().ToLower().ToString())
                    {
                        processTableData.ProcessType = (Int32)enProcessName.AdmitCardGeneration;
                    }

                    //end of get the statelist
                    processTableData.ServiceId = (long)enServiceType.NursingApplicationForm;
                    processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
                    processTableData.IsActive = true;
                    processTableData.CreatorUserId = GetCurrentUserId();
                    processTableData.CreationTime = DateTime.UtcNow;
                    processTableData.TenantId = Convert.ToInt64(3);
                    _MSUoW.ProcessTracker.Add(processTableData);
                    _MSUoW.Commit();

                    //GET THE STATE NAME TO MATCH WHICH STATE IT BELONGS TO 
                    stateUpdateName = param.State.Trim().Replace(" ", string.Empty).ToLower() == Constant.AdmitCardGenerationPending.Trim().Replace(" ", string.Empty).ToLower() ? _IConfiguration.GetSection("NursingFormFillup:ForwardToGenerateAdmitCard").Value : _IConfiguration.GetSection("ExamFormFillup:ForwardToGenerateRollNo").Value;

                    if (!param.IsAllSelected)
                    {
                        if (param != null && param.CollegeWiseAmount.Count > 0)
                        {
                            param.CollegeWiseAmount.ForEach(rec =>
                            {
                                BackgroundProcess backgroundObj = new BackgroundProcess();
                                backgroundObj.Id = Guid.NewGuid();
                                backgroundObj.ServiceType = (long)enServiceType.NursingApplicationForm;
                                backgroundObj.InstanceTrigger = stateUpdateName;
                                backgroundObj.Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending); //will be false after sent to COE
                                backgroundObj.TenantWorkflowId = 0;    //need to clarify for tenantworkflow id
                                backgroundObj.CreatorUserId = GetCurrentUserId();
                                backgroundObj.CreationTime = DateTime.UtcNow;
                                backgroundObj.IsActive = true;
                                backgroundObj.TenantId = Convert.ToInt64(3);
                                backgroundObj.CollegeCode = rec.CollegeCode; // as principal send only one college record
                                backgroundObj.CourseCode = param.CourseCode;
                                backgroundObj.StreamCode = param.StreamCode;
                                backgroundObj.CourseTypeCode = param.CourseTypeCode;
                                backgroundObj.DepartmentCode = param.DepartmentCode;
                                backgroundObj.SubjectCode = param.SubjectCode;
                                backgroundObj.SemesterCode = param.SemesterCode;
                                backgroundObj.AcademicStart = param.StartYear;
                                backgroundObj.CurrentAcademicYear = param.CurrentAcademicYear;
                                backgroundProcessesList.Add(backgroundObj);
                            });

                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU075" }; //if 0 record found then break
                    }
                    else
                    {
                        if (param != null && param.CollegeCode.Count > 0)
                        {
                            SearchParamDto parameter = new SearchParamDto();
                            parameter.CollegeCode = param.CollegeCode;
                            parameter.CourseTypeCode = param.CourseTypeCode;
                            parameter.CourseCode = param.CourseCode;
                            parameter.StreamCode = param.StreamCode;
                            parameter.StartYear = param.StartYear;
                            parameter.DepartmentCode = param.DepartmentCode;
                            parameter.SubjectCode = param.SubjectCode;
                            parameter.SemesterCode = param.SemesterCode;
                            parameter.CurrentYear = param.CurrentAcademicYear;
                            parameter.State = param.State;

                            List<CollegeWiseAmountForCoeDto> collegeWiseData = GetCollegeWiseFormFillUpAmount(parameter);
                            if (collegeWiseData != null)
                            {
                                collegeWiseData.ForEach(clg =>
                                {
                                    BackgroundProcess backgroundObj = new BackgroundProcess();
                                    backgroundObj.Id = Guid.NewGuid();
                                    backgroundObj.ServiceType = (long)enServiceType.NursingApplicationForm;
                                    backgroundObj.InstanceTrigger = stateUpdateName;
                                    backgroundObj.Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending); //will be false background updated
                                    backgroundObj.TenantWorkflowId = 0;    //need to clarify for tenantworkflow id
                                    backgroundObj.CreatorUserId = GetCurrentUserId();
                                    backgroundObj.CreationTime = DateTime.UtcNow;
                                    backgroundObj.IsActive = true;
                                    backgroundObj.TenantId = Convert.ToInt64(3);
                                    backgroundObj.CollegeCode = clg.CollegeCode;
                                    backgroundObj.CourseCode = param.CourseCode;
                                    backgroundObj.StreamCode = param.StreamCode;
                                    backgroundObj.CourseTypeCode = param.CourseTypeCode;
                                    backgroundObj.DepartmentCode = param.DepartmentCode;
                                    backgroundObj.SubjectCode = param.SubjectCode;
                                    backgroundObj.SemesterCode = param.SemesterCode;
                                    backgroundObj.CurrentAcademicYear = param.CurrentAcademicYear;
                                    backgroundObj.AcademicStart = param.StartYear;
                                    backgroundProcessesList.Add(backgroundObj);
                                });
                            }
                            else throw new MicroServiceException() { ErrorCode = "OUU075" }; //if 0 record found then break
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU075" }; //if 0 record found then break
                    }

                    _MSUoW.BackgroundProcess.AddRange(backgroundProcessesList);


                    ProcessTracker processTracker = new ProcessTracker();
                    processTracker = _MSUoW.ProcessTracker.GetAll().Where(w => w.Id == newGuid).FirstOrDefault();
                    processTracker.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.ReadyForProcessing);
                    processTracker.LastModifierUserId = processTracker.CreatorUserId;
                    processTracker.LastModificationTime = DateTime.UtcNow;
                    _MSUoW.ProcessTracker.Update(processTracker);
                    _MSUoW.Commit();
                    status = true;
                }
                else throw new MicroServiceException() { ErrorCode = "OUU075" };
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
            return status;
        }

        #region ADMIT CARD DOWNLOAD FOR PRINCIPAL
        public PDFDownloadFile DownloadAdmitCard(certificateDownloadDto obj)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);
            List<string> userRole = GetRoleListOfCurrentUser();
            if (userRole.Contains(Convert.ToString((long)enRoles.Principal)) || userRole.Contains(Convert.ToString((long)enRoles.HOD))
                || userRole.Contains(Convert.ToString((long)enRoles.COE)) || userRole.Contains(Convert.ToString((long)enRoles.AsstCoE)))
            {
                if (obj != null)
                {

                    #region DATA GATHERING FROM TABLE FOR ADMITCARD
                    var examFormData = _MSUoW.NursingFormFillUpRepository.GetAllNursingFormFillUpData().Where(x =>
                                        x.AcknowledgementNo != null
                                        && x.RollNumber.Trim().ToLower() == obj.RegdOrRollNo.Trim().ToLower()
                                        && x.AcknowledgementNo.Trim().ToString() == obj.AcknowNum.Trim().ToString()
                                        && ((x.State.Trim().ToLower() == _IConfiguration.GetSection("NursingFormFillup:AdmitcardGenerated").Value.Trim().ToLower())
                                        || (x.State.Trim().ToLower() == _IConfiguration.GetSection("NursingFormFillup:MarkUploaded").Value.Trim().ToLower()))
                                        )
                                        .OrderByDescending(o => o.CreationTime).FirstOrDefault();

                    if (examFormData != null)
                    {
                        //get all papermaster from cache
                        List<ExaminationPaperMaster> examPapersList = _commonAppService.GetActiveExaminationPaperMasterList().Where(x => x.IsActive).ToList();
                        IQueryable<ExaminationPaperDetail> examPaperIds = _MSUoW.ExaminationPaperDetail.FindBy(e => e.IsActive && e.ExaminationFormFillUpDetailsId == examFormData.Id);

                        List<ExaminationPaperMaster> appliedExamPapers = examPapersList.Where(a => examPaperIds.Select(b => b.ExaminationPaperId).Contains(a.Id)).ToList();
                        AdmitCardCenterAndExamNameView centerAndSession = new AdmitCardCenterAndExamNameView();
                        if (examFormData.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Ug))
                        {
                            centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                                   && e.SubjectCode == examFormData.SubjectCode && e.SemesterCode == examFormData.CurrentAcademicYear && e.CollegeCode == examFormData.CollegeCode
                                                   && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                        }
                        else
                        {
                            centerAndSession = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(e => e.IsActive && e.CourseType == examFormData.CourseTypeCode && e.StreamCode == examFormData.StreamCode
                                                   && e.StreamCode == examFormData.StreamCode && e.DepartmentCode == examFormData.DepartmentCode && e.SubjectCode == examFormData.SubjectCode
                                                   && e.SemesterCode == examFormData.CurrentAcademicYear && e.CollegeCode == examFormData.CollegeCode
                                                   && e.StartYear == examFormData.AcademicStart).FirstOrDefault();
                        }



                        #endregion

                        #region  Email content set part
                        if (centerAndSession != null)
                        {
                            List<ExaminationPaperMaster> allExamPaperList = examPapersList.Where(e => e.ExaminationMasterId == centerAndSession.Id).ToList();
                            id = examFormData.Id;
                            if (id != Guid.Empty)
                            {
                                UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == obj.ServiceType && serv.InstanceId == id && serv.IsActive)
                                            .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                                if (appliedData != null && !appliedData.IsAvailableToDL)
                                    throw new MicroServiceException() { ErrorCode = "OUU050" };
                            }
                            else
                                throw new MicroServiceException() { ErrorCode = "OUU051" };
                            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AdmitCardToStudent && s.IsActive == true).FirstOrDefault();

                            if (templateData != null)
                            {
                                string description = templateData.Body;
                                string url = _IConfiguration.GetSection("ApplicationURL").Value;
                                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                                string examControllerSign = _IConfiguration.GetSection("ControllerOfExaminationSignature").Value;

                                description = description.Replace("--collegeLogo--", url + collegeLogo);
                                centerAndSession.SemesterString = centerAndSession.SemesterString.Replace("Semester", "Year");
                                description = description.Replace("--examName--", centerAndSession.SubjectString + " " + centerAndSession.SemesterString + " " + " EXAMINATION - " + centerAndSession.ExamYear);
                                var image = examFormData.ImageDocument.Where(x => x.DocumentType == "EDU-01").FirstOrDefault();
                                if (image != null)
                                {
                                    description = description.Replace("--photo--", url + image.FilePath);
                                }
                                else
                                {
                                    description = description.Replace("--photo--", "No Photo Available");
                                }
                                description = description.Replace("--regdNo--", examFormData.RegistrationNumber);
                                description = description.Replace("--studentName--", examFormData.StudentName);
                                description = description.Replace("--rollNo--"," "+ examFormData.RollNumber);
                                if (centerAndSession.CenterCollegeString.Trim().ToLower() == examFormData.CollegeCodeString.Trim().ToLower())
                                {
                                    description = description.Replace("--college--", examFormData.CollegeCodeString);
                                    description = description.Replace("--center--", "");
                                }
                                else
                                {
                                    description = description.Replace("--college--", examFormData.CollegeCodeString);
                                    description = description.Replace("--center--", " / " + centerAndSession.CenterCollegeString);

                                }

                                description = description.Replace("--controllerSignature--", url + examControllerSign);

                                var stringtoappend = "";

                                //if (allExamPaperList.Count() == appliedExamPapers.Count())
                                //{
                                //    stringtoappend = "ALL";
                                //}
                                //else
                                //{
                                appliedExamPapers.ForEach(sub =>
                                {

                                    if (sub.IsParctical == true)
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T & P )" + " ,";
                                    }
                                    else
                                    {
                                        stringtoappend = stringtoappend + " " + sub.PaperName + " - ( T )" + " ,";
                                    }
                                });
                                stringtoappend = stringtoappend.Remove(stringtoappend.Length - 1);
                                //}

                                description = description.Replace("--papers--", stringtoappend);
                                flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                                flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                                flattenedPDFDownloadDetails.FileName = "AdmitCard_" + examFormData.RegistrationNumber + ".pdf";
                            }
                            else
                                throw new MicroServiceException { ErrorCode = "OUU009" };
                        }
                        else throw new MicroServiceException { ErrorCode = "OUU125" };
                    }
                    else throw new MicroServiceException { ErrorCode = "OUU125" };
                    #endregion
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU010" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU126" };
            }
            return flattenedPDFDownloadDetails;
        }
        #endregion
        public bool AddMarkDetailsToBackgroundTable(BackgroundProcessDto parameters)
        {
            if (parameters != null)
            {
                ExaminationMaster masterData = _MSUoW.ExaminationMaster.FindBy(a => a.CourseType == parameters.CourseTypeCode
                                            && a.SemesterCode == parameters.SemesterCode
                                            && a.StreamCode == parameters.StreamCode
                                            && a.SubjectCode == parameters.SubjectCode
                                            && a.TenantId == 3
                                            && a.IsActive == true).FirstOrDefault();
                if (masterData != null)
                {
                    ExaminationSession sessionData = _MSUoW.ExaminationSession.FindBy(sess => sess.ExaminationMasterId == masterData.Id && sess.IsActive && sess.StartYear == parameters.StartYear).FirstOrDefault();
                    if (sessionData != null)
                    {
                        ExaminationFormFillUpDates finalDate = _MSUoW.ExaminationFormFillUpDates.FindBy(dates => dates.ExaminationSessionId == sessionData.Id && dates.IsActive).OrderByDescending(a => a.SlotOrder).FirstOrDefault();
                        //var currTime = DateTime.UtcNow;
                        DateTime currTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TZConvert.GetTimeZoneInfo("India Standard Time"));
                        if (finalDate.EndDate < currTime)
                        {
                            var currentUserId = GetCurrentUserId();
                            long userRole = _MSUoW.Users.FindBy(u => u.IsActive && u.Id == currentUserId).Select(s => s.UserType).FirstOrDefault();
                            if (userRole == (long)enUserType.Principal)
                            {
                                //var currTime = DateTime.UtcNow;
                                //Process Tracker Table Entry
                                ProcessTracker processTableData = new ProcessTracker();
                                processTableData.Id = Guid.NewGuid();
                                Guid processTableId = processTableData.Id;
                                processTableData.ServiceId = (long)enServiceType.NursingApplicationForm;
                                processTableData.ProcessType = (Int32)enProcessName.MarkForwardToCOE;
                                processTableData.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.Added);
                                processTableData.IsActive = true;
                                processTableData.CreatorUserId = currentUserId;
                                processTableData.CreationTime = DateTime.UtcNow;
                                processTableData.TenantId = Convert.ToInt64(3);
                                _MSUoW.ProcessTracker.Add(processTableData);
                                _MSUoW.Commit();
                                AddMarkDetailsDataToBackgrounTable(parameters, currentUserId, processTableId);
                            }
                            else throw new MicroServiceException() { ErrorCode = "OUU093" };
                        }
                        else throw new MicroServiceException() { ErrorCode = "OUU095" };// Do finalDate error
                    }
                    else throw new MicroServiceException() { ErrorCode = "OUU114" };// Do sessionData error
                }
                else throw new MicroServiceException() { ErrorCode = "OUU114" }; // Do masterData error
            }
            else throw new MicroServiceException() { ErrorCode = "OUU075" };

            return true;
        }
        public async Task<bool> AddMarkDetailsDataToBackgrounTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            await Task.Run(() =>
            {
                AddMarkDetailsToBackgroundTable(parameters, userId, processTableId);
                return true;
            });
            return false;
        }
        public bool AddMarkDetailsToBackgroundTable(BackgroundProcessDto parameters, long userId, Guid processTableId)
        {
            using (var _MSUoW = new MicroServiceUOW(new RepositoryProvider(new RepositoryFactories()), _redisData, _logger))
            {
                List<BackgroundProcess> data = new List<BackgroundProcess>();
                try
                {

                    if (parameters.SelectedInstanceIds != null && parameters.IsAllSelected == false)
                    {
                        var ids = parameters.SelectedInstanceIds.Split(",").ToList();
                        data = _MSUoW.NursingViewExcludeBackgroundProcessTable
                                .GetAll()
                                .Where(d => d.IsActive
                                && ids.Contains(d.Id.ToString())
                                && d.State == parameters.State
                                && d.MarkUploaded == _MSCommon.GetEnumDescription(enMarkUploaded.Yes))
                                .Select(s => new BackgroundProcess
                                {
                                    Id = Guid.NewGuid(),
                                    ServiceType = (long)enServiceType.NursingApplicationForm,
                                    InstanceTrigger = _IConfiguration.GetSection("NursingFormFillup:MarkForwardToCOE").Value,
                                    InstanceId = s.Id,
                                    Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),//will be false after sent to COE
                                    TenantWorkflowId = s.TenantWorkflowId,
                                    CreatorUserId = userId,
                                    CreationTime = DateTime.UtcNow,
                                    IsActive = true,
                                    TenantId = Convert.ToInt64(3),
                                    CollegeCode = parameters.CollegeCode[0], // as principal send only one college record
                                    CourseCode = parameters.CourseCode,
                                    StreamCode = parameters.StreamCode,
                                    CourseTypeCode = parameters.CourseTypeCode,
                                    DepartmentCode = parameters.DepartmentCode,
                                    SubjectCode = parameters.SubjectCode,
                                    SemesterCode = parameters.SemesterCode,
                                    AcademicStart = parameters.StartYear
                                })
                                .ToList();
                    }
                    else if (parameters.SelectedInstanceIds == null && parameters.IsAllSelected)
                    {
                        if (parameters.DepartmentCode == null)
                        {
                            data = _MSUoW.NursingViewExcludeBackgroundProcessTable
                                .GetAll().Where(d => d.IsActive
                                && d.State == parameters.State
                                && d.CourseTypeCode == parameters.CourseTypeCode
                                && parameters.CollegeCode.Contains(d.CollegeCode)
                                && d.StreamCode == parameters.StreamCode
                                && d.CurrentAcademicYear == parameters.CurrentAcademicYear
                                && d.SubjectCode == parameters.SubjectCode
                                && d.AcademicStart == parameters.StartYear
                                && d.MarkUploaded == _MSCommon.GetEnumDescription(enMarkUploaded.Yes)
                                )
                                .Select(s => new BackgroundProcess
                                {
                                    Id = Guid.NewGuid(),
                                    ServiceType = (long)enServiceType.NursingApplicationForm,
                                    InstanceTrigger = _IConfiguration.GetSection("NursingFormFillup:MarkForwardToCOE").Value,
                                    InstanceId = s.Id,
                                    Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                    TenantWorkflowId = s.TenantWorkflowId,
                                    CreatorUserId = userId,
                                    CreationTime = DateTime.UtcNow,
                                    IsActive = true,
                                    TenantId = Convert.ToInt64(3),
                                    CollegeCode = parameters.CollegeCode[0],
                                    CourseCode = parameters.CourseCode,
                                    StreamCode = parameters.StreamCode,
                                    CourseTypeCode = parameters.CourseTypeCode,
                                    DepartmentCode = parameters.DepartmentCode,
                                    SubjectCode = parameters.SubjectCode,
                                    SemesterCode = parameters.SemesterCode,
                                    AcademicStart = parameters.StartYear
                                })
                                .ToList();
                        }
                        else
                        {
                            data = _MSUoW.NursingViewExcludeBackgroundProcessTable.FindBy(d => d.IsActive && d.State == parameters.State && d.CourseTypeCode == parameters.CourseTypeCode
                                                                   && parameters.CollegeCode.Contains(d.CollegeCode) && d.StreamCode == parameters.StreamCode && d.CurrentAcademicYear == parameters.CurrentAcademicYear
                                                                   && d.SubjectCode == parameters.SubjectCode && d.DepartmentCode == parameters.DepartmentCode && d.CourseCode == parameters.CourseCode && d.AcademicStart == parameters.StartYear)
                                                  .Select(s => new BackgroundProcess
                                                  {
                                                      Id = Guid.NewGuid(),
                                                      ServiceType = (long)enServiceType.NursingApplicationForm,
                                                      InstanceTrigger = _IConfiguration.GetSection("NursingFormFillup:MarkForwardToCOE").Value,
                                                      InstanceId = s.Id,
                                                      Status = _MSCommon.GetEnumDescription(enBackgroundProcessStatus.Pending),
                                                      TenantWorkflowId = s.TenantWorkflowId,
                                                      CreatorUserId = userId,
                                                      CreationTime = DateTime.UtcNow,
                                                      IsActive = true,
                                                      TenantId = Convert.ToInt64(3),
                                                      CollegeCode = parameters.CollegeCode[0],
                                                      CourseCode = parameters.CourseCode,
                                                      StreamCode = parameters.StreamCode,
                                                      CourseTypeCode = parameters.CourseTypeCode,
                                                      DepartmentCode = parameters.DepartmentCode,
                                                      SubjectCode = parameters.SubjectCode,
                                                      SemesterCode = parameters.SemesterCode,
                                                      AcademicStart = parameters.StartYear
                                                  }).ToList();
                        }
                    }
                    _MSUoW.BackgroundProcess.AddRange(data);

                    ProcessTracker processTracker = new ProcessTracker();
                    var currTime = DateTime.UtcNow;
                    processTracker = _MSUoW.ProcessTracker.GetAll().Where(w => w.Id == processTableId && w.CreatorUserId == userId).FirstOrDefault();
                    processTracker.ProcessStatus = _MSCommon.GetEnumDescription((enProcessStatus)enProcessStatus.ReadyForProcessing);
                    processTracker.LastModifierUserId = processTracker.CreatorUserId;
                    processTracker.LastModificationTime = currTime;
                    _MSUoW.ProcessTracker.Update(processTracker);
                    _MSUoW.Commit();
                }
                catch (Exception e)
                {
                }
                return true;
            }
            // string formStatus = _IConfiguration.GetSection("ExamFormFillup:FormStates").Value;

        }

        public IQueryable<NursingFormfillupDetails> GetNursingDetails()
        {
            return _MSUoW.NursingFormfillupDetails.GetAll().Where(n => n.IsActive);
        }
        public IQueryable<NursingAcademicStatementDto> GetNursingAcademicDetails()
        {
            return GetNursingDetails()
                .Where(x => x.PaymentDate != null
                && x.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:VerificationPending").Value.Trim().ToLower()
                && x.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:PaymentPendingState").Value.Trim().ToLower()
                && x.State.Trim().ToLower() != _IConfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower())
                .Select(s => new NursingAcademicStatementDto
                {
                    Id = s.Id,
                    AcademicStart = s.AcademicStart,
                    AcknowledgementNo = s.AcknowledgementNo,
                    CenterCharge = s.CenterCharges,
                    CollegeCode = s.CollegeCode,
                    CollegeName = s.CollegeCodeString,
                    CourseCode = s.CourseCode,
                    CourseTypeCode = s.CourseTypeCode,
                    CreationTime = s.CreationTime,
                    CreatorUserId = s.CreatorUserId,
                    CurrentAcademicYear = s.CurrentAcademicYear,
                    DepartmentCode = s.DepartmentCode,
                    FormFillUpMoneyToBePaid = s.FormFillUpMoneyToBePaid - s.LateFeeAmount,
                    InternetHandlingCharges = s.InternetHandlingCharges,
                    IsActive = s.IsActive,
                    LastModificationTime = s.LastModificationTime,
                    LastModifierUserId = s.LastModifierUserId,
                    LateFeeAmount = s.LateFeeAmount,
                    RegistrationNumber = s.RegistrationNumber,
                    RollNumber = s.RollNumber,
                    State = s.State,
                    StreamCode = s.StreamCode,
                    StudentName = s.StudentName,
                    SubjectCode = s.SubjectCode,
                    SubjectName = s.SubjectCodeString,
                    TenantId = s.TenantId,
                    TotalAmount = s.FormFillUpMoneyToBePaid == 0 ? 0 : s.FormFillUpMoneyToBePaid - s.CenterCharges
                });
        }
        public IQueryable<NursingMarkStatementView> GetAllNursingMarkDdetails()
        {
            return _MSUoW.NursingMarkStatementView.GetAll().Where(e => e.IsActive);
        }
        public List<NursingMarkStatementView> GetNursingMarkDetailsForL1Support(SearchParamDto input)
        {
            return GetAllNursingMarkDdetails()
                    .Where(e => input.CourseTypeCode == input.CourseTypeCode
                    && input.CollegeCode.Contains(e.CollegeCode)
                    && input.StreamCode == e.StreamCode
                    && input.CourseTypeCode == _MSCommon.GetEnumDescription(enCourseType.Pg) ?
                    (input.DepartmentCode == e.DepartmentCode
                    && input.CourseCode == e.CourseCode && e.IsActive) :
                    e.IsActive
                    && input.SubjectCode == e.SubjectCode
                    && input.SemesterCode == e.CurrentAcademicYear
                    && input.State.Trim().ToLower() == e.State.Trim().ToLower()
                    && input.StartYear == e.AcademicStart)
                    .OrderBy(o => o.StudentName)
                    .ThenBy(t => t.AcknowledgementNo)
                    .ToList();
        }

        public PDFDownloadFile DownloadSemesterExamPdfReport(SearchParamDto input)
        {
            PDFDownloadFile pdfDetails = new PDFDownloadFile();
            var predicate = PredicateBuilder.True<NursingFormfillupDetails>();

            if (input != null)
            {
                if (!string.IsNullOrEmpty(input.CourseTypeCode))
                {
                    predicate = predicate.And(i => i.CourseTypeCode.ToLower().Equals(input.CourseTypeCode.ToLower()));
                }
                if (input.CollegeCode.Count > 0)
                {
                    predicate = predicate.And(i => input.CollegeCode.Contains(i.CollegeCode));
                }
                if (!string.IsNullOrEmpty(input.StreamCode))
                {
                    predicate = predicate.And(i => i.StreamCode.ToLower().Equals(input.StreamCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.SubjectCode))
                {
                    predicate = predicate.And(i => i.SubjectCode.ToLower().Equals(input.SubjectCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.DepartmentCode))
                {
                    predicate = predicate.And(i => i.DepartmentCode.ToLower().Equals(input.DepartmentCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.CourseCode))
                {
                    predicate = predicate.And(i => i.CourseCode.ToLower().Equals(input.CourseCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.SemesterCode))
                {
                    predicate = predicate.And(i => i.CurrentAcademicYear.ToLower().Equals(input.SemesterCode.ToLower()));
                }
                if (!string.IsNullOrEmpty(input.State))
                {
                    predicate = predicate.And(i => i.State.ToLower().Equals(input.State.ToLower()));
                }
                if (input.StartYear > 0)
                {
                    predicate = predicate.And(i => i.AcademicStart.Equals(input.StartYear));
                }
            }

            List<ExaminationPaperMaster> papermaster = _commonAppService.GetActiveExaminationPaperMasterList();

            IQueryable<NursingFormfillupDetails> ugStudentExaminationDetailsList = _MSUoW.NursingFormfillupDetails.GetAll().
                                                                                        Where(predicate).Where(u => u.IsActive == true
                                                                                        && u.State == Constant.MarkUploaded)
                                                                                        .OrderBy(q => q.RollNumber);

            //List<ExaminationView> examinaionViewDetails = new List<ExaminationView>();

            List<NursingFormfillupDetails> ExamList = new List<NursingFormfillupDetails>();
            //gridModelDto.Count = ugStudentExaminationDetailsList.Count();
            //ExamList = ugStudentExaminationDetailsList.ToList();

            List<Guid> instanceids = ugStudentExaminationDetailsList.Select(z => z.Id).ToList();

            //examinaionViewDetails = _MSUoW.ExaminationView.GetAll().Where(w => w.IsActive && instanceids.Contains(w.Id)).ToList();
            List<ExaminationFormFillUpSemesterMarks> ExamPaperList = _MSUoW.ExaminationFormFillUpSemesterMarks
                                                                    .FindBy(p => instanceids.Contains(p.ExaminationFormFillUpDetailsId)
                                                                    && p.IsActive == true)
                                                                    .ToList();

            var data = from ugsed in ugStudentExaminationDetailsList
                       join ugespd in ExamPaperList on ugsed.Id equals ugespd.ExaminationFormFillUpDetailsId
                       orderby ugespd.RollNumber
                       join pm in papermaster on ugespd.ExaminationPaperId equals pm.Id
                       where pm.IsActive == true
                       orderby pm.CategoryAbbreviation
                       select new
                       {
                           Id = ugespd.Id,
                           InstantId = ugespd.ExaminationFormFillUpDetailsId,
                           StudentName = ugsed.StudentName,
                           //FatherName = ugsed.FatherName,
                           // GuardianName = ugsed.GuardianName,
                           //DateOfBirth = ugsed.DateOfBirth,
                           RegistrationNumber = ugsed.RegistrationNumber,
                           RollNumber = ugsed.RollNumber,
                           //AcknowledgementNo = ugsed.AcknowledgementNo,

                           CourseTypeCode = ugsed.CourseTypeCode,
                           CollegeCode = ugsed.CollegeCode,
                           StreamCode = ugsed.StreamCode,
                           SubjectCode = ugsed.SubjectCode,
                           DepartmentCode = ugsed.DepartmentCode,
                           CourseCode = ugsed.CourseCode,
                           //SemesterCode = ugsed.SemesterCode,
                           AcademicStart = ugsed.AcademicStart,
                           CourseTypeString = ugsed.CourseTypeCodeString,
                           CollegeString = ugsed.CollegeCodeString,
                           StreamString = ugsed.StreamCodeString,
                           SubjectString = ugsed.SubjectCodeString,
                           DepartmentString = ugsed.DepartmentCodeString,
                           CourseString = ugsed.CourseCodeString,
                           // SemesterString = ugsed.SemesterString,
                           CurrentAcademicYear = ugsed.CurrentAcademicYear,
                           CurrentAcademicYearString = ugsed.CurrentAcademicYearString,
                           CategoryAbbreviation = pm.CategoryAbbreviation,
                           PaperName = pm.PaperName,
                           TotalInternalMark = pm.InternalMark,
                           TotalPracticalMark = pm.PracticalMark,
                           ToralExternalMark = pm.ExternalMark,
                           SecuredInternalMark = ugespd.InternalMark == null ? "--" : ugespd.InternalMark.ToString(),
                           SecuredPracticalMark = ugespd.PracticalMark == null ? "--" : ugespd.PracticalMark.ToString(),
                           SecuredExternalMark = ugespd.ExternalMark == null ? "--" : ugespd.ExternalMark.ToString(),
                           IsActive = ugespd.IsActive,
                           ExamPaperId = ugespd.ExaminationPaperId
                       };

            data = data.OrderBy(q => q.StudentName).Where(p => input.CollegeCode.Contains(p.CollegeCode));


            if (data.Count() > 0)
            {
                //get email template
                EmailTemplate templateData = _commonAppService.GetEmailTemplateDataFromCache().Where(x => x.TemplateType == (long)enEmailTemplate.NursingSemesterMarkReport).FirstOrDefault();

                //creating table header object for college code, college name,stream, subject, academy year
                var examDetails = data.Select(x => new
                {
                    CollegeCode = x.CollegeCode,
                    CollegeName = x.CollegeString,
                    CourseType = x.CourseTypeCode,
                    CourseTypeName = x.CourseTypeString,
                    Stream = x.StreamCode,
                    StreamName = x.StreamString,
                    Subject = x.SubjectCode,
                    SubjectName = x.SubjectString,
                    CurrentAcademicYear = x.CurrentAcademicYear,
                    CurrentAcademicYearString = x.CurrentAcademicYearString,
                    AcademicStart = x.AcademicStart
                }).FirstOrDefault();

                //creating table object for pdf
                var headerObject = data
                    .GroupBy(g => new { g.CourseTypeCode, g.CollegeCode, g.CurrentAcademicYear, g.StreamCode, g.CourseCode, g.SubjectCode, g.DepartmentCode, g.AcademicStart, g.RollNumber })
                    .Select(x => new
                    {
                        Name = x.Select(bn => bn.StudentName).FirstOrDefault(),
                        RollNo = x.Key.RollNumber,
                        RegNo = x.Select(bn => bn.RegistrationNumber).FirstOrDefault(),
                        InstantId = x.Select(bn => bn.InstantId).FirstOrDefault(),
                        //x.RegistrationNumber,
                        ExamPaperId = x.Select(bn => bn.ExamPaperId).ToList(),
                    }).OrderBy(o => o.Name)
                //.ThenBy(to => to.CategoryAbbreviation)
                .ToList();

                ExaminationMaster examinationMaster = _commonAppService.GetActiveExaminationMasterList().Where(w =>
                                                       w.CourseType == input.CourseTypeCode &&
                                                       w.StreamCode == input.StreamCode &&
                                                       w.SubjectCode == input.SubjectCode &&
                                                       w.SemesterCode == input.SemesterCode).FirstOrDefault();
                List<ExaminationPaperMaster> allPaperdetails = new List<ExaminationPaperMaster>();
                if (examinationMaster != null)
                {
                    allPaperdetails = papermaster.Where(w => w.ExaminationMasterId == examinationMaster.Id).ToList();

                }

                if (allPaperdetails.Count > 0 && templateData != null)
                {
                    string description = templateData.Body;
                    description = description.Replace("--collegeName--", examDetails.CollegeName);
                    description = description.Replace("--collegeCode--", examDetails.CollegeCode);
                    description = description.Replace("--stream--", examDetails.StreamName);
                    description = description.Replace("--semester--", examDetails.CurrentAcademicYearString);
                    description = description.Replace("--courseType--", examDetails.CourseTypeName);
                    description = description.Replace("--subject--", examDetails.SubjectName);
                    description = description.Replace("--academicYear--", examDetails.AcademicStart.ToString());

                    // "<td align=\"center\" valign=\"middle\" style=\"width:15%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Student Name</td>" 

                    string fixedTableHeaderName = "<td align=\"center\" valign=\"middle\" style=\"width:15%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Roll No.</td>";
                    string tableHeaderName = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" colspan=\"4\">--columnName--</td>";
                    string replaceTableName = string.Empty;
                    string assignReplaceTableName = string.Empty;
                    string finalTableHeader = string.Empty;
                    string IPEString = "<td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">I</td> <td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">T</td> <td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">P</td><td align=\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\">Total</td>";
                    string SecondHeader = string.Empty;
                    string ValueRow = "<td align =\"center\" valign=\"middle\" width=\"50\" style=\"padding: .4rem .75rem; border: 1px solid #000; font-weight: 600;\">--value--</td>";
                    string remarkHeader = "<td align =\"center\" valign=\"middle\" style=\"width:25%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Remarks</td>";
                    string totalSemesterHeader = "<td align =\"center\" valign=\"middle\" style=\"width:25%;padding: .4rem .75rem; border: 1px solid #000; font-weight: 900;\" rowspan=\"2\">Total Semester</td>";
                    string replaceValue = string.Empty;
                    string FinalValue = string.Empty;
                    string FinalRowValue = string.Empty;

                    //for (int j = 0; j <= 100; j++)
                    //{
                    //    headerObject.Add(headerObject[j]);
                    //}
                    //creating header
                    for (int i = 0; i < headerObject.Count(); i++)
                    {
                        //get exam paper details from exam semester mark table
                        List<ExaminationFormFillUpSemesterMarks> markList = new List<ExaminationFormFillUpSemesterMarks>();
                        headerObject[i].ExamPaperId.ForEach(paper =>
                        {
                            ExaminationFormFillUpSemesterMarks paperMark = ExamPaperList.Where(ep => ep.ExaminationPaperId == paper && ep.ExaminationFormFillUpDetailsId == headerObject[i].InstantId).FirstOrDefault();
                            ExaminationPaperMaster paperMasterObj = papermaster.Where(w => w.Id == paperMark.ExaminationPaperId).FirstOrDefault();

                            //get paper code from paper master
                            paperMark.CategoryAbbreviation = paperMasterObj.CategoryAbbreviation;
                            paperMark.PaperAbbreviation = paperMasterObj.PaperAbbreviation;

                            markList.Add(paperMark);
                        });

                        FinalValue = string.Empty;
                        replaceValue = string.Empty;
                        //replaceValue += ValueRow;
                        //FinalValue += replaceValue.Replace("--value--", headerObject[i].Name);
                        //replaceValue = string.Empty;
                        replaceValue += ValueRow;
                        FinalValue += replaceValue.Replace("--value--", headerObject[i].RollNo);

                        //adding mark details
                        if (markList.Count > 0)
                        {
                            long TotalSemester = 0;
                            //Create subject heading
                            allPaperdetails.ForEach((subj) =>
                            {
                                replaceTableName = tableHeaderName;
                                assignReplaceTableName += replaceTableName.Replace("--columnName--", "Paper-" + subj.CategoryAbbreviation + "(" + subj.PaperAbbreviation + ")");
                                replaceTableName = string.Empty;
                                SecondHeader += IPEString;

                                ExaminationFormFillUpSemesterMarks mark = markList.Where(w => w.ExaminationPaperId == subj.Id).FirstOrDefault();
                                if (mark != null)
                                {
                                    //Bind value for each paper of 3 marks category
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", mark.InternalMark == null ? "--" : mark.InternalMark.ToString());
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", mark.ExternalMark == null ? "--" : mark.ExternalMark.ToString());
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", mark.PracticalMark == null ? "--" : mark.PracticalMark.ToString());
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    long? inter = mark.InternalMark != null ? mark.InternalMark : 0;
                                    long? external = mark.ExternalMark != null ? mark.ExternalMark : 0;
                                    long? pratical = mark.PracticalMark != null ? mark.PracticalMark : 0;
                                    long? total = inter + external + pratical;
                                    TotalSemester += (long)total;
                                    FinalValue += replaceValue.Replace("--value--", total == null ? "--" : total.ToString());
                                }
                                //student not apply for this subject 
                                else
                                {

                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                    replaceValue = string.Empty;
                                    replaceValue += ValueRow;
                                    FinalValue += replaceValue.Replace("--value--", "NA");
                                }

                            });
                            replaceValue = string.Empty;
                            replaceValue += ValueRow;
                            FinalValue += replaceValue.Replace("--value--", TotalSemester.ToString());// for total semester
                            replaceValue = string.Empty;
                            replaceValue += ValueRow;
                            FinalValue += replaceValue.Replace("--value--", string.Empty);// for remark
                        }
                        if (i == 0)
                        {
                            finalTableHeader += "<tr>" + fixedTableHeaderName + assignReplaceTableName + totalSemesterHeader + remarkHeader + "</tr>";
                            SecondHeader = "<tr>" + SecondHeader + "</tr>";
                            finalTableHeader += SecondHeader;
                        }

                        FinalRowValue += "<tr>" + FinalValue + "</tr>";
                        // header value section
                    }

                    description = description.Replace("--header--", finalTableHeader);
                    description = description.Replace("--headerValue--", FinalRowValue);

                    pdfDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Landscape);
                    pdfDetails.FileType = "application/octet-stream";
                    pdfDetails.FileName = "Nursing_Mark_Statement_" + examDetails.CollegeCode + ".pdf";
                }

            }


            return pdfDetails;
        }

        public PDFDownloadFile DownloadConductCertificate(certificateDownloadDto input)
        {
            if(input == null)
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            PDFDownloadFile pdfDetails = new PDFDownloadFile();
            List<string> userRole = GetRoleListOfCurrentUser();
            if (userRole.Contains(Convert.ToString((long)enRoles.Principal)) || userRole.Contains(Convert.ToString((long)enRoles.HOD)))
            {
                NursingFormfillupDetails nursingDetails = _MSUoW.NursingFormFillUpRepository.GetAllNursingFormFillUpData().Where(w => w.AcknowledgementNo == input.AcknowNum 
                && w.RollNumber == input.RegdOrRollNo && w.State.Trim().ToLower() == _IConfiguration.GetSection("NursingFormFillup:AdmitcardGenerated").Value.Trim().ToLower()).FirstOrDefault();
                if(nursingDetails == null)
                {
                    throw new MicroServiceException { ErrorMessage = "Student details not found in current state." };
                }
                CollegeMaster collegeDetails = _commonAppService.GetAllGetCollegeMasterListDataFromCache().Where(w => w.IsActive 
                && w.CollegeFilterCode == ((long)enCollegeType.Has_UG_PBN_BSCN).ToString() && w.LookupCode == nursingDetails.CollegeCode).FirstOrDefault();
                if(collegeDetails == null)
                {
                    throw new MicroServiceException { ErrorMessage = "College details not found." };
                }
                EmailTemplate emailDetails = _commonAppService.GetEmailTemplateDataFromCache().Where(w => w.TemplateType == (long)enEmailTemplate.NursingConductCertificates && w.IsActive
                     && w.ServiceType == (long)enServiceType.NursingApplicationForm).FirstOrDefault();
                if(emailDetails== null)
                {
                    throw new MicroServiceException { ErrorMessage = "Email details not found." };
                }
                string description = emailDetails.Body;
                description = description.Replace("--collegeName--", collegeDetails.LookupDesc);
                description = description.Replace("--collegeAdress--", collegeDetails.Address);
                description = description.Replace("--collgeCode--", collegeDetails.LookupCode);

                description = description.Replace("--studentName--", nursingDetails.StudentName);
                description = description.Replace("--rollNo--", nursingDetails.RollNumber);
                description = description.Replace("--subjectString--", nursingDetails.SubjectCodeString);
                description = description.Replace("--currentAcademicYear--", nursingDetails.CurrentAcademicYearString);
                description = description.Replace("--academicYear--", nursingDetails.AcademicStart.ToString());

                pdfDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                pdfDetails.FileType = "application/octet-stream";
                pdfDetails.FileName = "Attendance_Conduct_Certificate" + nursingDetails.RollNumber + ".pdf";
                return pdfDetails;
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Unauthorized Access." };
            }
        }
    }
}
