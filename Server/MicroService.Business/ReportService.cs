﻿using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.Report;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MicroService.Business
{
    public class ReportService : BaseService, IReportService
    {
        private IMicroServiceUOW _MSUoW;
        private IMicroserviceCommon _MSCommon;
        private IUserService _UserService;
        private IConfiguration _iconfiguration;
        private IExaminationFormFillUpService _exmService;
        private ICommonAppService _commonservice;

        public ReportService(
            IMicroServiceUOW MSUoW,
            IRedisDataHandlerService redisData,
            IMicroserviceCommon MSCommon,
            IUserService userService,
            IConfiguration iconfiguration,
            IExaminationFormFillUpService examinationFormFillUpService,
            ICommonAppService commonservice) : base(redisData)
        {
            _MSUoW = MSUoW;
            _MSCommon = MSCommon;
            _UserService = userService;
            _iconfiguration = iconfiguration;
            _exmService = examinationFormFillUpService;
            _commonservice = commonservice;
        }
        /// <summary>
		/// Author  :   Sumbul Samreen
		/// Date    :   03-12-2019
		/// Description :This method is used to call the sp and get all the data for CNR report.
		/// </summary>
		/// <param name="reportParam"></param>
		/// <returns></returns>
        public GridModelDto GetAllStudentReport(BackgroundProcessDto reportParam)
        {

            var re = _MSUoW.UserRepository.callSp(reportParam);

            return re;
        }
        /// <summary>
		/// Author  :   Sumbul Samreen
		/// Date    :   03-12-2019
		/// Description ::This method is used to call the sp and get all the data for CNR report download.
		/// </summary>
		/// <param name="reportParam"></param>
		/// <returns></returns>
        public PDFDownloadFile CallSPForReportDownload(BackgroundProcessDto reportParam)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            var result = _MSUoW.UserRepository.callSp(reportParam);
            switch(reportParam.ServiceId)
            {
                case (long)enServiceType.BCABBAExamApplicationForm:
                    flattenedPDFDownloadDetails = GetAllDownloadedReport(reportParam, result);
                    break;

                case (long)enServiceType.NursingApplicationForm:
                    flattenedPDFDownloadDetails = GetAllDownloadedReportForNursing(reportParam, result);
                    break;
            }
            return flattenedPDFDownloadDetails;
        }


        public PDFDownloadFile GetAllDownloadedReport(BackgroundProcessDto reportParam, GridModelDto result)
        {
            int pageWidth = 1240;
            int recordesPerPage = 15;

            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            //************************** GET ALL THE TABLE RECORD TO BIND THE HEADLINE OF THE CNR REPORT ************************************//
            ExaminationSession examSession = new ExaminationSession();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.CnrReport && s.IsActive == true).FirstOrDefault();
            List<Lookup> lookUpList = _MSUoW.Lookup.GetAll().Where(lk => lk.IsActive).ToList();
            List<SubjectMaster> subjMast = _MSUoW.SubjectMaster.GetAll().Where(sub => sub.IsActive).ToList();
            ExaminationMaster examMaster = _MSUoW.ExaminationMaster.FindBy(em => em.CourseType == reportParam.CourseTypeCode && em.StreamCode == reportParam.StreamCode && em.SubjectCode == reportParam.SubjectCode
                                          && em.CourseCode == reportParam.CourseCode && em.DepartmentCode == reportParam.DepartmentCode && em.SemesterCode == reportParam.SemesterCode
                                          && em.IsActive)
                                          .FirstOrDefault();
            List<CollegeMaster> clgMaster = _MSUoW.CollegeMaster.GetAll().Where(clg => clg.IsActive).ToList();
            if (examMaster != null)
            {
                examSession = _MSUoW.ExaminationSession.FindBy(es => es.IsActive && es.ExaminationMasterId == examMaster.Id).FirstOrDefault();
            }

            //************************** END OF GETTING ALL THE RECORD *******************************//

            string tbody = string.Empty;//= templateData.Body;

            string coldata = string.Empty;
            string value = string.Empty;

            decimal noOfTotalPages = (decimal)result.Rows.Count / recordesPerPage;
            int count = 0;
            int indexNumber = 0;

            indexNumber = result.Columns.FindIndex(x => x.ColumnName == "Record Count");
            if (indexNumber != 0)
            {
                result.Columns.RemoveAt(indexNumber);

                result.Rows.ForEach(item =>
                {
                    item.VisibleFields.RemoveAt(indexNumber);
                });
            }

            for (int pageNo = 1; pageNo <= Math.Ceiling(noOfTotalPages); pageNo++)
            {
                tbody += templateData.Body;
                string textBody = string.Empty;
                textBody +=
               "<table class='table' border=" + 1 + " cellspacing =" + 20 + " cellpadding" + 20 + " width = " + pageWidth + "><thead><tr>";
                textBody += "<th>Sl. No.</th>";
                foreach (var col in result.Columns)
                {
                    coldata = col.ColumnName;
                    textBody += "<th>" + coldata + "</th>";
                }
                textBody += "</tr></thead><tbody>";


                var pageRows = result.Rows.Skip((pageNo - 1) * recordesPerPage).Take(recordesPerPage).ToList();


                foreach (var rows in pageRows)
                {
                    textBody += "<tr>";
                    textBody += "<td>" + (int)(count + 1) + "</td> ";
                    var rowData = rows.VisibleFields;
                    foreach (var data in rowData)
                    {
                        value = data;
                        textBody += "<td>" + value + "</td> ";
                    }
                    textBody += "</tr>";
                    count++;
                }


                textBody += "</tbody>";
                textBody += "</table>";


                tbody = tbody.Replace("--textBody--", textBody);
                tbody = tbody.Replace("--pageNo--", "Page " + pageNo + " of " + Math.Ceiling(noOfTotalPages));

                if (pageNo != Convert.ToInt16(Math.Ceiling(noOfTotalPages)))
                {
                    tbody += "<div style='page-break-after: always;'> </div>";
                }
            }

            string applicationURL = _iconfiguration.GetSection("ApplicationURL").Value;
            string collegeLogo = _iconfiguration.GetSection("UULogo").Value;
            List<AdmitCardCenterAndExamNameView> adminCardCenterView = _MSUoW.AdmitCardCenterAndExamNameView.FindBy(ad => ad.IsActive && ad.SemesterCode == reportParam.SemesterCode
                                                                        && ad.CourseCode == reportParam.CourseCode && ad.CourseType == reportParam.CourseTypeCode && ad.ExamYear == reportParam.StartYear
                                                                        && ad.StreamCode == reportParam.StreamCode && ad.SubjectCode == reportParam.SubjectCode && ad.DepartmentCode == ad.DepartmentCode
                                                                        && reportParam.CollegeCode.Contains(ad.CenterCollegeCode)).ToList();
            //tbody = tbody.Replace("--collegeLogo--", applicationURL + collegeLogo);
            tbody = tbody.Replace("--collegeCode--", reportParam.CollegeCode.FirstOrDefault());
            tbody = tbody.Replace("--semesterName--", lookUpList.Where(lk => lk.LookupCode == reportParam.SemesterCode && lk.IsActive && lk.LookupTypeId == (long)enLookUpType.Semester)
                    .Select(lk => lk.LookupDesc).FirstOrDefault());
            tbody = tbody.Replace("--subjectName--", subjMast.Where(sub => sub.IsActive && sub.LookupCode == reportParam.SubjectCode).Select(sub => sub.LookupDesc).FirstOrDefault());
            tbody = tbody.Replace("--applicationType--", lookUpList.Where(lk => lk.IsActive && lk.LookupCode == reportParam.ApplicationType && lk.LookupTypeId == (long)enLookUpType.ApplicationType).Select(lk => lk.LookupDesc).FirstOrDefault());
            var calulaterAcademicYearNo = Math.Ceiling((decimal)Convert.ToInt32(reportParam.SemesterCode) / 2);
            var academicSessionStartYear = Convert.ToInt32(reportParam.StartYear + calulaterAcademicYearNo - 1);
            tbody = tbody.Replace("--academicSession--", academicSessionStartYear + " - " + (Convert.ToInt32(academicSessionStartYear) + 1));
            tbody = tbody.Replace("--examinationYear--", Convert.ToString(examSession.ExamYear));
            tbody = tbody.Replace("--collegeName--", clgMaster.Where(clg => clg.LookupCode == reportParam.CollegeCode.FirstOrDefault()).Select(clg => clg.LookupDesc).FirstOrDefault());
            tbody = tbody.Replace("--totalCandidate--", Convert.ToString(result.Rows.Count));

            flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(tbody, enOrientation.Landscape);
            flattenedPDFDownloadDetails.FileType = "application/octet-stream";
            flattenedPDFDownloadDetails.FileName = "CnrReport.pdf";
            return flattenedPDFDownloadDetails;
        }

        public PDFDownloadFile GetAllDownloadedReportForNursing(BackgroundProcessDto reportParam, GridModelDto result)
        {
            int pageWidth = 1240;
            int recordesPerPage = 15;

            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            //************************** GET ALL THE TABLE RECORD TO BIND THE HEADLINE OF THE CNR REPORT ************************************//
            ExaminationSession examSession = new ExaminationSession();
            long[] lookupIdList = new long[] { (long)enLookUpType.BscNursing, (long)enLookUpType.ApplicationType };
            IQueryable<Lookup> lookUpList = _commonservice.GetLookupData(lookupIdList);
            IQueryable<SubjectMaster> subjMast = _commonservice.GetAllSubject();
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.CnrReport && s.IsActive == true).FirstOrDefault();
            ExaminationMaster examMaster = _MSUoW.ExaminationMaster.FindBy(em => em.CourseType == reportParam.CourseTypeCode && em.StreamCode == reportParam.StreamCode && em.SubjectCode == reportParam.SubjectCode
                                          && em.CourseCode == reportParam.CourseCode && em.DepartmentCode == reportParam.DepartmentCode && em.SemesterCode == reportParam.CurrentAcademicYear
                                          && em.IsActive)
                                          .FirstOrDefault();
            List<CollegeMaster> clgMaster = _MSUoW.CollegeMaster.GetAll().Where(clg => clg.IsActive).ToList();
            if (examMaster != null)
            {
                examSession = _MSUoW.ExaminationSession.FindBy(es => es.IsActive && es.ExaminationMasterId == examMaster.Id).FirstOrDefault();
            }

            //************************** END OF GETTING ALL THE RECORD *******************************//

            string tbody = string.Empty;//= templateData.Body;

            string coldata = string.Empty;
            string value = string.Empty;

            decimal noOfTotalPages = (decimal)result.Rows.Count / recordesPerPage;
            int count = 0;
            int indexNumber = 0;

            indexNumber = result.Columns.FindIndex(x => x.ColumnName == "Record Count");
            if (indexNumber != 0)
            {
                result.Columns.RemoveAt(indexNumber);

                result.Rows.ForEach(item =>
                {
                    item.VisibleFields.RemoveAt(indexNumber);
                });
            }

            for (int pageNo = 1; pageNo <= Math.Ceiling(noOfTotalPages); pageNo++)
            {
                tbody += templateData.Body;
                string textBody = string.Empty;
                textBody +=
               "<table class='table' border=" + 1 + " cellspacing =" + 20 + " cellpadding" + 20 + " width = " + pageWidth + "><thead><tr>";
                textBody += "<th>Sl. No.</th>";
                foreach (var col in result.Columns)
                {
                    coldata = col.ColumnName;
                    textBody += "<th>" + coldata + "</th>";
                }
                textBody += "</tr></thead><tbody>";


                var pageRows = result.Rows.Skip((pageNo - 1) * recordesPerPage).Take(recordesPerPage).ToList();


                foreach (var rows in pageRows)
                {
                    textBody += "<tr>";
                    textBody += "<td>" + (int)(count + 1) + "</td> ";
                    var rowData = rows.VisibleFields;
                    foreach (var data in rowData)
                    {
                        value = data;
                        textBody += "<td>" + value + "</td> ";
                    }
                    textBody += "</tr>";
                    count++;
                }


                textBody += "</tbody>";
                textBody += "</table>";


                tbody = tbody.Replace("--textBody--", textBody);
                tbody = tbody.Replace("--pageNo--", "Page " + pageNo + " of " + Math.Ceiling(noOfTotalPages));

                if (pageNo != Convert.ToInt16(Math.Ceiling(noOfTotalPages)))
                {
                    tbody += "<div style='page-break-after: always;'> </div>";
                }
            }

            string applicationURL = _iconfiguration.GetSection("ApplicationURL").Value;
            string collegeLogo = _iconfiguration.GetSection("UULogo").Value;

            List<AdmitCardCenterAndExamNameView> adminCardCenterView = _MSUoW.AdmitCardCenterAndExamNameView.GetAll().Where(ad => ad.IsActive && ad.SemesterCode == reportParam.CurrentAcademicYear
                                                                        && ad.CourseCode == reportParam.CourseCode && ad.CourseType == reportParam.CourseTypeCode && ad.ExamYear == reportParam.StartYear
                                                                        && ad.StreamCode == reportParam.StreamCode && ad.SubjectCode == reportParam.SubjectCode && ad.DepartmentCode == ad.DepartmentCode
                                                                        && reportParam.CollegeCode.Contains(ad.CenterCollegeCode)).ToList();
            //tbody = tbody.Replace("--collegeLogo--", applicationURL + collegeLogo);
            tbody = tbody.Replace("--collegeCode--", reportParam.CollegeCode.FirstOrDefault());
            tbody = tbody.Replace("--semesterName--", lookUpList.Where(lk => lk.LookupCode == reportParam.CurrentAcademicYear && lk.IsActive && lk.LookupTypeId == (long)enLookUpType.BscNursing)
                    .Select(lk => lk.LookupDesc).FirstOrDefault());
            tbody = tbody.Replace("--subjectName--", subjMast.Where(sub => sub.IsActive && sub.LookupCode == reportParam.SubjectCode).Select(sub => sub.LookupDesc).FirstOrDefault());
            tbody = tbody.Replace("--applicationType--", lookUpList.Where(lk => lk.IsActive && lk.LookupCode == reportParam.ApplicationType && lk.LookupTypeId == (long)enLookUpType.ApplicationType).Select(lk => lk.LookupDesc).FirstOrDefault());
            var calulaterAcademicYearNo = Convert.ToInt32(reportParam.CurrentAcademicYear);
            var academicSessionStartYear = Convert.ToInt32(reportParam.StartYear + calulaterAcademicYearNo - 1);
            tbody = tbody.Replace("--academicSession--", academicSessionStartYear + " - " + (Convert.ToInt32(academicSessionStartYear) + 1));
            tbody = tbody.Replace("--examinationYear--", Convert.ToString(examSession.ExamYear));
            tbody = tbody.Replace("--collegeName--", clgMaster.Where(clg => clg.LookupCode == reportParam.CollegeCode.FirstOrDefault()).Select(clg => clg.LookupDesc).FirstOrDefault());
            tbody = tbody.Replace("--totalCandidate--", Convert.ToString(result.Rows.Count));

            flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(tbody, enOrientation.Landscape);
            flattenedPDFDownloadDetails.FileType = "application/octet-stream";
            flattenedPDFDownloadDetails.FileName = "Nursing_CnrReport_"+ reportParam.CollegeCode.FirstOrDefault() + ".pdf";
            return flattenedPDFDownloadDetails;
        }
        public PDFDownloadFile GetAccountstatementDownload(string collegename,string collegeCode,string stream, List<AcademicStatementView> Studentlist,string semester)
        {
            List<string> role = GetRoleListOfCurrentUser();
            string SignatureDisplay = string.Empty;

            int pageWidth = 1240;
            //for potrait view A4
            int recordesPerPage = 43;//43
            int recordsinStaringPage = 35;//32


            int pageNo = 1;
           
            int noOfRecord = Studentlist.Count;
            int noofPages = (int)Math.Ceiling((decimal)(noOfRecord / recordsinStaringPage));
            string AccountStatementHtml = string.Empty;
            string pagebreakstring = "<tr style='page -break-after: always; '></tr>";
            StringBuilder Pagebreak = new StringBuilder(pagebreakstring);

            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            // get template

            EmailTemplate templateData = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.AccountStatement && s.IsActive == true).FirstOrDefault();
            EmailTemplate pageno = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.Pageno && s.IsActive == true).FirstOrDefault();
            EmailTemplate Statementheader = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.StatementHeader && s.IsActive == true).FirstOrDefault();
            string applicationURL = _iconfiguration.GetSection("ApplicationURL").Value;
            string collegeLogo = _iconfiguration.GetSection("UULogo").Value;
           
            // data
            EmailTemplate rowtemp= _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.StatementRow && s.IsActive == true).FirstOrDefault();
            int index = 1;
            decimal totalformfillupamount=0;
            decimal totallatefee=0;
            decimal totalamount=0;
            decimal totalCenterCharge = 0;
            StringBuilder rowData = new StringBuilder();
            if(role.Contains(((int)enRoles.Principal).ToString()))
            {
                SignatureDisplay = _MSCommon.GetEnumDescription(enDisplyCss.Show);
            }
            else
            {
                SignatureDisplay = _MSCommon.GetEnumDescription(enDisplyCss.Hide);
            }
            //email temp
            if (rowtemp!=null)
            {

                int i = 1;
                foreach(var d in Studentlist)
                {
                    StringBuilder body = new StringBuilder(rowtemp.Body);                   
                    body.Replace("--slNo--", index.ToString());
                    body.Replace("--applicationNo--", d.AcknowledgementNo);
                    body.Replace("--studentName--", d.StudentName);
                    body.Replace("--formfillUpAmount--", d.FormFillUpMoneyToBePaid.ToString());
                    body.Replace("--lateFeeAmount--", d.TotalLateFeeAmount.ToString());
                    body.Replace("--centreCharge--", d.CentreCharges.ToString());               
                    body.Replace("--totalAmount--", (d.FormFillUpMoneyToBePaid - d.CentreCharges).ToString());
                    rowData.Append(body);
                    index++;
                    i++;
                    totalformfillupamount = totalformfillupamount + d.FormFillUpMoneyToBePaid;
                    totallatefee = totallatefee + d.TotalLateFeeAmount;
                    totalamount = totalamount + (d.FormFillUpMoneyToBePaid - d.CentreCharges);
                    totalCenterCharge = totalCenterCharge + d.CentreCharges;
                    if(pageNo==1)
                    {
                        if(i== recordsinStaringPage)
                        {
                            i = 1;
                            
                            StringBuilder page = new StringBuilder(pageno.Body);
                            StringBuilder header = new StringBuilder(Statementheader.Body);
                            page.Replace("--pageno--", pageNo.ToString());
                            //rowData.Append(page);
                            pageNo++;
                            rowData.Append(Pagebreak);
                            rowData.Append(header);
                        }

                    }
                    else
                    {
                        if (i == recordesPerPage)
                        {
                            i = 1;
                            
                            StringBuilder page = new StringBuilder(pageno.Body);
                            StringBuilder header = new StringBuilder(Statementheader.Body);
                            page.Replace("--pageno--", pageNo.ToString());
                            //rowData.Append(page);
                            pageNo++;
                            rowData.Append(Pagebreak);
                            rowData.Append(header);
                        }
                    }
                }
            }
            string rowdata = rowData.ToString();
            string amountToWord = _commonservice.DecimalToWords(totalamount)+" only.";
            if (templateData!=null)
            {
                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--logo--", applicationURL + collegeLogo);
                body.Replace("--collegeName--", collegename);
                body.Replace("--collegeCode--", collegeCode);
                body.Replace("--studentDetails--", rowdata);
                body.Replace("--stream--", stream);
                body.Replace("--totalFormFilledAmount--", totalformfillupamount.ToString());
                body.Replace("--TotalLateFee--", totallatefee.ToString());
                body.Replace("--TotalCentreCharge--", totalCenterCharge.ToString());
                body.Replace("--totalAmount--", totalamount.ToString());
                body.Replace("--AmountInWord--", amountToWord);
                body.Replace("--displaycss--", SignatureDisplay);
                body.Replace("--examType--", "Semester");               
                body.Replace("--Semester--", semester);
                AccountStatementHtml = body.ToString();
            }
            flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(AccountStatementHtml, enOrientation.Portrait);
            flattenedPDFDownloadDetails.FileType = "application/octet-stream";
            flattenedPDFDownloadDetails.FileName = "AccountStatementOf_"+collegename+".pdf";
            return flattenedPDFDownloadDetails;
        }
        public PDFDownloadFile DownloadStatement(SearchParamDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            List<NursingViewExcludeBackgroundProcessTable> nursingStudentDetails = new List<NursingViewExcludeBackgroundProcessTable>();
            List<AcademicStatementView> dataList = new List<AcademicStatementView>();
            string collegecode = param.CollegeCode.FirstOrDefault();
            string collegeName = _MSUoW.CollegeMaster.FindBy(f => f.IsActive == true && f.LookupCode == collegecode).FirstOrDefault().LookupDesc;
            string stream = string.Empty;
            SubjectMaster subject = _MSUoW.SubjectMaster.GetAll().Where(f => f.IsActive == true && f.CourseTypeCode == param.CourseTypeCode && f.LookupCode == param.SubjectCode).FirstOrDefault();
            if (subject != null)
            {
                stream = subject.LookupDesc;
            }
            switch (param.ServiceId)
            {
                case (long)enServiceType.NursingApplicationForm:
                    nursingStudentDetails = _MSUoW.NursingViewExcludeBackgroundProcessTable.GetAll().Where(sub => sub.IsActive == true &&
                                                     sub.CollegeCode == collegecode &&
                                                     sub.CourseTypeCode == param.CourseTypeCode &&
                                                     sub.StreamCode == param.StreamCode &&
                                                     sub.AcademicStart == param.StartYear &&
                                                     sub.SubjectCode == param.SubjectCode &&
                                                     sub.CurrentAcademicYear == param.CurrentYear &&
                                                     sub.State.Trim().ToLower() != _iconfiguration.GetSection("NursingFormFillup:VerificationPending").Value.Trim().ToLower() &&
                                                     sub.State.Trim().ToLower() != _iconfiguration.GetSection("NursingFormFillup:PaymentPendingState").Value.Trim().ToLower() &&
                                                     sub.State.Trim().ToLower() != _iconfiguration.GetSection("NursingFormFillup:ExamFormFillUpRejectedStatus").Value.Trim().ToLower()
                                                     ).ToList();

                    Guid examMasterId = _MSUoW.ExaminationMaster.GetAll().Where(w => w.CourseType == param.CourseTypeCode && w.StreamCode == param.StreamCode &&
                                                           w.SubjectCode == param.SubjectCode && w.SemesterCode == param.CurrentYear && w.IsActive).FirstOrDefault().Id;

                    ExaminationSession SessionData = _MSUoW.ExaminationSession.GetAll().Where(w => w.IsActive && w.ExaminationMasterId == examMasterId && w.IsActive).FirstOrDefault();

                    flattenedPDFDownloadDetails = GetAccountstatementForNursingDownload(collegeName, collegecode, stream, nursingStudentDetails, nursingStudentDetails[0].CurrentAcademicYearString,SessionData); 
                    break;
                case (long)enServiceType.BCABBAExamApplicationForm:
                      dataList = _exmService.GetAllAcademicStatementList(param).OrderBy(o => o.StudentName).ToList();
                     flattenedPDFDownloadDetails = GetAccountstatementDownload(collegeName, collegecode, stream, dataList, param.SemesterCode);
                    break;
            } 
            return flattenedPDFDownloadDetails;
        }

        #region downoad statement for nursing 
        public PDFDownloadFile GetAccountstatementForNursingDownload(string collegename, string collegeCode, string stream, List<NursingViewExcludeBackgroundProcessTable> Studentlist, string currentYear,ExaminationSession SessionData)
        {
            List<string> role = GetRoleListOfCurrentUser();
            string SignatureDisplay = string.Empty;

            int pageWidth = 1240;
            //for potrait view A4
            int recordesPerPage = 43;//43
            int recordsinStaringPage = 35;//32


            int pageNo = 1;

            int noOfRecord = Studentlist.Count;
            int noofPages = (int)Math.Ceiling((decimal)(noOfRecord / recordsinStaringPage));
            string AccountStatementHtml = string.Empty;
            string pagebreakstring = "<tr style='page -break-after: always; '></tr>";
            StringBuilder Pagebreak = new StringBuilder(pagebreakstring);

            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            // get template

            EmailTemplate templateData = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.AccountStatement && s.IsActive == true).FirstOrDefault();
            EmailTemplate pageno = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.Pageno && s.IsActive == true).FirstOrDefault();
            EmailTemplate Statementheader = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.StatementHeader && s.IsActive == true).FirstOrDefault();
            string applicationURL = _iconfiguration.GetSection("ApplicationURL").Value;
            string collegeLogo = _iconfiguration.GetSection("UULogo").Value;

            // data
            EmailTemplate rowtemp = _commonservice.GetEmailTemplateDataFromCache().Where(s => s.TemplateType == (long)enEmailTemplate.StatementRow && s.IsActive == true).FirstOrDefault();
            int index = 1;
            decimal totalformfillupamount = 0;
            decimal totallatefee = 0;
            decimal totalamount = 0;
            decimal totalCenterCharge = 0;
            StringBuilder rowData = new StringBuilder();
            if (role.Contains(((int)enRoles.Principal).ToString()))
            {
                SignatureDisplay = _MSCommon.GetEnumDescription(enDisplyCss.Show);
            }
            else
            {
                SignatureDisplay = _MSCommon.GetEnumDescription(enDisplyCss.Hide);
            }
            //email temp
            if (rowtemp != null)
            {

                int i = 1;
                foreach (var d in Studentlist)
                {
                    StringBuilder body = new StringBuilder(rowtemp.Body);
                    body.Replace("--slNo--", index.ToString());
                    body.Replace("--applicationNo--", d.AcknowledgementNo);
                    body.Replace("--studentName--", d.StudentName);
                    body.Replace("--formfillUpAmount--", (d.FormFillUpMoneyToBePaid - d.LateFeeAmount).ToString());
                    body.Replace("--lateFeeAmount--", d.LateFeeAmount.ToString());
                    body.Replace("--centreCharge--", d.CenterCharges.ToString());
                    body.Replace("--totalAmount--", (d.FormFillUpMoneyToBePaid - d.CenterCharges).ToString());
                    rowData.Append(body);
                    index++;
                    i++;
                    totalformfillupamount = totalformfillupamount + (d.FormFillUpMoneyToBePaid - d.LateFeeAmount);
                    totallatefee = totallatefee + d.LateFeeAmount;
                    totalamount = totalamount + (d.FormFillUpMoneyToBePaid - d.CenterCharges);
                    totalCenterCharge = totalCenterCharge + d.CenterCharges;
                    if (pageNo == 1)
                    {
                        if (i == recordsinStaringPage)
                        {
                            i = 1;

                            StringBuilder page = new StringBuilder(pageno.Body);
                            StringBuilder header = new StringBuilder(Statementheader.Body);
                            page.Replace("--pageno--", pageNo.ToString());
                            //rowData.Append(page);
                            pageNo++;
                            rowData.Append(Pagebreak);
                            rowData.Append(header);
                        }

                    }
                    else
                    {
                        if (i == recordesPerPage)
                        {
                            i = 1;

                            StringBuilder page = new StringBuilder(pageno.Body);
                            StringBuilder header = new StringBuilder(Statementheader.Body);
                            page.Replace("--pageno--", pageNo.ToString());
                            //rowData.Append(page);
                            pageNo++;
                            rowData.Append(Pagebreak);
                            rowData.Append(header);
                        }
                    }
                }
            }
            string rowdata = rowData.ToString();
            string amountToWord = _commonservice.DecimalToWords(totalamount) + " only.";
            if (templateData != null)
            {
                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--logo--", applicationURL + collegeLogo);
                body.Replace("--collegeName--", collegename);
                body.Replace("--collegeCode--", collegeCode);
                body.Replace("--studentDetails--", rowdata);
                body.Replace("--stream--", stream);
                body.Replace("--totalFormFilledAmount--", totalformfillupamount.ToString());
                body.Replace("--TotalLateFee--", totallatefee.ToString());
                body.Replace("--TotalCentreCharge--", totalCenterCharge.ToString());
                body.Replace("--totalAmount--", totalamount.ToString());
                body.Replace("--AmountInWord--", amountToWord);
                body.Replace("--displaycss--", SignatureDisplay);
                body.Replace("--examType--", "Current Year");
                body.Replace("--Semester--", currentYear +" Admission Batch "+ SessionData.StartYear + " & Examination "+ SessionData.ExamYear);
                AccountStatementHtml = body.ToString();
            }
            flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(AccountStatementHtml, enOrientation.Portrait);
            flattenedPDFDownloadDetails.FileType = "application/octet-stream";
            flattenedPDFDownloadDetails.FileName = "AccountStatementOf_" + collegename + ".pdf";
            return flattenedPDFDownloadDetails;
        }
        #endregion

    }
}
