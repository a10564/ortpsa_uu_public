﻿using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.Role;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.Business
{
    public class RoleAppService : BaseService, IRoleAppService
    {
        private IMicroServiceUOW _MSUoW;
        public RoleAppService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData
            ) : base(redisData)
        {
            _MSUoW = MSUoW;
        }

        public RoleDto CreateRole(CreateRoleDto input)
        {
            var role = AutoMapper.Mapper.Map<Role>(input);
            if (IsUniqueRoleName(input.Name, true, 0))
            {
                role.NormalizedName = role.Name.ToUpper();
                _MSUoW.Role.Add(role);
                _MSUoW.Commit();
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU008" };
            }
            // Since we are not sending any parameter as 'Permissions', we commented below lines
            //var grantedPermissions = PermissionManager
            //    .GetAllPermissions()
            //    .Where(p => input.Permissions.Contains(p.Name))
            //    .ToList();

            //await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);
            var roleData = AutoMapper.Mapper.Map<Role, RoleDto>(role);

            //return MapToEntityDto(role);
            return roleData;
        }

        private bool IsUniqueRoleName(string roleName, bool isCreateMode, long roleId)
        {
            bool isUniqueRoleName = false;
            try
            {
                var role = _MSUoW.Role.FindBy(x => x.Name.Trim().ToLower() == roleName.Trim().ToLower() && x.IsActive == true).FirstOrDefault();
                if (role == null)
                {// Obviously unique
                    isUniqueRoleName = true;
                }
                else
                {// Let us check if updating same record
                    if (isCreateMode == false && roleId == role.Id)
                    {
                        isUniqueRoleName = true;
                    }
                    else if (isCreateMode == false && GetCurrentTenantId() != role.TenantId)
                    {
                        isUniqueRoleName = true;
                    }
                }
            }
            catch (Exception ex)
            {// No role with following role name
                isUniqueRoleName = true;
            }
            return isUniqueRoleName;
        }

        //update role
        public RoleDto UpdateRole(long id, RoleDto input)
        {
           // CheckUpdatePermission();

            var role = new Role();//await _roleManager.GetRoleByIdAsync(id);

            if (IsUniqueRoleName(input.Name, false, id))
            {
                role = _MSUoW.Role.GetById(id);
                //await _roleManager.GetRoleByIdAsync(id);
                var roleResult=AutoMapper.Mapper.Map(input, role);
                _MSUoW.Role.Update(roleResult);
                GrantFeaturesAndApisToRole(input);
                _MSUoW.Commit();
                //ObjectMapper.Map(input, role);
                //CheckErrors(await _roleManager.UpdateAsync(role));
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU008" };
                //throw new UserFriendlyException("Role with same name already exists.");
            }

            //ObjectMapper.Map(input, role);

            //CheckErrors(await _roleManager.UpdateAsync(role));

            //var grantedPermissions = PermissionManager
            //    .GetAllPermissions()
            //    .Where(p => input.Permissions.Contains(p.Name))
            //    .ToList();

            //await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);

            //Grant features and api details to the role:custom
            

            //AutoMapper.Mapper.Map(role, input);
            var result=AutoMapper.Mapper.Map<Role, RoleDto>(role);
            return result;
            //return MapToEntityDto(role);

        }

        protected void GrantFeaturesAndApisToRole(RoleDto role)
        {
            //role.RoleFeatureApiMappers.ForEach(p => _roleFeatureApiMapperRepo.InsertOrUpdateAsync(p));

            //Changed as per the requirements of client side
            var existingApis =_MSUoW.RoleFeatureApiMapper.GetAll().Where(p => p.RoleId == role.Id).ToList();
            //_roleFeatureApiMapperRepo.GetAll().Where(p => p.RoleId == role.Id).ToList();
            existingApis.ForEach(p => p.IsActive = false);
            existingApis.ForEach(p => _MSUoW.RoleFeatureApiMapper.Update(p));
            //_roleFeatureApiMapperRepo.Delete(p.Id));
            foreach (var apiDto in role.RoleFeatureApiMappers)
            {
                var api = AutoMapper.Mapper.Map<RoleFeatureApiMapperCreateDto, RoleFeatureApiMapper>(apiDto);
                    //ObjectMapper.Map<RoleFeatureApiMapper>(apiDto);
                api.RoleId = role.Id;
                _MSUoW.RoleFeatureApiMapper.Add(api);
                //_roleFeatureApiMapperRepo.InsertAsync(api);
            }
        }
        
        //get roledetails
        public Role GetRoleDetails(long id)
        {
            var role = _MSUoW.RoleRepository.GetAllRolesData().Where(x => x.Id==id).FirstOrDefault();
            return role;
        }

        //delete role
        public bool DeleteRoleDetails(long id)
        {
            bool deleteRole = false;
            //CheckDeletePermission();

            var role = _MSUoW.Role.FindBy(x => x.Id == id && x.IsActive == true).FirstOrDefault();
            //await _roleManager.FindByIdAsync(id.ToString());
            //if (role.IsStatic)
            //{
            //    throw new UserFriendlyException(L("CanNotDeleteStaticRole"));
            //}

            if (role != null)
            {
                var users = _MSUoW.UserRoles.FindBy(x => x.RoleId == id && x.IsActive == true).ToList(); ;
                    //await _userManager.GetUsersInRoleAsync(role.NormalizedName);

                foreach (var user in users)
                {
                    user.IsActive = false;
                    _MSUoW.UserRoles.Update(user);
                    //CheckErrors(await _userManager.RemoveFromRoleAsync(user, role.NormalizedName));
                }
                //for soft delete setting isactive to false
                role.IsActive = false;
                _MSUoW.Role.Update(role);
                _MSUoW.Commit();
                deleteRole = true;
            }
            // CheckErrors(await _roleManager.DeleteAsync(role));
            return deleteRole;
        }

        public List<Role> GetRoleDetails()
        {
            return _MSUoW.RoleRepository.GetAllRolesData().ToList();
        }

        public IQueryable<Role> GetAllRolesData()
        {
            return _MSUoW.Role.GetAll().AsQueryable();
        }
    }
}
