﻿using AutoMapper;
using Microservice.Common;
using Microservice.Common.Abstract;
using MicroService.Business.Abstract;
using MicroService.Core;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Core.WorkflowTransactionHistrory;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Dto.MigrationService;
using MicroService.Domain.Models.Dto.StudentDashboard;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
using MicroService.Utility.Exception;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MicroService.Business
{
    public class StudentMigrationService : BaseService, IStudentMigrationService
    {
        static readonly object _object = new object();
        private IMicroServiceUOW _MSUoW;
        private readonly IHostingEnvironment _environment;
        IConfiguration _IConfiguration;
        private IWFTransactionHistroryAppService _WFTransactionHistroryAppService;
        private MetadataAssemblyInfo workflowHelper;
        private WorkflowDbContext _workflowDbContext;
        private IMicroserviceCommon _MSCommon;
        private readonly string WorkflowName = "";
        private IMicroServiceLogger _logger;

        public StudentMigrationService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData, IHostingEnvironment environment, IWFTransactionHistroryAppService WFTransactionHistroryAppService,
            IConfiguration IConfiguration, IMicroserviceCommon MSCommon, IMicroServiceLogger logger) : base(redisData)
        {
            _MSUoW = MSUoW;
            _environment = environment;
            _IConfiguration = IConfiguration;
            _WFTransactionHistroryAppService = WFTransactionHistroryAppService;
            _workflowDbContext = new WorkflowDbContext();
            workflowHelper = new MetadataAssemblyInfo();
            WorkflowName = _IConfiguration.GetSection("MigrationCertificate:WorkflowName").Value;
            _MSCommon = MSCommon;
            _logger = logger;
        }

        #region GET STUDENT DETAILS (UniversityMigrationDetail entity)
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 05-02-2020
        /// data of the students applied for migration 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UniversityMigrationDetail GetStudentMigrationDetailById(Guid id)
        {
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            UniversityMigrationDetail studentData = new UniversityMigrationDetail();
            if (id != default(Guid))
            {
                studentData = _MSUoW.UniversityMigrationDetailsRepository.GetAllDataById().Where(uni => uni.Id == id).FirstOrDefault();
            }
            else throw new MicroServiceException() { ErrorCode = "OUU017" };
            return studentData;
        }
        #endregion

        #region Migration Applied
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 06-02-2020
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityMigrationDetailDto StudentMigrationApplied(UniversityMigrationDetailDto input)
        {
            UniversityMigrationDetail studentApplied = _MSUoW.UniversityMigrationDetail.FindBy(element => element.CreatorUserId == GetCurrentUserId() && element.IsActive).OrderByDescending(d => d.LastModificationTime).FirstOrDefault();

            if (studentApplied != null && studentApplied.State.Trim().ToLower() != _IConfiguration.GetSection("MigrationCertificate:MigrationApplicationRejectedStatus").Value.Trim().ToLower())
            {
                throw new MicroServiceException() { ErrorCode = "OUU123", SubstitutionParams = new object[1] { studentApplied.State } };
            }
            long serviceId = (long)enServiceType.MigrationService;
            Users user = _MSUoW.Users.FindBy(u => u.Id == GetCurrentUserId() && u.IsActive).FirstOrDefault();
            if (user.UserType == (long)enRoles.Student)
            {
                if (input != null)
                {
                    if (ValidateInput(input))
                    {
                        UniversityMigrationDetail migrationData = Mapper.Map<UniversityMigrationDetailDto, UniversityMigrationDetail>(input);
                        var newGuid = Guid.NewGuid();
                        string guidForAck = newGuid.ToString();
                        migrationData.Id = newGuid;
                        Documents tempContent = new Documents();
                        migrationData.MigrationDocument = new List<Documents>();
                        // Writing photo on server
                        tempContent = input.MigrationDocument[0];
                        if (ImageValidation(tempContent))
                        {
                            tempContent.Id = Guid.NewGuid();
                            tempContent.SourceType = (long)enRegistartionDocType.RegistartionPhoto;
                            migrationData.MigrationDocument.Add(ImageWrite(tempContent, newGuid));
                        }
                        #region WORKFLOW
                        var tenantLatestWF = _workflowDbContext.TenantWorkflow
                         .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName)
                         .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();
                        if (tenantLatestWF != null)
                        {
                            migrationData.TenantWorkflowId = tenantLatestWF.Id;
                        }
                        else
                        {
                            var defaultWF = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == 0 && tenantWF.Workflow == WorkflowName)  //Default tenant id
                        .OrderByDescending(tenantWF => tenantWF.CreationTime).FirstOrDefault();
                            migrationData.TenantWorkflowId = defaultWF.Id;

                        }
                        input.LoggedInUserRoles = GetRoleListOfCurrentUser();
                        string studentMigrationInitialApplyTrigger = string.Empty;
                        try
                        {
                            studentMigrationInitialApplyTrigger = _IConfiguration.GetSection("MigrationCertificate:MigrationCertificateApplyTrigger").Value;
                        }
                        catch (Exception)
                        {
                            studentMigrationInitialApplyTrigger = "Apply";
                        }
                        string studentMigrationInitialApplyState = string.Empty;
                        try
                        {
                            studentMigrationInitialApplyState = _IConfiguration.GetSection("MigrationCertificate:MigrationCertificateInitialHistoryState").Value;
                        }
                        catch (Exception)
                        {
                            studentMigrationInitialApplyState = "Applied";
                        }
                        migrationData.State = studentMigrationInitialApplyState;
                        input._trigger = studentMigrationInitialApplyTrigger;
                        var executionStatus = input.ExecuteProcess(migrationData, input);

                        #endregion
                        migrationData.AcknowledgementNo = _MSCommon.GenerateAcknowledgementNo(_MSCommon.GetEnumDescription(enAcknowledgementFormat.ACK), serviceId, guidForAck);
                        _MSUoW.UniversityMigrationDetail.Add(migrationData);
                        // Service applied
                        UserServiceApplied usrSrvcApplied = new UserServiceApplied()
                        {
                            IsAvailableToDL = false,
                            LastStatus = migrationData.State,
                            ServiceId = (long)enServiceType.MigrationService,
                            UserId = GetCurrentUserId(),
                            InstanceId = newGuid,
                            IsActionRequired = true
                        };
                        _MSUoW.UserServiceApplied.Add(usrSrvcApplied);
                        Dictionary<string, object> triggerParameterDictionary = null;
                        _MSUoW.Commit();
                        #region MAIL SEND TO STUDENT
                        string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == serviceId && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                        SendAcknowledgementEmail(user.EmailAddress, migrationData.StudentName, serviceName, migrationData.AcknowledgementNo);
                        #endregion
                        #region SMS SEND TO STUDENT
                        EmailTemplate acknowledgementSMS = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationAcknowledgementSMS && s.IsActive == true).FirstOrDefault();
                        if (acknowledgementSMS != null)
                        {
                            string contentId = string.Empty;
                            contentId = acknowledgementSMS.SMSContentId;
                            string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                            OTPMessage otpObj = new OTPMessage();
                            StringBuilder body = new StringBuilder(acknowledgementSMS.Body);
                            body.Replace("--studentName--", migrationData.StudentName);
                            body.Replace("--serviceName--", serviceName);
                            body.Replace("--acknowledgementNo--", migrationData.AcknowledgementNo);
                            otpObj.Message = body.ToString();
                            otpObj.MobileNumber = user.PhoneNumber;
                            _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                        }

                        #endregion

                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, newGuid, DateTime.UtcNow, studentMigrationInitialApplyState, studentMigrationInitialApplyTrigger, migrationData.State, migrationData.TenantWorkflowId);
                        input = Mapper.Map<UniversityMigrationDetail, UniversityMigrationDetailDto>(migrationData);
                        return input;
                    }
                }
                else throw new MicroServiceException() { ErrorCode = "OUU013" };
            }
            else throw new MicroServiceException() { ErrorCode = "OUU052" };
            return input;
        }
        #endregion

        #region IMAGE VALIDATION
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 04-02-2020
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ImageValidation(Documents input)
        {
            if (input.FileExtension == "" || input.FileExtension == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU047" };
            }
            if (input.FileExtension != "" || input.FileExtension != null)
            {
                var regEx = new Regex(@"(jpg|jpeg)$");
                if (!regEx.IsMatch(input.FileExtension.ToLower()))
                {
                    throw new MicroServiceException() { ErrorCode = "OUU048" };
                }
            }
            if (input.FileContent.Length >= Convert.ToInt32(_IConfiguration.GetSection("RegistrationNumber:UploadImageSize").Value))
            {
                throw new MicroServiceException() { ErrorCode = "OUU049" };
            }
            return true;
        }
        #endregion

        #region PRIVATE COMMON DOCUMENT WRITE METHOD
        private Documents ImageWrite(Documents file, Guid id)
        {
            string[] name = id.ToString().Split("-");
            string finalName = name[1] + name[2] + name[4] + name[0] + name[3];
            //List <Documents> documentList = new List<Documents>();
            //input.RegistrationDocument.ForEach((file) =>
            //{
            string path = string.Empty;
            file.ParentSourceId = id;
            if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.win.ToString())
            {
                path = _environment.ContentRootPath;
            }
            else if (_IConfiguration.GetSection("DeploymentEnvironment").Value.ToLower().ToString() == enDevelopmentEnvironment.linux.ToString())
            {
                path = _IConfiguration.GetSection("LinuxDocumentPath").Value;
            }
            string fileName = finalName + "." + file.FileExtension;
            string DirectoryPath = _IConfiguration.GetSection("RegistrationNumber:RegistrationCertificatePath").Value; // "\\Documents\\RegistrationCertificates\\"
            var Path = path + DirectoryPath + fileName;

            if (!Directory.Exists(path + DirectoryPath))
            {
                Directory.CreateDirectory(path + DirectoryPath);
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
            byte[] bytes = (byte[])file.FileContent;
            File.WriteAllBytes(Path, bytes);
            file.FilePath = DirectoryPath + fileName;
            //file.FilePath = file.FilePath.Replace('\\', '/');
            //documentList.Add(file);
            //});

            return file;
        }
        #endregion

        #region INPUT VALIDATION
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 07-Nov-2019
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool ValidateInput(UniversityMigrationDetailDto input)
        {
            DateTime minDate = new DateTime(1950, 01, 01);
            DateTime maxDate = DateTime.UtcNow;
            DateTime maxDOBDate = maxDate.AddYears(-15); // 15 year minus on current date for student date of birth 
            string regularExpressionForName = @"^(?!\.)(?!.*\.$)(?!.*?\.\.)[a-zA-Z0-9 .]+$";//This regex is used to allow only alphabets, dot and spaces in string
            //string registrationExpression = @"^[0-9]{16}$";
            if (input.StudentName == null || input.StudentName.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU037" };
            }
            else if (!Regex.IsMatch(input.StudentName.Trim(), regularExpressionForName))
            {
                throw new MicroServiceException() { ErrorCode = "OUU038" };
            }
            else if (input.RegistrationNo == null || input.RegistrationNo.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU064" };
            }
            //else if (!Regex.IsMatch(input.RegistrationNo, registrationExpression))
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU091" };
            //}
            //else if (input.RollNo == null || input.RollNo.Trim() == String.Empty)
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU065" };
            //}
            //else if (!Regex.IsMatch(input.RollNo, registrationExpression))
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU108" };
            //}
            else if (input.DateOfBirth == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU041" };
            }
            else if (input.DateOfBirth != null)
            {
                if (minDate >= input.DateOfBirth && maxDOBDate <= input.DateOfBirth)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU069" };
                }
            }
            else if (input.CollegeCode == null || input.CollegeCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU042" };
            }
            else if (input.CourseType == null || input.CourseType.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU043" };
            }
            else if (input.StreamCode == null || input.StreamCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU044" };
            }
            else if (input.SubjectCode == null || input.SubjectCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU045" };
            }
            else if (input.YearOfAdmission == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU046" };
            }
            else if (input.YearOfAdmission != null)
            {
                if (minDate >= input.YearOfAdmission && maxDate <= input.YearOfAdmission)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU070" };
                }
            }
            else if (input.DepartmentCode == null || input.DepartmentCode == String.Empty)
            {
                input.DepartmentCode = "";
            }
            else if ((input.CourseType == "02") && (input.DepartmentCode == null || input.DepartmentCode == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU056" };
            }
            else if (input.CourseCode == null || input.CourseCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU059" };
            }
            //new added for migration
            else if (input.ReasonForMigrationCode == null || input.ReasonForMigrationCode.Trim() == String.Empty)
            {
                throw new MicroServiceException() { ErrorCode = "OUU063" };
            }
            //else if (input.RegistrationNo == null || input.RegistrationNo.Trim() == String.Empty)
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU064" };
            //}
            //else if (input.RollNo == null || input.RollNo.Trim() == String.Empty)
            //{
            //    throw new MicroServiceException() { ErrorCode = "OUU065" };
            //}
            else if (input.YearOfLeaving == null)
            {
                throw new MicroServiceException() { ErrorCode = "OUU066" };
            }
            else if (input.YearOfLeaving != null)
            {
                if (minDate >= input.YearOfLeaving && maxDate <= input.YearOfLeaving)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU071" };
                }
            }
            else if ((input.ReasonForMigrationCode == "01") && (input.StudyDetail == null || input.StudyDetail == String.Empty))
            {
                throw new MicroServiceException() { ErrorCode = "OUU067" };
            }
            input.StudentName = input.StudentName.Trim();
            input.CollegeCode = input.CollegeCode.Trim();
            input.CourseType = input.CourseType.Trim();
            input.StreamCode = input.StreamCode.Trim();
            input.SubjectCode = input.SubjectCode.Trim();
            input.ReasonForMigrationCode = input.ReasonForMigrationCode.Trim();
            input.RegistrationNo = input.RegistrationNo.Trim();
            //input.RollNo = input.RollNo.Trim();
            input.ReasonForMigrationCode = input.ReasonForMigrationCode.Trim();
            return true;
        }
        #endregion

        #region SEND ACKNOWLEDGEMENT EMAIL TO STUDENT
        /// Email send method
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 28-02-2020
        /// </summary>
        private bool SendAcknowledgementEmail(string studentEmailAddress, string studentName, string serviceName, string acknowledgementNo)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.AcknowledgementNo && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                body.Replace("--studentName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--acknowledgementNo--", acknowledgementNo);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }
        #endregion

        #region UPDATE STUDENT MIGRATION DETAILS
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 05-02-2020
        /// Description : This method is used to get the student Details by id 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityMigrationDetailDto GetStudentUniversityMigrationDetailsById(Guid id, UniversityMigrationDetailDto input)
        {

            long userType = GetUserType();
            if (id != default(Guid) && input != null)
            {
                UniversityMigrationDetail studentData = _MSUoW.UniversityMigrationDetail.FindBy(student => student.Id == id && student.IsActive == true).FirstOrDefault();
                if (studentData != null)
                {
                    if (ValidateInput(input))
                    {
                        studentData.StudentName = input.StudentName.Trim();
                        studentData.DateOfBirth = input.DateOfBirth;
                        studentData.CollegeCode = input.CollegeCode;
                        studentData.StreamCode = input.StreamCode;
                        studentData.SubjectCode = input.SubjectCode;
                        studentData.DepartmentCode = input.DepartmentCode;
                        studentData.YearOfAdmission = input.YearOfAdmission;
                        studentData.CourseType = input.CourseType;
                        studentData.CourseCode = input.CourseCode;
                        studentData.RegistrationNo = input.RegistrationNo;
                        studentData.ReasonForMigrationCode = input.ReasonForMigrationCode;
                        studentData.RollNo = input.RollNo;
                        studentData.StudyDetail = input.StudyDetail;
                        studentData.YearOfLeaving = input.YearOfLeaving;
                        studentData.PrincipalLastUpdateTime = DateTime.UtcNow;
                        _MSUoW.UniversityMigrationDetail.Update(studentData);
                        _MSUoW.Commit();
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == studentData.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                        {
                            string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                            SendUniversityMigrationUpdateConfirmationEmail(applyingStudent.EmailAddress, applyingStudent.Name, serviceName);
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationUpdatedSMS && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--serviceName--", serviceName);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }

                        }
                        input = Mapper.Map<UniversityMigrationDetail, UniversityMigrationDetailDto>(studentData);
                        return input;
                    }

                }

            }
            else throw new MicroServiceException() { ErrorCode = "OUU015" };

            return input;
        }
        #endregion

        #region ODATA METHOD
        /// <summary>
        /// Author: Sitikanta Pradhan
        /// Date: 08-Nov-2019 
        /// Description : Method is used for Odata Controller which will give examination student record as per usertype 
        /// </summary>
        /// <returns></returns>
        public IQueryable<MigrationView> GetAllMigrationStudentData()
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            var inboxDataAsPerRole = _MSUoW.MigrationView.GetAll();

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.IsActive);
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower() && data.IsActive);
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }
        #endregion

        #region GET STUDENT Registration DETAILS 
        /// <summary>
        /// Author: Sumbul Samreen
        /// Date: 04-02-2020
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityRegistrationDetail GetStudentUniversityRegistrationDetails()
        {
            UniversityRegistrationDetail studentData = new UniversityRegistrationDetail();
            studentData = _MSUoW.UniversityRegistrationDetail.FindBy(a => a.CreatorUserId == GetCurrentUserId() && a.IsActive
            && a.IsRegistrationNumberGenerated && a.State == _MSCommon.GetEnumDescription((enState)enState.RegNoApplicationApproved)).OrderByDescending(d => d.LastModificationTime).FirstOrDefault();

            return studentData;
        }
        #endregion

        #region Generate Migration Number
        /// <summary>
        /// Sitikanta Pradhan
        /// 13-feb-2020
        /// </summary>
        /// <returns></returns>
        public string GenerateMigrationNumber()
        {
            string serialNumber = string.Empty;
            //create migration number and insert universitymigrationdetails table
            try
            {
                NumberGenerator currRecord = new NumberGenerator();
                currRecord = _MSUoW.NumberGenerator.FindBy(A => A.ProcessType == Convert.ToInt64(EnProcessType.Migration)).FirstOrDefault();

                if (currRecord == null)
                    return null;
                StringBuilder newSeqNumber = new StringBuilder();
                //Get the start seq no
                string startSeqNumber = Convert.ToString(currRecord.StartNumberSeq);
                long newSeq = currRecord.LastGeneratedNumber + Convert.ToInt32(currRecord.NumberIncrementedBy);
                newSeqNumber.Append(Convert.ToString(newSeq));
                int zerosToBeAppended = startSeqNumber.Length - Convert.ToString(newSeq).Length;
                for (int i = 0; i < zerosToBeAppended; i++)
                {
                    newSeqNumber.Insert(0, "0");
                }

                //Update the current seq number
                currRecord.LastGeneratedNumber = newSeq;
                _MSUoW.NumberGenerator.Update(currRecord);
                _MSUoW.Commit();

                serialNumber = currRecord.PreFixWith + newSeqNumber.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return serialNumber;
        }
        #endregion

        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   28-02-2020
        /// Description : This method is used to change status of the workflow
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityMigrationDetailDto UpdateMigrationApplicationStatus(Guid id, UniversityMigrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityMigrationDetail = _MSUoW.UniversityMigrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityMigrationDetail != null && input.Id == id)
            {
                string sourceState = universityMigrationDetail.State;
                if (universityMigrationDetail.TenantWorkflowId != 0)
                {
                    #region Step c
                    universityMigrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<UniversityMigrationDetail>(universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId, trigger, input, ref universityMigrationDetail, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(universityMigrationDetail, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).ToList();
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityMigrationDetail.State, input.ParentWorkflowId);
                    UpdateUserServiceAppliedDetail(universityMigrationDetail, IsLastTransition);
                    #endregion


                    #region Step f
                    _MSUoW.UniversityMigrationDetail.Update(universityMigrationDetail);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<UniversityMigrationDetail, UniversityMigrationDetailDto>(universityMigrationDetail);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();
                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email
                            var nextApprovingUser = new Users();

                            // get the user whether principal/ HOD
                            bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == universityMigrationDetail.CollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1 && isAffiliated)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityMigrationDetail.CollegeCode).FirstOrDefault();
                            }
                            else if ((Array.IndexOf(approvingRoleArray, (long)enRoles.HOD) != -1) && !isAffiliated && universityMigrationDetail.CourseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                            {
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == universityMigrationDetail.CollegeCode.Trim().ToLower()
                                && usr.DepartmentCode.Trim().ToLower() == universityMigrationDetail.DepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityMigrationDetail.LastStatusUpdateDate, sourceState, trigger, universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId);
                    #endregion

                    #region Step k
                    //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityMigrationDetail.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null && input.State != _MSCommon.GetEnumDescription((enState)enState.RegNoApplicationRejected))
                        {
                            //string emailIdOfApplyingUser = ;
                            //Send email about workflow completion
                            SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, universityMigrationDetail.RegistrationNo);

                            // Send SMS to students about Registration Number Generation
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGeneratedSMS && s.ServiceType == (long)enServiceType.RegistrationService && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                        }
                        else
                        {
                            //Send email to students about Registration Number Rejection
                            string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                            if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                            {
                                SendEmailToStudentOnMigrationCertificateRejection(applyingStudent.EmailAddress, applyingStudent.UserName, serviceName);
                            }

                            //Send SMS to students about Registration Number Rejection
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedSMS && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                            //Send email to principal about Registration Number Rejection
                            var principalData = _MSUoW.Users.FindBy(a => a.CollegeCode == input.CollegeCode && a.UserType == (long)enUserType.Principal && a.IsActive).FirstOrDefault();
                            SendEmailToPrincipalOnMigrationCertificateRejection(principalData.EmailAddress, applyingStudent.UserName);

                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
            //return true;
        }

        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   28-02-2020
        /// </summary>
        /// <returns></returns>
        private bool IsLastTransitionOfWorkflow(List<TenantWorkflowTransitionConfig> possibleTransitions, string currentState, long? parentWFId)
        {
            bool isLastStateTransition = false;
            var transitionsAvailable = possibleTransitions.Where(transition => transition.SourceState == currentState).FirstOrDefault();
            if (transitionsAvailable == null)
            {
                isLastStateTransition = true;
            }
            return isLastStateTransition;
        }

        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   28-02-2020
        /// Description : This method is used to update userServiceApplied table.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private void UpdateUserServiceAppliedDetail(UniversityMigrationDetail universityMigrationDetail, bool isAvailableToDownload)
        {
            UserServiceApplied usrSrvcApplied = _MSUoW.UserServiceApplied.FindBy(usrSrvc => usrSrvc.InstanceId == universityMigrationDetail.Id).OrderByDescending(usrSrvc => usrSrvc.CreationTime).FirstOrDefault();
            if (usrSrvcApplied != null)
            {
                usrSrvcApplied.LastStatus = universityMigrationDetail.State;
                if (isAvailableToDownload == true && usrSrvcApplied.IsAvailableToDL == false)
                {
                    usrSrvcApplied.IsAvailableToDL = isAvailableToDownload;
                }
                try
                {
                    var actionRequiringStateListForStudent = (_IConfiguration.GetSection("MigrationCertificate:ActionRequiringStates").Value).Split(",");
                    if (actionRequiringStateListForStudent.Count() > 0 && Array.IndexOf(actionRequiringStateListForStudent, usrSrvcApplied.LastStatus) != -1)
                    {// Latest state requires student action
                        usrSrvcApplied.IsActionRequired = true;
                    }
                    else
                    {// Latest state does not require student action
                        usrSrvcApplied.IsActionRequired = false;
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    usrSrvcApplied.IsActionRequired = false;
                }
            }
            _MSUoW.UserServiceApplied.Update(usrSrvcApplied);
        }

        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   28-02-2020
        /// Description : This method is used to send mail to next approving authority.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="userName"></param>
        /// <param name="serviceName"></param>
        /// <returns></returns>

        private bool SendEmailToApprovingAuthorityOnStateChange(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ServiceRequestReceived && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    // Decide whether 
                    string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", ClientApplicationURL);
                    body.Replace("--serviceName--", serviceName);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   28-02-2020
        /// Description : This method is used to send mail to students regarding collection of migration certificate.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool SendEmailOnWorkflowCompletion(string emailAddress, string userName, string regNumber)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGenerated && s.ServiceType == (long)enServiceType.RegistrationService && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--url--", ClientApplicationURL);
                    body.Replace("--regNo--", regNumber);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   15-01-2020
        /// Description :  Send email to students about Rejection of Migration Certificate
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="userName"></param>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        /// 
        private bool SendEmailToStudentOnMigrationCertificateRejection(string emailAddress, string userName, string serviceName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedMail && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string clientApplicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--userName--", userName);
                    body.Replace("--serviceName--", serviceName);
                    body.Replace("--url--", clientApplicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;

        }

        /// <summary>
        /// Author  :   Sumbul Samreen
        /// Date    :   15-01-2020
        /// Description :  Send email to principal about Rejection of Migration Certificate
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="userName"></param>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        /// 
        private bool SendEmailToPrincipalOnMigrationCertificateRejection(string emailAddress, string userName)
        {
            if (emailAddress != null && emailAddress != string.Empty)
            {
                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.MailToPrincipalOnRegistrationNumberRejection && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;

                    StringBuilder body = new StringBuilder(templateData.Body);
                    body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                    body.Replace("--studentName--", userName);
                    body.Replace("--url--", applicationUrl);
                    _MSCommon.SendMails(emailAddress, templateData.Subject, body.ToString());
                }
            }
            return true;
        }

        /// <summary>
        /// Author              : Sumbul Samreen
        /// Date                : 28-02-2020
        /// Description         : This method is called when Principal forwards the reg.no.application form to computer cell.
        /// </summary>
        /// <param name="id">Instance id of the reg.num. application form entity</param>
        /// <param name="input">Dto carrying all the modified information of the reg.num.application form</param>
        /// <returns></returns>
        public UniversityMigrationDetailDto MigrationCertificateApplicationStatusUpdateByPrincipal(Guid id, UniversityMigrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityMigrationDetail = _MSUoW.UniversityMigrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityMigrationDetail != null && input.Id == id)
            {
                var principalLastUpdateTime = universityMigrationDetail.PrincipalLastUpdateTime;
                int minimumLockingHours = 0;
                try
                {
                    minimumLockingHours = Convert.ToInt32(_IConfiguration.GetSection("MigrationCertificate:MinimumLockingHoursForStateChangeByPrincipal").Value);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                    minimumLockingHours = 0;
                }
                if (principalLastUpdateTime == null || (principalLastUpdateTime != null && ((DateTime)principalLastUpdateTime).AddMinutes(minimumLockingHours) < DateTime.UtcNow))
                {// Either principal has not updated or principal has updated the record more than 48 hours ago
                    string sourceState = universityMigrationDetail.State;
                    if (universityMigrationDetail.TenantWorkflowId != 0)
                    {
                        #region Step c
                        universityMigrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                        #endregion

                        #region Step d
                        Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                        _WFTransactionHistroryAppService.SetEntityValue<UniversityMigrationDetail>(universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId, trigger, input, ref universityMigrationDetail, ref triggerParameterDictionary);
                        var executionStatus = input.ExecuteProcess(universityMigrationDetail, input);
                        #endregion

                        #region Step e
                        // Update the service applied table with latest data
                        input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).ToList();
                        bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityMigrationDetail.State, input.ParentWorkflowId);
                        UpdateUserServiceAppliedDetail(universityMigrationDetail, IsLastTransition);
                        #endregion

                        #region Step f
                        _MSUoW.UniversityMigrationDetail.Update(universityMigrationDetail);
                        #endregion

                        #region Step g
                        List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                        string workflowMessage = input.WorkflowMessage;
                        input = AutoMapper.Mapper.Map<UniversityMigrationDetail, UniversityMigrationDetailDto>(universityMigrationDetail);
                        input.PossibleWorkflowTransitions = possibleTransition;
                        input.WorkflowMessage = workflowMessage;
                        _MSUoW.Commit();

                        // Custom developement, sending email after every state change
                        var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                            .Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId
                                    && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                        // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                        if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                        {// Find user details of that role
                            var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                            { // No student role involved, send email
                                var nextApprovingUser = new Users();
                                if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                                { // Find the corresponding principal of the applying student
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityMigrationDetail.CollegeCode).FirstOrDefault();
                                }
                                else
                                {
                                    var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                    if (nextApprovingUserRole != null) // Checking this
                                    {
                                        nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                    }
                                }

                                //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                if (nextApprovingUser != null)
                                {
                                    string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                    if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                        SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                                }
                            }
                        }
                        // Sending email ends here
                        #endregion

                        #region Step h
                        _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityMigrationDetail.LastStatusUpdateDate, sourceState, trigger, universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId);
                        #endregion

                        #region Step k
                        //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                        if (IsLastTransition == true)
                        {
                            var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityMigrationDetail.CreatorUserId).FirstOrDefault();
                            if (applyingStudent != null)
                            {
                                //string emailIdOfApplyingUser = ;
                                //Send email about workflow completion
                                SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, universityMigrationDetail.RegistrationNo);
                            }

                        }
                        #endregion
                    }
                }
                else
                {
                    var remainingTime = Math.Round(((DateTime)principalLastUpdateTime).AddHours(minimumLockingHours).Subtract(DateTime.UtcNow).TotalHours);
                    throw new MicroServiceException { ErrorCode = "OUU060", SubstitutionParams = new object[1] { remainingTime } };
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
            //return true;
        }

        #region WORKFLOW EXECUTION
        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   02-03-2020
        /// Description : This method is used to change status by comp cell 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityMigrationDetailDto UpdateMigrationApplicationStatusByCompCell(Guid id, UniversityMigrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityMigrationDetail = _MSUoW.UniversityMigrationDetail.FindBy(univMig => univMig.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityMigrationDetail != null && input.Id == id)
            {
                string sourceState = universityMigrationDetail.State;
                if (universityMigrationDetail.TenantWorkflowId != 0)
                {
                    #region Step c
                    universityMigrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<UniversityMigrationDetail>(universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId, trigger, input, ref universityMigrationDetail, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(universityMigrationDetail, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).ToList();
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityMigrationDetail.State, input.ParentWorkflowId);
                    UpdateUserServiceAppliedDetail(universityMigrationDetail, IsLastTransition);
                    #endregion


                    #region Step f
                    string migrationNumber = GenerateMigrationNumber();
                    universityMigrationDetail.MigrationNumber = migrationNumber;
                    _MSUoW.UniversityMigrationDetail.Update(universityMigrationDetail);

                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<UniversityMigrationDetail, UniversityMigrationDetailDto>(universityMigrationDetail);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();
                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email
                            var nextApprovingUser = new Users();
                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityMigrationDetail.CollegeCode).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityMigrationDetail.LastStatusUpdateDate, sourceState, trigger, universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId);
                    #endregion

                    #region Step k
                    //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityMigrationDetail.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null && input.State != _MSCommon.GetEnumDescription((enState)enState.RegNoApplicationRejected))
                        {
                            //string emailIdOfApplyingUser = ;
                            //Send email about workflow completion
                            SendEmailOnWorkflowCompletion(applyingStudent.EmailAddress, applyingStudent.UserName, universityMigrationDetail.RegistrationNo);

                            // Send SMS to students about Registration Number Generation
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.RegistrationNumberGeneratedSMS && s.ServiceType == (long)enServiceType.RegistrationService && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                        }
                        else
                        {
                            //Send email to students about Registration Number Rejection
                            string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                            if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                            {
                                SendEmailToStudentOnMigrationCertificateRejection(applyingStudent.EmailAddress, applyingStudent.UserName, serviceName);
                            }

                            //Send SMS to students about Registration Number Rejection
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedSMS && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }
                            //Send email to principal about Registration Number Rejection
                            // var principalData = _MSUoW.Users.FindBy(a => a.CollegeCode == input.CollegeCode && a.UserType == (long)enUserType.Principal && a.IsActive).FirstOrDefault();
                            // SendEmailToPrincipalOnRegistrationNumberRejection(principalData.EmailAddress, applyingStudent.UserName);

                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
            //return true;
        }
        #endregion

        #region WORKFLOW EXECUTION
        /// <summary>
        /// Author :   Sumbul Samreen
        /// Date        :   02-03-2020
        /// Description : This method is used to change status of the workflow on rejection of Migration certificate by asst.coe
        /// </summary>
        /// <param name="id"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public UniversityMigrationDetailDto UpdateMigrationApplicationStatusToRejectState(Guid id, UniversityMigrationDetailDto input)
        {
            #region Step b starts
            input.LoggedInUserRoles = GetRoleListOfCurrentUser();
            var universityMigrationDetail = _MSUoW.UniversityMigrationDetail.FindBy(univReg => univReg.Id == id).FirstOrDefault();

            string trigger = input._trigger;
            #endregion
            if (universityMigrationDetail != null && input.Id == id)
            {
                string sourceState = universityMigrationDetail.State;
                if (universityMigrationDetail.TenantWorkflowId != 0)
                {
                    #region Step c
                    universityMigrationDetail.LastStatusUpdateDate = DateTime.UtcNow;
                    #endregion

                    #region Step d
                    Dictionary<string, object> triggerParameterDictionary = new Dictionary<string, object>();
                    _WFTransactionHistroryAppService.SetEntityValue<UniversityMigrationDetail>(universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId, trigger, input, ref universityMigrationDetail, ref triggerParameterDictionary);
                    var executionStatus = input.ExecuteProcess(universityMigrationDetail, input);
                    #endregion

                    #region Step e
                    // Update the service applied table with latest data
                    input.PossibleWorkflowTransitions = _workflowDbContext.TenantWorkflowTransitionConfig.Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).ToList();
                    bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, universityMigrationDetail.State, input.ParentWorkflowId);
                    UpdateUserServiceAppliedDetail(universityMigrationDetail, false);
                    #endregion


                    #region Step f

                    _MSUoW.UniversityMigrationDetail.Update(universityMigrationDetail);
                    #endregion

                    #region Step g
                    List<TenantWorkflowTransitionConfig> possibleTransition = input.PossibleWorkflowTransitions;
                    string workflowMessage = input.WorkflowMessage;
                    input = AutoMapper.Mapper.Map<UniversityMigrationDetail, UniversityMigrationDetailDto>(universityMigrationDetail);
                    input.PossibleWorkflowTransitions = possibleTransition;
                    input.WorkflowMessage = workflowMessage;
                    _MSUoW.Commit();

                    // Custom developement, sending email after every state change
                    var nextApprovingRoles = _workflowDbContext.TenantWorkflowTransitionConfig
                        .Where(transConfig => transConfig.TenantWorkflowId == universityMigrationDetail.TenantWorkflowId
                                && transConfig.IsActive && transConfig.SourceState == universityMigrationDetail.State).Select(transConfig => transConfig.Roles).FirstOrDefault();

                    // It is assumed that a single role will be fetched, no need to separate these with 'comma'.
                    if (nextApprovingRoles != null && nextApprovingRoles != string.Empty)
                    {// Find user details of that role
                        var approvingRoleArray = nextApprovingRoles.Split(",").Select(role => Convert.ToInt64(role)).ToArray();
                        var nextApprovingUser = new Users();
                        if (Array.IndexOf(approvingRoleArray, (long)enRoles.Student) == -1)
                        { // No student role involved, send email

                            // get the user whether principal/ HOD
                            bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == universityMigrationDetail.CollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();

                            if (Array.IndexOf(approvingRoleArray, (long)enRoles.Principal) != -1 && isAffiliated)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == universityMigrationDetail.CollegeCode).FirstOrDefault();
                            }
                            else if ((Array.IndexOf(approvingRoleArray, (long)enRoles.HOD) != -1) && !isAffiliated && universityMigrationDetail.CourseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                            {
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == universityMigrationDetail.CollegeCode.Trim().ToLower()
                                && usr.DepartmentCode.Trim().ToLower() == universityMigrationDetail.DepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                            }
                            else
                            {
                                var nextApprovingUserRole = _MSUoW.UserRoles.FindBy(usrRole => approvingRoleArray.Contains(usrRole.RoleId) && usrRole.IsActive).FirstOrDefault();
                                if (nextApprovingUserRole != null) // Checking this
                                {
                                    nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                                }
                            }

                            //var nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.Id == nextApprovingUserRole.UserId).FirstOrDefault();
                            if (nextApprovingUser != null)
                            {
                                string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                                if (serviceName != null && serviceName != string.Empty && nextApprovingUser.EmailAddress != null && nextApprovingUser.EmailAddress != string.Empty)
                                    SendEmailToApprovingAuthorityOnStateChange(nextApprovingUser.EmailAddress, nextApprovingUser.Name, serviceName);
                            }
                        }
                    }
                    // Sending email ends here
                    #endregion

                    #region Step h
                    _WFTransactionHistroryAppService.BuildWorkflowTransactionHistory(triggerParameterDictionary, id, universityMigrationDetail.LastStatusUpdateDate, sourceState, trigger, universityMigrationDetail.State, universityMigrationDetail.TenantWorkflowId);
                    #endregion

                    #region Step k
                    //bool IsLastTransition = IsLastTransitionOfWorkflow(input.PossibleWorkflowTransitions, input.State, input.ParentWorkflowId); // Moved to step e
                    if (IsLastTransition == true)
                    {
                        var applyingStudent = _MSUoW.Users.FindBy(usr => usr.Id == universityMigrationDetail.CreatorUserId).FirstOrDefault();
                        if (applyingStudent != null)
                        {
                            //Send email to students about Registration Number Rejection
                            string serviceName = _MSUoW.ServiceMaster.FindBy(i => i.Id == (long)enServiceType.MigrationService && i.IsActive).Select(s => s.ServiceName).FirstOrDefault();
                            if (serviceName != null && serviceName != string.Empty && applyingStudent.EmailAddress != null && applyingStudent.EmailAddress != string.Empty)
                            {
                                SendEmailToStudentOnMigrationCertificateRejection(applyingStudent.EmailAddress, applyingStudent.UserName, serviceName);
                            }

                            //Send SMS to students about Registration Number Rejection
                            if (applyingStudent.PhoneNumber != null && applyingStudent.PhoneNumber != string.Empty && applyingStudent.PhoneNumber.Length == 10)
                            {
                                string SMSAPIUrl = _IConfiguration.GetSection("SMSApiPath").Value;
                                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationRejectedSMS && s.IsActive == true).FirstOrDefault();
                                OTPMessage otpObj = new OTPMessage();
                                if (templateData != null)
                                {
                                    string contentId = string.Empty;
                                    contentId = templateData.SMSContentId;
                                    string applicationUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                                    StringBuilder body = new StringBuilder(templateData.Body);
                                    body.Replace("--studentName--", applyingStudent.Name);
                                    body.Replace("--serviceName--", serviceName);
                                    body.Replace("--url--", applicationUrl);
                                    otpObj.Message = body.ToString();
                                    otpObj.MobileNumber = applyingStudent.PhoneNumber;
                                    _MSCommon.SendSMS<OTPMessage>(SMSAPIUrl, otpObj, contentId);
                                }
                            }

                            ////Send email to principal/hod about Registration Number Rejection

                            bool isAffiliated = _MSUoW.CollegeMaster.FindBy(clg => clg.IsActive && clg.LookupCode.Trim().ToLower() == input.CollegeCode.Trim().ToLower()).Select(s => s.IsAffiliationRequired).FirstOrDefault();

                            Users nextApprovingUser = new Users();

                            if (isAffiliated)
                            { // Find the corresponding principal of the applying student
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.Principal && usr.CollegeCode == input.CollegeCode).FirstOrDefault();

                            }
                            else if (!isAffiliated && input.CourseType.Trim().ToString() == _MSCommon.GetEnumDescription(enCourseType.Pg).Trim().ToString())
                            {
                                //if isAffiliated false and pg then mail should go to HOD
                                nextApprovingUser = _MSUoW.Users.FindBy(usr => usr.UserType == (long)enUserType.HOD && usr.CollegeCode.Trim().ToLower() == input.CollegeCode.Trim().ToLower() && usr.DepartmentCode.Trim().ToLower() == input.DepartmentCode.Trim().ToLower() && usr.IsActive).FirstOrDefault();
                            }
                            //var principalData = _MSUoW.Users.FindBy(a => a.CollegeCode == input.CollegeCode && a.UserType == (long)enUserType.Principal && a.IsActive).FirstOrDefault();
                            SendEmailToPrincipalOnMigrationCertificateRejection(nextApprovingUser.EmailAddress, applyingStudent.UserName);

                        }
                    }
                    #endregion
                }
            }
            else
            {
                throw new MicroServiceException { ErrorMessage = "Invalid data received." };
            }
            return input;
        }
        #endregion

        /// <summary>
        /// Sumbul Samreen
        /// 25-02-2020
        /// Method is used for Odata Controller which will give only items to be work upon's count as per usertype.
        /// <returns></returns>
        public IQueryable<MigrationView> GetRoleBasedActiveRegistrationInboxCountData()
        {
            // Modified method not yet tested, view yet to be modified. Waiting for VPN connection
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            IQueryable<MigrationView> inboxDataAsPerRole = null;

            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            if (roleNames.Contains(Convert.ToString((long)enRoles.COE)))
            {
                List<string> stateList = GetRoleSpecificWorkflowStateList();
                inboxDataAsPerRole = _MSUoW.MigrationView.GetAll().Where(w => w.IsActive && stateList.Contains(w.State));
            }
            else
            {
                inboxDataAsPerRole = _MSUoW.MigrationView.FindBy(t => t.IsActive == true && workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId));
            }

            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower());
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower());
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }

        /// <summary>
        /// Author              : Sonymon Mishra
        /// Date                : 22-01-2020
        /// Description         : Get the List of all the states for the role of the loggedin user
        /// </summary>	
        public List<string> GetRoleSpecificWorkflowStateList()
        {
            List<string> roleNames = GetRoleListOfCurrentUser();
            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            long maxTenantWorkflowId = tenantWFTrans.Max(p => p.TenantWorkflowId);

            List<TenantWorkflowTransitionConfig> workflowTransition = new List<TenantWorkflowTransitionConfig>();
            if (roleNames.Contains(((int)enRoles.COE).ToString()))
            {
                List<string> RoleIds = new List<string>();
                RoleIds.Add(Convert.ToString((long)enRoles.Student));
                RoleIds.Add(Convert.ToString((long)enRoles.Principal));
                RoleIds.Add(Convert.ToString((long)enRoles.HOD));

                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).Any(x => !RoleIds.Contains(x))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();
            }
            else
            {

                workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList().Contains(role))
                    && tWFTransConfig.TenantWorkflowId == maxTenantWorkflowId
                    ).ToList();

            }

            string wfInitState = _IConfiguration.GetSection("WorkflowInitialState").Value;
            List<string> listOfStates = workflowTransition.Where(s => s.SourceState != wfInitState).Select(p => p.SourceState).Distinct().ToList();
            return listOfStates;
        }

        #region MIGRATION CERTIFICATE DOWNLOAD
        /// <summary>
        /// Author :         Sumbul Samreen
        /// Date        :    06-03-2020
        /// Description   :  This methode is used to download certificate in pdf format
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public PDFDownloadFile DownloadMigrationCertificate(certificateDownloadDto param)
        {
            PDFDownloadFile flattenedPDFDownloadDetails = new PDFDownloadFile();
            Guid id = default(Guid);
            string userName = string.Empty;
            MigrationView migrationObj = new MigrationView();
            List<string> userRole = GetRoleListOfCurrentUser();
            if (userRole.Contains(Convert.ToString((long)enRoles.Principal)) || userRole.Contains(Convert.ToString((long)enRoles.HOD)))
            {
                if (param == null)
                {
                    throw new MicroServiceException() { ErrorCode = "OUU072" };
                }
                migrationObj = _MSUoW.MigrationView.FindBy(u => u.AcknowledgementNo != null && u.AcknowledgementNo.Trim().ToLower() == param.AcknowNum.Trim().ToLower()
                              && u.RegistrationNo.Trim() == param.RegdOrRollNo.Trim() && u.IsActive && u.MigrationNumber != null).OrderByDescending(d => d.CreationTime)
                          .FirstOrDefault();

                id = migrationObj.Id;
                userName = migrationObj.StudentName;

                if (id != Guid.Empty)
                {
                    UserServiceApplied appliedData = _MSUoW.UserServiceApplied.FindBy(serv => serv.ServiceId == param.ServiceType && serv.InstanceId == id && serv.IsActive)
                                .OrderByDescending(d => d.CreationTime).FirstOrDefault();

                    if (appliedData != null && !appliedData.IsAvailableToDL)
                        throw new MicroServiceException() { ErrorCode = "OUU050" };
                }
                else
                    throw new MicroServiceException() { ErrorCode = "OUU051" };

                EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.MigrationCertificate && s.IsActive == true).FirstOrDefault();
                if (templateData != null)
                {
                    //EmailTemplateDto emailTemplate = AutoMapper.Mapper.Map<EmailTemplate, EmailTemplateDto>(templateData);

                    string description = templateData.Body;
                    string qrCode = MicroserviceCommon.GenerateQrCode(migrationObj.MigrationNumber);
                    string currentDateTime = DateTime.UtcNow.ToString("dd-MM-yyyy hh:mm:ss tt");
                    string url = _IConfiguration.GetSection("ApplicationURL").Value;
                    string clientUrl = _IConfiguration.GetSection("ClientApplicationURL").Value;
                    string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                    string examControllerSign = _IConfiguration.GetSection("ControllerOfExaminationSignature").Value;
                    //creating the qrcodeurl for validating the qrcode
                    string qrCodeUrl = clientUrl + _IConfiguration.GetSection("QRCodeValidate:ClientReturnUrl").Value + (long)enServiceType.MigrationService + "/" + migrationObj.MigrationNumber;

                    description = description.Replace("--qrCode--", MicroserviceCommon.GenerateQrCode(qrCodeUrl));
                    description = description.Replace("--sl.no.--", migrationObj.MigrationNumber);
                    //description = description.Replace("--qrCode--", qrCode);
                    description = description.Replace("--studentName--", userName);
                    description = description.Replace("--url--", url);
                    description = description.Replace("--registrationNo--", migrationObj.RegistrationNo);
                    description = description.Replace("--collegeNameString--", migrationObj.CollegeString);
                    description = description.Replace("--streamString--", migrationObj.StreamString);
                    description = description.Replace("--yearOfAdmission--", migrationObj.YearOfAdmission.ToString("yyyy"));
                    description = description.Replace("--collegeLogo--", url + collegeLogo);
                    description = description.Replace("--examControllerSig--", url + examControllerSign);
                    description = description.Replace("--certificateConfrimDate--", Convert.ToDateTime(migrationObj.LastModificationTime).ToString("dd-MM-yyyy"));
                    //description = description.Replace("--certificateConfrimDate--", Convert.ToString(registrationObj.LastModificationTime).Substring(0,9));
                    description = description.Replace("--currentDateTime--", currentDateTime);

                    //flattenedPDFDownloadDetails.PdfData = HtmlToPdfConverter.ExportToPDF(description, 600, 1000);
                    flattenedPDFDownloadDetails.PdfData = _MSCommon.ExportToPDF(description, enOrientation.Portrait);
                    flattenedPDFDownloadDetails.FileType = "application/octet-stream";
                    //flattenedPDFDownloadDetails.FileName = param.RollNo + "_" + param.TransanctionId + ".pdf";
                    flattenedPDFDownloadDetails.FileName = "MigrationCertificate.pdf";
                }
                else
                    throw new MicroServiceException { ErrorCode = "OUU009" };
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU137" };
            }
            return flattenedPDFDownloadDetails;
        }
        #endregion

        /// <summary>
        /// Author              : Sumbul Samreen
        /// Date                : 05-03-2020
        /// Description         : Sends email to student when principal updates the university migration application form submitted by student
        /// </summary>
        /// <param name="studentEmailAddress">Student email address</param>
        /// <param name="studentName">Student name</param>
        /// <returns>True if email is sent</returns>

        private bool SendUniversityMigrationUpdateConfirmationEmail(string studentEmailAddress, string studentName, string serviceName)
        {
            EmailTemplate templateData = _MSUoW.EmailTemplate.FindBy(s => s.TemplateType == (long)enEmailTemplate.ApplicationModified && s.IsActive == true).FirstOrDefault();
            if (templateData != null)
            {
                if (studentName == null || studentName.Trim() == string.Empty)
                {
                    studentName = "User";
                }
                string ClientApplicationURL = _IConfiguration.GetSection("ClientApplicationURL").Value;
                string applicationUrl = _IConfiguration.GetSection("ApplicationURL").Value;
                string collegeLogo = _IConfiguration.GetSection("UULogo").Value;
                StringBuilder body = new StringBuilder(templateData.Body);
                body.Replace("--collegeLogo--", applicationUrl + collegeLogo);
                body.Replace("--userName--", studentName);
                body.Replace("--serviceName--", serviceName);
                body.Replace("--url--", ClientApplicationURL);
                _MSCommon.SendMails(studentEmailAddress, templateData.Subject, body.ToString());
            }
            return true;
        }


        /// <summary>
        /// Author              : Sumbul Samreen
        /// Date                : 05-03-2020
        /// Description         : Method is used for Odata Controller which will give student migration record as per usertype
        /// </summary>
        /// <returns></returns>
        public IQueryable<MigrationView> GetRoleBasedActiveMigrationInboxData()
        {
            // Modified method not yet tested, view yet to be modified. Waiting for VPN connection
            List<string> roleNames = GetRoleListOfCurrentUser();
            long currentUserType = GetUserType();
            string CollegeCode = GetCollegeCode();
            string DepartmentCode = GetDepartmentCode();
            IQueryable<MigrationView> inboxDataAsPerRole = null;

            var tenantWFIdList = _workflowDbContext.TenantWorkflow
                        .Where(tenantWF => tenantWF.TenantId == GetCurrentTenantId() && tenantWF.Workflow == WorkflowName).Select(wf => wf.Id).ToList();
            var tenantWFTrans = _workflowDbContext.TenantWorkflowTransitionConfig
                    .Where(tWFTransConfig => tenantWFIdList.Contains(tWFTransConfig.TenantWorkflowId)).ToList();

            List<TenantWorkflowTransitionConfig> workflowTransition =
                    tenantWFTrans
                    .Where(tWFTransConfig => roleNames.Any(role => tWFTransConfig.Roles.Split(',', StringSplitOptions.None).ToList()
                    .Contains(role))).ToList();

            if (roleNames.Contains(Convert.ToString((long)enRoles.COE)))
            {
                List<string> stateList = GetRoleSpecificWorkflowStateList();
                inboxDataAsPerRole = _MSUoW.MigrationView.GetAll().Where(w => w.IsActive && stateList.Contains(w.State));
            }
            else
            {
                inboxDataAsPerRole = _MSUoW.MigrationView.FindBy(t => t.IsActive == true && workflowTransition
                .Any(transition => transition.SourceState == t.State && transition.TenantWorkflowId == t.TenantWorkflowId));
            }

            if (roleNames.Contains(((int)enRoles.Principal).ToString()) || roleNames.Contains(((int)enRoles.HOD).ToString()))
            {
                TenantWorkflowTransitionConfig workFlowObj = tenantWFTrans.Where(tt => tt.IsActive && tenantWFIdList.Contains(tt.TenantWorkflowId)
                                                             && tt.TransitState.Trim().ToLower() == _IConfiguration.GetSection("MigrationCertificate:MigrationApplicationApprovedStatus").Value.Trim().ToLower())
                                                            .OrderByDescending(d => d.TenantWorkflowId).FirstOrDefault();
                IQueryable<MigrationView> acknowledgmentList = _MSUoW.MigrationView.FindBy(t => t.IsActive == true && t.State == workFlowObj.TransitState && workFlowObj.TenantWorkflowId == t.TenantWorkflowId);
                inboxDataAsPerRole = inboxDataAsPerRole.Concat(acknowledgmentList);
            }


            switch (currentUserType)
            {
                case (long)enUserType.Principal:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower());
                    break;
                case (long)enUserType.HOD:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CollegeCode.Trim().ToLower() == CollegeCode.Trim().ToLower() && data.DepartmentCode.Trim().ToLower() == DepartmentCode.Trim().ToLower());
                    break;
                case (long)enUserType.Student:
                    inboxDataAsPerRole = inboxDataAsPerRole.Where(data => data.CreatorUserId == GetCurrentUserId() && data.IsActive);
                    break;
            }

            return inboxDataAsPerRole;
        }
    }
}


