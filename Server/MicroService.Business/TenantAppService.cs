﻿using Microservice.Common;
using MicroService.Business.Abstract;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.Tenants;
using MicroService.Domain.Models.Entity;
using MicroService.Utility.Common.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MicroService.Domain.Models.Dto;
using MicroService.Utility.Exception;

namespace MicroService.Business
{
    public class TenantAppService : BaseService, ITenantAppService
    {
        public const string AdminUserName = "admin";

        private IMicroServiceUOW _MSUoW;
        public TenantAppService(IMicroServiceUOW MSUoW, IRedisDataHandlerService redisData
            ) : base(redisData)
        {
            _MSUoW = MSUoW;
        }

        public TenantDto CreateTenant(CreateTenantDto input)
        {
            if (input != null && input.TenancyName != string.Empty)
            {
                //CheckCreatePermission();

                // Create tenant
                var tenant = AutoMapper.Mapper.Map<Tenants>(input);
                var existingSameNamedTenant = _MSUoW.Tenants.FindBy(obj => obj.Name.ToLower() == tenant.Name.ToLower()).FirstOrDefault();
                if (existingSameNamedTenant == null)
                {
                    _MSUoW.Tenants.Add(tenant);
                    //tenant.ConnectionString = input.ConnectionString.IsNullOrEmpty()
                    //    ? null
                    //    : SimpleStringCipher.Instance.Encrypt(input.ConnectionString);

                    //var defaultEdition = await _editionManager.FindByNameAsync(EditionManager.DefaultEditionName);
                    //if (defaultEdition != null)
                    //{
                    //    tenant.EditionId = defaultEdition.Id;
                    //}



                    // Creating static roles for new tenants and place it inside tenant main object
                    Role tenantAdminRole = CreateStaticRoles();
                    tenant.TenantRoles = new List<Role>();
                    tenant.TenantRoles.Add(tenantAdminRole);

                    // Grant all permissions to admin role
                    //Role tenantAdminRole = _MSUoW.Role.FindBy(r => r.Name == StaticRoleNames.Tenants.Admin).FirstOrDefault();

                    //await _roleManager.GrantAllPermissionsAsync(tenantAdminRole);


                    // Create admin user for the tenant
                    string tenantEmailAddress = String.Format("{0}.admin@bipros.com", tenant.TenancyName.ToLower());
                    Users tenantAdminUser = CreateTenantAdminUser(tenantEmailAddress);
                    tenant.TenantUsers.Add(tenantAdminUser);
                    //tenantAdminUser.Password = Encoding.ASCII.GetBytes(Users.DefaultPassword);
                    //tenantAdminUser.Salt = EncryptionDecryption.GenerateSalt();
                    //tenantAdminUser.Password = EncryptionDecryption.GenerateHashWithSalt(tenantAdminUser.Password, tenantAdminUser.Salt);
                    // tenantAdminUser.Password = _passwordHasher.HashPassword(tenantAdminUser, Users.DefaultPassword);
                    //_MSUoW.Users.Add(tenantAdminUser);
                    //CheckErrors(await _userManager.CreateAsync(tenantAdminUser));
                    //await CurrentUnitOfWork.SaveChangesAsync(); // To get admin user's id

                    // Assign admin user to role!

                    CreateUserRole(tenantAdminUser, tenantAdminRole.Id);

                    //CheckErrors(await _userManager.AddToRoleAsync(adminUser, tenantAdminRole.Name));

                    //await CurrentUnitOfWork.SaveChangesAsync();

                    //    // Create static roles for new tenant
                    //CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));

                    //await CurrentUnitOfWork.SaveChangesAsync(); // To get static role ids

                    // Grant all permissions to admin role
                    //var tenantAdminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
                    //await _roleManager.GrantAllPermissionsAsync(tenantAdminRole);
                    _MSUoW.Commit();

                    var tenantResult = AutoMapper.Mapper.Map<Tenants, TenantDto>(tenant);
                    return tenantResult;
                }
                else
                {
                    throw new MicroServiceException() { ErrorCode = "OUU006" };
                }
            }
            else
            {
                throw new MicroServiceException() { ErrorCode = "OUU019" };
            }
        }

        //create new userroles
        private void CreateUserRole(Users adminUser, long roleId)
        {
            var userRole = new UserRoles
            {
                RoleId = roleId,
                UserId = adminUser.Id,
                TenantId = adminUser.TenantId
            };
            _MSUoW.UserRoles.Add(userRole);
        }

        //create new static role "admin" for particular tenant
        private Role CreateStaticRoles()
        {
            return new Role
            {
                Description="Tenant Admin Role",
                Name = "Admin",
                DisplayName = "Admin",
                IsStatic = true
            };
            //_MSUoW.Role.Add(role);
        }

        //create new user
        private Users CreateTenantAdminUser(string emailAddress)
        {
            Users user = new Users
            {
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Password = Encoding.ASCII.GetBytes(Users.DefaultPassword),
                Salt = EncryptionDecryption.GenerateSalt()
            };
            user.Password = EncryptionDecryption.GenerateHashWithSalt(user.Password, user.Salt);
            return user;
        }
        //FOR UPDATE TENANT
        public TenantDto UpdateTenant(long id, TenantDto input)
        {
            var tenant = _MSUoW.Tenants.GetById(id);
            Tenants tenantUpdate = new Tenants();
            //await _tenantManager.GetByIdAsync(id);
            if (tenant != null)
            {
                tenantUpdate = AutoMapper.Mapper.Map(input, tenant);
                //MapToEntity(input, tenant);
                _MSUoW.Tenants.Update(tenantUpdate);
                //await _tenantManager.UpdateAsync(tenant);
                _MSUoW.Commit();
            }
            var tenantResult = AutoMapper.Mapper.Map<Tenants, TenantDto>(tenantUpdate);
            return input;
        }

        public Tenants GetTenantDetails(long id)
        {
            return _MSUoW.Tenants.GetById(id);
                //await _tenantManager.GetByIdAsync(id);
        }

        public bool DeleteTenantDetails(long id)
        {
            bool deleteTenant = false;
            var tenant = _MSUoW.Tenants.GetById(id);
            if(tenant!=null)
            {

                tenant.IsActive = false;
                _MSUoW.Tenants.Update(tenant);
                _MSUoW.Commit();
                deleteTenant = true;
            }
            return deleteTenant;            
        }

        public IQueryable<Tenants> GetAllTenantsData()
        {
            return _MSUoW.Tenants.GetAll().AsQueryable();
        }
    }
}
