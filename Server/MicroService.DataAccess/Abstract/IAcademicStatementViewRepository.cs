﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IAcademicStatementViewRepository : IRepository<AcademicStatementView>
    {
        IQueryable<AcademicStatementView> GetAllAcademicStatementDetails(long tenantId);
        IQueryable<AcademicStatementView> GetAllAcademicStatementDetailswithsearchparam(SearchParamDto searchParam, long tenantId);


    }
}
