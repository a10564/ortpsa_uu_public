﻿using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface ICLCRepository : IRepository<ApplyCLC>
    {
        IQueryable<ApplyCLC> GetAllClcData();
        //IQueryable<ExaminationFormFillUpStudentAddress> GetStudentAddress(string addressType, Guid instanceId);
		IQueryable<ServiceStudentAddress> GetStudentAddress(string addressType, Guid instanceId);
	}
}
