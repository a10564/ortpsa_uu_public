﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IExaminationFormFillUpRepository : IRepository<ExaminationFormFillUpDetails>
    {
        IQueryable<ExaminationFormFillUpDetails> GetAllExamFormFillUpData();
        IQueryable<CollegeWiseAmountForCoeDto> GetAllStudentStateWise(SearchParamDto param,string instanceTrigger);
        IQueryable<ServiceStudentAddress> GetStudentAddress(string addressType, Guid instanceId);
    }
}
