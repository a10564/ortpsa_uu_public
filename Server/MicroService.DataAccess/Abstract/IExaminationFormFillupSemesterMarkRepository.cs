﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IExaminationFormFillupSemesterMarkRepository : IRepository<ExaminationFormFillUpSemesterMarks>
    {
        IQueryable<ExaminationPaperWiseMarkDto> GetAllPaperWiseMark();

    }
}
