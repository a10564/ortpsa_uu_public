﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.DataAccess.Abstract
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAllIgnoreGlobalFilter();
        T GetById(long id);
        void Add(T entity);
        void Update(T entity);
        void UpdateRange(List<T> entity);
        //IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        void AddRange(List<T> entity);
        void Remove(T entity);
        void RemoveRange(List<T> entity);
        long Add_GetPrimaryKey(T entity);
        void UpdateSingleColumn(T entity, string property);
        List<T> CallStoredProcedureGetDetails(string StoredProcedureName, List<string> searchParams);
    }
}
