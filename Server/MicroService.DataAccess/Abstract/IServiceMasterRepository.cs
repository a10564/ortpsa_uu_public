﻿using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IServiceMasterRepository : IRepository<ServiceMaster>
    {
        ServiceMaster GetServiceMasterById(long id);
        List<ServiceMaster> GetServiceMaster(long currentUserType);
        List<ServiceMaster> GetServiceMaster(string collegeCode);
    }    
}
