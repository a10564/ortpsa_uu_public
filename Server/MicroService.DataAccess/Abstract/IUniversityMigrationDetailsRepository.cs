﻿using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IUniversityMigrationDetailsRepository : IRepository<UniversityMigrationDetail>
    {
        IQueryable<UniversityMigrationDetail> GetAllData();
        IQueryable<UniversityMigrationDetail> GetAllDataById();
    }
}
