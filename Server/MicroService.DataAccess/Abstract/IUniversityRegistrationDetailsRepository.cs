﻿using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess.Abstract
{
    public interface IUniversityRegistrationDetailsRepository : IRepository<UniversityRegistrationDetail>
    {
        IQueryable<UniversityRegistrationDetail> GetAllData();
        IQueryable<UniversityRegistrationDetail> GetAllDataById();
    }
}
