﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class CLCRepository : MicroServiceGenericRepository<ApplyCLC>, ICLCRepository
    {
        public CLCRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<ApplyCLC> GetAllClcData()
        {
            return DbSet.Include(a => a.clcFormFillUpStudentAddresses).Include(a => a.ClcDocument);
        }

        public IQueryable<ServiceStudentAddress> GetStudentAddress(string addressType, Guid instanceId)
        {
            return from addr in DbContext.Set<ServiceStudentAddress>()
                   join cntrlk in DbContext.Set<Lookup>() on addr.Country equals cntrlk.LookupCode
                   join stlk in DbContext.Set<Lookup>() on addr.State equals stlk.LookupCode
                   join ctlk in DbContext.Set<Lookup>() on addr.City equals ctlk.LookupCode
                   where addr.InstanceId == instanceId && addr.AddressType == addressType
                   && cntrlk.IsActive && addr.IsActive && stlk.IsActive && ctlk.IsActive
                   select new ServiceStudentAddress
				   {
                       Address = addr.Address,
                       Country = cntrlk.LookupDesc,
                       State = stlk.LookupDesc,
                       City = ctlk.LookupDesc,
                       Zip = addr.Zip,
                       AddressType = addr.AddressType
                   };
        }
    }
}
