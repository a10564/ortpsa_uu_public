﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class ExaminationFormFillupSemesterMarkRepository : MicroServiceGenericRepository<ExaminationFormFillUpSemesterMarks>, IExaminationFormFillupSemesterMarkRepository
    {
        public ExaminationFormFillupSemesterMarkRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<ExaminationPaperWiseMarkDto> GetAllPaperWiseMark()
        {
            var data = from exam in DbContext.Set<ExaminationView>()
                       join mark in DbSet on exam.Id equals mark.ExaminationFormFillUpDetailsId
                       join paper in DbContext.Set<ExaminationPaperMaster>() on mark.ExaminationPaperId equals paper.Id
                       where exam.State == "Mark Uploaded"
                       select new ExaminationPaperWiseMarkDto
                       {
                           Id=exam.Id,
                           CreationTime=exam.CreationTime,
                           CreatorUserId=exam.CreatorUserId,
                           LastModificationTime=exam.LastModificationTime,
                           LastModifierUserId=exam.LastModifierUserId,
                           IsActive=exam.IsActive,
                           RollNumber=exam.RollNumber,
                           RegistrationNumber=exam.RegistrationNumber,
                           CourseTypeCode=exam.CourseTypeCode,
                           CourseTypeCodeString=exam.CourseTypeString,
                           CollegeCode=exam.CollegeCode,
                           CollegeCodeString=exam.CollegeString,
                           StreamCode=exam.StreamCode,
                           StreamCodeString=exam.StreamString,
                           SubjectCode=exam.SubjectCode,
                           SubjectCodeString=exam.SubjectString,
                           SemesterCode=exam.SemesterCode,
                           SemesterCodeString=exam.SemesterString,
                           AcademicStart=exam.AcademicStart,
                           AcknowledgementNo=exam.AcknowledgementNo,
                           State=exam.State,
                           PaymentDate= exam.PaymentDate ,
                           ExaminationPaperId=mark.ExaminationPaperId,
                           InternalMarkSecured=mark.InternalMark,
                           ExternalMarkSecured=mark.ExternalMark,
                           PracticalMarkSecured=mark.PracticalMark,
                           IsPratical=mark.IsPractical,
                           ExaminationMasterId=paper.ExaminationMasterId,
                           PaperName=paper.PaperName,
                           PaperAbbreviation = paper.PaperAbbreviation,
                           Category = paper.Category,
                           CategoryAbbreviation = paper.CategoryAbbreviation,
                           InternalMark = paper.InternalMark,
                           ExternalMark = paper.ExternalMark,
                           PracticalMark = paper.PracticalMark,
                           IsExternalAttained = mark.IsExternalAttained
                       };
            return data;
        }
    }
}
