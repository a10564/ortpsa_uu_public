﻿using MicroService.DataAccess.Abstract;
using MicroService.DataAccess.Helpers;
using MicroService.Domain.Models.Entity;
using MicroService.Domain.Models.Entity.Lookup;
using MicroService.Domain.Models.Entity.Nursing;
//using MicroService.Domain.Models.Entity.Workflow;
using MicroService.Domain.Models.Views;
using MicroService.Utility.Common.Abstract;
//using MicroService.Domain.Models;
//using MicroService.Domain.Models.AggregateRoot;
//using MicroService.Domain.Models.Entity;
using MicroService.Utility.Logging.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

//using MService = MicroService.Domain.Models.Entity.Workflow;

namespace MicroService.DataAccess
{
    public class MicroServiceUOW : IMicroServiceUOW, IDisposable
    {
        private MicroServiceDbContext DbContext { get; set; }
        protected IRepositoryProvider RepositoryProvider { get; set; }
        private IRedisDataHandlerService _redisData;
        private IMicroServiceLogger _logger;
        public MicroServiceUOW(IRepositoryProvider repositoryProvider, IRedisDataHandlerService redisData, IMicroServiceLogger logger)
        {
            _redisData = redisData;
            _logger = logger;
            CreateDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;


        }

        protected void CreateDbContext()
        {
            DbContext = new MicroServiceDbContext(GetCurrentUserId<long>(), GetCurrentTenantId<int>(), _logger);

            // Do NOT enable proxied entities, else serialization fails
            //  DbContext.Configuration.ProxyCreationEnabled = false;

            // Load navigation properties explicitly (avoid serialization trouble)
            //DbContext.Configuration.LazyLoadingEnabled = false;

            // Because Web API will perform validation, we don't need/want EF to do so
            //DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = false;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        public void Commit()
        {
            DbContext.SaveChanges(true);
        }

        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }


        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }
        #endregion

        #region ENTITY REPOSITORY
        public IRepository<Users> Users
        {
            get { return GetStandardRepo<Users>(); }
        }

        public IRepository<UserLoginDetail> UserLoginDetail
        {
            get { return GetStandardRepo<UserLoginDetail>(); }
        }

        public IRepository<UserOtp> UserOtp { get { return GetStandardRepo<UserOtp>(); } }

        public IRepository<Tenants> Tenants
        {
            get { return GetStandardRepo<Tenants>(); }
        }

        public IRepository<UserRoles> UserRoles
        {
            get { return GetStandardRepo<UserRoles>(); }
        }

        public IRepository<UserRoleGeographyMapper> UserRoleGeographyMapper
        {
            get { return GetStandardRepo<UserRoleGeographyMapper>(); }
        }

        public IRepository<RoleFeatureApiMapper> RoleFeatureApiMapper
        {
            get { return GetStandardRepo<RoleFeatureApiMapper>(); }
        }

        public IRepository<Role> Role
        {
            get { return GetStandardRepo<Role>(); }
        }

        public IRepository<Feature> Feature
        {
            get { return GetStandardRepo<Feature>(); }
        }

        public IRepository<FeatureApiMapper> FeatureApiMapper
        {
            get { return GetStandardRepo<FeatureApiMapper>(); }
        }

        public IRepository<FeatureOrder> FeatureOrder
        {
            get { return GetStandardRepo<FeatureOrder>(); }
        }

        public IRepository<ApiDetail> ApiDetail
        {
            get { return GetStandardRepo<ApiDetail>(); }

        }
        public IRepository<GeographyOrder> GeographyOrder
        {
            get { return GetStandardRepo<GeographyOrder>(); }

        }
        public IRepository<GeographyDetail> GeographyDetail
        {
            get { return GetStandardRepo<GeographyDetail>(); }
        }
        public IRepository<Language> Language
        {
            get { return GetStandardRepo<Language>(); }
        }
        public IRepository<Module> Module
        {
            get { return GetStandardRepo<Module>(); }
        }
        public IRepository<Multilingual> Multilingual
        {
            get { return GetStandardRepo<Multilingual>(); }
        }
        public IRepository<ServiceMaster> ServiceMaster
        {
            get { return GetStandardRepo<ServiceMaster>(); }
        }
        public IRepository<UserServiceApplied> UserServiceApplied
        {
            get { return GetStandardRepo<UserServiceApplied>(); }
        }
        public IRepository<EmailTemplate> EmailTemplate
        {
            get { return GetStandardRepo<EmailTemplate>(); }
        }
        //Some changes
        public IRepository<LookupType> LookupType
        {
            get { return GetStandardRepo<LookupType>(); }
        }

        public IRepository<SubjectMaster> SubjectMaster
        {
            get { return GetStandardRepo<SubjectMaster>(); }
        }

        public IRepository<DepartmentMaster> DepartmentMaster
        {
            get { return GetStandardRepo<DepartmentMaster>(); }
        }

        public IRepository<CollegeMaster> CollegeMaster
        {
            get { return GetStandardRepo<CollegeMaster>(); }
        }

        public IRepository<Lookup> Lookup
        {
            get { return GetStandardRepo<Lookup>(); }
        }

        public IRepository<UserAddress> UserAddress
        {
            get { return GetStandardRepo<UserAddress>(); }
        }

        //public IRepository<Document> Document
        //{
        //    get { return GetStandardRepo<Document>(); }
        //}

        public IRepository<UniversityRegistrationDetail> UniversityRegistrationDetail
        {
            get { return GetStandardRepo<UniversityRegistrationDetail>(); }
        }
        //clone table of Document
        public IRepository<Documents> Documents
        {
            get { return GetStandardRepo<Documents>(); }
        }

        public IRepository<DuplicateRegistrationDetail> DuplicateRegistrationDetail
        {
            get { return GetStandardRepo<DuplicateRegistrationDetail>(); }
        }

        public IRepository<UniversityMigrationDetail> UniversityMigrationDetail
        {
            get { return GetStandardRepo<UniversityMigrationDetail>(); }
        }
        public IRepository<ProvisionalCertificateApplicationDetail> ProvisionalCertificateApplicationDetail
        {
            get { return GetStandardRepo<ProvisionalCertificateApplicationDetail>(); }
        }

        public IRepository<ExaminationPaperMaster> ExaminationPaperMaster
        {
            get { return GetStandardRepo<ExaminationPaperMaster>(); }
        }

        public IRepository<BackgroundProcess> BackgroundProcess
        {
            get { return GetStandardRepo<BackgroundProcess>(); }
        }

        public IRepository<ExaminationMaster> ExaminationMaster
        {
            get { return GetStandardRepo<ExaminationMaster>(); }
        }

        public IRepository<ExaminationFormFillUpDates> ExaminationFormFillUpDates
        {
            get { return GetStandardRepo<ExaminationFormFillUpDates>(); }
        }

        public IRepository<ExaminationSession> ExaminationSession
        {
            get { return GetStandardRepo<ExaminationSession>(); }
        }

        public IRepository<ExaminationFormFillUpDetails> ExaminationFormFillUpDetails
        {
            get { return GetStandardRepo<ExaminationFormFillUpDetails>(); }
        }

        public IRepository<ServiceStudentAddress> ServiceStudentAddress
        {
            get { return GetStandardRepo<ServiceStudentAddress>(); }
        }

        public IRepository<ExaminationPaperDetail> ExaminationPaperDetail
        {
            get { return GetStandardRepo<ExaminationPaperDetail>(); }
        }
        public IRepository<ProcessTracker> ProcessTracker
        {
            get { return GetStandardRepo<ProcessTracker>(); }
        }

		public IRepository<NumberGenerator> NumberGenerator
		{
			get { return GetStandardRepo<NumberGenerator>(); }
		}

		public IRepository<TransactionLog> TransactionLog
        {
			get { return GetStandardRepo<TransactionLog>(); }
		}


		public IRepository<ExaminationPricingMaster> ExaminationPricingMaster
		{
			get { return GetStandardRepo<ExaminationPricingMaster>(); }
		}

		public IRepository<LateFineMaster> LateFineMaster
		{
			get { return GetStandardRepo<LateFineMaster>(); }
		}
        public IRepository<CenterAllocation> CenterAllocation
        {
            get { return GetStandardRepo<CenterAllocation>(); }            
        }
        public IRepository<ApplyCLC> ApplyCLC
        {
            get { return GetStandardRepo<ApplyCLC>(); }
        }
        public IRepository<CollegeSubjectMapper> CollegeSubjectMapper
        {
            get { return GetStandardRepo<CollegeSubjectMapper>(); }
        }
        public IRepository<RoleServiceMapper> RoleServiceMapper
        {
            get { return GetStandardRepo<RoleServiceMapper>(); }
        }
        public IRepository<StudentProfile> StudentProfile
        {
            get { return GetStandardRepo<StudentProfile>(); }
        }
        public IRepository<ExaminationFormFillUpSemesterMarks> ExaminationFormFillUpSemesterMarks
        {
            get { return GetStandardRepo<ExaminationFormFillUpSemesterMarks>(); }
        }
        public IRepository<NursingFormfillupDetails> NursingFormfillupDetails
        {
            get { return GetStandardRepo<NursingFormfillupDetails>(); }
        }
        public IRepository<CollegeServiceMapper> CollegeServiceMapper
        {
            get { return GetStandardRepo<CollegeServiceMapper>(); }
        }
        public IRepository<RoleUserWiseSubjectMapper> RoleUserWiseSubjectMapper
        {
            get { return GetStandardRepo<RoleUserWiseSubjectMapper>(); }
        }

        #region WORKFLOW ENTITY
        //public IRepository<MessageTemplate> MessageTemplate
        //{
        //    get { return GetStandardRepo<MessageTemplate>(); }
        //}

        //public IRepository<MService.Microservice> Microservice
        //{
        //    get { return GetStandardRepo<MService.Microservice>(); }
        //}

        //public IRepository<RoleConfiguration> RoleConfiguration
        //{
        //    get { return GetStandardRepo<RoleConfiguration>(); }
        //}

        //public IRepository<TenantWorkflowTransitionHistory> TenantWorkflowTransitionHistory
        //{
        //    get { return GetStandardRepo<TenantWorkflowTransitionHistory>(); }
        //}

        //public IRepository<WorkflowTransitionEscalationRule> WorkflowTransitionEscalationRule
        //{
        //    get { return GetStandardRepo<WorkflowTransitionEscalationRule>(); }
        //}

        #endregion

        #endregion

        #region VIEW REPOSITORY
        public IRepository<UserView> UserView
        {
            get { return GetStandardRepo<UserView>(); }
        }
        public IRepository<FeatureView> FeatureView
        {
            get { return GetStandardRepo<FeatureView>(); }
        }
        public IRepository<GeographyDetailView> GeographyDetailView
        {
            get { return GetStandardRepo<GeographyDetailView>(); }
        }
        public IRepository<AcknowledgementView> AcknowledgementView
        {
            get { return GetStandardRepo<AcknowledgementView>(); }
        }
        public IRepository<DuplicateRegistrationView> DuplicateRegistrationView
        {
            get { return GetStandardRepo<DuplicateRegistrationView>(); }
        }
        public IRepository<MigrationView> MigrationView
        {
            get { return GetStandardRepo<MigrationView>(); }
        }
        public IRepository<ExaminationView> ExaminationView
        {
            get { return GetStandardRepo<ExaminationView>(); }
        }
        public IRepository<ExaminationViewExcludeBackgroundProcessTable> ExaminationViewExcludeBackgroundProcessTable
        {
            get { return GetStandardRepo<ExaminationViewExcludeBackgroundProcessTable>(); }
        }
        public IRepository<FormFillupViewForAdmitCard> FormFillupViewForAdmitCard
        {
            get { return GetStandardRepo<FormFillupViewForAdmitCard>(); }
        }
        public IRepository<AdmitCardCenterAndExamNameView> AdmitCardCenterAndExamNameView
        {
            get { return GetStandardRepo<AdmitCardCenterAndExamNameView>(); }
        }
        public IRepository<CLCView> CLCView
        {
            get { return GetStandardRepo<CLCView>(); }
        }
        public IRepository<StudentProfileView> StudentProfileView
        {
            get { return GetStandardRepo<StudentProfileView>(); }
        }
        public IRepository<AcademicStatementView> AcademicStatementView
        {
            get { return GetStandardRepo<AcademicStatementView>(); }
        }
        public IRepository<MarkStatementView> MarkStatementView
        {
            get { return GetStandardRepo<MarkStatementView>(); }
        }
        public IRepository<NursingViewExcludeBackgroundProcessTable> NursingViewExcludeBackgroundProcessTable
        {
            get { return GetStandardRepo<NursingViewExcludeBackgroundProcessTable>(); }
        }
        public IRepository<NursingMarkStatementView> NursingMarkStatementView
        {
            get { return GetStandardRepo<NursingMarkStatementView>(); }
        }
        #endregion

        #region CUSTOM REPOSITORY
        //public IFarmerRepository FarmerRepository
        //{
        //    get { return GetRepo<IFarmerRepository>(); }
        //}

        public IUserRepository UserRepository
        {
            get { return GetRepo<IUserRepository>(); }
        }
        public IRoleRepository RoleRepository
        {
            get { return GetRepo<IRoleRepository>(); }
        }
        public IFeatureApiMapperRepository FeatureApiMapperRepository
        {
            get { return GetRepo<IFeatureApiMapperRepository>(); }
        }
        public IUniversityRegistrationDetailsRepository UniversityRegistrationDetailsRepository
        {
            get { return GetRepo<IUniversityRegistrationDetailsRepository>(); }
        }
        public IExaminationFormFillUpRepository ExaminationFormFillUpRepository
        {
            get { return GetRepo<IExaminationFormFillUpRepository>(); }
        }
        public IExamFormFillupCenterAndCollegeNameRepository ExamFormFillupCenterAndCollegeNameRepository
        {
            get { return GetRepo<IExamFormFillupCenterAndCollegeNameRepository>(); }
        }

		public IServiceMasterRepository ServiceMasterRepository
		{
			get { return GetRepo<IServiceMasterRepository>(); }
		}
        public IUniversityMigrationDetailsRepository UniversityMigrationDetailsRepository
        {
            get { return GetRepo<IUniversityMigrationDetailsRepository>(); }
        }
        public ICLCRepository CLCRepository
        {
            get { return GetRepo<ICLCRepository>(); }
        }
        public IAcademicStatementViewRepository AcademicStatementViewRepository
        {
            get { return GetRepo<IAcademicStatementViewRepository>(); }
        }
        public IExaminationFormFillupSemesterMarkRepository ExaminationFormFillupSemesterMarkRepository 
        {
            get { return GetRepo<IExaminationFormFillupSemesterMarkRepository>(); }
        }
        public ISubjectMasterRepository SubjectMasterRepository
        {
            get { return GetRepo<ISubjectMasterRepository>(); }
        }
        public INursingFormFillUpRepository NursingFormFillUpRepository
        {
            get { return GetRepo<INursingFormFillUpRepository>(); }
        }
        public IExaminationMasterRepository ExaminationMasterRepository
        {
            get { return GetRepo<IExaminationMasterRepository>(); }
        }
        public ITransactionLogRepository TransactionLogRepository
        {
            get { return GetRepo<ITransactionLogRepository>(); }
        }
        #endregion

        #region CURRENT USER INFO

        //protected T GetCurrentUserId<T>()
        //{
        //    return _redisData.GetClaimsData<T>("Id");
        //}

        //protected T GetCurrentTenantId<T>()
        //{
        //    return _redisData.GetClaimsData<T>("TenantId");
        //}

        protected T GetCurrentUserId<T>()
        {
            try
            {
                var userIdClaims = _redisData.GetClaimsData<string>("Id");
                if (userIdClaims != null)
                    return (T)Convert.ChangeType(Convert.ToInt64(userIdClaims.ToString()), typeof(T));
                else
                    return (T)Convert.ChangeType(0, typeof(T));
            }
            catch(Exception ex)
            {
                return (T)Convert.ChangeType(0, typeof(T));
            }
        }

        protected T GetCurrentTenantId<T>()
        {
            //return _redisData.GetClaimsData<T>("TenantId");
            try
            {
                var userTenantIdClaims = _redisData.GetClaimsData<string>("TenantId");
                if (userTenantIdClaims != null)
                    return (T)Convert.ChangeType(Convert.ToInt64(userTenantIdClaims.ToString()), typeof(T));
                else
                    return (T)Convert.ChangeType(0, typeof(T));
            }
            catch (Exception ex)
            {
                return (T)Convert.ChangeType(0, typeof(T));
            }
        }
        #endregion
    }
}


