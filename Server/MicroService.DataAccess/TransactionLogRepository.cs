﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class TransactionLogRepository : MicroServiceGenericRepository<TransactionLog>, ITransactionLogRepository
    {
        public TransactionLogRepository(DbContext dbContext) : base(dbContext)
        {

        }

        public IQueryable<TransactionLog> GetTransactionLogs()
        {
            return DbSet.AsQueryable();
        }
    }
}
