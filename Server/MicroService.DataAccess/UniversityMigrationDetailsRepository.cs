﻿using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    class UniversityMigrationDetailsRepository : MicroServiceGenericRepository<UniversityMigrationDetail>, IUniversityMigrationDetailsRepository
    {
        public UniversityMigrationDetailsRepository(DbContext dbContext) : base(dbContext) { }

        public IQueryable<UniversityMigrationDetail> GetAllData()
        {
            return DbSet.Include(a => a.MigrationDocument);
        }

        public IQueryable<UniversityMigrationDetail> GetAllDataById()
        {
            var data = from a in DbSet
                       join b in DbContext.Set<Documents>() on a.Id equals b.ParentSourceId
                       where a.IsActive == true
                       select new UniversityMigrationDetail
                       {
                           Id = a.Id,
                           StudentName = a.StudentName,
                           DateOfBirth = a.DateOfBirth,
                           CollegeCode = a.CollegeCode,
                           CourseType = a.CourseType,
                           StreamCode = a.StreamCode,
                           SubjectCode = a.SubjectCode,
                           CourseCode = a.CourseCode,
                           DepartmentCode = a.DepartmentCode,
                           YearOfAdmission = a.YearOfAdmission,
                           YearOfLeaving=a.YearOfLeaving,
                           RegistrationNo=a.RegistrationNo,
                           ReasonForMigrationCode=a.ReasonForMigrationCode,
                           StudyDetail=a.StudyDetail,
                           AcknowledgementNo=a.AcknowledgementNo,
                           State = a.State,
                           TenantWorkflowId = a.TenantWorkflowId,
                           MigrationDocument = a.MigrationDocument
                                                  .Where(doc => doc.ParentSourceId == a.Id && doc.IsActive)
                                                  .Select(p => new Documents
                                                  {
                                                      Id = p.Id,
                                                      FileName = p.FileName,
                                                      FilePath = p.FilePath,
                                                      FileExtension = p.FileExtension
                                                  }).ToList()
                       };
            return data;
        }
    }

}

