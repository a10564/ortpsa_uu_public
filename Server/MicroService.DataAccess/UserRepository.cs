﻿using Microservice.Common;
using MicroService.DataAccess.Abstract;
using MicroService.Domain.Models.Dto.BackgroundProcess;
using MicroService.Domain.Models.Dto.Report;
using MicroService.Domain.Models.Entity;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace MicroService.DataAccess
{
    public class UserRepository : MicroServiceGenericRepository<Users>, IUserRepository
    {
        protected DbContext DbContext;
        public UserRepository(DbContext dbContext) : base(dbContext)
        {
            DbContext = dbContext;
        }

        public dynamic callSp(BackgroundProcessDto param)
        {

            using (var command = DbContext.Database.GetDbConnection().CreateCommand())
            {
                GetSpParamByService(param, command);

                DbContext.Database.OpenConnection();
                var reader = command.ExecuteReader();

                DataTable dataTable = new DataTable();

                dataTable.Load(reader);


                GridModelDto gridModelDto = new GridModelDto();

                gridModelDto.Columns = new List<GridColumn>();
                gridModelDto.Rows = new List<GridRow>();

                foreach (DataColumn col in dataTable.Columns)
                {
                    gridModelDto.Columns.Add(new GridColumn
                    {
                        ColumnLabel = col.ColumnName.ToString(),
                        ColumnName = col.ColumnName.ToString()
                    });
                }

                foreach (DataRow row in dataTable.Rows)
                {
                    List<String> li = new List<string>();
                    foreach (var item in row.ItemArray)
                    {
                        li.Add(item.ToString());
                    }

                    gridModelDto.Rows.Add(new GridRow
                    {
                        VisibleFields = li
                    });

                }
                DbContext.Database.CloseConnection();
                return gridModelDto;
            }


        }

        private static void GetSpParamByService(BackgroundProcessDto param, DbCommand command)
        {
            switch(param.ServiceId)
            {
                case (long)enServiceType.BCABBAExamApplicationForm:

                    command.CommandText = "sp_candidate_list";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new MySqlParameter("CourseTypeCode", param.CourseTypeCode));
                    command.Parameters.Add(new MySqlParameter("StreamCode", param.StreamCode));
                    command.Parameters.Add(new MySqlParameter("SubjectCode", param.SubjectCode));
                    command.Parameters.Add(new MySqlParameter("CourseCode", param.CourseCode));
                    command.Parameters.Add(new MySqlParameter("DepartmentCode", param.DepartmentCode));
                    command.Parameters.Add(new MySqlParameter("SemesterCode", param.SemesterCode));
                    command.Parameters.Add(new MySqlParameter("ApplicationType", param.ApplicationType));
                    command.Parameters.Add(new MySqlParameter("CollegeCode", param.CollegeCode[0]));
                    command.Parameters.Add(new MySqlParameter("YearOfAdmission", param.StartYear));
                    command.Parameters.Add(new MySqlParameter("State", param.State));
                    command.Parameters.Add(new MySqlParameter("NumberofRows", param.NumberofRows));
                    command.Parameters.Add(new MySqlParameter("PageNumber", param.PageNumber));
                    break;
                case (long)enServiceType.NursingApplicationForm:

                    command.CommandText = "SP_NURSING_CNR";
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new MySqlParameter("CourseTypeCode", param.CourseTypeCode));
                    command.Parameters.Add(new MySqlParameter("StreamCode", param.StreamCode));
                    command.Parameters.Add(new MySqlParameter("SubjectCode", param.SubjectCode));
                    command.Parameters.Add(new MySqlParameter("CourseCode", param.CourseCode));
                    command.Parameters.Add(new MySqlParameter("DepartmentCode", param.DepartmentCode));
                    command.Parameters.Add(new MySqlParameter("CurrentAcademicYear", param.CurrentAcademicYear));
                    command.Parameters.Add(new MySqlParameter("ApplicationType", param.ApplicationType));
                    command.Parameters.Add(new MySqlParameter("CollegeCode", param.CollegeCode[0]));
                    command.Parameters.Add(new MySqlParameter("YearOfAdmission", param.StartYear));
                    command.Parameters.Add(new MySqlParameter("State", param.State));
                    command.Parameters.Add(new MySqlParameter("NumberofRows", param.NumberofRows));
                    command.Parameters.Add(new MySqlParameter("PageNumber", param.PageNumber));
                    break;
            }
            
        }

        public IQueryable<Users> GetAllUsersData()
        {
            var x = DbSet.Include(a => a.UserRoles).Include(a => a.UserRoleGeographyMappers);
            //var x = from usr in DbSet
            //        join rl in DbContext.Set<UserRoles>() on usr.Id equals rl.UserId
            //        join usrgm in DbContext.Set<UserRoleGeographyMapper>() on usr.Id equals usrgm.UserId
            //        where usr.IsActive == true && usr.TenantId == rl.TenantId && usrgm.TenantId == rl.TenantId && usrgm.RoleId == rl.RoleId
            //        select new Users
            //        {
            //            Id = usr.Id,
            //            UserName = usr.UserName,
            //            EmailAddress = usr.EmailAddress,
            //            Name = usr.Name,
            //            NormalizedEmailAddress = usr.NormalizedEmailAddress,
            //            NormalizedUserName = usr.NormalizedUserName,
            //            Password = usr.Password,
            //            Salt = usr.Salt,
            //            PhoneNumber = usr.PhoneNumber,
            //            Surname = usr.Surname,
            //            UserAddressDetails = usr.UserAddressDetails,
            //            UserDetails = usr.UserDetails,
            //            ObjectId = usr.ObjectId,
            //            TenantId = usr.TenantId,
            //            UserRoles = usr.UserRoles,
            //            UserRoleGeographyMappers = usr.UserRoleGeographyMappers
            //        };

            return x;

            //return DbSet.Where(farmers => farmers.IsActive == true);
        }

        public IQueryable<Users> GetAllUsersWithAddress()
        {
            return DbSet.Include(a => a.UserRoles).Include(stu=>stu.StudentProfiles).Include(a => a.UserRoleGeographyMappers).Include(a => a.UserAddresses);
        }

    }
}
