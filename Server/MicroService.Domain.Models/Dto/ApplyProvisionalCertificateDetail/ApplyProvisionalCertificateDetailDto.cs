﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ApplyProvisionalCertificateDetail
{
        public class ProvisionalCertificateApplicationDetailDto : AuditedEntity<Guid>
    {
            public string CandidateName { get; set; }
            public string RollNo { get; set; }
            public string UniversityRegdNo { get; set; }
            public string CourseType { get; set; }
            public string StreamCode { get; set; }
            public string DepartmentCode { get; set; }
            public string SubjectCode { get; set; }
            public string ExamCode { get; set; }
            public int ExamYear { get; set; }
            public int ExamMonth { get; set; }
            public bool IsCompartmental { get; set; }
            public string BackExamRollNo { get; set; }
            public string BackExamCode { get; set; }
            public int BackExamYear { get; set; }
            public int BackExamMonth { get; set; }
            public string Subjects { get; set; }
            public string CollegeCode { get; set; }
            public string DivisionCode { get; set; }
            public string Distinction { get; set; }
            public string CourseCode { get; set; }
            public string AcknowledgementNo { get; set; }        
    }
}
