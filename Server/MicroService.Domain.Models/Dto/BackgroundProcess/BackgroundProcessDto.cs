﻿using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.BackgroundProcess
{
    public class BackgroundProcessDto
    {
        public string InstanceTrigger { get; set; }
        public string SelectedInstanceIds { get; set; }
        public string CourseTypeCode { get; set; }
        public List<string> CollegeCode { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }        
        public string State { get; set; }
        public string SemesterCode { get; set; }
        public bool IsAllSelected { get; set; }
        public string DepartmentCode { get; set; }
        public string CourseCode { get; set; }
        public string ApplicationType { get; set; }
        public string PageNumber { get; set; }
        public string NumberofRows { get; set; }
        public int StartYear { get; set; }
        public List<CollegeWiseAmountForCoeDto> CollegeWiseAmount { get; set; }
        public string CurrentAcademicYear { get; set; }
        public long ServiceId { get; set; }
    }
}
