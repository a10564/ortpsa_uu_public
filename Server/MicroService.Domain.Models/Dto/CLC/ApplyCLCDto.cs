﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.CLC
{
    public class ApplyCLCDto : WorkflowAuditedEntity<Guid>
    {
        [RADFormControl(FormControlIgnore = true)]
        public new Guid Id { get; set; }
        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Course Type")]
        public string CourseTypeCode { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(40, ErrorMessage = "Student Name cannot be greater than 40 characters.")]
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string CollegeCode { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CourseCode { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentCode { get; set; }
        public int YearOfAdmission { get; set; }
        public int YearOfPassing { get; set; }
        public string Result { get; set; }
        public string ReasonForTransfer { get; set; }
        public string StudentType { get; set; }
        public string ClcApplicationType { get; set; }
        public string ReasonForApply { get; set; }
        public bool IsSameAsPresent { get; set; }
        public string AcknowledgementNo { get; set; }
        public string CLCNumber { get; set; }

        [ForeignKey("ParentSourceId")]
        public List<Documents> ClcDocument { get; set; }
        [ForeignKey("InstanceId")]       
		public List<ServiceStudentAddressDto> clcFormFillUpStudentAddresses { get; set; }
		//public List<ExaminationFormFillUpStudentAddressDto> clcFormFillUpStudentAddresses { get; set; }
	}
}