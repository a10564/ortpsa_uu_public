﻿using MicroService.Core.WorkflowEntity;
using System;

namespace MicroService.Domain.Models.Dto.DuplicateRegistrationDetail
{
    public class DuplicateRegistrationDetailDto: WorkflowAuditedEntity<Guid>
    {
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string CourseCode { get; set; }
        public string RegistrationNo { get; set; }
        public string AcknowledgementNo { get; set; }
    }
}
