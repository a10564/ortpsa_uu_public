﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class CollegeAndAllocatedCenterNames
    {
        public string CollegeCode { get; set; }
        public string CollegeString { get; set; }
        public string CenterCode { get; set; }
        public string CenterName { get; set; }
        [NotMapped]
        public long Id { get; set; }
    }
}
