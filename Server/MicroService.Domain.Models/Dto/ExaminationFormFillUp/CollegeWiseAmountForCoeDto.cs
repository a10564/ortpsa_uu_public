﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class CollegeWiseAmountForCoeDto
    {
        public string CollegeCode { get; set; }
        public string CollegeName { get; set; }
        public long RecordCount { get; set; }
        public Decimal FormFillUpAmount { get; set; }
        public bool IsSelected { get; set; }   }
}
