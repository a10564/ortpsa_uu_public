﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class ExamFormFillupDatesDto:AuditedEntity<Guid>
    {
        //public Guid ExaminationMasterId { get; set; }
        public int SlotOrder { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
       // public Guid ExaminationSessionId { get; set; }
    }
}
