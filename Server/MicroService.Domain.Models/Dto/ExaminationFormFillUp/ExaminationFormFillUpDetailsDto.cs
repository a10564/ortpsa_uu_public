﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    [BelongsToWorkflow("Examination Form Fillup Workflow")]
    [UIConfigurable]
    public class ExaminationFormFillUpDetailsDto : WorkflowAuditedEntity<Guid>
    {
        [RADFormControl(FormControlIgnore = true)]
        public new Guid Id { get; set; }
        public string CourseTypeCode { get; set; }
        [MaxLength(16, ErrorMessage = "Registration Number cannot be greater than 16 characters.")]
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }

        //student info
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(200, ErrorMessage = "Student Name cannot be greater than 200 characters.")]
        public string StudentName { get; set; }
        [Required(ErrorMessage = "FatherName field is required")]
        [MaxLength(200, ErrorMessage = "Father Name cannot be greater than 200 characters.")]
        public string FatherName { get; set; }
        [Required(ErrorMessage = "Guardian Name field is required")]
        [MaxLength(200, ErrorMessage = "Guardian Name cannot be greater than 200 characters.")]
        public string GuardianName { get; set; }
        public string Nationality { get; set; }
        public string Caste { get; set; }
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsSpeciallyAbled { get; set; }
        public bool IsSameAsPresent { get; set; }

        //last education info
        public int YearOfMatriculation { get; set; }       
        public int YearOfPreviousExamPassed { get; set; }
        [Required(ErrorMessage = "Previous Academic Subject field is required")]
        [MaxLength(200, ErrorMessage = "Previous Academic Subject cannot be greater than 200 characters.")]
        public string PreviousAcademicSubject { get; set; }

        
        public string CollegeCode { get; set; }
        public string CourseCode { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public string SemesterCode { get; set; }
        public string AcademicSession { get; set; }
        public int AcademicStart { get; set; }
        

        public string ApplicationType { get; set; }
        public string AcknowledgementNo { get; set; }

        public Decimal FormFillUpMoneyToBePaid { get; set; }
        public Decimal InternetHandlingCharges { get; set; }
        public Decimal LateFeeAmount { get; set; }

        public List<Documents> ImageDocument { get; set; }
        public List<ServiceStudentAddressDto> ServiceStudentAddresses { get; set; }
        public List<ExaminationPaperDetailsDto> ExaminationPaperDetails { get; set; }


        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Principal/HOD Remarks")]
        public string PrincipalHODRemarks { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }

    }
}
