﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class ExaminationPaperDetailsDto : AuditedEntity<Guid>
    {
        [NotMapped]
        public Guid ExaminationFormFillUpDetailsId { get; set; }
        public Guid ExaminationPaperId { get; set; }
    }
}
