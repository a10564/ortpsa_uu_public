﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class ExaminationPaperWiseMarkDto: AuditedEntity<Guid>
    {
        //public Guid Id { get; set; }
        public string RollNumber { get; set; }
        public string RegistrationNumber { get; set; }
        public string CourseTypeCode { get; set; }
        public string CourseTypeCodeString { get; set; }
        public string CollegeCode { get; set; }
        public string CollegeCodeString  { get; set; }
        public string StreamCode { get; set; }
        public string StreamCodeString { get; set; } 
        public string SubjectCode { get; set; }
        public string SubjectCodeString { get; set; }
        public string SemesterCode { get; set; }
        public string SemesterCodeString { get; set; }
        public int AcademicStart { get; set; }
        public string AcknowledgementNo { get; set; }
        public string State { get; set; }
        public DateTime? PaymentDate { get; set; }
        public Guid ExaminationPaperId { get; set; }
        public long? InternalMarkSecured { get; set; }
        public long? ExternalMarkSecured { get; set; }
        public long? PracticalMarkSecured { get; set; }
        public bool IsPratical { get; set; } 
        public Guid ExaminationMasterId { get; set; }
        public string PaperName { get; set; }
        public string PaperAbbreviation { get; set; }
        public string Category { get; set; }
        public string CategoryAbbreviation { get; set; }
        public long InternalMark { get; set; }
        public long ExternalMark { get; set; }
        public long PracticalMark { get; set; }
        public bool IsExternalAttained { get; set; }
    }
}
