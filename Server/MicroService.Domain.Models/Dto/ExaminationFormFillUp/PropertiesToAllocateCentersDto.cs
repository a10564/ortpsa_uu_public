﻿using MicroService.Domain.Models.Dto.AdminActivity;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
   public class PropertiesToAllocateCentersDto
    {
        public ExamDateFilterPropertiesDto FilterProperties { get; set; }
        public List<CollegeAndAllocatedCenterNames> CollegeAndCenters { get; set; }
    }
}
