﻿using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class SemesterAndStudentDetailsDto 
    { 
        public string Name { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string AcknowledgementNo { get; set; }

        public string State { get; set; }
        public string CourseType{ get; set; }
        public string CourseTypeCode { get; set; }
        public string College { get; set; }
        public string CollegeCode { get; set; }
        public string Stream { get; set; }
        public string StreamCode { get; set; }
        public string Subject { get; set; }
        public string SubjectCode { get; set; }
        public string Semester { get; set; }
        public string SemesterCode { get; set; }
        public string YearOfAddmission { get; set; }

        public List<ExaminationPaperMaster> ExaminationPaperMasters = new List<ExaminationPaperMaster>();
    }
}
