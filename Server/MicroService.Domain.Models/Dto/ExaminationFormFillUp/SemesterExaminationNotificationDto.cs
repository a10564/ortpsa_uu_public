﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class SemesterExaminationNotificationDto
    {
        public string Semester { get; set; }
        public string NotificationText { get; set; }
        public Guid ExaminationMasterId { get; set; }
        public Guid ExaminationSessionId { get; set; }
    }
}
