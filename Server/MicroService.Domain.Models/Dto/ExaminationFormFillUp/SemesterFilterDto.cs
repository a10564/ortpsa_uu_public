﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.ExaminationFormFillUp
{
    public class SemesterFilterDto
    {
        public string State { get; set; }
        public string CourseTypeCodeString { get; set; }
        public string CourseTypeCode { get; set; }
        public string CollegeString { get; set; }
        public string CollegeCode { get; set; }
        public string StreamString { get; set; }
        public string StreamCode { get; set; }
        public string SubjectString { get; set; }
        public string SubjectCode { get; set; }
        public string SemesterString { get; set; }
        public string SemesterCode { get; set; }
        public string DepartmentCodeString { get; set; }
        public string DepartmentCode { get; set; }
        public string CourseCodeString { get; set; }
        public string CourseCode { get; set; }
        public int YearOfAddmission { get; set; }
    }
}
