﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class ApiDetailsDto : AuditedEntity<long>
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
