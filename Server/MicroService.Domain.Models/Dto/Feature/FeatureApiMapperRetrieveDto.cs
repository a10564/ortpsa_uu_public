﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.Feature
{
    public class FeatureApiMapperRetrieveDto: AuditedEntity<long>
    {
        public long FeatureId { get; set; }
        public long ApiDetailId { get; set; }
        public bool IsMandatory { get; set; }
        public string Path { get; set; }
        [NotMapped]
        public List<ApiDetailRetrieveDto> ApiDetails { get; set; }
        public string ApiName { get; set; }
        //public string Icon { get; set; }
    }
}
