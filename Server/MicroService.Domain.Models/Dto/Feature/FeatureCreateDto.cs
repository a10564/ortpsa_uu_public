﻿using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Feature
{
    public class FeatureCreateDto : AuditedEntity<long>
    {
        public long FeatureOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
        public int? Sequence { get; set; }
        //public List<FeatureApiMapper> FeatureApiMappers { get; set; } // To remove FeatureCreateDtoId unknown col error
    }
}
