﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.Feature
{
    public class FeatureOrderCreateDto : AuditedEntity<long>
    {
        public long Order { get; set; }
        public string Name { get; set; }
        [ForeignKey("FeatureOrderId")]
        public List<FeatureCreateDto> FeatureDetails { get; set; }
    }
}
