﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.Feature
{
    public class FeatureOrderDto : AuditedEntity<long>
    {
        public long Order { get; set; }
        public string Name { get; set; }
        [ForeignKey("FeatureOrderId")]
        public List<FeatureDto> FeatureDetails { get; set; }
    }
}
