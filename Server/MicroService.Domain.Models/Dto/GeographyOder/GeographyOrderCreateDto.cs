﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.GeographyOder
{
    public class GeographyOrderDto : AuditedEntity<long>
    {
        public long Order { get; set; }
        public string Name { get; set; }
        public List<GeographyDetailDto> GeographyDetails { get; set; }
    }
    public class GeographyOrderCreateDto : AuditedEntity<long>
    {
        public long Order { get; set; }
        public string Name { get; set; }
        public List<GeographyDetailCreateDto> GeographyDetails { get; set; }
    }
}
