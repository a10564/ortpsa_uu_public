﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection.Emit;
using System.Text;

namespace MicroService.Domain.Models.Dto.MigrationService
{
    [BelongsToWorkflow("Migration Certificate Workflow")]
    [UIConfigurable]
    public class UniversityMigrationDetailDto : WorkflowAuditedEntity<Guid>
    {
        [RADFormControl(FormControlIgnore = true)]
        public new Guid Id { get; set; }
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(40, ErrorMessage = "Student Name cannot be greater than 40 characters.")]
        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Course Type")]
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public string CourseCode { get; set; }
        public DateTime YearOfAdmission { get; set; }
        public string RegistrationNo { get; set; }
        public string ReasonForMigrationCode { get; set; }
        public string RollNo { get; set; }
        public DateTime YearOfLeaving { get; set; }
        public string StudyDetail { get; set; }
        public string AcknowledgementNo { get; set; }
        public List<Documents> MigrationDocument { get; set; }

        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Principal/HOD Remarks")]
        [MaxLength(500, ErrorMessage = "Principal Remarks cannot be greater than 500 characters.")]
        public string PrincipalRemarks { get; set; }

        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Computer Cell Remarks")]
        [MaxLength(500, ErrorMessage = "Computer Cell Remarks cannot be greater than 500 characters.")]
        public string ComputerCellRemarks { get; set; }

        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Assistant COE Remarks")]
        [MaxLength(500, ErrorMessage = "Assistant COE cannot be greater than 500 characters.")]
        public string AssistantCOERemarks { get; set; }
        public DateTime? PrincipalLastUpdateTime { get; set; }
        public string serviceId { get; set; }
        public string MigrationNumber { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }

    }
}
