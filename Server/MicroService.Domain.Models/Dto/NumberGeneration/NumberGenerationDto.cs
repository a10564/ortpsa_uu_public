﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.NumberGeneration
{
   public class NumberGenerationDto
    {
        public string YearOfAdmission { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string CourseType { get; set; }
        public string CollegeCode { get; set; }
        public int ProcessType { get; set; }
        public string CourseCode { get; set; }

    }  
}
