﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Nursing
{
    public class NursingAcademicStatementDto : AuditedEntity<Guid>
    {
        public string StudentName { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string AcknowledgementNo { get; set; }
        public string CollegeCode { get; set; }
        public string StreamCode { get; set; }
        public string CourseTypeCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public string CollegeName { get; set; }
        public string SubjectName { get; set; }
        public string CurrentAcademicYear { get; set; }
        public string State { get; set; }
        public string CourseCode { get; set; }
        public int AcademicStart { get; set; }
        public string AcademicSession { get; set; }
        public decimal FormFillUpMoneyToBePaid { get; set; }
        public decimal InternetHandlingCharges { get; set; }
        public decimal CenterCharge { get; set; }
        public decimal LateFeeAmount { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
