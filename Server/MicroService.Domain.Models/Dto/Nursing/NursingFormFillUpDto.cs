﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Dto.ExaminationFormFillUp;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.Nursing
{
    [BelongsToWorkflow("Nursing Form Fillup Workflow")]
    [UIConfigurable]
    public class NursingFormFillUpDto : WorkflowAuditedEntity<Guid>
    {
        [RADFormControl(FormControlIgnore = true)]
        public new Guid Id { get; set; }
        public string CourseTypeCode { get; set; }
        public string CourseTypeCodeString { get; set; } 
        [MaxLength(16, ErrorMessage = "Registration Number cannot be greater than 16 characters.")]
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }

        //student info
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(200, ErrorMessage = "Student Name cannot be greater than 200 characters.")]
        public string StudentName { get; set; }
        //[Required(ErrorMessage = "FatherName field is required")]
        //[MaxLength(200, ErrorMessage = "Father Name cannot be greater than 200 characters.")]
        //public string FatherName { get; set; }
        [Required(ErrorMessage = "Nationality field is required")]
        public string Nationality { get; set; }
        [Required(ErrorMessage = "Nationality field is required")]
        public string NationalityString { get; set; } 

        public string Caste { get; set; }
        public string CasteString { get; set; }
        [Required(ErrorMessage = "Gender field is required")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "Gender field is required")]
        public string GenderString { get; set; }
        [Required(ErrorMessage = "DateOfBirth field is required")]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage = "IsPwd field is required")]
        public bool IsPwd { get; set; }
        [Required(ErrorMessage = "IsPwd field is required")]
        public string IsPwdString { get; set; }
        //adress
        [Required(ErrorMessage = "IsSameAsPresent field is required")]
        public bool IsSameAsPresent { get; set; }
        [Required(ErrorMessage = "Present Address field is required")]
        public string PresentAddress { get; set; }
        [Required(ErrorMessage = "Present Country field is required")]
        public string PresentCountry { get; set; } 
        [Required(ErrorMessage = "Present Country field is required")]
        public string PresentCountryCode { get; set; }
        [Required(ErrorMessage = "Present State field is required")]
        public string PresentState { get; set; }
        [Required(ErrorMessage = "Present State field is required")]
        public string PresentStateCode { get; set; } 
        [Required(ErrorMessage = "Present City field is required")]
        public string PresentCity { get; set; }
        [Required(ErrorMessage = "Present City field is required")]
        public string PresentCityCode { get; set; }
        [Required(ErrorMessage = "Present Zip field is required")]
        public string PresentZip { get; set; }
        [Required(ErrorMessage = "Permanent Address field is required")]
        public string PermanentAddress { get; set; }
        [Required(ErrorMessage = "Permanent Country field is required")]
        public string PermanentCountry { get; set; }
        [Required(ErrorMessage = "Permanent Country field is required")]
        public string PermanentCountryCode { get; set; }
        [Required(ErrorMessage = "Permanentt State field is required")]
        public string PermanentState { get; set; } 
        [Required(ErrorMessage = "Permanentt State field is required")]
        public string PermanentStateCode { get; set; }
        [Required(ErrorMessage = "Permanent City field is required")]
        public string PermanentCity { get; set; }
        [Required(ErrorMessage = "Permanent City field is required")]
        public string PermanentCityCode { get; set; } 
        [Required(ErrorMessage = "Permanent Zip field is required")]
        public string PermanentZip { get; set; }

        public int? YearOfMatriculation { get; set; }       
        public int YearOfPassingPreMedical { get; set; }
        //filter
        public string CollegeCode { get; set; }
        public string CollegeCodeString { get; set; } 
        public string CourseCode { get; set; }
        public string CourseCodeString { get; set; }
        public string StreamCode { get; set; }
        public string StreamCodeString { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentCodeString { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectCodeString { get; set; }
        //public string SemesterCode { get; set; }
        //public string SemesterCodeString { get; set; }
        //public int  { get; set; }
        //public int CurrentAcademicYearString { get; set; } 
        public string CurrentAcademicYear { get; set; } 
        public int AcademicStart { get; set; }
        public string CurrentAcademicYearString { get; set; }
        public string ApplicationType { get; set; }
        public string AcknowledgementNo { get; set; }

        public Decimal FormFillUpMoneyToBePaid { get; set; }
        public Decimal InternetHandlingCharges { get; set; }
        public Decimal LateFeeAmount { get; set; }
        public Decimal CenterCharges { get; set; }
        public List<Documents> ImageDocument { get; set; }

        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
          ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Principal/HOD Remarks")]
        public string PrincipalHODRemarks { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool IsGnmQualified { get; set; }
        public bool IsHscQualified { get; set; }
        public decimal HscPercentage { get; set; }
        public List<ExaminationPaperDetailsDto> ExaminationPaperDetails { get; set; }
    }
}
