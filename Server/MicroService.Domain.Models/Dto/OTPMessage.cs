﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class OTPMessage
    {
        public string Message { get; set; }
        public string MobileNumber { get; set; }
    }
}
