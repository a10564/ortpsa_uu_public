﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Entity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Payment
{
   
    public class PaymentPricingInfoDto : AuditedEntity<Guid>
    {
        
        public new Guid Id { get; set; }


        public string CourseTypeCode { get; set; }
        public string RegistrationNumber { get; set; }
      
        public string StudentName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public string ServiceType { get; set; }


		public decimal ExaminationFees { get; set; }
		public decimal CentreCharges { get; set; }
		public decimal EnrollmentFees { get; set; }
		public decimal FeesForSupervision { get; set; }
		public decimal FeesForMarks { get; set; }
		public decimal ExamRegistrationFee { get; set; }		
		public decimal LateFee { get; set; }
		public decimal AdditionalCentreCharges { get; set; }
		public decimal ProvisionalCertificateFee { get; set; }
		public decimal OriginalCertificateFee { get; set; }
		public decimal ExaminationFeePerPaper { get; set; }

		public decimal ServiceCharge { get; set; }
		public decimal TotalFee { get; set; }
		public decimal TotalAmountToBePaid { get; set; }
		public string PaymentStatus { get; set; }
		public string ErrorMessage { get; set; }

		public List<ServicePaymentDto> ServicePayments { get; set; }
		public List<ServicePaymentDto> ConveniencePayments { get; set; }
		public bool IsPayAmount { get; set; }

    }
}


