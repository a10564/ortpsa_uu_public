﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.QRCode
{
    public class QRCodeValidateResponseDto
    {
        public string ApplicantName { get; set; }
        public string CertificateNumber { get; set; }
        public bool IsValid { get; set; }
    }
}
