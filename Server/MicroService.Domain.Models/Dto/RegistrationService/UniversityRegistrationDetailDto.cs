﻿using MicroService.Core.WorkflowAttribute;
using MicroService.Core.WorkflowCommon;
using MicroService.Core.WorkflowEntity;
using MicroService.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.StudentDashboard
{
    [BelongsToWorkflow("Registration Number Workflow")]
    [UIConfigurable]
    public class UniversityRegistrationDetailDto : WorkflowAuditedEntity<Guid>
    {
        [RADFormControl(FormControlIgnore = true)]
        public new Guid Id { get; set; }
        [Required(ErrorMessage = "Student Name field is required")]
        [MaxLength(40, ErrorMessage = "Student Name cannot be greater than 40 characters.")]
        public string StudentName { get; set; }
        [Required(ErrorMessage = "Father Name field is required")]
        [MaxLength(40, ErrorMessage = "Father Name cannot be greater than 40 characters.")]
        public string FatherName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Course Type")]
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string RegistrationNo { get; set; }
        public List<Documents> RegistrationDocument { get; set; }
        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Principal/HOD Remarks")]
        [MaxLength(500, ErrorMessage = "Principal Remarks cannot be greater than 500 characters.")]
        public string PrincipalRemarks { get; set; }
        //[RADFormControl(ControlType = EnRADFormControlType.TextArea,
        //   ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Registrar Remarks")]
        //[MaxLength(500, ErrorMessage = "Registrar Remarks cannot be greater than 500 characters.")]
        //public string RegistrarRemarks { get; set; }
        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Computer Cell Remarks")]
        [MaxLength(500, ErrorMessage = "Computer Cell Remarks cannot be greater than 500 characters.")]
        public string ComputerCellRemarks { get; set; }
        [RADFormControl(ControlType = EnRADFormControlType.TextArea,
           ConfigType = EnRADConfigType.WorkflowConfigurable, Label = "Assistant COE Remarks")]
        [MaxLength(500, ErrorMessage = "Assistant COE cannot be greater than 500 characters.")]
        public string AssistantCOERemarks { get; set; }
        public string AcknowledgementNo { get; set; }
        public bool IsRegistrationNumberGenerated { get; set; }
        public DateTime? PrincipalLastUpdateTime { get; set; }
        public string CourseCode { get; set; }
        public string serviceId { get; set; }
        public string CSCPayid { get; set; }
        public DateTime? PaymentDate { get; set; }
    }
}
