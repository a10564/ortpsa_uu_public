﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.Role
{
    public class RoleFeatureApiMapperCreateDto:AuditedEntity<long>
    {
        public long RoleId { get; set; }
        public long FeatureId { get; set; }
        public long ApiDetailId { get; set; }
        //public string ApiName { get; set; }
    }
}
