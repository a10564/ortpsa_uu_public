﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class ServiceStudentAddressDto : AuditedEntity<Guid>
    {
        [NotMapped]
        public long ServiceType { get; set; }
        [NotMapped]
        public Guid InstanceId { get; set; }
        [Required(ErrorMessage = "Address field is required")]
        [MaxLength(500, ErrorMessage = "Address cannot be greater than 500 characters.")]
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string AddressType { get; set; }
    }
}
