﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.StudentDashboard
{
    public class EmailTemplateDto
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public long TemplateType { get; set; }
        public bool IsActive { get; set; }
        public long ServiceType { get; set; }
    }

    public class PDFDownloadFile
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public byte[] PdfData { get; set; }
    }
}
