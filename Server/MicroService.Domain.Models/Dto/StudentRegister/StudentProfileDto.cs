﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Dto.StudentRegister
{
    public class StudentProfileDto : AuditedEntity<Guid>
    {
        public long UserId { get; set; }
        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public int YearOfAdmission { get; set; }
        public string CourseCode { get; set; }
        public long ServiceId { get; set; }
    }
}
