﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto
{
    public class CreateTenantDto
    {
        public const string TenancyNameRegex = "^[a-zA-Z][a-zA-Z0-9_-]{1,}$";
        public const int MaxNameLength = 128;
        public const int MaxTenancyNameLength = 64;
        public const int MaxEmailAddressLength = 150;
        public const int MaxConnectionStringLength = 1024;

        [Required]
        [StringLength(MaxTenancyNameLength)]
        [RegularExpression(TenancyNameRegex)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(MaxEmailAddressLength)]
        public string AdminEmailAddress { get; set; }

        [StringLength(MaxConnectionStringLength)]
        public string ConnectionString { get; set; }

        public bool IsActive { get; set; }
    }
}
