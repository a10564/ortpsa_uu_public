﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.Tenants
{
    public class TenantDto
    {
        public const int MaxTenancyNameLength = 64;
        public const string TenancyNameRegex = "^[a-zA-Z][a-zA-Z0-9_-]{1,}$";
        public const int MaxNameLength = 128;
        [Required]
        [StringLength(MaxTenancyNameLength)]
        [RegularExpression(TenancyNameRegex)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(MaxNameLength)]
        public string Name { get; set; }
        [Key]
        public long Id { get; set; }
        //
        // Summary:
        //     Gets or sets the ID of the user who created this entity.
        public long CreatorUserId { get; set; }
        //
        // Summary:
        //     Gets or sets the date in which this entity was created.
        public DateTime CreationTime { get; set; }
        //
        // Summary:
        //     Gets or sets the ID of the user who last updated this entity.
        public long? LastModifierUserId { get; set; }
        //
        // Summary:
        //     Gets or sets the date in which this entity was last updated.
        public DateTime? LastModificationTime { get; set; }
        //
        // Summary:
        //     Gets or sets a value indicating if this record is active or not.
        public bool IsActive { get; set; }
    }
}
