﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MicroService.Domain.Models.Dto.User
{
    public class UserAddressDto : AuditedEntity<long>
    {
        public long UserId { get; set; }
        [Required(ErrorMessage = "Address field is required")]
        [MaxLength(500, ErrorMessage = "Address cannot be greater than 500 characters.")]
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}
