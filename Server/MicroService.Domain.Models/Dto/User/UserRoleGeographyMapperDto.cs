﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Dto.User
{
    public class UserRoleGeographyMapperDto: AuditedEntity<long>
    {
        public long UserId { get; set; }
        public long RoleId { get; set; }
        public long GeographyDetailsId { get; set; }
        public long GeographicalOrder { get; set; }
    }
}
