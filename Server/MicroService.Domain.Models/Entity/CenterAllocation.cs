﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class CenterAllocation:AuditedEntity<long>
    {
        public Guid ExaminationSessionId { get; set; }
        public string CollegeCode { get; set; }
        public string CenterCollegeCode { get; set; }
         
    }
}
