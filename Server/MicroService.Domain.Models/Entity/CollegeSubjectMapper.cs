﻿using MicroService.Domain.Models.Entity.Lookup;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class CollegeSubjectMapper : AuditedEntity<long>
    {
        public string CollegeCode { get; set; }
        public long SubjectMasterId { get; set; }
        public bool IsAffiliationCompleted { get; set; }

        public virtual SubjectMaster SubjectMaster { get; set; }
    }
}
