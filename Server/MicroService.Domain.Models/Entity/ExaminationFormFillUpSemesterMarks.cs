﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationFormFillUpSemesterMarks : AuditedEntity<Guid>
    {
        public string AcknowledgementNo { get; set; }
        public Guid ExaminationFormFillUpDetailsId { get; set; }
        public Guid ExaminationPaperId { get; set; }        
        public long? InternalMark { get; set; }
        public long? ExternalMark { get; set; }
        public bool IsPractical { get; set; }
        public long? PracticalMark { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        [NotMapped]
        public string CategoryAbbreviation { get; set; }
        [NotMapped]
        public string PaperAbbreviation { get; set; }
        public bool IsInternalAttained { get; set; }
        public bool IsExternalAttained { get; set; }
        public bool IsPracticalAttained { get; set; }

    }
}
