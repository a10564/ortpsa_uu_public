﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ExaminationPaperMaster : AuditedEntity<Guid>
    {
        public Guid ExaminationMasterId { get; set; }
        public string PaperName { get; set; }
        public string PaperAbbreviation { get; set; }
        public string Category { get; set; }
        public string CategoryAbbreviation { get; set; }
        public bool IsParctical { get; set; }
        [NotMapped]
        public bool IsSelected { get; set; }
        [NotMapped]
        public bool IsModified { get; set; }
        public long InternalMark { get; set; }
        public long ExternalMark { get; set; }
        public long PracticalMark { get; set; }
    }
}
