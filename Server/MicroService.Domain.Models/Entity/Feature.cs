﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class Feature : AuditedEntity<long>
    {
       // [ForeignKey("FeatureOrder")]
        public long FeatureOrderId { get; set; }
        public long ParentId { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }

        //Navigational properties
        //To keep the FeatureApiMapper mapping: with Feature Custom
        [ForeignKey("FeatureId")]
        public List<FeatureApiMapper> FeatureApiMappers { get; set; }
        [ForeignKey("ParentId")]
        public Feature ParentFeature { get; set; }
        public List<Feature> ChildFeatures { get; set; }
        [NotMapped]
        public List<ApiDetail> ApiList { get; set; }       
    }
}
