﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Lookup
{
    public class SubjectMaster : AuditedEntity<long>
    {
        [Column("SubjectName")]
        public string LookupDesc { get; set; }      // Name
        //public string FullDesc { get; set; }        // Description
        [Column("SubjectCode")]
        public string LookupCode { get; set; }      // Subject-code
        public long LookupSequence { get; set; }    // Sequence
        public string CourseTypeCode { get; set; }  // Lookup 'course-type' code
        public string StreamCode { get; set; }      // Lookup 'stream' code
        public string DepartmentCode { get; set; }  // 'Department-master' table lookup code
        public long SubjectDuration { get; set; }   // Academic Duration for the course
        public string CourseCode { get; set; }
        public bool IsFormFillUpAvailibility { get; set; }
        public long? IsSemesterOrYear { get; set; }
        public virtual ICollection<CollegeSubjectMapper> CollegeSubjectMapper { get; set; } = new List<CollegeSubjectMapper>();
    }
}
