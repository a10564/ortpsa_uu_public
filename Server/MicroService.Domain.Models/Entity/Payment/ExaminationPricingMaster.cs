﻿using MicroService.Core.WorkflowEntity;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
	[Table("examinationpricingmaster")]
    public class ExaminationPricingMaster : AuditedEntity<Guid>
	{
		public Guid ExaminationMasterId { get; set; }
		public decimal ExaminationFees { get; set; }
        public decimal CentreCharges { get; set; }
        public decimal EnrollmentFees { get; set; }
        public decimal FeesForSupervision { get; set; }
        public decimal FeesForMarks { get; set; }
        public decimal AdditionalCentreCharges { get; set; }
		public decimal ProvisionalCertificateFee { get; set; }
		public decimal OriginalCertificateFee { get; set; }
		public decimal ExaminationFeePerPaper { get; set; }
		public decimal ServiceCharge { get; set; }
		public decimal ExamRegistrationFee { get; set; }

		public string Cast { get; set; }
        public bool? IsSpeciallyAbled { get; set; }
        public string Gender { get; set; }


    }
}


	

