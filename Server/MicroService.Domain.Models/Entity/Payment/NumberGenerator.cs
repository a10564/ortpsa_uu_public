﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
   // [Table("numbergenerator")]
    public class NumberGenerator
    {
        public int Id { get; set; }
        public int NumberIncrementedBy { get; set; }
        public string StartNumberSeq { get; set; }
        public string PreFixWith { get; set; }
        public long LastGeneratedNumber { get; set; }
        public int ProcessType { get; set; }
        public string YearOfAdmission { get; set; }
        public string StreamCode { get; set; }
        public string CourseCode { get; set; }
        public string SubjectCode { get; set; }
        public string CollegeCode { get; set; }
        [NotMapped]
        public string CourseType { get; set; }
        public long ServiceId { get; set; }
    }
}
