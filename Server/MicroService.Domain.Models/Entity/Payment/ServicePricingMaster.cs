﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Payment
{
    [Table("servicepricingmaster")]
    public class ServicePricingMaster : AuditedEntity<long>
    {
        [ForeignKey("ServiceMaster")]
        public long? ServiceId { get; set; }
        public string FeeDescription { get; set; }
        public decimal? FeeAmount { get; set; }
		public bool IsConvenienceFee { get; set; }

		public virtual ServiceMaster ServiceMaster { get; set; }
    }
}
