﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class ProcessMaster :AuditedEntity<Guid>
    {
        public string ProcessName { get; set; }
        public string PocessType { get; set; }
    }
}
