﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Report
{
    public class GridModel
    {
        public ColumnModel[] Columns { get; set; }
        public GridRow[] Rows { get; set; }
    }
    public class ColumnModel
    {
        public string ColumnName { get; set; }
        public string ColumnLabel { get; set; }
        // other relevant column metadata for sorting, formatting, etc
    }
    public class GridRow
    {
        public string[] HiddenFields { get; set; }
        public string[] VisibleFields { get; set; }
    }
}
