﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    [Table("Roles")]
    public class Role: AuditedEntity<long>
    {
        public const int MaxDescriptionLength = 5000;

        [StringLength(MaxDescriptionLength)]
        public string Description { get; set; }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public bool IsStatic { get; set; }

        //To keep the role-feature-api mapping: with role Custom
        [ForeignKey("RoleId")]
        public List<RoleFeatureApiMapper> RoleFeatureApiMappers { get; set; }
        public new long TenantId { get; set; } // Hiding tenant id property from parent class to define foreign key relation between Tenant -> Role

    }
}
