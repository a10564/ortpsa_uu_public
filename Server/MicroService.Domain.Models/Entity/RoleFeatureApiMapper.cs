﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class RoleFeatureApiMapper : AuditedEntity<long>
    {
        //[ForeignKey("Role")]
        public long RoleId { get; set; }
        //[ForeignKey("Feature")]
        public long FeatureId { get; set; }
        //[ForeignKey("ApiDetail")]
        public long ApiDetailId { get; set; }
    }
}
