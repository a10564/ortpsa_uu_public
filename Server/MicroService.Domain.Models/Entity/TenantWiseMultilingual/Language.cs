﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class Language : AuditedEntity<long>
    {
        public string LanguageName { get; set; }
        public string LanguageCode { get; set; }
    }
}
