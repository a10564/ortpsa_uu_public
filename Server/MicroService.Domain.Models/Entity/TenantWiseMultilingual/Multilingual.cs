﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class Multilingual : AuditedEntity<long>
    {
        public string Key { get; set; }
        public string Label { get; set; }
        public string Value { get; set; }
        public long ModuleId { get; set; }
        public long LanguageId { get; set; }
    }
}
