﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class Tenants//:AuditedEntity<long>
    {
        public const string TenancyNameRegex = "^[a-zA-Z][a-zA-Z0-9_-]{1,}$";
        public const int MaxNameLength = 128;
        public const int MaxTenancyNameLength = 64;

        //public long EditionId { get; set; }

        [Required]
        [StringLength(MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(MaxTenancyNameLength)]
        [RegularExpression(TenancyNameRegex)]
        public string TenancyName { get; set; }


        //
        // Summary:
        //     Gets or sets the Primary key of the entity
        [Key]
        public long Id { get; set; }
        //
        // Summary:
        //     Gets or sets the ID of the user who created this entity.
        public long CreatorUserId { get; set; }
        //
        // Summary:
        //     Gets or sets the date in which this entity was created.
        public DateTime CreationTime { get; set; }
        //
        // Summary:
        //     Gets or sets the ID of the user who last updated this entity.
        public long? LastModifierUserId { get; set; }
        //
        // Summary:
        //     Gets or sets the date in which this entity was last updated.
        public DateTime? LastModificationTime { get; set; }
        //
        // Summary:
        //     Gets or sets a value indicating if this record is active or not.
        public bool IsActive { get; set; }
        [ForeignKey("TenantId")]
        public List<Role> TenantRoles { get; set; }
        [ForeignKey("TenantId")]
        public List<Users> TenantUsers { get; set; }
        
    }
}
