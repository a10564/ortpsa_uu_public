﻿using MicroService.SharedKernel;
using System.ComponentModel.DataAnnotations;

namespace MicroService.Domain.Models.Entity
{
    public class UserAddress:AuditedEntity<long>
    {
        public long UserId { get; set; }
        [MaxLength(500, ErrorMessage = "Address field character can't be more than 500")]
        public string Address { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
    }
}
