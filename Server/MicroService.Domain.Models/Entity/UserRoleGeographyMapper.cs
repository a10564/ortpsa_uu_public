﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UserRoleGeographyMapper : AuditedEntity<long>
    {
        //[ForeignKey("Users")]
        public long UserId { get; set; }
        //[ForeignKey("Role")]
        public long RoleId { get; set; }
        //[ForeignKey("GeographyDetail")]
        public long GeographyDetailsId { get; set; }
        public long GeographicalOrder { get; set; }
    }
}
