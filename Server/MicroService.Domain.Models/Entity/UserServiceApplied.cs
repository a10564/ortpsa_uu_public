﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class UserServiceApplied : AuditedEntity<long>
    {
        public long UserId { get; set; }
        public long ServiceId { get; set; }
        public bool IsAvailableToDL { get; set; }
        public string LastStatus { get; set; }
        public Guid InstanceId { get; set; }
        public bool IsActionRequired { get; set; }
    }
}
