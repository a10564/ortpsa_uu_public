﻿using MicroService.Domain.Models.Dto.User;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity
{
    public class Users : AuditedEntity<long>
    {
        public const string DefaultPassword = "bipros";

        public string Name { get; set; }
        public string Surname { get; set; }

        public string EmailAddress { get; set; }
        public string NormalizedEmailAddress { get; set; }

        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }

        public byte[] Password { get; set; }
        public byte[] Salt { get; set; }
        //PasswordResetCode
        public string PhoneNumber { get; set; }
        public string UserAddressDetails { get; set; }
        public string UserDetails { get; set; }
        public string ObjectId { get; set; }




        // Navigation property
        [ForeignKey("UserId")]
        public List<UserRoleGeographyMapper> UserRoleGeographyMappers { get; set; }
        [ForeignKey("UserId")]
        public List<UserRoles> UserRoles { get; set; }

        [NotMapped]
        public long[] RoleIds { get; set; }
        [NotMapped]
        public bool IsAdmin { get; set; }
        [NotMapped]
        public long[] RoleFeatureApiIds { get; set; }
        [NotMapped]
        public bool IsEntityUpdatable { get; set; }
        public new long TenantId { get; set; } // Hiding tenant id property from parent class to define foreign key relation between Tenant -> User
        [ForeignKey("UserId")]
        public List<UserAddress> UserAddresses { get;set;}
        public long UserType { get; set; }
        public string CollegeCode { get; set; }
        public string DepartmentCode { get; set; }
        
        [NotMapped]
        public string DeviceType { get; set; }
        [NotMapped]
        public bool IsAffiliated { get; set; }

        [ForeignKey("UserId")]
        public List<StudentProfile> StudentProfiles { get; set; }

        [NotMapped]
        public List<Documents> StudentDocuments { get; set; }

        //Remarks
        //SecurityStamp
        //State
        //TenantWorkflowId
        //AccessFailedCount
        //ApprovedBy
        //AuthenticationSource
        //ConcurrencyStamp       
        //EmailConfirmationCode              
        //IsEmailConfirmed
        //IsLockoutEnabled
        //IsPhoneNumberConfirmed
        //IsTwoFactorEnabled
        //LastLoginTime
        //LockoutEndDateUtc
        //DeletionTime
        //DeleterUserId
        //IsDeleted
    }
}
