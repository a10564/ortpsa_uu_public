﻿using MicroService.Core.WorkflowAttribute;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    //[AppTask("Email")]
    public class EmailTaskDetails : AuditedEntity<long> //, IAppTask
    {
        [ForeignKey("TaskConfiguration")]
        public long TaskConfigId { get; set; }
        [AppTaskParam]
        public string To { get; set; }

        public string From { get; set; }

        public string FromName { get; set; }
        [AppTaskParam]
        public string Cc { get; set; }
        [AppTaskParam]
        public string Subject { get; set; }

        public string Attachment { get; set; }
        public bool? HasAttachment { get; set; }
        public int MessageTemplateId { get; set; }
        public string BodyParameters { get; set; }
        public List<EmailAttachmentDetail> Attachments { get; set; }



        //public TaskConfig TaskConfiguration { get; set; }
        public EmailTaskDetails()
        {
            Attachments = new List<EmailAttachmentDetail>();
            //LastModificationTime = Clock.Now;
        }
    }

    public class EmailAttachmentDetail : AuditedEntity<long>
    {
        [ForeignKey("EmailTaskDetails")]
        public int EmailTaskDetailsId { get; set; }

        public string BodyParameters { get; set; }

        public int MessageTemplateId { get; set; }

        public string AttachmentType { get; set; }

        //public EmailAttachmentDetail()
        //{
        //    LastModificationTime = Clock.Now;
        //}

    }
}
