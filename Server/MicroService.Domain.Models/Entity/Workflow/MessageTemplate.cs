﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class MessageTemplate : AuditedEntity<long>
    {
        public MessageTemplate() { }
        //public int MessageTemplateId { get; private set; }
        public string TaskType { get; private set; }
        public int WorkflowId { get; private set; }
        public string TemplateName { get; private set; }
        public string Message { get; private set; }

        public static MessageTemplate CreateMessageTemplate(string type, int workflow, string name, string message)
        {
            return new MessageTemplate { TaskType = type, WorkflowId = workflow, TemplateName = name, Message = message };
        }
    }
}
