﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class Microservice : AuditedEntity<long>
    {
        public string MicroserviceName { get; set; }
        public string MicroserviceDescription { get; set; }
        public bool IsProcess { get; set; }
        public string MicroserviceUrl { get; set; }
        [ForeignKey("MicroserviceId")]
        public List<Workflow> Workflows { get; set; }
    }
}
