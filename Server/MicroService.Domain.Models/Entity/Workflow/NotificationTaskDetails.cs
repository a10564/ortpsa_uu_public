﻿using MicroService.Core.WorkflowAttribute;
using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    //[AppTask("Notification")]
    public class NotificationTaskDetails : AuditedEntity<long> //, IAppTask
    {
        [ForeignKey("TaskConfiguration")]
        public int TaskConfigId { get; set; }
        [AppTaskParam]
        public string NotificationTo { get; set; }
        public int MessageTemplateId { get; set; }
        public string BodyParameters { get; set; }
        //public TaskConfig TaskConfiguration { get; set; }
        //public NotificationTaskDetails()
        //{
        //    LastModificationTime = Clock.Now;
        //}
    }
}
