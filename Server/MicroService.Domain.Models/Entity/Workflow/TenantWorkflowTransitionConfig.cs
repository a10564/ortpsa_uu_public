﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class TenantWorkflowTransitionConfig : AuditedEntity<long>
    {
        public int WorkflowId { get; set; }
        public int TenantWorkflowId { get; set; } // fk TenantWorkflow
        public string SourceState { get; set; }
        public string Trigger { get; set; }
        public string TransitState { get; set; }
        public string TransitionRule { get; set; } // transition validation rule e.g (patient => patient.Age < 10)

        // This also needs to be added
        public string ApiUrl { get; set; } //e.g  api/Patients/{id}/Admission
        public string ApiUrlParams { get; set; } // {id : patient.id}
        public string ApiVerb { get; set; } // PUT/POST/PATCH/DELETE
        public string Roles { get; set; } // Roles who has access for this transition (comma separated values) -- we may have to maintain it in a different table  
        public string TriggerParameters { get; set; }
        public string UIConfig { get; set; }
        public bool IsEntityUpdatable { get; set; }
        //public List<TaskConfig> TenantWorkflowTransitionTasks { get; set; }
        public string TransitionRuleConfig { get; set; }
        public string PreviousTransitionTriggerParameters { get; set; }
        public bool TriggerLevelUpdateEntity { get; set; }
        public bool IsSubProcessTrigger { get; set; }
        public int? SubProcessId { get; set; }
        public bool AutoStateTransition { get; set; }
        public string TaskType { get; set; }
        public string ParallelActionRule { get; set; }

        public List<WorkflowTransitionEscalationRule> Escalations { get; set; }


    }
}
