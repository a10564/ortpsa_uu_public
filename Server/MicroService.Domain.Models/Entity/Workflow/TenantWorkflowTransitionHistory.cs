﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class TenantWorkflowTransitionHistory : AuditedEntity<long>
    {
        //public TenantWorkflowTransitionHistory()
        //{
        //    this.LastModificationTime = Clock.Now;
        //}

        public int TenantWorkflowId { get; set; }
        public int WorkflowInstanceId { get; set; }
        [NotMapped]
        public int SourceState { get; set; }
        public int Trigger { get; set; }
        public int TransitState { get; set; }
    }
}
