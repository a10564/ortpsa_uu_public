﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Entity.Workflow
{
    public class Workflow : AuditedEntity<long>
    {
        public string WorkflowName { get; set; }
       // [ForeignKey("Workflows")]
        public long MicroserviceId { get; set; }
        public string MicroserviceName { get; set; }
        public string EntityType { get; set; }
        public string DtoType { get; set; }
    }
}
