﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("admitcardcenterandexamnameview")]
    public class AdmitCardCenterAndExamNameView : AuditedEntity<Guid>
    {
        public string CourseCode { get; set; }
        public string CourseType { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SemesterCode { get; set; }
        public string StreamString { get; set; }
        public string SubjectString { get; set; }
        public int StartYear { get; set; }
        public int ExamYear { get; set; }
        public string CenterCollegeString { get; set; }
        public string CenterCollegeCode { get; set; }
        public string SemesterString { get; set; }
        public string CollegeCode { get; set; }
    }
}
