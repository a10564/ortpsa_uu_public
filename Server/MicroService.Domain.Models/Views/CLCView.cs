﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("collegeleavingcertificateview")]
    public class CLCView : AuditedEntity<Guid>
    {

        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string CourseTypeCode { get; set; }
        public string CourseTypeString { get; set; }
        public string CollegeCode { get; set; }
        public string CollegeString { get; set; }
        public string CourseCode { get; set; }
        public string CourseString { get; set; }
        public string StreamCode { get; set; }
        public string StreamString { get; set; }
        public string DepartmentCode { get; set; }
        public string DepartmentString { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectString { get; set; }
        public bool IsSameAsPresent { get; set; }
        public int YearOfAdmission { get; set; }
        public int YearOfPassing { get; set; }
        public string AcknowledgementNo { get; set; }
        public string Result { get; set; }
        public string ResultString { get; set; }
        public string ReasonForTransfer { get; set; }
        public string ReasonForTransferString { get; set; }
        public string StudentType { get; set; }
        public string StudentTypeString { get; set; }
        public string ClcApplicationType { get; set; }
        public string ClcApplicationTypeString { get; set; }
        public string ReasonForApply { get; set; }
        public string ReasonForApplyString { get; set; }
        public string State { get; set; }
        public string WorkflowState { get; set; }
        //Documents
        public string FileName { get; set; }
        //ExaminationFormFillUpStudentAddress
        public long ServiceTypeCode { get; set; }
        public string ServiceString { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string CountryString { get; set; }
        public string StateString { get; set; }
        public string City { get; set; }
        public string CityString { get; set; }
        public string ZipCode { get; set; }
        public string AddressType { get; set; }
        public string AddressTypeString { get; set; }
    }
}
