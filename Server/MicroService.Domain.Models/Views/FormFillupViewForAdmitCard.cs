﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("formfillupviewforadmitcard")]
    public class FormFillupViewForAdmitCard:AuditedEntity<Guid>
    {
        public string StudentName { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string CollegeName { get; set; }
        public string CourseTypeCode { get; set; }
        public string CollegeCode { get; set; }
        public string CourseCode { get; set; }
        public string DepartmentCode { get; set; }
        public string StreamCode { get; set; }
        public string SubjectCode { get; set; }
        public string SemesterCode { get; set; }
        public int AcademicStart { get; set; }
        public string AcademicSession { get; set; }
        public string ImagePath { get; set; }
        public string AcknowledgementNo { get; set; }
        public string State { get; set; }

    }
}
