﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("migrationview")]
    public class MigrationView : AuditedEntity<Guid>
    {
        public string StudentName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string CollegeCode { get; set; }
        public string StreamCode { get; set; }
        public string DepartmentCode { get; set; }
        public string SubjectCode { get; set; }
        public DateTime YearOfAdmission { get; set; }
        public string CourseType { get; set; }
        public string RegistrationNo { get; set; }
        public string StreamString { get; set; }
        public string SubjectString { get; set; }
        public string CollegeString { get; set; }
        public string CourseTypeString { get; set; }
        public string DepartmentString { get; set; }
        public string State { get; set; }
        public long TenantWorkflowId { get; set; }
        public string AcknowledgementNo { get; set; }
        public string CourseCode { get; set; }
        public string CourseString { get; set; }
        public string ReasonForMigrationCode { get; set; }
        public string RollNo { get; set; }
        public DateTime YearOfLeaving { get; set; }
        public string StudyDetail { get; set; }
        public string ReasonForMigrationString { get; set; }
        public string MigrationNumber { get; set; }
        public int AdmissionYearInt { get; set; }
    }
}
