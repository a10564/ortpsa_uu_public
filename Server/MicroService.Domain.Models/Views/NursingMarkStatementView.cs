﻿using MicroService.SharedKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MicroService.Domain.Models.Views
{
    [Table("nursing_mark_statement_view")]
    public class NursingMarkStatementView : AuditedEntity<Guid>
    {
        public string StudentName { get; set; }
        public string RegistrationNumber { get; set; }
        public string RollNumber { get; set; }
        public string AcknowledgementNo { get; set; }
        public string CourseTypeCode { get; set; }
        public string CourseTypeCodeString { get; set; }
        public string CollegeCode { get; set; }
        public string CollegeCodeString { get; set; }
        public string CourseCode { get; set; }
        public string DepartmentCode { get; set; }
        public string StreamCode { get; set; }
        public string StreamCodeString { get; set; }
        public string SubjectCode { get; set; }
        public string SubjectCodeString { get; set; }
        //public string SemesterCode { get; set; }
        //public string SemesterString { get; set; }
        public int AcademicStart { get; set; }
        public string CurrentAcademicYear { get; set; }
        public string CurrentAcademicYearString { get; set; }
        public string State { get; set; }
        public string MarkUploaded { get; set; }
        public string PaperWiseMarkDetails { get; set; }
    }
}
