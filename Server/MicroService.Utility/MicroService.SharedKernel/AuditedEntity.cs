﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MicroService.SharedKernel
{
    /// <summary>
    /// This entity contains the 'PrimaryKey', 'TenantId', 'IsActive' and all the audit columns.
    /// </summary>
    /// <typeparam name="TId">Datatype of PrimaryKey</typeparam>
    public abstract class AuditedEntity<TId>
    {
        /// <summary>
        /// Gets or sets the Primary key of the entity
        /// </summary>
        [Key]
        public TId Id { get; set; }
        /// <summary>
        /// Gets or sets the ID of the user who created this entity.
        /// </summary>
        public long CreatorUserId { get; set; }
        /// <summary>
        /// Gets or sets the date in which this entity was created.
        /// </summary>
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// Gets or sets the ID of the user who last updated this entity.
        /// </summary>
        public long? LastModifierUserId { get; set; }
        /// <summary>
        /// Gets or sets the date in which this entity was last updated.
        /// </summary>
        public DateTime? LastModificationTime { get; set; }
        /// <summary>
        /// Gets or sets a value indicating if this record is active or not.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Gets or sets the tenant id of the current entity
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// Generates new Guid type id if it is having a default value
        /// </summary>
        /// <param name="primaryKey">Primary key of Guid type</param>
        /// <returns></returns>
        public static Guid GenerateKey(Guid primaryKey)
        {
            if (Equals(primaryKey, default(Guid)))
            {
                return Guid.NewGuid();
            }
            return primaryKey;
        }
    }
}
