﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.SharedKernel
{
    /// <summary>
    /// This is used as the API return formate
    /// </summary>
    public class RequestResult
    {
        /// <summary>
        /// Default constructor only sets the flag of unauthorised request prop.to false
        /// </summary>
        public RequestResult()
        {
            UnAuthorizedRequest = false;
        }
        /// <summary>
        /// This is api responce message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// API error details are send through this property
        /// </summary>
        public ErrorDetail Error { get; set; }
        /// <summary>
        /// API data is send through Result
        /// It is a object type.You can send any type of data
        /// </summary>
        public Object Result { get; set; }
        /// <summary>
        /// This return true or false 
        /// true if API returns data successfully
        /// false if the API call failed
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// Authorize status of the current request. Default false
        /// </summary>
        public bool UnAuthorizedRequest { get; set; }
    }

    /// <summary>
    /// This is used in Error property of RequestResult
    /// </summary>
    public class ErrorDetail
    {
        /// <summary>
        /// this contains the error message
        /// </summary>
        public string Message { get; set; }
    }
}
