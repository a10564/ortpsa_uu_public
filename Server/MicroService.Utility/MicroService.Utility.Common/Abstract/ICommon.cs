﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Utility.Common.Abstract
{
    public interface ICommon
    {
        
        Boolean SendMail(string to, string subject, string message, List<Attachment> attach);
        Boolean SendMailWithAttachment(string to, string subject, string message);
        string ConvertHTMLToString(string source);
        string GetDateString(DateTime date);
        string GetImageSource(string url);
        string GetImageUrl(string url);
        bool SendEmail(string email, string subject, string message);
        int GenerateRandomNo();
        string GetCurrentFinancialYear();
        bool SendMail(string to, string subject, string message, string imagePath, string replacementParam);
        bool SendMail(string to, string subject, string message);
    }
}
