﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Common.Abstract
{
    public interface IExcelParserCommon
    {
        string getStringValueOfCell(Cell c, WorkbookPart workbookPart);
        bool detectFormulaInExcel(string path);
    }
}
