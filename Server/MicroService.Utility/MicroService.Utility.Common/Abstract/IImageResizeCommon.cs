﻿using MicroService.Utility.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Common.Abstract
{
    public interface IImageResizeCommon
    {
        byte[] ResizeImage(byte[] PassedImage, EnImageSize LargestSide, EnImageExtension extension);
    }
}
