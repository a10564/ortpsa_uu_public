﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Common.Abstract
{
    public interface IJWTCacheDataHandlerService
    {
        T GetCurrentJwtCacheData<T>(string key);
        IHeaderDictionary GetJwtRequestHeader();
    }
}
