﻿using MicroService.Utility.Common.Abstract;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace MicroService.Utility.Common
{
    public class JWTCacheDataHandlerService : IJWTCacheDataHandlerService
    {
        private readonly IHttpContextAccessor _context;

        public JWTCacheDataHandlerService(IHttpContextAccessor context) {
            _context = context;
        }

        public T GetCurrentJwtCacheData<T>(string key)
        {
            JwtSecurityToken jsonToken = GetJwtCache();
            T redisData = default(T);
            try
            {
                var claimUserId = jsonToken.Claims.First(claim => claim.Type == key).Value;
                redisData = (T)Convert.ChangeType(claimUserId, typeof(T));
            }
            catch (Exception)
            {
                redisData = default(T);
            }
            return redisData;
        }

        private JwtSecurityToken GetJwtCache()
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken jsonToken = new JwtSecurityToken();
            try
            {
                var authorization = _context.HttpContext.Request.Headers["Authorization"];
                if (!StringValues.IsNullOrEmpty(authorization))
                {
                    string authTokenKey = Convert.ToString(AuthenticationHeaderValue.Parse(authorization)).Replace("Bearer", "").Trim();
                    jsonToken = handler.ReadToken(authTokenKey) as JwtSecurityToken;
                }
            }
            catch (Exception)
            {
                throw new Exception("User data not found.");
            }
            return jsonToken;
        }

        public IHeaderDictionary GetJwtRequestHeader()
        {
            return _context.HttpContext.Request.Headers;
        }

    }
}
