﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Common
{
    public class ListOfDates
    {
        public List<DateTime> GetListLastDateOfMonth(int year, int month) {
            int monthDifference = month == 0 ? DateTime.Now.Month-1 : month;
            List<DateTime> listDates = new List<DateTime>();
            for (int singleMonth = 1; singleMonth <= monthDifference; singleMonth++) {
                listDates.Add(new DateTime(year, singleMonth, DateTime.DaysInMonth(year, singleMonth)));
            }
            return listDates;
        }

        public List<DateTime> GetListOfDatesOfCurrentMonth() {
            int day = DateTime.Now.Day;
            List<DateTime> listDates = new List<DateTime>();
            while (day != 0) {
                day--;
                listDates.Add(DateTime.Now.AddDays(-day).Date);
            }
            return listDates;
        }

        public List<DateTime> GetListOfDatesOfPreviousYearThisMonth()
        {
            int day = DateTime.Now.Day;
            List<DateTime> listDates = new List<DateTime>();
            while (day != 0)
            {
                day--;
                listDates.Add(DateTime.Now.AddYears(-1).AddDays(-day).Date);
            }
            return listDates;
        }

        public DateTime GetLastDateOfMonth(int year, int month)
        {
            DateTime listDates = (new DateTime(year, month, DateTime.DaysInMonth(year, month)));
            return listDates;
        }

        public List<DateTime> GetListOfDatesOfMonthYear(int year,int month)
        {
            DateTime lastDateOfMonth = GetLastDateOfMonth(year, month);
            int day = lastDateOfMonth.Day;
            List<DateTime> listDates = new List<DateTime>();
            while (day != 0)
            {
                day--;
                listDates.Add(lastDateOfMonth.AddDays(-day).Date);
            }
            return listDates;
        }
    }
}
