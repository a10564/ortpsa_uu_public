﻿using MicroService.Utility.Common.Abstract;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace MicroService.Utility.Common
{
    public class MicroServiceCommon : ICommon
    {
        public Boolean SendMail(string to, string subject, string message, List<Attachment> attach)
        {
            bool sendMail = false;
            try
            {
                MailMessage mailMsg = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
                string[] toAddressList = to.Split(';', ',');
                foreach (string address in toAddressList)
                {
                    if (address.Length > 0)
                    {
                        mailMsg.To.Add(address);
                    }
                }
                mailMsg.Subject = subject;
                if (attach.Count() != 0)
                {
                    foreach (Attachment ms in attach)
                    {
                        mailMsg.Attachments.Add(ms);
                    }
                }
                mailMsg.Body = message;
                mailMsg.IsBodyHtml = true;
                mailMsg.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                smtpClient.Send(mailMsg);
                sendMail = true;
            }
            catch (Exception ex)
            {
                //sendMail = false;
                throw ex;
            }
            return sendMail;
        }

        public string ConvertHTMLToString(string source)
        {
            try
            {
                // http://pastebin.com/NswerNkQ
                // http://stackoverflow.com/questions/8419517/convert-html-to-plain-text-while-preserving-p-br-ul-ol
                // http://www.codeproject.com/KB/HTML/HTML_to_Plain_Text.aspx

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                // ReSharper disable LocalizableElement
                var result = source.Replace("\r", @" ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", @" ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // ReSharper restore LocalizableElement
                // Remove repeating spaces because browsers ignore them
                result = Regex.Replace(result, @"(\s)+", " ", RegexOptions.Singleline);

                // Remove the header (prepare first by clearing attributes)
                result = Regex.Replace(result,
                                        @"<( )*head([^>])*>", @"<head>",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"(<( )*(/)( )*head( )*>)", @"</head>",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"(<head>).*(</head>)", string.Empty,
                                        RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = Regex.Replace(result,
                                        @"<( )*script([^>])*>", "<script>",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"(<( )*(/)( )*script( )*>)", "</script>",
                                        RegexOptions.IgnoreCase);
                //result = Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"(<script>).*(</script>)", string.Empty,
                                        RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = Regex.Replace(result,
                                        @"<( )*style([^>])*>", @"<style>",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"(<( )*(/)( )*style( )*>)", @"</style>",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"(<style>).*(</style>)", string.Empty,
                                        RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = Regex.Replace(result,
                                        @"<( )*td([^>])*>", "\t",
                                        RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = Regex.Replace(result,
                                        @"<( )*br( )*>", "\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"<( )*li( )*>", "\r- ",
                                        RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = Regex.Replace(result,
                                        @"<( )*div([^>])*>", "\r\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"<( )*tr([^>])*>", "\r\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"<( )*p([^>])*>", "\r\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"<( )*ol([^>])*>", "\r\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"<( )*ul([^>])*>", "\r\r",
                                        RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = Regex.Replace(result,
                                        @"<[^>]*>", string.Empty,
                                        RegexOptions.IgnoreCase);

                // replace special characters:
                result = Regex.Replace(result,
                                        @" ", " ",
                                        RegexOptions.IgnoreCase);

                result = Regex.Replace(result,
                                        @"&bull;", " * ",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&lsaquo;", "<",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&rsaquo;", ">",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&trade;", "(tm)",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&frasl;", "/",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&lt;", "<",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&gt;", ">",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&copy;", "(c)",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        @"&reg;", "(r)",
                                        RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see
                // http://hotwired.lycos.com/webmonkey/reference/special_characters/
                result = Regex.Replace(result,
                                        @"&(.{2,6});", string.Empty,
                                        RegexOptions.IgnoreCase);

                // for testing
                //Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       RegexOptions.IgnoreCase);

                // make line breaking consistent
                // ReSharper disable LocalizableElement
                //result = result.Replace("\n", "\r");

                // ReSharper restore LocalizableElement

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = Regex.Replace(result,
                                        "(\r)( )+(\r)", "\r\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        "(\t)( )+(\t)", "\t\t",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        "(\t)( )+(\r)", "\t\r",
                                        RegexOptions.IgnoreCase);
                result = Regex.Replace(result,
                                        "(\r)( )+(\t)", "\r\t",
                                        RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = Regex.Replace(result,
                                        "(\r)(\t)+(\r)", "\r\r",
                                        RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = Regex.Replace(result,
                                        "(\r)(\t)+", "\r\t",
                                        RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                // ReSharper disable LocalizableElement
                var breaks = "\r\r\r";
                // ReSharper restore LocalizableElement
                // Initial replacement target string for tabs
                // ReSharper disable LocalizableElement
                var tabs = "\t\t\t\t\t";
                // ReSharper restore LocalizableElement
                for (var index = 0; index < result.Length; index++)
                {
                    // ReSharper disable LocalizableElement
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                    // ReSharper restore LocalizableElement
                }

                // UK: Space at the beginning.
                // ReSharper disable LocalizableElement
                result = result.Replace("\r ", "\r");
                // ReSharper restore LocalizableElement

                // UK: Normalize.
                // ReSharper disable LocalizableElement
                result = result.Replace("\r", Environment.NewLine);
                // ReSharper restore LocalizableElement
                //me
                result = result.Replace("\n", " ");
                result = result.Replace("\r", " ");
                // That's it.
                return result.Trim();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetDateString(DateTime date)
        {
            string str = date.ToString("d{0} MMM yyyy", new System.Globalization.CultureInfo("en-us"));
            string format;
            switch (date.Day)
            {
                case 1:
                case 21:
                case 31:
                    format = "st";
                    break;
                case 2:
                case 22:
                    format = "nd";
                    break;
                case 3:
                case 33:
                    format = "rd";
                    break;
                default:
                    format = "th";
                    break;
            }

            return String.Format(str, format);
        }

        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            int randomNumber = _rdm.Next(_min, _max);
            return randomNumber;
        }

        public string GetEnumDescription(object enumValue)
        {
            string defDesc = string.Empty;
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return defDesc;
        }

        public string GetImageSource(string url)
        {
            return "<img src='" + url + "'/>";
        }

        public string GetImageUrl(string url)
        {
            return "<img src='" + url + "'/>";
        }

        public bool SendMailWithAttachment(string to, string subject, string message)
        {

            System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage();

            mm.To.Add(to);

            mm.Subject = subject;

            mm.Body = message;

            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment("C://inetpub//wwwroot//AerialAccessEquipment//Resources//pdf//AAE_Credit_Application_2012.pdf");
            attachment.Name = "AAE_Credit_Application_2012.pdf";
            mm.Attachments.Add(attachment);

            //mm.Attachments.Add(new System.Net.Mail.Attachment(ms, 'AAE_Credit_Application_2012.pdf', System.Net.Mime.MediaTypeNames.Application.Octet));

            mm.IsBodyHtml = true;

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

            smtp.Send(mm);
            return true;



        }

        public bool SendEmail(string email, string subject, string message)
        {
            bool isSend = false;
            try
            {
                //From Address  
                string FromAddress = "bipros2015@gmail.com";
                string FromAdressTitle = "Bipros";
                //To Address  
                string ToAddress = email;
                string ToAdressTitle = "Microsoft ASP.NET Core";
                string Subject = subject;
                string BodyContent = message;

                //Smtp Server  
                string SmtpServer = "smtp.gmail.com";
                //Smtp Port Number  
                int SmtpPortNumber = 587;

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress
                                        (FromAdressTitle,
                                         FromAddress
                                         ));
                mimeMessage.To.Add(new MailboxAddress
                                         (ToAdressTitle,
                                         ToAddress
                                         ));
                mimeMessage.Subject = Subject; //Subject
                mimeMessage.Body = new TextPart("html")
                {
                    Text = BodyContent
                };
                var client = new MailKit.Net.Smtp.SmtpClient();
                client.Connect(SmtpServer, SmtpPortNumber, false);
                client.Authenticate(
                    "bipros2015@gmail.com",
                    "bipros@2015"
                    );
                client.SendAsync(mimeMessage);
                client.DisconnectAsync(true);
                isSend = true;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSend;
        }
        
        public bool SendEmailHtml(string email, string subject, string message)
        {
            try
            {
                
                //From Address  
                string FromAddress = "bipros2015@gmail.com";
                string FromAdressTitle = "Bipros";
                //To Address  
                string ToAddress = email;
                string ToAdressTitle = "Microsoft ASP.NET Core";
                string Subject = subject;
                string BodyContent = message;

                //Smtp Server  
                string SmtpServer = "smtp.gmail.com";
                //Smtp Port Number  
                int SmtpPortNumber = 587;

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress
                                        (FromAdressTitle,
                                         FromAddress
                                         ));
                mimeMessage.To.Add(new MailboxAddress
                                         (ToAdressTitle,
                                         ToAddress
                                         ));
                mimeMessage.Subject = Subject; //Subject
                mimeMessage.Body = new TextPart("html")
                {
                    Text = BodyContent
                };
                var client = new MailKit.Net.Smtp.SmtpClient();
                client.Connect(SmtpServer, SmtpPortNumber, false);
                client.Authenticate(
                    "bipros2015@gmail.com",
                    "bipros@2015"
                    );
                client.SendAsync(mimeMessage);
                client.DisconnectAsync(true);
                return true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

      

        public string GetCurrentFinancialYear()
        {
            int CurrentYear = DateTime.Today.Year;
            int PreviousYear = DateTime.Today.Year - 1;
            int NextYear = DateTime.Today.Year + 1;
            string PreYear = PreviousYear.ToString();
            string NexYear = NextYear.ToString();
            string CurYear = CurrentYear.ToString();
            string FinYear = null;
            if (DateTime.Today.Month > 3)
                FinYear = CurYear + "-" + NexYear;
            else
                FinYear = PreYear + "-" + CurYear;
            return FinYear;
        }
        

        public static string GetDictionaryValueByKey(Dictionary<string, object> dictionary, string key)
        {
            string value = string.Empty;
            // See whether Dictionary contains this string.
            if (dictionary.ContainsKey(key))
            {
                value = Convert.ToString(dictionary[key]);
            }
            return value;
        }

        public static string EncryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5 
            // We use the MD5 hash generator as the result is a 128 bit byte array 
            // which is a valid length for the TripleDES encoder we use below

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

            // Step 2. Create a new TripleDESCryptoServiceProvider object 
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the encoder 
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            // Step 4. Convert the input string to a byte[] 
            byte[] DataToEncrypt = UTF8.GetBytes(Message);

            // Step 5. Attempt to encrypt the string 
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                // Clear the TripleDes and Hashprovider services of any sensitive information 
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            // Step 6. Return the encrypted string as a base64 encoded string 
            return Convert.ToBase64String(Results);
        }

        public static string DecryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

            // Step 1. We hash the passphrase using MD5 
            // We use the MD5 hash generator as the result is a 128 bit byte array 
            // which is a valid length for the TripleDES encoder we use below

            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

            // Step 2. Create a new TripleDESCryptoServiceProvider object 
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

            // Step 3. Setup the decoder 
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;

            // Step 4. Convert the input string to a byte[] 
            byte[] DataToDecrypt = Convert.FromBase64String(Message);

            // Step 5. Attempt to decrypt the string 
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                // Clear the TripleDes and Hashprovider services of any sensitive information 
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }

            // Step 6. Return the decrypted string in UTF8 format 
            return UTF8.GetString(Results);
        }

        public string GetDescription(Enum value)
        {
            if (value != null)
            {
                var type = value.GetType();
                var name = Enum.GetName(type, value);
                if (name == null) return value.ToString();
                var field = type.GetField(name);
                if (field == null) return value.ToString();
                var attr =
                    Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) as DescriptionAttribute;
                return attr != null ? attr.Description : value.ToString();
            }
            else
            {
                return "";
            }
        }

        #region Send Email With Inline Image



        ///----------------------------------------------------------------------------------------------------------------------------------
        ///   Author: Sujit Mohanty             
        ///   Date: 27th-Nov-2019
        ///   Description: Used to send email if the body contains image
        ///   Revision History:
        ///   Name:         Date:     Description:
        ///----------------------------------------------------------------------------------------------------------------------------------
        ///----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Email Id Confirmation by the User.
        /// </summary>
        /// <param name="to">Receiver email address</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Message body</param>
        /// <param name="imagePath">Image Path url</param>
        /// <param name="replacementParam">Item to be replaced</param>
        /// <returns>Send email confirmation status</returns>
        ///----------------------------------------------------------------------------------------------------------------------------------


        public bool SendMail(string to, string subject, string message, string imagePath, string replacementParam)
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = configurationBuilder.Build();
            string fromEmailAddress = configuration.GetSection("FromEmailAddress").Value;
            string password = configuration.GetSection("Password").Value;
            string smtpServer = configuration.GetSection("SmtpServer").Value;

            SmtpClient client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(fromEmailAddress, password);
            client.Host = smtpServer;
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(fromEmailAddress);

            string[] toAddressList = to.Split(';', ',');
            foreach (string address in toAddressList)
            {
                if (address.Length > 0)
                {
                    mailMessage.To.Add(address);
                }
            }

            if (!string.IsNullOrEmpty(imagePath) && !string.IsNullOrEmpty(imagePath))
            {
                string filePath = Path.Combine(Directory.GetCurrentDirectory(), imagePath);

                var inlineLogo = new LinkedResource(filePath, MediaTypeNames.Image.Jpeg);
                inlineLogo.ContentId = Guid.NewGuid().ToString();

                message = message.Replace(replacementParam, inlineLogo.ContentId);
                var view = AlternateView.CreateAlternateViewFromString(message, null, "text/html");
                view.LinkedResources.Add(inlineLogo);
                mailMessage.AlternateViews.Add(view);
            }
            mailMessage.Body = message;
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = true;
            mailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
            client.EnableSsl = true;
            client.Send(mailMessage);
            return true;
        }
        #endregion

        #region Send Email 

        ///----------------------------------------------------------------------------------------------------------------------------------
        ///   Author: Sujit Mohanty             
        ///   Date: 27th-Nov-2019
        ///   Description: Used to send email if the body contains image
        ///   Revision History:
        ///   Name:         Date:     Description:
        ///----------------------------------------------------------------------------------------------------------------------------------
        ///----------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Email Id Confirmation by the User.
        /// </summary>
        /// <param name="to">Receiver email address</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Message body</param>
       /// <returns>Send email confirmation status</returns>
        ///----------------------------------------------------------------------------------------------------------------------------------


        public bool SendMail(string to, string subject, string message)
        {
            bool sendMail = false;
            try
            {
                var configurationBuilder = new ConfigurationBuilder();
                var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
                configurationBuilder.AddJsonFile(path, optional: true, reloadOnChange: true);
                IConfigurationRoot configuration = configurationBuilder.Build();
                string fromEmailAddress = configuration.GetSection("FromEmailAddress").Value;
                string password = configuration.GetSection("Password").Value;
                string smtpServer = configuration.GetSection("SmtpServer").Value;


                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(fromEmailAddress, password);
                client.Host = smtpServer;
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromEmailAddress);

                string[] toAddressList = to.Split(';', ',');
                foreach (string address in toAddressList)
                {
                    if (address.Length > 0)
                    {
                        mailMessage.To.Add(address);
                    }
                }
                mailMessage.Body = message;
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                client.EnableSsl = true;
                client.Send(mailMessage);
            }
            catch (Exception ex)
            {
                sendMail = false;
                //throw ex;
            }
            return sendMail;
        }

        #endregion


    }

    public class FinacialYear
    {
        public DateTime StartFinalcialYear { get; set; }
        public DateTime EndFinalcialYear { get; set; }
    }
}

