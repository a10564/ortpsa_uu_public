﻿using MicroService.Utility.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MicroService.Utility.Common
{
    
    public  static class SMSCAPI
    {
        private static  WebProxy objProxy1 = null;


        /// <summary>
        /// Author : Satyabrata Senapati
        /// Date : 14/11/2018
        /// Comment: Send SMS  
        /// </summary>
        /// <param name="smsProviderUrl"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static  string SendSMS(string smsProviderUrl,string user, string password, string mobileNumber, string message)
        {
            string stringpost = null;          
            stringpost = "user=" + user + "&passwd=" + password + "&mobilenumber=" + mobileNumber + "&message=" + message + "&mtype=N &DR=Y";

            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = null;
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;

            try
            {

                string stringResult = null;               
                objWebRequest = (HttpWebRequest)WebRequest.Create(smsProviderUrl);
                
                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }


                // Use below code if you want to SETUP PROXY.
                //Parameters to pass: 1. ProxyAddress 2. Port
                //You can find both the parameters in Connection settings of your internet explorer.

                //WebProxy myProxy = new WebProxy("YOUR PROXY", PROXPORT);
                //myProxy.BypassProxyOnLocal = true;
                //wrGETURL.Proxy = myProxy;

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return stringResult;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {

                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }

        }

        /// <summary>
        /// Author : Satyabrata Senapati
        /// Date : 14/11/2018
        /// Comment: SENd SMS
        /// </summary>
        /// <param name="smsProviderUrl"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="mobileNumber"></param>
        /// <param name="senderId"></param>
        /// <param name="messageType"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string SendSMS(string smsProviderUrl, string user, string password, string mobileNumber, string senderId,string messageType,string message)
        {
            string stringpost = null;
            if(messageType.ToLower() == Constant.unicodeMessageType.ToString().ToLower())
            {
                message = ConvertStringToHex(message);
            }
            stringpost = "username=" + user + "&password=" + password + "&messageType="+ messageType + "&mobile=" + mobileNumber + "&senderId="+ senderId + "&message=" + message;
           
            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = null;
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;

            try
            {

                string stringResult = null;
               
                objWebRequest = (HttpWebRequest)WebRequest.Create(smsProviderUrl);

                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }


                // Use below code if you want to SETUP PROXY.
                //Parameters to pass: 1. ProxyAddress 2. Port
                //You can find both the parameters in Connection settings of your internet explorer.

                //WebProxy myProxy = new WebProxy("YOUR PROXY", PROXPORT);
                //myProxy.BypassProxyOnLocal = true;
                //wrGETURL.Proxy = myProxy;

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return stringResult;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {

                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }

        }

        /// <summary>
        /// /// Author : Satyabrata Senapati
        /// Date : 14/11/2018
        /// Comment: Converts the ascii code to hexadecimal string
        /// </summary>
        /// <param name="asciiString"></param>
        /// <returns></returns>
        public static string ConvertStringToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                // Get the integral value of the character.
                int tmp = Convert.ToInt32(c);
                // Convert the decimal value to a hexadecimal value in string form.
                hex += String.Format("{0:X}", (uint)System.Convert.ToUInt32(tmp.ToString()));               
            }
            return hex;
        }
    }
}
