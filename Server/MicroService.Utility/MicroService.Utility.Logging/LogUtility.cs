﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService.Utility.Logging
{
    public class LogUtility
    {
        public static string BuildExceptionMessage(Exception x)
        {

            Exception logException = x;
            if (x.InnerException != null)
                logException = x.InnerException;

            string strErrorMsg = Environment.NewLine; //+ "Error in Path :" + System.Web.HttpContext.Current.Request.Path;

            // Get the QueryString along with the Virtual Path
            //strErrorMsg += Environment.NewLine + "Raw Url :" + System.Web.HttpContext.Current.Request.RawUrl;

            // Get the error message
            strErrorMsg += Environment.NewLine + "Message :" + logException.Message;

            // Source of the message
            strErrorMsg += Environment.NewLine + "Source :" + logException.Source;

            // Stack Trace of the error

            strErrorMsg += Environment.NewLine + "Stack Trace :" + logException.StackTrace;

            // Method where the error occurred
            //strErrorMsg += Environment.NewLine + "TargetSite :" + logException.TargetSite;
            return strErrorMsg;
        }
        
        public static AzureLogEntity BuildAzureLogData(Exception ex,string logLevel,string activity, string tenantId,string userId)
        {
            return new AzureLogEntity
            {
                Category = AzureLogConfiguration.Instance.MicroserviceName,
                Description = BuildExceptionMessage(ex),
                LogDateDateTime = DateTime.UtcNow,
                ID = Guid.NewGuid(),
                LogLevel= logLevel,
                Activity= activity,
                TenantId= tenantId,
                UserId= userId
            };
        }
        public static AzureLogEntity BuildAzureLogData(string ex, string logLevel, string activity, string tenantId, string userId)
        {
            return new AzureLogEntity
            {
                Category = AzureLogConfiguration.Instance.MicroserviceName,
                Description = ex,
                LogDateDateTime = DateTime.UtcNow,
                ID = Guid.NewGuid(),
                LogLevel = logLevel,
                Activity = activity,
                TenantId = tenantId,
                UserId = userId
            };
        }
    }
    public enum EnLoggingType
    {
        Azure = 1
    }
}
