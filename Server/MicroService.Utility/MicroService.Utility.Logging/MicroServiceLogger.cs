﻿using System;
using MicroService.Utility.Logging.Abstract;
using System.Threading.Tasks;
using Serilog.Events;
using Serilog;
using LogAnalyticsWrappers;
using Newtonsoft.Json;



namespace MicroService.Utility.Logging
{
    public class MicroServiceLogger : IMicroServiceLogger
    {
        //private static LoggerConfiguration logger = new LoggerConfiguration();

        public MicroServiceLogger()
        {

        }

        public void Verbose(string Message)
        {
            Logging(LogEventLevel.Verbose, Message, null, null, null, null);
        }

        public void Debug(string Message)
        {
            Log.Debug(Message);
            Logging(LogEventLevel.Debug, Message, null, null, null, null);
        }

        public void Info(string Message)
        {
            Log.Information(Message);
            Logging(LogEventLevel.Information, Message, null, null, null, null);
        }
        public void Info(object data,string activity,string tenantId,string userId)
        {
            string message = JsonConvert.SerializeObject(data);
            Log.Information(message + "|"+ activity + "|" + tenantId + "|" + userId);
            Logging(LogEventLevel.Information, message, null, activity, tenantId, userId);
        }
        public void Warn(string Message)
        {
            Log.Warning(Message);
            Logging(LogEventLevel.Warning, Message, null, null, null, null);
        }

        public void Error(string Message)
        {
            Log.Error(Message);
            Logging(LogEventLevel.Error, Message, null, null, null, null);
        }
        public void Error(Exception ex)
        {
            Log.Error(LogUtility.BuildExceptionMessage(ex));
            Logging(LogEventLevel.Error, null, ex, null, null, null);

        }

        public void Fatal(string Message)
        {
            Log.Fatal(Message);
            Logging(LogEventLevel.Fatal, Message, null, null, null, null);
        }


        public void Fatal(Exception ex)
        {
            Log.Fatal(LogUtility.BuildExceptionMessage(ex));
            Logging(LogEventLevel.Fatal,null,ex,null,null,null);
        }


        private void Logging(LogEventLevel logEvent, string msg,Exception ex, string activity, string tenantId, string userId)
        {
            if (AzureLogConfiguration.Instance != null && AzureLogConfiguration.Instance.LoggerType != null) //modified by Sonymon : Azure configuration is not mandatory for logging.
            {
                switch (Enum.Parse(typeof(EnLoggingType), AzureLogConfiguration.Instance.LoggerType))
                {
                    case EnLoggingType.Azure:
                        var log = ex != null ? LogUtility.BuildAzureLogData(ex, logEvent.ToString(), activity, tenantId, userId) : LogUtility.BuildAzureLogData(msg, logEvent.ToString(), activity, tenantId, userId);
                        LoggingUsingDataCollectorApi(log);
                        break;
                    default:
                        //Nothing 
                        break;
                }
            }
        }

 
        private async void LoggingUsingDataCollectorApi(AzureLogEntity logEntity)
        {
            if (logEntity != null)
            {
                await Task.Run(() =>
                {
                    LogAnalyticsWrapper azureLogger = new LogAnalyticsWrapper(
                                                        workspaceId: AzureLogConfiguration.Instance.WorkSpaceID,
                                                        sharedKey: AzureLogConfiguration.Instance.AuthenticationID);
                    azureLogger.SendLogEntry(logEntity, AzureLogConfiguration.Instance.LoggingSchema);
                });
            }
        }
    }
}
