﻿//using DinkToPdf;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Microservice.Common.Abstract
{
    public interface IMicroserviceCommon
    {
        //bool SendMails(string to, string subject, string message);
		Task<bool> SendMails(string to, string subject, string message);
		string GenerateAcknowledgementNo(string prefix, long serviceId,string guId);
        //string GetEnumDescription(object enumValue);
        string GetEnumDescription(Enum value);
        bool SendSMSGetStatus<T>(string apiURL, T value);
		//Task SendSMS<T>(string apiURL, T value);
		Task SendSMS<T>(string apiURL, T value, string contentId);
		//HttpResponseMessage SendSMS<T>(string apiURL, T value);
		long GenerateOtp();
		byte[] ExportToPDF(string htmlContent, enOrientation orientation);
		void SendSMSRequest<T>(string apiURL, T value);

		string AddOrdinal(int num);

	}
}
