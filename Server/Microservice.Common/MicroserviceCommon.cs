﻿using System;
using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Configuration;
using Microservice.Common.Abstract;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using System.Security.Cryptography;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
//using System.IO;
//using DotNetCorePdf.Converters;
//using DotNetCorePdf.Models;
//using DotNetCorePdf.Enums;
using DinkToPdf.Contracts;
using DinkToPdf;
using System.IO;
using System.Threading.Tasks;
using QRCoder;
using System.Drawing;
using System.Drawing.Imaging;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Collections.Generic;

namespace Microservice.Common
{
    public class MicroserviceCommon : IMicroserviceCommon
    {
        IConfiguration _IConfiguration;
        static IConfiguration _IsConfiguration;
        private IConverter _converter;
		private WebProxy objProxy1 = null;

		public MicroserviceCommon(IConfiguration IConfiguration, IConverter converter)
		{
            _IConfiguration = IConfiguration;

			_converter = converter;
		}
	
		public async Task<bool> SendMails(string to, string subject, string message)
		{
			#region old code
			//         try
			//         {
			//	string fromAddress = _IConfiguration.GetSection("EmailDetail:FromAddress").Value;
			//	string fromAddressTitle = _IConfiguration.GetSection("EmailDetail:FromAddressTitle").Value;
			//	string host = _IConfiguration.GetSection("EmailDetail:Host").Value;
			//	string authenticatepassword = _IConfiguration.GetSection("EmailDetail:Authenticatepassword").Value;
			//	string azureSendGridKey = _IConfiguration.GetSection("EmailDetail:AzureSendGridKey").Value;
			//	//MailMessage mailMessage = new MailMessage();
			//	//mailMessage.From = new MailAddress(fromAddress, fromAddressTitle);
			//	//mailMessage.To.Add(to);
			//	//mailMessage.Body = message;
			//	//mailMessage.Subject = subject;
			//	//mailMessage.IsBodyHtml = true;
			//	//mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
			//	//mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
			//	//mailMessage.Priority = MailPriority.High;
			//	//using (var smtpClient = new SmtpClient())
			//	//{
			//	//	smtpClient.UseDefaultCredentials = false;
			//	//	smtpClient.Credentials = new NetworkCredential(fromAddress, authenticatepassword);
			//	//	smtpClient.Host = host;
			//	//	smtpClient.EnableSsl = true;
			//	//	await smtpClient.SendMailAsync(mailMessage);
			//	//}
			//	var client = new SendGridClient(azureSendGridKey);
			//	var msg = new SendGridMessage();
			//	msg.SetFrom(new EmailAddress(fromAddress, subject));
			//	var recipients = new List<EmailAddress>
			//	{
			//		new EmailAddress(to)
			//	};
			//	msg.AddTos(recipients);
			//	msg.SetSubject(subject);
			//	msg.AddContent(MimeType.Html, message);
			//	await client.SendEmailAsync(msg);

			//	return true;
			//}
			//catch (Exception)
			//{
			//	return false;
			//}
			#endregion
			try
			{
				if (_IConfiguration.GetSection("DeploymentEnvironmentNotice").Value.ToLower() == _IConfiguration.GetSection("DeploymentEnvironmentNotice").Value.ToLower())
				{
					string fromAddress = _IConfiguration.GetSection("EmailDetail:FromAddress").Value;
					string SubjectName = subject;
					string AzureSendGridKey = _IConfiguration.GetSection("EmailDetail:AzureSendGridKey").Value;
					var client = new SendGridClient(AzureSendGridKey);
					var msg = new SendGridMessage();
					try
					{
						msg.SetFrom(new EmailAddress(fromAddress, subject));
						var recipients = new List<EmailAddress>
						{
							new EmailAddress(to)
						};
						msg.AddTos(recipients);
						msg.SetSubject(subject);
						msg.AddContent(MimeType.Html, message);
						var response = await client.SendEmailAsync(msg);
					}
					catch (Exception)
					{
						msg = null;
						client = null;
					}
					finally
					{
						msg = null;
						client = null;
					}
					return true;
				}
				else
				{

					string fromAddress = _IConfiguration.GetSection("EmailDetailDev:FromAddress").Value;
					string fromAddressTitle = _IConfiguration.GetSection("EmailDetailDev:FromAddressTitle").Value;
					string host = _IConfiguration.GetSection("EmailDetailDev:Host").Value;
					string authenticatepassword = _IConfiguration.GetSection("EmailDetailDev:Authenticatepassword").Value;
					MailMessage mailMessage = new MailMessage();
					mailMessage.From = new MailAddress(fromAddress, fromAddressTitle);
					mailMessage.To.Add(to);
					mailMessage.Body = message;
					mailMessage.Subject = subject;
					mailMessage.IsBodyHtml = true;
					mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
					mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;
					mailMessage.Priority = MailPriority.High;
					using (var smtpClient = new SmtpClient())
					{
						smtpClient.UseDefaultCredentials = false;
						smtpClient.Credentials = new NetworkCredential(fromAddress, authenticatepassword);
						smtpClient.Host = host;
						smtpClient.EnableSsl = true;
						await smtpClient.SendMailAsync(mailMessage);
					}
					return true;

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
			#region new code
			//try
			//{
			//	string fromAddress = _IConfiguration.GetSection("EmailDetail:FromAddress").Value;
			//	string SubjectName = subject;
			//	string AzureSendGridKey = _IConfiguration.GetSection("EmailDetail:AzureSendGridKey").Value;
			//	var client = new SendGridClient(AzureSendGridKey);
			//	var msg = new SendGridMessage();
			//	try
			//	{
			//		msg.SetFrom(new EmailAddress(fromAddress, subject));
			//		var recipients = new List<EmailAddress>
			//	{
			//		new EmailAddress(to)
			//	};
			//		msg.AddTos(recipients);

			//		msg.SetSubject(subject);

			//		//msg.AddContent(MimeType.Text, messages);  
			//		msg.AddContent(MimeType.Html, message);
			//		var response = await client.SendEmailAsync(msg);
			//	}
			//	catch (Exception)
			//	{
			//		msg = null;
			//		client = null;
			//	}
			//	finally
			//	{
			//		msg = null;
			//		client = null;
			//	}
			//	return true;
			//}
			//catch (Exception)
			//{
			//	throw;
			//}
			#endregion
		}

        #region GENERATE ACKNOWLEDMENTNO
        /// <summary>
        /// Anurag Digal
        /// 18-10-19
        /// This Method is Generating unique AcknowlegementNo based on Applied serviceId
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public string GenerateAcknowledgementNo(string prefix, long serviceId, string guId)
        {
            string lastSequence = guId.Substring(0, 8);
            string time = DateTime.UtcNow.Ticks.ToString();
            string acknowledgementNo = prefix + (serviceId > 9 ? serviceId.ToString() : '0' + serviceId.ToString()) + time.Substring(time.Length - 4) + lastSequence.Substring(lastSequence.Length - 4);
            return acknowledgementNo.ToUpper();
        }
        #endregion
       

        /// <summary>
        /// Author          : Dattatreya Dash
        /// Date            : 09-11-2019
        /// Descritpion     : Fetches desciption of Enum property
        /// </summary>
        /// <param name="value">Enum value</param>
        /// <returns></returns>
        public string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }


		public static string EncryptString(string Message, string Passphrase, bool BrowserSafety)
		{
			

			byte[] Results;
			System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

			// Step 1. We hash the passphrase using MD5 
			// We use the MD5 hash generator as the result is a 128 bit byte array 
			// which is a valid length for the TripleDES encoder we use below

			MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
			byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

			// Step 2. Create a new TripleDESCryptoServiceProvider object 
			TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

			// Step 3. Setup the encoder 
			TDESAlgorithm.Key = TDESKey;
			TDESAlgorithm.Mode = CipherMode.ECB;
			TDESAlgorithm.Padding = PaddingMode.PKCS7;

			// Step 4. Convert the input string to a byte[] 
			byte[] DataToEncrypt = UTF8.GetBytes(Message);

			// Step 5. Attempt to encrypt the string 
			try
			{
				ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
				Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
			}
			finally
			{
				// Clear the TripleDes and Hashprovider services of any sensitive information 
				TDESAlgorithm.Clear();
				HashProvider.Clear();
			}

			// Step 6. Return the encrypted string as a base64 encoded string 
			string ctext = Convert.ToBase64String(Results);

			if (BrowserSafety)
			{
				ctext = ctext.Replace("=", "#eq#");
				ctext = ctext.Replace("+", "#pls#");
				ctext = ctext.Replace("?", "#qs#");
			}
			return ctext;
		}

		public static string DecryptString(string Message, string Passphrase, bool BrowserSafety)
		{

			if (BrowserSafety)
			{
				Message = Message.Replace("#eq#","=");
				Message = Message.Replace("#pls#", "+");
				Message = Message.Replace("#qs#" ,"?");
			}

			byte[] Results;
			System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();

			// Step 1. We hash the passphrase using MD5 
			// We use the MD5 hash generator as the result is a 128 bit byte array 
			// which is a valid length for the TripleDES encoder we use below

			MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
			byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));

			// Step 2. Create a new TripleDESCryptoServiceProvider object 
			TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();

			// Step 3. Setup the decoder 
			TDESAlgorithm.Key = TDESKey;
			TDESAlgorithm.Mode = CipherMode.ECB;
			TDESAlgorithm.Padding = PaddingMode.PKCS7;

			// Step 4. Convert the input string to a byte[] 
			byte[] DataToDecrypt = Convert.FromBase64String(Message);

			// Step 5. Attempt to decrypt the string 
			try
			{
				ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
				Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
			}
			finally
			{
				// Clear the TripleDes and Hashprovider services of any sensitive information 
				TDESAlgorithm.Clear();
				HashProvider.Clear();
			}

			// Step 6. Return the decrypted string in UTF8 format 
			return UTF8.GetString(Results);
		}

        /// <summary>
        /// Author          : Sumbul Samreen
        /// </summary>
        /// <typeparam name="T">Type of the object to be sent</typeparam>
        /// <param name="apiURL">API of the hosted SMS gateway</param>
        /// <param name="value">Object to be sent</param>
        /// <returns>HttpResponseMessage containing proper HttpStatusCode and reason phrase if SMS send is unsuccessful</returns>
        //public HttpResponseMessage SendSMS<T>(string apiURL, T value)
        //{
        //    HttpResponseMessage responseMessage = null;
        //    using (HttpClient httpClient = new HttpClient())
        //    {
        //        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        try
        //        {
        //            responseMessage = httpClient.PostAsync(apiURL, new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json")).Result;
        //        }
        //        catch (Exception ex)
        //        {
        //            if (responseMessage == null)
        //            {
        //                responseMessage = new HttpResponseMessage();
        //            }
        //            responseMessage.StatusCode = HttpStatusCode.InternalServerError;
        //            responseMessage.ReasonPhrase = string.Format("RestHttpClient.SendRequest failed: {0}", ex);
        //        }
        //    }
        //    return responseMessage;
        //}

        #region send SMS

        #region old code
        //      public async Task SendSMS<T>(string apiURL, T value)
        //{
        //	string environment = _IConfiguration.GetSection("DeploymentEnvironmentNotice").Value;

        //	if (environment.Trim() == "dev")
        //          {
        //		await Task.Run(() =>
        //		{
        //			string MobileNumber = (string)value.GetType().GetProperty("MobileNumber").GetValue(value);
        //			string Message = (string)value.GetType().GetProperty("Message").GetValue(value);

        //			var re = JsonConvert.SerializeObject(value);
        //			string stringpost = null;
        //			stringpost = "username=cscotpapi&password=cscotpapi@123&messageType=text" + "&mobile=" + MobileNumber + "&senderId=CSCSPV" + "&message=" + Message;

        //			HttpWebRequest objWebRequest = null;
        //			HttpWebResponse objWebResponse = null;
        //			StreamWriter objStreamWriter = null;
        //			StreamReader objStreamReader = null;
        //			try
        //			{
        //				string stringResult = null;
        //				objWebRequest = (HttpWebRequest)WebRequest.Create("https://bulksmsapi.vispl.in/");
        //				objWebRequest.Method = "POST";
        //				if ((objProxy1 != null))
        //				{
        //					objWebRequest.Proxy = objProxy1;
        //				}
        //				objWebRequest.ContentType = "application/x-www-form-urlencoded";

        //				objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
        //				objStreamWriter.Write(stringpost);
        //				objStreamWriter.Flush();
        //				objStreamWriter.Close();

        //				objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
        //				objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
        //				stringResult = objStreamReader.ReadToEnd();
        //				objStreamReader.Close();
        //			}
        //			catch (Exception ex)
        //			{
        //				if ((objStreamWriter != null))
        //				{
        //					objStreamWriter.Close();
        //				}
        //				if ((objStreamReader != null))
        //				{
        //					objStreamReader.Close();
        //				}
        //				objWebRequest = null;
        //				objWebResponse = null;
        //				objProxy1 = null;
        //			}
        //			finally
        //			{
        //				if ((objStreamWriter != null))
        //				{
        //					objStreamWriter.Close();
        //				}
        //				if ((objStreamReader != null))
        //				{
        //					objStreamReader.Close();
        //				}
        //				objWebRequest = null;
        //				objWebResponse = null;
        //				objProxy1 = null;
        //			}
        //		});
        //	}
        //	else if (environment.Trim() == "prod")
        //          {
        //		await Task.Run(() =>
        //		{
        //			apiURL = _IConfiguration.GetSection("AppServiceURL:SMSUrl").Value;
        //			SendSMSRequest(apiURL, value);
        //		});
        //	}
        //}
        #endregion

        public async void SendSMSRequest<T>(string apiURL, T value)
		{
			using (HttpClient httpClient = new HttpClient())
			{
				httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				try
				{
					await httpClient.PostAsync(apiURL, new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json"));
				}
				catch (Exception ex)
				{

				}
			}
		}

		public async Task SendSMS<T>(string apiURL, T value, string contentId)
		{
			await Task.Run(() =>
			{
				string MobileNumber = (string)value.GetType().GetProperty("MobileNumber").GetValue(value);
				string Message = (string)value.GetType().GetProperty("Message").GetValue(value);

				var re = JsonConvert.SerializeObject(value);
				string stringpost = null;
				stringpost = "username=" + _IConfiguration.GetSection("SmsDetail:UserName").Value + "&password=" + _IConfiguration.GetSection("SmsDetail:Password").Value + "&messageType=text&mobile=" + MobileNumber + "&senderId=" + _IConfiguration.GetSection("SmsDetail:SenderId").Value + "&ContentID=" + contentId + "&EntityID=" + _IConfiguration.GetSection("SmsDetail:EntityID").Value + "&message=" + Message;

				HttpWebRequest objWebRequest = null;
				HttpWebResponse objWebResponse = null;
				StreamWriter objStreamWriter = null;
				StreamReader objStreamReader = null;
				try
				{
					string stringResult = null;
					objWebRequest = (HttpWebRequest)WebRequest.Create("https://bulksmsapi.vispl.in/");
					objWebRequest.Method = "POST";
					if ((objProxy1 != null))
					{
						objWebRequest.Proxy = objProxy1;
					}
					objWebRequest.ContentType = "application/x-www-form-urlencoded";

					objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
					objStreamWriter.Write(stringpost);
					objStreamWriter.Flush();
					objStreamWriter.Close();

					objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
					objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
					stringResult = objStreamReader.ReadToEnd();
					objStreamReader.Close();
				}
				catch (Exception)
				{
					if ((objStreamWriter != null))
					{
						objStreamWriter.Close();
					}
					if ((objStreamReader != null))
					{
						objStreamReader.Close();
					}
					objWebRequest = null;
					objWebResponse = null;
					objProxy1 = null;
				}
				finally
				{
					if ((objStreamWriter != null))
					{
						objStreamWriter.Close();
					}
					if ((objStreamReader != null))
					{
						objStreamReader.Close();
					}
					objWebRequest = null;
					objWebResponse = null;
					objProxy1 = null;
				}
			});
		}
		#endregion

		/// <summary>
		/// Author          : Sumbul Samreen
		/// </summary>
		/// <typeparam name="T">Type of the object to be sent</typeparam>
		/// <param name="apiURL">API of the hosted SMS gateway</param>
		/// <param name="value">Object to be sent</param>
		/// <returns>True if SMS is sent successfully, otherwise false</returns>
		public bool SendSMSGetStatus<T>(string apiURL, T value)
        {
            bool isSMSSent = false;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                try
                {
                    var result = httpClient.PostAsync(apiURL, new StringContent(JsonConvert.SerializeObject(value), Encoding.UTF8, "application/json")).Result;
                }
                catch (Exception ex)
                {
                    isSMSSent = false;
                }
            }
            return isSMSSent;
        }

        public long GenerateOtp()
        {
            Random random = new Random();
            long otp = random.Next(100000, 999999);
            return otp;
        }
		
		public byte[] ExportToPDF(string HtmlContent, enOrientation orientation = enOrientation.Portrait)
		{
			Orientation ori = Orientation.Portrait;
			if (orientation == enOrientation.Portrait)
			{
				ori = Orientation.Portrait;
			}
			else if (orientation == enOrientation.Landscape)
			{
				ori = Orientation.Landscape;
			}


			var globalSettings = new GlobalSettings
			{
				//ColorMode = ColorMode.Color,
				//Orientation = Orientation.Portrait,
				Orientation = ori,
				PaperSize = PaperKind.A4,
				//Margins = new MarginSettings { Top = 10 },
				//DocumentTitle = "PDF Report",
				//Out = @"D:\PDFCreator\Employee_Report.pdf"
			};

			var objectSettings = new ObjectSettings
			{
				//PagesCount = true,
				HtmlContent = HtmlContent,
				//WebSettings = { DefaultEncoding = "utf-8" },
				//HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
				//FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" }
			};

			var pdf = new HtmlToPdfDocument()
			{
				GlobalSettings = globalSettings,
				Objects = { objectSettings }
			};

			return _converter.Convert(pdf);
		}

        /// <summary>
        /// Author  : Sitikant Pradhan
        /// Date    :   25-02-2020
        /// Description :  Generating QrCode by accepting input parameter
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static string GenerateQrCode(string inputString)
        {            
            QRCodeGenerator qrGenerator = new QRCodeGenerator();  
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(inputString, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            MemoryStream ms = new MemoryStream();
            qrCodeImage.Save(ms, ImageFormat.Jpeg);
            //qrCodeImage.Save(xpsFileName + ".final.png", ImageFormat.Png);
            byte[] byteImage = ms.ToArray();
            var qrBase64 = "data:image/jpeg;base64," + Convert.ToBase64String(byteImage);
            return qrBase64;
        }

		/// <summary>
		/// Author  : Anurag Digal
		/// Date    :   08-11-2021
		/// Description :  This methode is used to return the number Ordinal in string format
		/// </summary>
		/// <param name="num"></param>
		/// <returns></returns>
		public string AddOrdinal(int num)
		{
			if (num <= 0) return num.ToString();

			switch (num % 100)
			{
				case 11:
				case 12:
				case 13:
					return num + "th";
			}

			switch (num % 10)
			{
				case 1:
					return num + "st";
				case 2:
					return num + "nd";
				case 3:
					return num + "rd";
				default:
					return num + "th";
			}
		}
	}

}




